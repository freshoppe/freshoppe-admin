<?php

namespace App\Contracts;

/**
 * Interface ProcessingContract
 * @package App\Contracts
 */
interface ProcessingContract
{
    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listProcessing(string $order = 'id', string $sort = 'desc', array $columns = ['*']);

    /**
     * @param int $id
     * @return mixed
     */
    public function findProcessingById(int $id);

    /**
     * @param array $params
     * @return mixed
     */
    public function createProcessing(array $params);

    /**
     * @param array $params
     * @return mixed
     */
    public function updateProcessing(array $params);

    /**
     * @param $id
     * @return bool
     */
    public function deleteProcessing($id);
	
}
