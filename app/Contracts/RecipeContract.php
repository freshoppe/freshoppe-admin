<?php

namespace App\Contracts;

/**
 * Interface RecipeContract
 * @package App\Contracts
 */
interface RecipeContract
{
    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listRecipes(string $order = 'id', string $sort = 'desc', array $columns = ['*']);

    /**
     * @param int $id
     * @return mixed
     */
    public function findRecipeById(int $id);

    /**
     * @param array $params
     * @return mixed
     */
    public function createRecipe(array $params);

    /**
     * @param array $params
     * @return mixed
     */
    public function updateRecipe(array $params);

    /**
     * @param $id
     * @return bool
     */
    public function deleteRecipe($id);
	
}
