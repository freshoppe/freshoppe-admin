<?php

namespace App\Contracts;

/**
 * Interface StoreContract
 * @package App\Contracts
 */
interface StoreContract
{
    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listStores(string $order = 'id', string $sort = 'desc', array $columns = ['*']);

    /**
     * @param int $id
     * @return mixed
     */
    public function findStoreById(int $id);

    /**
     * @param array $params
     * @return mixed
     */
    public function createStore(array $params);

    /**
     * @param array $params
     * @return mixed
     */
    public function updateStore(array $params);

    /**
     * @param $id
     * @return bool
     */
    public function deleteStore($id);
	
}
