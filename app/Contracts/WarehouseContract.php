<?php

namespace App\Contracts;

/**
 * Interface WarehouseContract
 * @package App\Contracts
 */
interface WarehouseContract
{
    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listWarehouses(string $order = 'id', string $sort = 'desc', array $columns = ['*']);

    /**
     * @param int $id
     * @return mixed
     */
    public function findWarehouseById(int $id);

    /**
     * @param array $params
     * @return mixed
     */
    public function createWarehouse(array $params);

    /**
     * @param array $params
     * @return mixed
     */
    public function updateWarehouse(array $params);

    /**
     * @param $id
     * @return bool
     */
    public function deleteWarehouse($id);
	
}
