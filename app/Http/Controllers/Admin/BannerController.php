<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\BannerContract;
use App\Http\Controllers\BaseController;

/**
 * Class BannerController
 * @package App\Http\Controllers\Admin
 */
class BannerController extends BaseController
{

	/**
     * @var RecipeContract
     */
    protected $bannerRepository;
	
	/**
     * BannerController constructor.
     * @param BannerContract $bannerRepository
     */
    public function __construct(BannerContract $bannerRepository)
    {
        $this->bannerRepository 	= $bannerRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
		$banners = $this->bannerRepository->listBanners();
        $this->setPageTitle('Banners', 'List of all banners');
        return view('admin.banners.index', compact('banners'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>  'required|max:191',
            'link'=>  'required|max:191',
            'image'=>  'required',
        ]);

        $params = $request->except('_token');

        $banner = $this->bannerRepository->createBanner($params);

        return response()->json($banner, 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $banner = $this->bannerRepository->findBannerById($id);
        return response()->json($banner);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>  'required|max:191',
            'link'=>  'required|max:191',
        ]);

        $params = $request->except('_token');

        $banner = $this->bannerRepository->updateBanner($params);

        return response()->json($banner, 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $banner = $this->bannerRepository->deleteBanner($id);

        return response()->json($banner, 200);
    }
	
	
}
