<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use App\Contracts\StoreContract;

class LoginController extends Controller
{
    use AuthenticatesUsers;

	/**
     * @var StoreContract
     */
	protected $storeRepository;
	
    /**
     * Where to redirect admins after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(StoreContract $storeRepository)
    {
        $this->middleware('guest:admin')->except('logout');
		$this->storeRepository = $storeRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        if (Auth::guard('admin')->attempt([
            'email' => $request->email,
            'password' => $request->password,
			'status' => 1
        ], $request->get('remember'))) {
			Session::put('sess_staff_type', Auth::guard('admin')->user()->staff_type);
			Session::put('sess_department_id', Auth::guard('admin')->user()->department_id);
			if(Auth::guard('admin')->user()->staff_type == 1){
				$whStore = $this->storeRepository->getWarehouseStore(Auth::guard('admin')->user()->department_id);
				Session::put('sess_store_id', $whStore->id);
			}else{
				Session::put('sess_store_id', Auth::guard('admin')->user()->department_id);
			}
			return redirect()->intended(route('admin.dashboard'));
        }
        //return back()->withInput($request->only('email', 'remember'));
		return redirect()->back()->with('message', 'Invalid credentials!')->withInput($request->only('email', 'remember'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        $request->session()->invalidate();
        return redirect()->route('admin.login');
    }
}
