<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\OrderContract;
use App\Contracts\ProductContract;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
    protected $orderRepository;
	
    protected $productRepository;

    public function __construct(OrderContract $orderRepository, ProductContract $productRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $orders 	= $this->orderRepository->listOrders();
		
		$new	 	= $orders->filter(function ($value, $key) {
			return $value->order_status == 1;
		});

		$processing = $orders->filter(function ($value, $key) {
			return $value->order_status == 2;
		});
		
		$despatched 	= $orders->filter(function ($value, $key) {
			return $value->order_status == 3;
		});
		
		$delivery 	= $orders->filter(function ($value, $key) {
			return $value->order_status == 4;
		});
		
		$completed 	= $orders->filter(function ($value, $key) {
			return $value->order_status == 5;
		});
		
		$declined 	= $orders->filter(function ($value, $key) {
			return $value->order_status == 6;
		});
		
		$cancelled 	= $orders->filter(function ($value, $key) {
			return $value->order_status == 7;
		});
		
        $this->setPageTitle('Orders', 'List of all orders');
        return view('admin.orders.index', compact('orders','new','processing','despatched','delivery','completed','declined','cancelled'));
    }
	
	public function billing()
    {
        $orders 	= $this->orderRepository->listOrders();
		
        $this->setPageTitle('Billing', 'List of all orders');
        return view('admin.billing.index', compact('orders'));
    }

    public function show($orderNumber)
    {
        $order 			= $this->orderRepository->findOrderByNumber($orderNumber);
		$paymentTypes 	= $this->orderRepository->getPaymentTypes();
        $this->setPageTitle('Order Details', $orderNumber);
        return view('admin.orders.show', compact('order','paymentTypes'));
    }
	
	
	public function billingShow($orderNumber)
    {
        $order 			= $this->orderRepository->findOrderByNumber($orderNumber);
		$paymentTypes 	= $this->orderRepository->getPaymentTypes();
        $this->setPageTitle('Order Details', $orderNumber);
        return view('admin.billing.show', compact('order','paymentTypes'));
    }
	
	
	/**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatus(Request $request)
    {
		$params = $request->except('_token');
        $order = $this->orderRepository->updateOrderStatus($params);
		return $this->responseJson(false, 200, 'Order status updated successfully');
    }
	
	public function create()
    {
		$paymentTypes 	= $this->orderRepository->getPaymentTypes();
        $this->setPageTitle('Orders', 'Create Order');
        return view('admin.orders.create',compact('paymentTypes'));
    }

    public function store(Request $request)
    {
		$this->validate($request, [
            //'customer_name' =>  'required|max:191',
			'mobile_number' =>  'digits:10',
        ]);
		
		$params = $request->except('_token');
		$order = $this->orderRepository->createOrder($params);
		
		return redirect('/admin/orders/');
    }
	
	public function edit($orderNumber)
    {
		$order 			= $this->orderRepository->findOrderByNumber($orderNumber);
		if($order->order_status == 1){
			$paymentTypes 	= $this->orderRepository->getPaymentTypes();
			$this->setPageTitle('Orders', 'Edit Order : '.$orderNumber);
			return view('admin.orders.edit',compact('paymentTypes','order'));
		}else
			return $this->responseRedirectBack('You can only edit a new order.', 'error', true, true);
    }
	
	/**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
		$this->validate($request, [
            'customer_name' =>  'required|max:191',
			'mobile_number' =>  'digits:10',
        ]);
		
		$params = $request->except('_token');
		$order = $this->orderRepository->updateOrder($params);
		
		return redirect('/admin/orders/');
	}
	
	public function getOrderDetails($id){
		$order = $this->orderRepository->findOrderById($id);
		$storeId		= 2;
		$itemArray		= array();
		if(!empty($order->items)){
			foreach($order->items as $key=>$item){
				$product 		= $this->productRepository->findProductById($item->product_id);
				$itemId    	  	= $product->raw_item_id;
				$itemArray[$key]['product_id'] 			= $item->product_id;
				$itemArray[$key]['product_gst'] 		= $item->gst_rate;
				$itemArray[$key]['product_number'] 		= $item->product_id;
				$itemArray[$key]['product_name'] 		= $item->product_id;
				$itemArray[$key]['product_price'] 		= $item->price;
				$itemArray[$key]['product_qty'] 		= $item->quantity;
				$itemArray[$key]['product_gross_wt'] 	= $item->gross_weight;
				$itemArray[$key]['product_net_wt'] 		= $item->net_weight;
				$itemArray[$key]['line_total'] 			= $item->sub_total;
				$itemArray[$key]['line_gst'] 			= 0;
				$itemArray[$key]['balance'] 			= $this->productRepository->getItemBalance($itemId,$storeId);
			}
		}
		$returnArry['order'] = $order;
		$returnArry['payments'] = $order->payments;
		$returnArry['orderItems'] = $itemArray;
		
		return $returnArry;
	}
}
