<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Staff;
use Illuminate\Http\Request;

class PermissionController extends BaseController
{
    public function permission()
    {   
	
		$permissions = [
			'Dashboard',
			'Warehouse Creation',
			'Warehouse List',
			'Warehouse Edit',
			'Designation Creation',
			'Designation List',
			'Designation Edit',
			'Store Creation',
			'Store List',
			'Store Edit',
			'Staff Creation',
			'Staff List',
			'Staff Edit',
			'Location Creation',
			'Location List',
			'Location Edit',
			'Item Master',
			'Supplier Master',
			'Processing Way Master',
			'Product Category Master',
			'Quality Management',
			'Stock Reports',
			'Item Purchase',
			'Item Purchase - Return',
			'Item Transfer to Stores',
			'Product Purchase',
			'Product Purchase - Return',
			'Product Transfer to Stores',
			'Product Create',
			'Product List',
			'Product Edit',
			'Web Content Management',
			'Offer Creations',
			'Offer Edits',
			'Product Receiving',
			'Product Returns',
			'Invoicing',
			'Cash Receiving',
			'Daily Cash Reports',
			'Online Order Payment Details',
			'Online Order Taking',
			'Offline Order Taking',
			'Order Processing',
			'Customer Management',
			'Customer Feedback',
			'Order Reports',

        ];
		foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
		die;
    	$dev_permission = Permission::where('slug','create-tasks')->first();
		$manager_permission = Permission::where('slug', 'edit-users')->first();

		//RoleTableSeeder.php
		$dev_role = new Role();
		$dev_role->slug = 'developer';
		$dev_role->name = 'Front-end Developer';
		$dev_role->save();
		$dev_role->permissions()->attach($dev_permission);

		$manager_role = new Role();
		$manager_role->slug = 'manager';
		$manager_role->name = 'Assistant Manager';
		$manager_role->save();
		$manager_role->permissions()->attach($manager_permission);

		$dev_role = Role::where('slug','developer')->first();
		$manager_role = Role::where('slug', 'manager')->first();

		$createTasks = new Permission();
		$createTasks->slug = 'create-tasks';
		$createTasks->name = 'Create Tasks';
		$createTasks->save();
		$createTasks->roles()->attach($dev_role);

		$editUsers = new Permission();
		$editUsers->slug = 'edit-users';
		$editUsers->name = 'Edit Users';
		$editUsers->save();
		$editUsers->roles()->attach($manager_role);

		$dev_role = Role::where('slug','developer')->first();
		$manager_role = Role::where('slug', 'manager')->first();
		$dev_perm = Permission::where('slug','create-tasks')->first();
		$manager_perm = Permission::where('slug','edit-users')->first();

		$developer = new Staff();
		$developer->name = 'Harsukh Makwana';
		$developer->email = 'harsukh21@gmail.com';
		$developer->password = bcrypt('harsukh21');
		$developer->save();
		$developer->roles()->attach($dev_role);
		$developer->permissions()->attach($dev_perm);

		$manager = new Staff();
		$manager->name = 'Jitesh Meniya';
		$manager->email = 'jitesh21@gmail.com';
		$manager->password = bcrypt('jitesh21');
		$manager->save();
		$manager->roles()->attach($manager_role);
		$manager->permissions()->attach($manager_perm);

		
		return redirect()->back();
    }
}
