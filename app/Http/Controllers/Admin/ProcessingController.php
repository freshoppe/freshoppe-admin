<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\ProcessingContract;

use App\Http\Controllers\BaseController;

/**
 * Class ProcessingController
 * @package App\Http\Controllers\Admin
 */
class ProcessingController extends BaseController
{
    /**
     * @var ProcessingContract
     */
    protected $processingRepository;

	
    /**
     * ProcessingController constructor.
     * @param ProcessingContract $processingRepository
     */
    public function __construct(ProcessingContract $processingRepository)
    {
        $this->processingRepository = $processingRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $processing = $this->processingRepository->listProcessing();

        $this->setPageTitle('Processing Ways', 'List of all Processing Ways');
        return view('admin.processing.index', compact('processing'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->setPageTitle('Processing Ways', 'Create Processing Way');
        return view('admin.processing.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'processing_name'	=>  'required|max:191',
            'code'				=>  'required|max:191|unique:processing_ways,code'
        ]);

        $params = $request->except('_token');

        $processing = $this->processingRepository->createProcessing($params);

        if (!$processing) {
            return $this->responseRedirectBack('Error occurred while creating processing way.', 'error', true, true);
        }
        return $this->responseRedirect('admin.processing.index', 'Processing way added successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $processing = $this->processingRepository->findProcessingById($id);
        $this->setPageTitle('Processing Ways', 'Edit Processing : '.$processing->processing_name);
        return view('admin.processing.edit', compact('processing'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'processing_name'	=>  'required|max:191',
			'code'				=>  'required|max:191|unique:processing_ways,code,'.$request->id
        ]);

        $params = $request->except('_token');

        $processing = $this->processingRepository->updateProcessing($params);

        if (!$processing) {
            return $this->responseRedirectBack('Error occurred while updating processing way.', 'error', true, true);
        }
        return $this->responseRedirectBack('Processing way updated successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $processing = $this->processingRepository->deleteProcessing($id);

        if (!$processing) {
            return $this->responseRedirectBack('Error occurred while deleting processing way.', 'error', true, true);
        }
        return $this->responseRedirect('admin.stores.index', 'Processing way deleted successfully' ,'success',false, false);
    }
	
}
