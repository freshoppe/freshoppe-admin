<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\CategoryContract;
use App\Contracts\ProductContract;
use App\Contracts\ProcessingContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StoreProductFormRequest;
use App\Models\ItemTransactionHeader;
use App\Models\Order;
use App\Models\Product;
use Config;


class ProductController extends BaseController
{
    protected $categoryRepository;

    protected $productRepository;
	
    protected $processingRepository;

    public function __construct(
        CategoryContract $categoryRepository,
        ProductContract $productRepository,
        ProcessingContract $processingRepository
    )
    {
        $this->categoryRepository 	= $categoryRepository;
        $this->productRepository 	= $productRepository;
        $this->processingRepository = $processingRepository;
    }

    public function index()
    {
        $products = $this->productRepository->listProducts();

        $this->setPageTitle('Products', 'Products List');
        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        $categories 	= $this->categoryRepository->listCategories('name', 'asc');
        $rawItems 		= $this->productRepository->listItems('item_code', 'asc');
        $productUnits 	= $this->productRepository->productUnits('product_unit', 'asc');
        $processings 	= $this->processingRepository->listActiveProcessing('code', 'asc');

        $this->setPageTitle('Products', 'Create Product');
        return view('admin.products.create', compact('categories','rawItems','processings','productUnits'));
    }

    public function store(StoreProductFormRequest $request)
    {
        $params = $request->except('_token');

        $product = $this->productRepository->createProduct($params);

        if (!$product) {
            return $this->responseRedirectBack('Error occurred while creating product.', 'error', true, true);
        }
		return $this->responseRedirect('admin.products.index', 'Product added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $product 		= $this->productRepository->findProductById($id);
        $categories 	= $this->categoryRepository->listCategories('name', 'asc');
		$rawItems 		= $this->productRepository->listItems('item_code', 'asc');
		
		$processings 	= $this->productRepository->getItemProcessing($product->raw_item_id);
		
		//$processings 	= $this->processingRepository->listActiveProcessing('code', 'asc');
		$productUnits 	= $this->productRepository->productUnits('product_unit', 'asc');
		
		$ecomSettings	= array();
		
		if($product->new_arrival ==1)
			$ecomSettings['1'] = 'New Arrivals';
		if($product->speed_deals ==1)
			$ecomSettings['2'] = 'Speed Deals';
		if($product->todays_special ==1)
			$ecomSettings['3'] = 'Todays Specials';
		
        $this->setPageTitle('Products', 'Edit Product');
        return view('admin.products.edit', compact('categories', 'product', 'rawItems', 'processings' ,'productUnits','ecomSettings'));
    }

    public function update(StoreProductFormRequest $request)
    {
        $params = $request->except('_token');

        $product = $this->productRepository->updateProduct($params);

        if (!$product) {
            return $this->responseRedirectBack('Error occurred while updating product.', 'error', true, true);
        }
		return $this->responseRedirectBack('Product updated successfully.', 'success', false, false);
    }
	
	public function getActiveProducts()
	{
		$products = $this->productRepository->getAdminProducts();
		return $products;
	}
	
	public function details($id)
    {
        $product 		= $this->productRepository->findProductById($id);
		
		$itemId    	  	= $product->raw_item_id;
		$storeId		= 2;
		
		$product->unit	=	$product->productUnit->product_unit;
		$product->balance = $this->productRepository->getItemBalance($itemId,$storeId);
        return response()->json([
            'product'=>$product
        ]);
    }
	
	public function itemProcessing($id)
	{
		$processings = $this->productRepository->getItemProcessing($id);
		return response()->json($processings);
	}
}
