<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\RecipeContract;
use App\Contracts\ProductContract;

use App\Http\Controllers\BaseController;

/**
 * Class RecipeController
 * @package App\Http\Controllers\Admin
 */
class RecipeController extends BaseController
{
    /**
     * @var RecipeContract
     */
    protected $recipeRepository;

	
    /**
     * RecipeController constructor.
     * @param RecipeContract $recipeRepository
     */
    public function __construct(RecipeContract $recipeRepository, ProductContract $productRepository)
    {
        $this->recipeRepository 	= $recipeRepository;
        $this->productRepository 	= $productRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $recipes = $this->recipeRepository->listRecipes();

        $this->setPageTitle('Recipes', 'List of all Recipes');
        return view('admin.recipes.index', compact('recipes'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->setPageTitle('Recipes', 'Create Recipe');
		
		$products = $this->productRepository->listProducts('name', 'asc',array('name','id'));
        return view('admin.recipes.create',compact('products'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id'	=>  'required',
            'name'			=>  'required|max:191',
            'ingredients'	=>  'required|max:1000',
            'method'		=>  'required|max:1000',
            'time'			=>  'required|max:191',
			'image'			=>  'required',
        ]);

        $params = $request->except('_token');

        $recipe = $this->recipeRepository->createRecipe($params);

        if (!$recipe) {
            return $this->responseRedirectBack('Error occurred while creating recipe.', 'error', true, true);
        }
        return $this->responseRedirect('admin.recipes.index', 'Recipe added successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $recipe 	= $this->recipeRepository->findRecipeById($id);
		$products 	= $this->productRepository->listProducts('name', 'asc',array('name','id'));
		
        $this->setPageTitle('Recipes', 'Edit Recipe : '.$recipe->name);
        return view('admin.recipes.edit', compact('recipe','products'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'product_id'	=>  'required',
            'name'			=>  'required|max:191',
            'ingredients'	=>  'required|max:1000',
            'method'		=>  'required|max:1000',
            'time'			=>  'required|max:191',
            'image'			=>  'required',
        ]);

        $params = $request->except('_token');

        $recipe = $this->recipeRepository->updateRecipe($params);

        if (!$recipe) {
            return $this->responseRedirectBack('Error occurred while updating recipe.', 'error', true, true);
        }
        return $this->responseRedirectBack('Recipe updated successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $recipe = $this->recipeRepository->deleteRecipe($id);

        if (!$recipe) {
            return $this->responseRedirectBack('Error occurred while deleting recipe.', 'error', true, true);
        }
        return $this->responseRedirect('admin.recipes.index', 'Recipe deleted successfully' ,'success',false, false);
    }
	
}
