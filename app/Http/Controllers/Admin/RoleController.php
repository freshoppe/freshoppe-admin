<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends BaseController
{
    public function index()
    {   
    	$roles = Role::where('slug','<>','super-admin')->orderBy('id','asc')->get();
		
		$this->setPageTitle('Designations', 'List of all designations');
		return view('admin.roles.index',compact('roles'));
    }
	
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
			
		$this->setPageTitle('Designations', 'Show Designation : '.$role->name);	
        return view('admin.roles.show',compact('role'));
    }
	
	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role 		= Role::find($id);
        $permission = Permission::orderBy('name','asc')->get();
			
		$this->setPageTitle('Designations', 'Edit Designation : '.$role->name);
        return view('admin.roles.edit',compact('role','permission'));
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);

        $role = Role::find($request->id);
        $role->name = $request->input('name');
        $role->save();

        $role->permissions()->sync($request->input('permission'));
		
        if (!$role) {
            return $this->responseRedirectBack('Error occurred while updating designation.', 'error', true, true);
        }
        return $this->responseRedirectBack('Designation updated successfully' ,'success',false, false);
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $permission = Permission::orderBy('name','asc')->get();
		
		$this->setPageTitle('Designations', 'Create Designation');
        return view('admin.roles.create',compact('permission'));
    }
	
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);

        $role = Role::create(['name' => $request->input('name')]);
        $role->permissions()->sync($request->input('permission'));
		
        if (!$role) {
            return $this->responseRedirectBack('Error occurred while creating designation.', 'error', true, true);
        }
        return $this->responseRedirect('admin.roles.index', 'Designation created successfully' ,'success',false, false);

    }
	
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::where('id',$id)->delete();
		return $this->responseRedirect('admin.roles.index', 'Designation deleted successfully' ,'success',false, false);
    }
}
