<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Models\Staff;
use App\Models\Role;
use App\Models\Permission;
use Hash;
use App\Contracts\StoreContract;
use App\Contracts\WarehouseContract;


/**
 * Class StaffController
 * @package App\Http\Controllers\Admin
 */
class StaffController extends BaseController
{
    
	/**
     * @var StoreContract
     */
	protected $storeRepository;
	
	/**
     * @var WarehouseContract
     */
	protected $warehouseRepository;
	
    /**
     * StoreController constructor.
     * @param StoreContract $storeRepository
     */
    public function __construct(StoreContract $storeRepository, WarehouseContract $warehouseRepository)
    {
		$this->storeRepository = $storeRepository;
		$this->warehouseRepository = $warehouseRepository;
    }
	
	
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $staffs = Staff::where('super_admin',0)->orderBy('id','DESC')->get();

        $this->setPageTitle('Staffs', 'List of all staffs');
        return view('admin.staffs.index', compact('staffs'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
		$roles 		= Role::where('slug','<>','super-admin')->get();
		$warehouses = $this->warehouseRepository->getActiveWarehouses();
        $this->setPageTitle('Staffs', 'Create Staff');
        return view('admin.staffs.create', compact('roles','warehouses'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:staffs,email',
            'password' => 'required|same:password_confirmation',
            'staff_type' => 'required',
            'warehouse_id' => 'required',
            'roles' => 'required',
			'store_id' => "required_if:staff_type,==,2"
        ]);

		$department_id = NULL;
		$input = $request->except('_token');
		$input['status']= ($request->status) ? 1 : 0;
		if($input['staff_type'] == 1)
			$department_id = $input['warehouse_id'];
		else if($input['staff_type'] == 2)
			$department_id = $input['store_id'];
			
		$input['password'] 		= Hash::make($input['password']);
		$input['department_id'] = $department_id;
		
        $staff = Staff::create($input);
		$staff->roles()->attach($input['roles']);

        if (!$staff) {
            return $this->responseRedirectBack('Error occurred while creating staff.', 'error', true, true);
        }
        return $this->responseRedirect('admin.staffs.index', 'Staff added successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $staff 		= Staff::find($id);
        $roles 		= Role::where('slug','<>','super-admin')->get();
		$warehouses = $this->warehouseRepository->getActiveWarehouses();
		$stores		= array();
		$warehouse_id = NULL;
		if($staff->staff_type == 1)
			$warehouse_id	=	$staff->department_id;
			
		if($staff->staff_type == 2){
			$store_id		=	$staff->department_id;
			$store			=	$this->storeRepository->findStoreById($store_id);
			$warehouse_id	=	$store->warehouse->id;
			$stores			=	$this->storeRepository->getStoresByWarehouse($warehouse_id);
		}
        $this->setPageTitle('Staff', 'Edit Staff : '.$staff->name);
        return view('admin.staffs.edit',compact('staff','roles','warehouses','warehouse_id','stores'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
		$id = $request->id;
		
        $this->validate($request, [
            'name'	=>  'required|max:191',
            'email' => 'required|email|unique:staffs,email,'.$id,
            'password' => 'same:password_confirmation',
            'roles' => 'required',
			'staff_type' => 'required',
            'warehouse_id' => 'required',
			'store_id' => "required_if:staff_type,==,2"
        ]);

        $input 			= $request->all();
		$department_id 	= NULL;
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
			$input = $request->except('password');
        }
		
		$input['status']= ($request->status) ? 1 : 0;
		if($input['staff_type'] == 1)
			$department_id = $input['warehouse_id'];
		else if($input['staff_type'] == 2)
			$department_id = $input['store_id'];
		$input['department_id'] = $department_id;
		
        $staff = Staff::find($id);
        $staff->update($input);
		$staff->roles()->sync($input['roles']);
		
        if (!$staff) {
            return $this->responseRedirectBack('Error occurred while updating staff.', 'error', true, true);
        }
		return $this->responseRedirect('admin.staffs.index', 'Staff updated successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        
    }
}
