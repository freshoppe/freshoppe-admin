<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\StoreContract;
use App\Contracts\LocationContract;
use App\Contracts\WarehouseContract;

use App\Http\Controllers\BaseController;

/**
 * Class StoreController
 * @package App\Http\Controllers\Admin
 */
class StoreController extends BaseController
{
    /**
     * @var StoreContract
     */
    protected $storeRepository;

	protected $locationRepository;
	
	protected $warehouseRepository;
	
    /**
     * StoreController constructor.
     * @param StoreContract $storeRepository
     */
    public function __construct(StoreContract $storeRepository, LocationContract $locationRepository, WarehouseContract $warehouseRepository)
    {
        $this->storeRepository = $storeRepository;
		$this->locationRepository = $locationRepository;
		$this->warehouseRepository = $warehouseRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $stores = $this->storeRepository->listStores();

        $this->setPageTitle('Stores', 'List of all stores');
        return view('admin.stores.index', compact('stores'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->setPageTitle('Stores', 'Create Store');
		$locations 	= $this->locationRepository->getActiveLocations();
		$warehouses = $this->warehouseRepository->getActiveWarehouses();
		
        return view('admin.stores.create',compact('locations','warehouses'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'store_name'	=>  'required|max:191',
            'store_code'	=>  'required|max:191|unique:stores,store_code',
            'location_id' 	=>  'required',
            'warehouse_id' 	=>  'required',
            'pincode' 		=>  'required|digits:6',
            'address' 		=>  'required',
            'store_type'    =>  'required'
        ]);

        $params = $request->except('_token');

        $store = $this->storeRepository->createStore($params);

        if (!$store) {
            return $this->responseRedirectBack('Error occurred while creating store.', 'error', true, true);
        }
        return $this->responseRedirect('admin.stores.index', 'Store added successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $targetStore = $this->storeRepository->findStoreById($id);
		$locations 	= $this->locationRepository->getActiveLocations();
		$warehouses = $this->warehouseRepository->getActiveWarehouses();
		
        $this->setPageTitle('Stores', 'Edit Store : '.$targetStore->store_name);
        return view('admin.stores.edit', compact('targetStore','locations','warehouses'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'store_name'	=>  'required|max:191',
			'store_code'	=>  'required|max:191|unique:stores,store_code,'.$request->id,
            'location_id' 	=>  'required',
            'warehouse_id' 	=>  'required',
            'pincode' 		=>  'required|digits:6',
            'address' 		=>  'required',
            'store_type'    =>  'required'
        ]);

        $params = $request->except('_token');

        $store = $this->storeRepository->updateStore($params);

        if (!$store) {
            return $this->responseRedirectBack('Error occurred while updating store.', 'error', true, true);
        }
        return $this->responseRedirectBack('Store updated successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $store = $this->storeRepository->deleteStore($id);

        if (!$store) {
            return $this->responseRedirectBack('Error occurred while deleting store.', 'error', true, true);
        }
        return $this->responseRedirect('admin.stores.index', 'Store deleted successfully' ,'success',false, false);
    }
	
	public function getWarehousesByLocation(Request $request, $id)
	{
		if($request->ajax())
		{
			$warehouseArray	= $this->warehouseRepository->getWarehousesByLocation($id);
			return response()->json($warehouseArray);
		} 
		
	}
}
