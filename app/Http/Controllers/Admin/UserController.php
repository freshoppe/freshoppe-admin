<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\UserContract;
use App\Http\Controllers\BaseController;

class UserController extends BaseController
{
    /**
     * @var UserContract
     */
    protected $userRepository;

    /**
     * CategoryController constructor.
     * @param UserContract $userRepository
     */
    public function __construct(UserContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $customers = $this->userRepository->listUsers();
        $this->setPageTitle('Customers', 'List of all customers');
        return view('admin.customers.index', compact('customers'));
    }
	
	/**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = $this->userRepository->findUserById($id);
        $this->setPageTitle('Customers', 'Edit Customer : '.$user->name);
        return view('admin.customers.edit', compact('user'));
    }
	
	/**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
		$this->validate($request, [
            'first_name'      =>  'required|max:191',
			'last_name'      =>  'required|max:191',
			'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$request->id],
			'address'      =>  'required|max:191',
			'city'      =>  'required|max:191',
			'state'      =>  'required|max:191',
			'pincode'      =>  'required|max:191',
			'country'      =>  'required|max:191',
        ]);
		
		$params = $request->except('_token');
		$user = $this->userRepository->updateUser($params);
		if (!$user) {
            return $this->responseRedirectBack('Error occurred while updating user.', 'error', true, true);
        }
        return $this->responseRedirectBack('User updated successfully' ,'success',false, false);
	}
	
	/**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	 public function destroy($id)
	 {
		$delete = $this->userRepository->deleteUser($id);
		if(!$delete):
			return $this->responseRedirectBack('Error occurred while deleting user.', 'error', true, true);
		endif;
		return $this->responseRedirectBack('User deleted successfully' ,'success',false, false);
	 }
	 
	 /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	 public function activate_user(Request $request)
	 {
		 $user = $this->userRepository->activate_user($request->customerid);
		 if($user):
		 echo "success";
		 else:
		 echo "error";
		 endif;
	 }
}
