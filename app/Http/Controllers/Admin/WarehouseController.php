<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\WarehouseContract;
use App\Contracts\LocationContract;
use App\Contracts\StoreContract;
use App\Http\Controllers\BaseController;

/**
 * Class WarehouseController
 * @package App\Http\Controllers\Admin
 */
class WarehouseController extends BaseController
{
    /**
     * @var WarehouseContract
     */
    protected $warehouseRepository;
	
    protected $locationRepository;
	
    protected $storeRepository;

    /**
     * WarehouseController constructor.
     * @param WarehouseContract $warehouseRepository
     */
    public function __construct(WarehouseContract $warehouseRepository, LocationContract $locationRepository, StoreContract $storeRepository)
    {
        $this->warehouseRepository 	= $warehouseRepository;
        $this->locationRepository 	= $locationRepository;
        $this->storeRepository 		= $storeRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $warehouses = $this->warehouseRepository->listWarehouses();

        $this->setPageTitle('Warehouses', 'List of all warehouses');
        return view('admin.warehouses.index', compact('warehouses'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->setPageTitle('Warehouses', 'Create Warehouse');
		$locations = $this->locationRepository->getActiveLocations();
		
        return view('admin.warehouses.create',compact('locations'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'warehouse_name'=>  'required|max:191',
            'warehouse_code'=>  'required|max:191|unique:warehouses,warehouse_code',
            'location_id' 	=>  'required',
            'pincode' 		=>  'required|digits:6',
        ]);

        $params = $request->except('_token');

        $warehouse = $this->warehouseRepository->createWarehouse($params);

        if (!$warehouse) {
            return $this->responseRedirectBack('Error occurred while creating warehouse.', 'error', true, true);
        }
        return $this->responseRedirect('admin.warehouses.index', 'Warehouse added successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $warehouse = $this->warehouseRepository->findWarehouseById($id);
		$locations = $this->locationRepository->getActiveLocations();
		
        $this->setPageTitle('Warehouses', 'Edit Warehouse : '.$warehouse->warehouse_name);
        return view('admin.warehouses.edit', compact('warehouse','locations'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'warehouse_name'=>  'required|max:191',
            'warehouse_code'=>  'required|max:191|unique:warehouses,warehouse_code,'.$request->id,
            'location_id' 	=>  'required',
            'pincode' 		=>  'required|digits:6',
        ]);

        $params = $request->except('_token');

        $warehouse = $this->warehouseRepository->updateWarehouse($params);

        if (!$warehouse) {
            return $this->responseRedirectBack('Error occurred while updating warehouse.', 'error', true, true);
        }
        return $this->responseRedirectBack('Warehouse updated successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $warehouse = $this->warehouseRepository->deleteWarehouse($id);

        if (!$warehouse) {
            return $this->responseRedirectBack('Error occurred while deleting warehouse.', 'error', true, true);
        }
        return $this->responseRedirect('admin.warehouses.index', 'Warehouse deleted successfully' ,'success',false, false);
    }
	
	/**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
	public function getStores(Request $request, $id)
	{
		if($request->ajax())
		{
			$storeArray	= $this->storeRepository->getStoresByWarehouse($id);
			return response()->json($storeArray);
		} 
		
	}
}
