<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	/**
     * @param bool $error
     * @param int $responseCode
     * @param array $message
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($message = '', $data = null)
    {
        $response =[
            'success'       =>  true,
            'message'       => $message,
            'data'          =>  $data
        ];
		
		return response()->json($response, 200);
    }
	
	/**
     * return error response.
     *
     * @param $error
     * @param  array  $errorMessages
     * @param  int  $code
     *
     * @return JsonResponse
     */
    public function sendError($error, $code = 200, $errorMessages = null)
    {
        $response = [
            'success' => false,
            'message' => $error,
            'data' => [],
        ];
		
		if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }
		
        return response()->json($response, $code);
    }
}
