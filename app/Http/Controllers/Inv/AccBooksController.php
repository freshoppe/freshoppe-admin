<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth; 
use Hash;
use Carbon\Carbon;
use Session;


class AccBooksController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	
	
//============================USER TABLE INSERT=======================================//

    public function index()
    {
		
		$store_id = Session::get('sess_store_id');
		$req = new \Illuminate\Http\Request();
		
		$active_accounts_headers = $this->get_all_active_accounts_headers($req);

        $this->setPageTitle('LEDGERS', 'List of all Account Heads');
        return view('admin.inventory.acc-books.index', compact('active_accounts_headers'));
    }
	
	
    public function suppliers()
    {
		$req = new \Illuminate\Http\Request();
		
		$active_accounts_headers = $this->get_all_active_suppliers($req);

        $this->setPageTitle('SUPPLIERS', 'List of all Suppliers');
        return view('admin.inventory.acc-books.suppliers', compact('active_accounts_headers'));
    }


	public function get_all_active_accounts_headers(Request $req)
    {
		$active_accounts_headers = DB::table('accounts_headers')
			->leftJoin('suppliers', 'accounts_headers.id', '=', 'suppliers.acc_head_id')
			->select('accounts_headers.*', 'suppliers.supplier_address AS supplier_address')
			->whereNotNull('accounts_headers.active')
			->whereNull('accounts_headers.deleted_at')
			->get();
				
		return $active_accounts_headers;
    }


	public function get_all_active_suppliers(Request $req)
    {
		$active_accounts_headers = DB::table('accounts_headers')
			->leftJoin('suppliers', 'accounts_headers.id', '=', 'suppliers.acc_head_id')
			->select('accounts_headers.*', 'suppliers.supplier_address AS supplier_address')
			->where([['accounts_headers.acc_group_id', '=', 16]])
			->whereNotNull('accounts_headers.active')
			->whereNull('accounts_headers.deleted_at')
			->get();
				
		return $active_accounts_headers;
    }



    public function acc_ledger(Request $req)
    {
		$head_id = $req->id;
		$req = new \Illuminate\Http\Request();
		$req->initialize(['head_id' => $head_id]);
		
		$head_name = DB::table('accounts_headers')->where([['id', '=', $head_id]])->value('head_name');
		
        $accounts_details = $this->get_accounts_details_by_header_id($req);

        $this->setPageTitle('ACCOUNT BALANCE: '. $head_name, 'Transactions of ' . $head_name);
        return view('admin.inventory.acc-books.acc-ledger', compact('accounts_details', 'head_name', 'head_id'));
		
    }
	
	
    public function get_common_from_to_dates()
    {
		$from_date = Carbon::now()->firstOfMonth()->format('Y-m-d');
		$to_date = Carbon::now()->format('Y-m-d');

		return [$from_date, $to_date ]; 
		
    }
	
    public function get_accounts_details_by_header_id(Request $req)
    {
		$store_id = Session::get('sess_store_id');
		$head_id = $req->head_id;
		
		list($from_date, $to_date ) = $this->get_common_from_to_dates();
				
		(request('from_date') !== null) && $from_date = request('from_date');
		(request('to_date') !== null) && $to_date = request('to_date');
				
		$accounts_details = DB::table('accounts_details')
			->leftJoin('accounts_headers as dr_accounts_headers', 'accounts_details.dr_acc_head_id', '=', 'dr_accounts_headers.id')
			->leftJoin('accounts_headers as cr_accounts_headers', 'accounts_details.cr_acc_head_id', '=', 'cr_accounts_headers.id')
			->leftJoin('stores as source_stores', 'accounts_details.acc_store_id', '=', 'source_stores.id')
			->leftJoin('stores as trgt_stores', 'accounts_details.acc_trgt_store_id', '=', 'trgt_stores.id')
			->leftJoin('acc_transaction_modes', 'accounts_details.acc_trn_mode_id', '=', 'acc_transaction_modes.id')
			->leftJoin('items_transaction_headers', 'accounts_details.ith_id', '=', 'items_transaction_headers.id')
			->leftJoin('products_transaction_headers', 'accounts_details.pth_id', '=', 'products_transaction_headers.id')
			->leftJoin('stock_transaction_types as ith_stock_transaction_types', 'items_transaction_headers.stk_trn_type_id', '=', 'ith_stock_transaction_types.id')
			->leftJoin('stock_transaction_types as pth_stock_transaction_types', 'products_transaction_headers.stk_trn_type_id', '=', 'pth_stock_transaction_types.id')
			->select('accounts_details.*', 
			'dr_accounts_headers.head_name as dr_head_name', 
			'cr_accounts_headers.head_name as cr_head_name', 
			'trgt_stores.store_name as trgt_store_name', 
			'acc_transaction_modes.transaction_mode', 
			'items_transaction_headers.ith_trn_no', 
			'products_transaction_headers.pth_trn_no', 
			'ith_stock_transaction_types.transaction_type as ith_transaction_type', 
			'ith_stock_transaction_types.itt_code', 
			'pth_stock_transaction_types.transaction_type as pth_transaction_type', 
			'pth_stock_transaction_types.ptt_code', 'source_stores.store_name as source_store_name')
			->where([['accounts_details.dr_acc_head_id', '=', $head_id], ['accounts_details.acc_store_id', '=', $store_id]])
			->where([['accounts_details.acc_trn_date', '>=', $from_date], ['accounts_details.acc_trn_date', '<=', $to_date]])
			->orWhere([['accounts_details.cr_acc_head_id', '=', $head_id], ['accounts_details.acc_store_id', '=', $store_id]])
			->where([['accounts_details.acc_trn_date', '>=', $from_date], ['accounts_details.acc_trn_date', '<=', $to_date]])
			->orderBy('accounts_details.acc_trn_date', 'ASC')
			->get();
			

        return $accounts_details;
		
    }
	
    public function cash_book(Request $req)
    {
		$head_id = 1;
		$req = new \Illuminate\Http\Request();
		$req->initialize(['head_id' => $head_id]);
		
		$head_name = DB::table('accounts_headers')->where([['id', '=', $head_id]])->value('head_name');
		
        $accounts_details = $this->get_accounts_details_by_header_id($req);
				
		$from_date = request('from_date');
		$to_date = request('to_date');

        $this->setPageTitle('CASH BOOK ', 'Cash Transactions');
        return view('admin.inventory.acc-books.cash-book', compact('accounts_details', 'head_name', 'head_id', 'from_date', 'to_date'));
    }
	
	
    public function purchase_book(Request $req)
    {
		$head_id = 2;
		$req = new \Illuminate\Http\Request();
		$req->initialize(['head_id' => $head_id]);
		
		$head_name = DB::table('accounts_headers')->where([['id', '=', $head_id]])->value('head_name');
		
        $accounts_details = $this->get_accounts_details_by_header_id($req);
		
		$from_date = request('from_date');
		$to_date = request('to_date');

        $this->setPageTitle('PURCHASE BOOK ', 'Purchase Transactions');
        return view('admin.inventory.acc-books.purchase-book', compact('accounts_details', 'head_name', 'head_id', 'from_date', 'to_date'));
		
    }
	
    public function purchase_return_book(Request $req)
    {
		$head_id = 7;
		$req = new \Illuminate\Http\Request();
		$req->initialize(['head_id' => $head_id]);
		
		$head_name = DB::table('accounts_headers')->where([['id', '=', $head_id]])->value('head_name');
		
        $accounts_details = $this->get_accounts_details_by_header_id($req);
		
		$from_date = request('from_date');
		$to_date = request('to_date');

        $this->setPageTitle('PURCHASE RETURN BOOK ', 'Purchase Return Transactions');
        return view('admin.inventory.acc-books.purchase-return-book', compact('accounts_details', 'head_name', 'head_id', 'from_date', 'to_date'));
		
		
    }
	
	
	
    public function sale_book(Request $req)
    {
		$head_id = 3;
		$req = new \Illuminate\Http\Request();
		$req->initialize(['head_id' => $head_id]);
		
		$head_name = DB::table('accounts_headers')->where([['id', '=', $head_id]])->value('head_name');
		
        $accounts_details = $this->get_accounts_details_by_header_id($req);
		
		$from_date = request('from_date');
		$to_date = request('to_date');

        $this->setPageTitle('SALES BOOK ', 'Sale Transactions');
        return view('admin.inventory.acc-books.sale-book', compact('accounts_details', 'head_name', 'head_id', 'from_date', 'to_date'));
		
    }
	 
	
    public function transfer_book(Request $req)
    {

		$head_id = 4;
		$req = new \Illuminate\Http\Request();
		$req->initialize(['head_id' => $head_id]);
		
		$head_name = DB::table('accounts_headers')->where([['id', '=', $head_id]])->value('head_name');
		
        $accounts_details = $this->get_accounts_details_by_header_id($req);
		
		$from_date = request('from_date');
		$to_date = request('to_date');

        $this->setPageTitle('STOCK TRANSFER BOOK ', 'Stock Transfer Transactions');
        return view('admin.inventory.acc-books.transfer-book', compact('accounts_details', 'head_name', 'head_id', 'from_date', 'to_date'));
		
    }
	
	
    public function transfer_return_book(Request $req)
    {
		$head_id = 8;
		$req = new \Illuminate\Http\Request();
		$req->initialize(['head_id' => $head_id]);
		
		$head_name = DB::table('accounts_headers')->where([['id', '=', $head_id]])->value('head_name');
		
        $accounts_details = $this->get_accounts_details_by_header_id($req);
		
		$from_date = request('from_date');
		$to_date = request('to_date');

        $this->setPageTitle('TRANSFER RETURN BOOK ', 'Transfer Return Transactions');
        return view('admin.inventory.acc-books.transfer-return-book', compact('accounts_details', 'head_name', 'head_id', 'from_date', 'to_date'));
		
		
    }
	
	
    public function receive_book(Request $req)
    {
		$head_id = 5;
		$req = new \Illuminate\Http\Request();
		$req->initialize(['head_id' => $head_id]);
		
		$head_name = DB::table('accounts_headers')->where([['id', '=', $head_id]])->value('head_name');
		
        $accounts_details = $this->get_accounts_details_by_header_id($req);
		
		$from_date = request('from_date');
		$to_date = request('to_date');		

        $this->setPageTitle('STOCK RECEIVE BOOK ', 'Stock Receive Transactions');
        return view('admin.inventory.acc-books.receive-book', compact('accounts_details', 'head_name', 'head_id', 'from_date', 'to_date'));
		
    } 
	 
	
    public function supplier_book(Request $req)
    {
		$head_id = $req->id;
		
		$req = new \Illuminate\Http\Request();
		$req->initialize(['head_id' => $head_id]);
		
		$head_name = DB::table('accounts_headers')->where([['id', '=', $head_id]])->value('head_name');
		
        $accounts_details = $this->get_accounts_details_by_header_id($req);		

		$from_date = request('from_date');
		$to_date = request('to_date');
		

        $this->setPageTitle('SUPPLIER BALANCE: '. $head_name, 'Transactions of ' . $head_name);
        return view('admin.inventory.acc-books.supplier-book', compact('accounts_details', 'head_name', 'head_id', 'from_date', 'to_date'));
		
    }

	
	
}
