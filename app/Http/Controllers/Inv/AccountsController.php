<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;
use Hash;
use Carbon\Carbon;
use Session;


class AccountsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	
//============================USER TABLE INSERT=======================================//	

	public function delete_supplier(Request $req)
    {
		
				
			DB::table('acc_heads')
					->where([['id', '=', $req->id] ])
					->update
					(
						[
							'deleted_at' => Carbon::now()->toDateTimeString(), 
							'user_id_recorded_by' => Auth::guard('ela_user')->user()->id
							
						]
					);
					
					
			DB::table('suppliers')
					->where([['acc_head_id', '=', $req->id]])
					->update
					(
						[
							'deleted_at' => Carbon::now()->toDateTimeString(),
							'user_id_recorded_by' => Auth::guard('ela_user')->user()->id
							
						]
					);
				
				
			return 1;
				
	}	




	public function update_supplier(Request $req)
    {
				
		$resp_count = DB::table('acc_heads')
				->where([['head_name', '=', $req->supplier_name], ['id', '!=', $req->item_id]])
				->count();
						
		if(!$resp_count)
		{
		
			DB::table('acc_heads')
					->where([['id', '=', $req->item_id] ])
					->update
					(
						[
							'head_name' => $req->supplier_name,
							'user_id_recorded_by' => Auth::guard('ela_user')->user()->id
							
						]
					);
					
					
			DB::table('suppliers')
					->where([['acc_head_id', '=', $req->item_id] ])
					->update
					(
						[
							'supplier_address' => $req->supplier_address,
							'user_id_recorded_by' => Auth::guard('ela_user')->user()->id
							
						]
					);
					
			
			return 1;
			
		}
		else
		{
			return 0;
		}
		
		
    }



	public function insert_supplier(Request $req)
    {
						
		$resp_count = DB::table('acc_heads')
				->where([['head_name', '=', $req->supplier_name]])
				->count();
						
		if(!$resp_count)
		{
		
			DB::table('acc_heads')
					->insert
					(
						[
							'head_name' => $req->supplier_name,
							'acc_group_id' => 16,
							'user_id_recorded_by' => Auth::guard('ela_user')->user()->id
							
						]
					);
					
						$acc_head_id = DB::getPdo()->lastInsertId();
						
			DB::table('suppliers')
					->insert
					(
						[
							'acc_head_id' => $acc_head_id,
							'supplier_address' => $req->supplier_address,
							'user_id_recorded_by' => Auth::guard('ela_user')->user()->id
							
						]
					);
						
						
			
			return 1;
			
		}
		else
		{
			return 0;
		}
		
		
    }


	public function get_supplier_by_id(Request $req)
    {
		
		$suppliers = DB::table('acc_heads')
		->leftJoin('suppliers', 'acc_heads.id', '=', 'suppliers.acc_head_id')
		->select('acc_heads.*', 'suppliers.supplier_address AS supplier_address')
		->where([['acc_heads.id', '=', $req->id] ]) 
		->first();
		
				
		return [$suppliers];
			
		
    }

	
	public function get_suppliers(Request $req)
    {
	
		$suppliers = DB::table('acc_heads')
			->leftJoin('suppliers', 'acc_heads.id', '=', 'suppliers.acc_head_id')
			->select('acc_heads.*', 'suppliers.supplier_address AS supplier_address')
			->where([['acc_heads.acc_group_id', '=', 16], ['acc_heads.id', '!=', $req->id]])
			->whereNull('acc_heads.deleted_at')
			->get();
				
		return $suppliers;
		
    }
	
	
	
	
}
