<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;
use Hash;
use Carbon\Carbon;
use Session;


class CommonController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	
//============================USER TABLE INSERT=======================================//	

	public function get_all_acive_stores_except_self(Request $req)    
	{		
		$store_id = Session::get('sess_store_id');
		
		$acive_stores = DB::table('stores')
		->where([['status', '=', 1]])
		->where([['id', '!=', $store_id] ]) 
		->get();
				
		return $acive_stores ;
		
	}

	public function get_all_acive_stores(Request $req)    
	{		
				
		$acive_stores = DB::table('stores')
		->where([['status', '=', 1]])
		->get();
				
		return $acive_stores ;
		
	}
	
	public function get_all_active_products(Request $req)
    {
	
		$products = DB::table('products')
		->leftJoin('raw_items', 'products.raw_item_id', '=', 'raw_items.id')
		->leftJoin('item_units', 'raw_items.item_unit_id', '=', 'item_units.id')
		->select('products.id','products.name','products.number', 'raw_items.item_price', 'raw_items.item_unit_id', 'raw_items.item_gst', 'item_units.item_unit')
		->where([['status', '=', 1] ]) 
		->get();
				
		return $products;
		
    }
	
	
	public function get_all_active_products_with_balance(Request $req)
    {
		$store_id = Session::get('sess_store_id');
		
		$products = DB::table('products')
		->leftJoin('product_units', 'products.product_unit_id', '=', 'product_units.id')
		->leftJoin('products_transaction_details', 'products.id', '=', 'products_transaction_details.ptd_product_id')
		->leftJoin('products_transaction_headers', 'products_transaction_details.products_transaction_header_id', '=', 'products_transaction_headers.id')
		->leftJoin('stock_transaction_types', 'products_transaction_headers.stk_trn_type_id', '=', 'stock_transaction_types.id')
		->select('products.id','products.name','products.number','product_units.product_unit', 
			DB::raw('sum(case when stock_transaction_types.in_out = 1 AND products_transaction_headers.store_id = '. $store_id . ' then products_transaction_details.ptd_unit_qty END) as cr_qty_sum'),
			DB::raw('sum(case when stock_transaction_types.in_out = 2 AND products_transaction_headers.store_id = '. $store_id . ' then products_transaction_details.ptd_unit_qty END) as dr_qty_sum'))
		->whereNotNull('products.active')
		->groupBy('products.id')
		->get();
		
		
		return $products;
		
    }
	

	
	
}
