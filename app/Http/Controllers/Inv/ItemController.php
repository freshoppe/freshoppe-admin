<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Contracts\ProcessingContract;
use Auth;
use Hash;
use Carbon\Carbon;
use Session;
use App\Models\RawItem;
use App\Models\ItemTypes;

class ItemController extends BaseController
{

    protected $processingRepository;

    public function __construct(
        ProcessingContract $processingRepository
    )
    {
        $this->processingRepository = $processingRepository;
    }
	
	
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	

    public function index()
    {
		$req = new \Illuminate\Http\Request();
		
        $items = $this->get_all_active_items($req);

        $this->setPageTitle('Items', 'List of all Items');
        return view('admin.inventory.items.index', compact('items'));
    }
	
    public function create()
    {

        $item_units 	= $this->get_item_units();
        $item_types 	= $this->get_item_types();
        $processings 	= $this->processingRepository->listActiveProcessing('code', 'asc');

        $this->setPageTitle('Items', 'Create Item');
        return view('admin.inventory.items.create', compact('item_units', 'item_types' ,'processings'));
    }
	
	
	public function store(Request $req)
    {
		$params 	= $req->except('_token');
		$collection = collect($params);
		$resp_count = DB::table('raw_items')
				->where([['item_code', '=', $req->item_code], ['active', '=', 1]])
				->orWhere([['item_name', '=', $req->item_name], ['active', '=', 1]])
				->count();
				
		if(!$resp_count)
		{
			$latest 		= RawItem::where('item_type_id',$req->item_type_id)->latest()->first();
			$itemTypes 		= ItemTypes::find($req->item_type_id);
			$item_type_code = $itemTypes->item_code;
			if(!empty($latest)){
			    $last_digits    = str_replace($item_type_code,"",$latest->item_code);
				$item_code 		= $item_type_code.(str_pad((int)$last_digits + 1, 4, '0', STR_PAD_LEFT));
			}else{
				$item_code 		= $item_type_code.'0001';
			}
			$item = new RawItem();
			$item->item_code 	= $item_code;
			$item->item_name 	= $req->item_name;
			$item->item_price 	= $req->item_price;
			$item->item_gst 	= $req->item_gst;
			$item->item_unit_id = $req->item_unit_id;
			$item->item_type_id = $req->item_type_id;
			$item->recorded_by 	= Auth::guard('admin')->user()->id;
			$item->save();
			/* $item = DB::table('raw_items')
					->insert
					(
						[
							'item_code' => $req->item_code,
							'item_name' => $req->item_name,
							'item_price' => $req->item_price,
							'item_gst' => $req->item_gst,
							'item_unit_id' => $req->item_unit_id,
							'item_type_id' => $req->item_type_id,
							'recorded_by' => Auth::guard('admin')->user()->id
						]
					); */
			if (count($req->processing) > 0) {
                $item->processings()->sync($req->processing);
            }		
			
			return 1;
			
		}
		else
		{
			return 0;
		}
		
		
    }
	
	
	public function update(Request $req)
    {
		
		$resp_count = DB::table('raw_items')
				->where([['item_code', '=', $req->item_code], ['active', '=', 1], ['id', '!=', $req->item_id]])
				->orWhere([['item_name', '=', $req->item_name], ['active', '=', 1], ['id', '!=', $req->item_id]])
				->count();
		if(!$resp_count)
		{
			$item 	= RawItem::find($req->item_id);
			$item->item_code 	= $req->item_code;
			$item->item_name 	= $req->item_name;
			$item->item_price 	= $req->item_price;
			$item->item_gst 	= $req->item_gst;
			$item->item_unit_id = $req->item_unit_id;
			$item->item_type_id = $req->item_type_id;
			$item->recorded_by 	= Auth::guard('admin')->user()->id;
			$item->save();
			
			/* DB::table('raw_items')
					->where([['id', '=', $req->item_id] ])
					->update
					(
						[
							'item_code' => $req->item_code,
							'item_name' => $req->item_name,
							'item_price' => $req->item_price,
							'item_gst' => $req->item_gst,
							'item_unit_id' => $req->item_unit_id,
							'item_type_id' => $req->item_type_id,
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
					 */
			if (count($req->processing) > 0) {
				$item->processings()->sync($req->processing);
			}
			return 1;
			
		}
		else
		{
			return 0;
		}
		
		
    }
	
	
    public function edit($id)
    {
        $item 			= RawItem::find($id);
        $item_units 	= $this->get_item_units();
        $item_types 	= $this->get_item_types();
		$processings 	= $this->processingRepository->listActiveProcessing('code', 'asc');
        $this->setPageTitle('Items', 'Edit Item : '.$item->item_name);
        return view('admin.inventory.items.edit', compact( 'item_units', 'item_types', 'processings','item'));
    }
	
	
	public function delete(Request $req)
    {
		
		DB::table('raw_items')
				->where([['id', '=', $req->id] ])
				->update
				(
					[
						'active' => null,
						'recorded_by' => Auth::guard('admin')->user()->id
						
					]
				);
				
		return 1;
		
    }

	
	
	public function get_all_items(Request $req)
    {
	
		$items = DB::table('raw_items')
		->leftJoin('item_units', 'raw_items.item_unit_id', '=', 'item_units.id')
		->select('raw_items.*','item_units.item_unit')
		->get();
				
		return $items;
		
    }
	
	public function get_all_active_items(Request $req)
    {
	
		$items = DB::table('raw_items')
		->leftJoin('item_units', 'raw_items.item_unit_id', '=', 'item_units.id')
		->select('raw_items.*','item_units.item_unit')
		->whereNotNull('raw_items.active')
		->get();
				
		return $items;
		
    }
	
	
	public function get_all_active_items_with_balance(Request $req)
    {
		$store_id = Session::get('sess_store_id');
		
		$items = DB::table('raw_items')
		->leftJoin('item_units', 'raw_items.item_unit_id', '=', 'item_units.id')
		->leftJoin('items_transaction_details', 'raw_items.id', '=', 'items_transaction_details.itd_item_id')
		->leftJoin('items_transaction_headers', 'items_transaction_details.items_transaction_header_id', '=', 'items_transaction_headers.id')
		->leftJoin('stock_transaction_types', 'items_transaction_headers.stk_trn_type_id', '=', 'stock_transaction_types.id')
		->select('raw_items.*','item_units.item_unit', 
			DB::raw('sum(case when stock_transaction_types.in_out = 1 AND items_transaction_headers.store_id = '. $store_id . ' then items_transaction_details.itd_unit_qty END) as cr_qty_sum'),
			DB::raw('sum(case when stock_transaction_types.in_out = 2 AND items_transaction_headers.store_id = '. $store_id . ' then items_transaction_details.itd_unit_qty END) as dr_qty_sum'))
		->whereNotNull('raw_items.active')
		->groupBy('raw_items.id')
		->get();
		
		
		return $items;
		
    }


	public function get_by_id(Request $req)
    {
		$items = DB::table('raw_items')
		->leftJoin('item_units', 'raw_items.item_unit_id', '=', 'item_units.id')
		->select('raw_items.*','item_units.item_unit')
		->where([['raw_items.id', '=', $req->id] ]) 
		->first();
		
		return $items;
    }
	
	public function get_by_id_json(Request $req)
    {
		$items = DB::table('raw_items')
		->leftJoin('item_units', 'raw_items.item_unit_id', '=', 'item_units.id')
		->select('raw_items.*','item_units.item_unit')
		->where([['raw_items.id', '=', $req->id] ]) 
		->get();
		
		return $items;
    }
	
	
	public function get_item_units()
    {
	
		$item_units = DB::table('item_units')
		->get();
				
		return $item_units;
		
    }

	public function get_item_types()
    {
	
		$item_types = DB::table('item_types')
		->get();
				
		return $item_types;
		
    }

	
	
}
