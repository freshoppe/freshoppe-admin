<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;
use Hash;
use Carbon\Carbon;
use Session;

class ItemPurchaseReturnController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	
	
//============================USER TABLE INSERT=======================================//	

	
	

    public function index()
    {
		$req = new \Illuminate\Http\Request();
		
        //$suppliers = $this->get_all_active_suppliers($req);
        $all_transactions = $this->get_all_drafted_and_confirmed_transactions($req);

        $this->setPageTitle('Item Purchase Returns', 'List of Item Purchase Returns');
        return view('admin.inventory.item-purchase-returns.index', compact('all_transactions' ));
    }
	
    public function create()
    {
		$req = new \Illuminate\Http\Request();
		
		//$prh_rtn_no = $this->get_next_prh_rtn_no($req);	
		$stk_trn_type_id = 7;  /* purchase return */
		
		$prh_rtn_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no($stk_trn_type_id); /*  purchase return srl  */
		
		$suppliers = app('App\Http\Controllers\Inv\SupplierController')->get_all_active_suppliers($req);
		
		$items = app('App\Http\Controllers\Inv\ItemController')->get_all_active_items($req);

        $this->setPageTitle('Item Purchase Return', 'New Purchase Return');
        return view('admin.inventory.item-purchase-returns.create', compact('suppliers', 'items', 'prh_rtn_no'));
    }
	
	
    public function edit($id)
    {
		$req = new \Illuminate\Http\Request();
		$req->initialize(['id' => $id]);
		//$prh_rtn_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(1); /*  purchase srl  */
		
        $targetRecord = $this->get_purchase_by_id($req);
		
        $transaction_details = $this->get_transaction_details_by_id($req);
		
		$suppliers = app('App\Http\Controllers\Inv\SupplierController')->get_all_active_suppliers($req);
		
		$items = app('App\Http\Controllers\Inv\ItemController')->get_all_active_items($req);

        $this->setPageTitle('Item Purchase Returns', 'Edit Purchase Return');
		
        return view('admin.inventory.item-purchase-returns.edit', compact('targetRecord', 'suppliers', 'items', 'targetRecord', 'transaction_details'));
    }


	public function store(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		$prh_rtn_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(7); /*  purchase srl  */
		$store_id = Session::get('sess_store_id');
				
		DB::table('items_transaction_headers')
				->insert
				(
					[
						'stk_trn_type_id' => 7,
						'ith_trn_no' => $prh_rtn_no,
						'store_id' => $store_id,
						'supplier_id' => $prhdata['supplier_id'],
						'ith_date' => $prhdata['ith_date'],
						'ith_inv_no' => $prhdata['ith_inv_no'],
						'ith_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
					]
				);
				
			$ith_id = DB::getPdo()->lastInsertId();
			
			DB::table('items_transaction_payments')
					->insert
					(
						[
							'ith_id' => $ith_id,
							'ith_sum_amnt' => $prhdata['ith_sum_amnt'],
							'ith_sum_sgst' => $prhdata['ith_sum_sgst'],
							'ith_sum_cgst' => $prhdata['ith_sum_cgst'],
							'ith_gross_amnt' => $prhdata['ith_gross_amnt'],
							'ith_ded_amnt' => $prhdata['ith_ded_amnt'],
							'ith_net_amnt' => $prhdata['ith_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
			
			

		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
		
			foreach ($unitdata as $key)
			{
				
				DB::table('items_transaction_details')
						->insert
						(
							[
								'store_id' => $store_id,
								'items_transaction_header_id' => $ith_id,
								'itd_item_id' => $key['itd_item_id'],
								'itd_date' => $prhdata['ith_date'],
								'itd_srl' => $key['itd_srl'],
								'itd_unit_rate' => $key['itd_unit_rate'],
								'itd_unit_gst_rate' => $key['itd_unit_gst_rate'],
								'itd_unit_qty' => $key['itd_unit_qty'],
								'itd_item_unit_id' => $key['itd_item_unit_id'],
								'itd_unit_sum_amnt' => $key['itd_unit_sum_amnt'],
								'itd_unit_sgst' => ($key['unit_gst']/2),
								'itd_unit_cgst' => ($key['unit_gst']/2),
								'itd_unit_gross_amnt' => $key['itd_unit_gross_amnt']
								
							]
						);

			}		
			
		/*------------------------ACCOUNTS-------------------------------------------------------------*/
		$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(3);
		
		DB::table('accounts_details')
				->insert
				(
					[
						
						'acc_store_id' => $store_id,
						'acc_trn_type_id' => 6,  /*    purchase return  - accounts  */
						'acc_trn_mode_id' => 3,
						'ith_id' => $ith_id,
						'acc_trn_srl' => $next_acc_trn_srl,
						'acc_trn_date' => $prhdata['ith_date'],
						'dr_acc_head_id' => $prhdata['supplier_id'],
						'cr_acc_head_id' => 7,  /*   purchase return a/c.   */
						'acc_trn_amnt' => $prhdata['ith_net_amnt'],
						'acc_trn_dscr' => 'prh.rtn.no. ' . $prh_rtn_no,
						'recorded_by' => Auth::guard('admin')->user()->id,
						'wh_credit' => $prhdata['wh_credit']
					]
				);
				
				
		if($prhdata['wh_credit']==0)  //IF NOT CREDIT, MEANS IF CASH RECEIVED
		{	
			$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(1);

		
			DB::table('accounts_details')
					->insert
					(
						[
							
							'acc_store_id' => $store_id,
							'acc_trn_type_id' =>6,	 /*    purchase return  - accounts  */
							'acc_trn_mode_id' => 1,  /* receipt */
							'ith_id' => $ith_id,
							'acc_trn_srl' => $next_acc_trn_srl,
							'acc_trn_date' => $prhdata['ith_date'],
							'dr_acc_head_id' => 1,
							'cr_acc_head_id' => $prhdata['supplier_id'],
							'acc_trn_amnt' => $prhdata['ith_net_amnt'],
							'acc_trn_dscr' => 'prh.rtn.no. ' . $prh_rtn_no,
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
		}	  
			
		return $ith_id;
		
    }
	
	
	public function draft(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		//$prh_draft_no = $this->get_next_prh_draft_no($req);	
		$prh_draft_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(10); /*  purchase return draft srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('items_transaction_headers')
				->insert
				(
					[
						'stk_trn_type_id' => 10,  /* draft purchase return*/
						'ith_trn_no' => $prh_draft_no,
						'store_id' => $store_id,
						'supplier_id' => $prhdata['supplier_id'],
						'ith_date' => $prhdata['ith_date'],
						'ith_inv_no' => $prhdata['ith_inv_no'],
						'ith_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
					]
				);
				
			$ith_id = DB::getPdo()->lastInsertId();
			
			DB::table('items_transaction_payments')
					->insert
					(
						[
							'ith_id' => $ith_id,
							'ith_sum_amnt' => $prhdata['ith_sum_amnt'],
							'ith_sum_sgst' => $prhdata['ith_sum_sgst'],
							'ith_sum_cgst' => $prhdata['ith_sum_cgst'],
							'ith_gross_amnt' => $prhdata['ith_gross_amnt'],
							'ith_ded_amnt' => $prhdata['ith_ded_amnt'],
							'ith_net_amnt' => $prhdata['ith_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
			

		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
		
			foreach ($unitdata as $key)
			{
				
				DB::table('items_transaction_details')
						->insert
						(
							[
								'itd_wh_draft' => 1,
								'store_id' => $store_id,
								'items_transaction_header_id' => $ith_id,
								'itd_item_id' => $key['itd_item_id'],
								'itd_date' => $prhdata['ith_date'],
								'itd_srl' => $key['itd_srl'],
								'itd_unit_rate' => $key['itd_unit_rate'],
								'itd_unit_gst_rate' => $key['itd_unit_gst_rate'],
								'itd_unit_qty' => $key['itd_unit_qty'],
								'itd_item_unit_id' => $key['itd_item_unit_id'],
								'itd_unit_sum_amnt' => $key['itd_unit_sum_amnt'],
								'itd_unit_sgst' => ($key['unit_gst']/2),
								'itd_unit_cgst' => ($key['unit_gst']/2),
								'itd_unit_gross_amnt' => $key['itd_unit_gross_amnt']
								
							]
						);

			}		
			
		return 1;
		
    }
	
	
	public function update_draft(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		$store_id = Session::get('sess_store_id');
				
		DB::table('items_transaction_headers')
				->where('id', $prhdata['id'])
				->update
				(
					[
						'supplier_id' => $prhdata['supplier_id'],
						'ith_date' => $prhdata['ith_date'],
						'ith_inv_no' => $prhdata['ith_inv_no'],
						'ith_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
												
					]
				);
				
				
		DB::table('items_transaction_payments')
				->where('ith_id', $prhdata['id'])
				->update
				(
					[
						'ith_sum_amnt' => $prhdata['ith_sum_amnt'],
						'ith_sum_sgst' => $prhdata['ith_sum_sgst'],
						'ith_sum_cgst' => $prhdata['ith_sum_cgst'],
						'ith_gross_amnt' => $prhdata['ith_gross_amnt'],
						'ith_ded_amnt' => $prhdata['ith_ded_amnt'],
						'ith_net_amnt' => $prhdata['ith_net_amnt'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
					]
				);
				
			

		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
			$items_transaction_header_id = $prhdata['id'];
		
			DB::table('items_transaction_details')->where('items_transaction_header_id', $items_transaction_header_id)->delete();
			
			foreach ($unitdata as $key)
			{
				
				DB::table('items_transaction_details')
						->insert
						(
							[
								'itd_wh_draft' => 1,
								'store_id' => $store_id,
								'items_transaction_header_id' => $items_transaction_header_id,
								'itd_item_id' => $key['itd_item_id'],
								'itd_date' => $prhdata['ith_date'],
								'itd_srl' => $key['itd_srl'],
								'itd_unit_rate' => $key['itd_unit_rate'],
								'itd_unit_gst_rate' => $key['itd_unit_gst_rate'],
								'itd_unit_qty' => $key['itd_unit_qty'],
								'itd_item_unit_id' => $key['itd_item_unit_id'],
								'itd_unit_sum_amnt' => $key['itd_unit_sum_amnt'],
								'itd_unit_sgst' => ($key['unit_gst']/2),
								'itd_unit_cgst' => ($key['unit_gst']/2),
								'itd_unit_gross_amnt' => $key['itd_unit_gross_amnt']
								
								
							]
						);

			}		
			
		return 1;
		
    }
	
	
	public function update_draft_as_purchase_return(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		$prh_rtn_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(7); /*  purchase return srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('items_transaction_headers')
				->where('id', $prhdata['id'])
				->update
				(
					[
						'stk_trn_type_id' => 7,  /* draft as purchase return */
						'ith_trn_no' => $prh_rtn_no,
						'supplier_id' => $prhdata['supplier_id'],
						'ith_date' => $prhdata['ith_date'],
						'ith_inv_no' => $prhdata['ith_inv_no'],
						'ith_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
												
					]
				);
				
		DB::table('items_transaction_payments')
				->where('ith_id', $prhdata['id'])
				->update
				(
					[
						'ith_sum_amnt' => $prhdata['ith_sum_amnt'],
						'ith_sum_sgst' => $prhdata['ith_sum_sgst'],
						'ith_sum_cgst' => $prhdata['ith_sum_cgst'],
						'ith_gross_amnt' => $prhdata['ith_gross_amnt'],
						'ith_ded_amnt' => $prhdata['ith_ded_amnt'],
						'ith_net_amnt' => $prhdata['ith_net_amnt'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
					]
				);
			

		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/

			$items_transaction_header_id = $prhdata['id'];
		
			DB::table('items_transaction_details')->where('items_transaction_header_id', $items_transaction_header_id)->delete();
			
			foreach ($unitdata as $key)
			{
				
				DB::table('items_transaction_details')
						->insert
						(
							[
								'itd_wh_draft' => 0,
								'store_id' => $store_id,
								'items_transaction_header_id' => $items_transaction_header_id,
								'itd_item_id' => $key['itd_item_id'],
								'itd_date' => $prhdata['ith_date'],
								'itd_srl' => $key['itd_srl'],
								'itd_unit_rate' => $key['itd_unit_rate'],
								'itd_unit_gst_rate' => $key['itd_unit_gst_rate'],
								'itd_unit_qty' => $key['itd_unit_qty'],
								'itd_item_unit_id' => $key['itd_item_unit_id'],
								'itd_unit_sum_amnt' => $key['itd_unit_sum_amnt'],
								'itd_unit_sgst' => ($key['unit_gst']/2),
								'itd_unit_cgst' => ($key['unit_gst']/2),
								'itd_unit_gross_amnt' => $key['itd_unit_gross_amnt']
								
								
							]
						);

			}		
			
		/*------------------------ACCOUNTS-------------------------------------------------------------*/
		$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(3);
		
		DB::table('accounts_details')
				->insert
				(
					[
						
						'acc_store_id' => $store_id,
						'acc_trn_type_id' => 6,  /*    purchase return  - accounts  */
						'acc_trn_mode_id' => 3,
						'ith_id' => $items_transaction_header_id,
						'acc_trn_srl' => $next_acc_trn_srl,
						'acc_trn_date' => $prhdata['ith_date'],
						'dr_acc_head_id' => $prhdata['supplier_id'],
						'cr_acc_head_id' => 7,  /*   purchase return a/c.   */
						'acc_trn_amnt' => $prhdata['ith_net_amnt'],
						'acc_trn_dscr' => 'prh.rtn.no. ' . $prh_rtn_no,
						'recorded_by' => Auth::guard('admin')->user()->id,
						'wh_credit' => $prhdata['wh_credit']
					]
				);
				
				
		if($prhdata['wh_credit']==0)  //IF NOT CREDIT, MEANS IF CASH RECEIVED
		{	
			$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(1);
		
			DB::table('accounts_details')
					->insert
					(
						[
							
							'acc_store_id' => $store_id,
							'acc_trn_type_id' =>6,	 /*    purchase return  - accounts  */
							'acc_trn_mode_id' => 1,  /* receipt */
							'ith_id' => $items_transaction_header_id,
							'acc_trn_srl' => $next_acc_trn_srl,
							'acc_trn_date' => $prhdata['ith_date'],
							'dr_acc_head_id' => 1,
							'cr_acc_head_id' => $prhdata['supplier_id'],
							'acc_trn_amnt' => $prhdata['ith_net_amnt'],
							'acc_trn_dscr' => 'prh.rtn.no. ' . $prh_rtn_no,
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
		}	  
			
		return 1;
		
    }
	
	
	public function delete_draft(Request $req)
    {
		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
		
		$items_transaction_header_id = $req->id;
	
		DB::table('items_transaction_details')->where('items_transaction_header_id', $items_transaction_header_id)->delete();
		
		DB::table('items_transaction_headers')->where('id', $items_transaction_header_id)->delete();
			
		return 1;
		
    }
	
	
						
	public function get_all_drafted_and_confirmed_transactions(Request $req)
    {
		$drafted_and_confirmed_purchase_returns = DB::table('items_transaction_headers')
			->leftJoin('items_transaction_payments', 'items_transaction_headers.id', '=', 'items_transaction_payments.ith_id')
			->leftJoin('accounts_headers', 'items_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->leftJoin('stock_transaction_types', 'items_transaction_headers.stk_trn_type_id', '=', 'stock_transaction_types.id')
			->select('items_transaction_headers.*', 
			'items_transaction_payments.ith_sum_amnt', 
			'items_transaction_payments.ith_sum_sgst', 
			'items_transaction_payments.ith_sum_cgst', 
			'items_transaction_payments.ith_gross_amnt', 
			'items_transaction_payments.ith_ded_amnt', 
			'items_transaction_payments.ith_net_amnt', 
			'accounts_headers.head_name as head_name', 'stock_transaction_types.transaction_type', 'stock_transaction_types.itt_code', 'stock_transaction_types.ptt_code')
			->whereIn('items_transaction_headers.stk_trn_type_id', [7,10]) 
			->where([['items_transaction_headers.store_id', '=', Session::get('sess_store_id')]])
			->orderBy('items_transaction_headers.stk_trn_type_id', 'ASC') 
			->get();
			
						
		return $drafted_and_confirmed_purchase_returns;
    }
	
	
	public function get_all_draft_purchase_returns(Request $req)
    {
	
		$draft_purchase_returns = DB::table('items_transaction_headers')
			->leftJoin('items_transaction_payments', 'items_transaction_headers.id', '=', 'items_transaction_payments.ith_id')
			->leftJoin('accounts_headers', 'items_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->leftJoin('stock_transaction_types', 'items_transaction_headers.stk_trn_type_id', '=', 'stock_transaction_types.id')
			->select('items_transaction_headers.*', 
			'items_transaction_payments.ith_sum_amnt', 
			'items_transaction_payments.ith_sum_sgst', 
			'items_transaction_payments.ith_sum_cgst', 
			'items_transaction_payments.ith_gross_amnt', 
			'items_transaction_payments.ith_ded_amnt', 
			'items_transaction_payments.ith_net_amnt', 
			'accounts_headers.head_name as head_name', 'stock_transaction_types.transaction_type', 'stock_transaction_types.itt_code', 'stock_transaction_types.ptt_code' )
			->where([['items_transaction_headers.stk_trn_type_id', '=', 10], ['items_transaction_headers.store_id', '=', Session::get('sess_store_id')]])
			->get();
				
		return $draft_purchase_returns;
    }
	
	
	public function get_purchase_by_id(Request $req)
    {
		$purchase = DB::table('items_transaction_headers')
			->leftJoin('items_transaction_payments', 'items_transaction_headers.id', '=', 'items_transaction_payments.ith_id')
			->leftJoin('accounts_headers', 'items_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->select('items_transaction_headers.*', 
			'items_transaction_payments.ith_sum_amnt', 
			'items_transaction_payments.ith_sum_sgst', 
			'items_transaction_payments.ith_sum_cgst', 
			'items_transaction_payments.ith_gross_amnt', 
			'items_transaction_payments.ith_ded_amnt', 
			'items_transaction_payments.ith_net_amnt', 
			'accounts_headers.head_name as head_name')
			->where([['items_transaction_headers.id', '=', $req->id]])
			->first();
				
		return $purchase;
    }
	
	public function get_transaction_details_by_id(Request $req)
    {
		$transaction_details = DB::table('items_transaction_details')
			->leftJoin('raw_items', 'items_transaction_details.itd_item_id', '=', 'raw_items.id')
			->leftJoin('item_units', 'items_transaction_details.itd_item_unit_id', '=', 'item_units.id')
			->select('items_transaction_details.*', 'raw_items.item_name as item_name', 'item_units.item_unit as item_unit')
			->where([['items_transaction_details.items_transaction_header_id', '=', $req->id]])
			->get();
				
		return $transaction_details;
    }
	
	
}
