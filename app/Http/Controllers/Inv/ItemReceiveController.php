<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;
use Hash;
use Carbon\Carbon;
use Session;

class ItemReceiveController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	
//============================USER TABLE INSERT=======================================//	

	
	

    public function index()
    {
		$req = new \Illuminate\Http\Request();
		
        //$suppliers = $this->get_all_active_suppliers($req);
        $pending_transfers = $this->get_all_pending_transfers($req);
		
		$received_transfers = $this->get_all_received_transfers($req);

        $this->setPageTitle('Item Receives', 'List of Item Receives');
        return view('admin.inventory.item-receives.index', compact('pending_transfers', 'received_transfers' ));
    }
	
	
	
    public function receive($id)
    {
		$req = new \Illuminate\Http\Request();
		$req->initialize(['id' => $id]);
		//$next_receive_no = $this->get_next_receive_no($req);	
		$next_receive_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(5); /*  receive srl  */
		
        $targetRecord = $this->get_transfer_by_id($req);
		
        $transaction_details = $this->get_transaction_details_by_id($req);
		
		$acive_stores = app('App\Http\Controllers\Inv\CommonController')->get_all_acive_stores($req);
		
		$items = app('App\Http\Controllers\Inv\ItemController')->get_all_active_items($req);

        $this->setPageTitle('Item Receives', 'Receive Transfer No: '.$targetRecord->ith_trn_no);
		
        return view('admin.inventory.item-receives.receive', compact('targetRecord', 'acive_stores', 'items', 'next_receive_no', 'transaction_details'));
    }
	
	
    public function view($id)
    {
		$req = new \Illuminate\Http\Request();
		$req->initialize(['id' => $id]);
		//$next_receive_no = $this->get_next_receive_no($req);	
		$next_receive_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(5); /*  receive srl  */
		
        $targetRecord = $this->get_transfer_by_id($req);
		
        $transaction_details = $this->get_transaction_details_by_id($req);
		
		$acive_stores = app('App\Http\Controllers\Inv\CommonController')->get_all_acive_stores($req);
		
		$items = app('App\Http\Controllers\Inv\ItemController')->get_all_active_items($req);

        $this->setPageTitle('Item Receives', 'Receive Transfer No: '.$targetRecord->ith_trn_no);
		
        return view('admin.inventory.item-receives.view', compact('targetRecord', 'acive_stores', 'items', 'next_receive_no', 'transaction_details'));
    }
	
	public function store(Request $req)
    {
	
		
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------Transfer-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		//$next_receive_no = $this->get_next_receive_no($req);	
		$next_receive_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(5); /*  receive srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('items_transaction_headers')
				->insert
				(
					[
						
						'stk_trn_type_id' => 5, /* stock receive */
						'ith_trn_no' => $next_receive_no,
						'store_id' => $store_id,
						'target_store_id' => $prhdata['target_store_id'],
						'ith_date' => $prhdata['ith_date'],
						'ith_inv_no' => $prhdata['ith_inv_no'],
						'ith_wh_credit' => $prhdata['wh_credit'],
						'ith_transfer_id' => $prhdata['ith_transfer_id'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
												
						
					]
				);
				
				
			$ith_id = DB::getPdo()->lastInsertId();
			
			DB::table('items_transaction_headers')
					->where('id', $prhdata['ith_transfer_id'])
					->update
					(
						[
							
							'store_receive_status' => 1
							
						]
					);
					
			
			DB::table('items_transaction_payments')
					->insert
					(
						[
							'ith_id' => $ith_id,
							'ith_sum_amnt' => $prhdata['ith_sum_amnt'],
							'ith_sum_sgst' => $prhdata['ith_sum_sgst'],
							'ith_sum_cgst' => $prhdata['ith_sum_cgst'],
							'ith_gross_amnt' => $prhdata['ith_gross_amnt'],
							'ith_ded_amnt' => $prhdata['ith_ded_amnt'],
							'ith_net_amnt' => $prhdata['ith_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);

			

		/*----------------------Transfer DETAIL---------------------------------------------------------------*/
		
			foreach ($unitdata as $key)
			{
				
				DB::table('items_transaction_details')
						->insert
						(
							[
																
								
								'store_id' => $store_id,
								'items_transaction_header_id' => $ith_id,
								'itd_item_id' => $key['itd_item_id'],
								'itd_date' => $prhdata['ith_date'],
								'itd_srl' => $key['itd_srl'],
								'itd_unit_rate' => $key['itd_unit_rate'],
								'itd_unit_gst_rate' => $key['itd_unit_gst_rate'],
								'itd_unit_qty' => $key['itd_unit_qty'],
								'itd_item_unit_id' => $key['itd_item_unit_id'],
								'itd_unit_sum_amnt' => $key['itd_unit_sum_amnt'],
								'itd_unit_sgst' => ($key['unit_gst']/2),
								'itd_unit_cgst' => ($key['unit_gst']/2),
								'itd_unit_gross_amnt' => $key['itd_unit_gross_amnt']
								
								
								
								
							]
						);

			}		
			
		/*------------------------ACCOUNTS-------------------------------------------------------------*/
		//$next_acc_trn_srl = $this->get_next_acc_trn_srl($req, 3);
		$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(3);
		
		DB::table('accounts_details')
				->insert
				(
					[
						
						'acc_store_id' => $store_id,
						'acc_trn_type_id' =>5, /*   receive   */
						'acc_trn_mode_id' => 3,	
						'ith_id' => $ith_id,
						'acc_trn_srl' => $next_acc_trn_srl,
						'acc_trn_date' => $prhdata['ith_date'],
						'dr_acc_head_id' => 5,
						'cr_acc_head_id' => 6,
						'acc_trn_amnt' => $prhdata['ith_net_amnt'],
						'acc_trn_dscr' => 'journal.no. ' . $next_acc_trn_srl,
						'acc_trgt_store_id' => $prhdata['target_store_id'],
						'recorded_by' => Auth::guard('admin')->user()->id,
						'wh_credit' => $prhdata['wh_credit']
						
					]
				);
				
				
		if($prhdata['wh_credit']==0)  //IF NOT CREDIT, MEANS IF CASH PAID
		{	
			//$next_acc_trn_srl = $this->get_next_acc_trn_srl($req, 2);	
			$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(2);
		
			DB::table('accounts_details')
					->insert
					(
						[
							
							'acc_store_id' => $store_id,
							'acc_trn_type_id' =>5, /*   receive   */
							'acc_trn_mode_id' => 2,  /*  payment    */
							'ith_id' => $ith_id,
							'acc_trn_srl' => $next_acc_trn_srl,
							'acc_trn_date' => $prhdata['ith_date'],
							'dr_acc_head_id' => 6,
							'cr_acc_head_id' => 1,
							'acc_trn_amnt' => $prhdata['ith_net_amnt'],
							'acc_trn_dscr' => 'payment.no. ' . $next_acc_trn_srl,
							'acc_trgt_store_id' => $prhdata['target_store_id'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
							
						]
					);
		}	  
			
		return $ith_id;
		
    }
	
	
	public function get_all_pending_transfers(Request $req)
    {
		$pending_transfers = DB::table('items_transaction_headers')
			->leftJoin('items_transaction_payments', 'items_transaction_headers.id', '=', 'items_transaction_payments.ith_id')
			->leftJoin('accounts_headers', 'items_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->leftJoin('stores as source_stores', 'items_transaction_headers.store_id', '=', 'source_stores.id')
			->leftJoin('stores as trgt_stores', 'items_transaction_headers.target_store_id', '=', 'trgt_stores.id')
			->select('items_transaction_headers.*', 
			'items_transaction_payments.ith_sum_amnt', 
			'items_transaction_payments.ith_sum_sgst', 
			'items_transaction_payments.ith_sum_cgst', 
			'items_transaction_payments.ith_gross_amnt', 
			'items_transaction_payments.ith_ded_amnt', 
			'items_transaction_payments.ith_net_amnt', 
			'accounts_headers.head_name as head_name', 'trgt_stores.store_name as trgt_store_name', 'source_stores.store_name as source_store_name' )
			->whereIn('items_transaction_headers.stk_trn_type_id', [2]) 
			->where([['items_transaction_headers.store_receive_status', '=', 0]])
			->where([['items_transaction_headers.target_store_id', '=', Session::get('sess_store_id')]])
			->orderBy('items_transaction_headers.stk_trn_type_id', 'ASC') 
			->get();
			
		return $pending_transfers;
    }
	
	
	public function get_all_received_transfers(Request $req)
    {
		$received_transfers = DB::table('items_transaction_headers')
			->leftJoin('items_transaction_payments', 'items_transaction_headers.id', '=', 'items_transaction_payments.ith_id')
			->leftJoin('accounts_headers', 'items_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->leftJoin('stores as source_stores', 'items_transaction_headers.store_id', '=', 'source_stores.id')
			->leftJoin('stores as trgt_stores', 'items_transaction_headers.target_store_id', '=', 'trgt_stores.id')
			->select('items_transaction_headers.*', 
			'items_transaction_payments.ith_sum_amnt', 
			'items_transaction_payments.ith_sum_sgst', 
			'items_transaction_payments.ith_sum_cgst', 
			'items_transaction_payments.ith_gross_amnt', 
			'items_transaction_payments.ith_ded_amnt', 
			'items_transaction_payments.ith_net_amnt',  
			'accounts_headers.head_name as head_name', 'trgt_stores.store_name as source_store_name', 'source_stores.store_name as  trgt_store_name' )
			->whereIn('items_transaction_headers.stk_trn_type_id', [5]) 
			->where([['items_transaction_headers.store_id', '=', Session::get('sess_store_id')]])
			->orderBy('items_transaction_headers.stk_trn_type_id', 'ASC') 
			->get();
			
		return $received_transfers;
    }
	
	
	
	
	public function get_transfer_by_id(Request $req)
    {
		$transfer = DB::table('items_transaction_headers')
			->leftJoin('stores as source_stores', 'items_transaction_headers.store_id', '=', 'source_stores.id')
			->leftJoin('items_transaction_payments', 'items_transaction_headers.id', '=', 'items_transaction_payments.ith_id')
			->leftJoin('accounts_headers', 'items_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->select('items_transaction_headers.*', 
			'items_transaction_payments.ith_sum_amnt', 
			'items_transaction_payments.ith_sum_sgst', 
			'items_transaction_payments.ith_sum_cgst', 
			'items_transaction_payments.ith_gross_amnt', 
			'items_transaction_payments.ith_ded_amnt', 
			'items_transaction_payments.ith_net_amnt', 
			'accounts_headers.head_name as head_name', 'source_stores.store_name as source_store_name' )			
			->where([['items_transaction_headers.id', '=', $req->id]])
			->first();
				
		return $transfer;
    }
	
	
	public function get_transaction_details_by_id(Request $req)
    {
			
		$transaction_details = DB::table('items_transaction_details')
			->leftJoin('raw_items', 'items_transaction_details.itd_item_id', '=', 'raw_items.id')
			->leftJoin('item_units', 'items_transaction_details.itd_item_unit_id', '=', 'item_units.id')
			->select('items_transaction_details.*', 'raw_items.item_name as item_name', 'item_units.item_unit as item_unit')
			->where([['items_transaction_details.items_transaction_header_id', '=', $req->id]])
			->get();
			
				
		return $transaction_details;
    }
	
	
}
