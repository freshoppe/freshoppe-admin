<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;
use Hash;
use Carbon\Carbon;
use Session;

class ItemTransferReturnController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	
	
//============================USER TABLE INSERT=======================================//	

	
	

    public function index()
    {
		$req = new \Illuminate\Http\Request();
		
        $all_transactions = $this->get_all_drafted_and_confirmed_transactions($req);

        $this->setPageTitle('Item Transfer Returns', 'List of Item Transfer Returns');
        return view('admin.inventory.item-transfer-returns.index', compact('all_transactions' ));
    }
	
	
    public function create()
    {
		$req = new \Illuminate\Http\Request();
		
		$next_transfer_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(2); /*  transfer srl  */
		
		$except_self_acive_stores = app('App\Http\Controllers\Inv\CommonController')->get_all_acive_stores_except_self($req);
		
		
		$items = app('App\Http\Controllers\Inv\ItemController')->get_all_active_items($req);

        $this->setPageTitle('Item Transfer Returns', 'Item Transfer Returns');
        return view('admin.inventory.item-transfer-returns.create', compact('except_self_acive_stores', 'next_transfer_no', 'items'));
    }
	
	
    public function edit($id)
    {
		$req = new \Illuminate\Http\Request();
		$req->initialize(['id' => $id]);
		
        $targetRecord = $this->get_transfer_by_id($req);
		
        $transaction_details = $this->get_transaction_details_by_id($req);
		
		$except_self_acive_stores = app('App\Http\Controllers\Inv\CommonController')->get_all_acive_stores_except_self($req);
		
		$items = app('App\Http\Controllers\Inv\ItemController')->get_all_active_items($req);

        $this->setPageTitle('Item Transfer Returns', 'Edit Item Transfer Returns: '.$targetRecord->ith_trn_no);
		
        return view('admin.inventory.item-transfer-returns.edit', compact('targetRecord', 'except_self_acive_stores', 'items',  'targetRecord', 'transaction_details'));
    }
	
	
	public function store(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		$trn_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(8); /*  transfer return srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('items_transaction_headers')
				->insert
				(
					[
						'stk_trn_type_id' => 8, /*  transfer return*/
						'ith_trn_no' => $trn_no,
						'store_id' => $store_id,
						'target_store_id' => $prhdata['target_store_id'],
						'ith_date' => $prhdata['ith_date'],
						'ith_inv_no' => $prhdata['ith_inv_no'],
						'ith_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
																								
					]
				);
				
			$ith_id = DB::getPdo()->lastInsertId();
			
			DB::table('items_transaction_payments')
					->insert
					(
						[
							'ith_id' => $ith_id,
							'ith_sum_amnt' => $prhdata['ith_sum_amnt'],
							'ith_sum_sgst' => $prhdata['ith_sum_sgst'],
							'ith_sum_cgst' => $prhdata['ith_sum_cgst'],
							'ith_gross_amnt' => $prhdata['ith_gross_amnt'],
							'ith_ded_amnt' => $prhdata['ith_ded_amnt'],
							'ith_net_amnt' => $prhdata['ith_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
			

		/*----------------------TRANSFER DETAIL---------------------------------------------------------------*/
		
			foreach ($unitdata as $key)
			{
				
				DB::table('items_transaction_details')
						->insert
						(
							[
								'store_id' => $store_id,
								'items_transaction_header_id' => $ith_id,
								'itd_item_id' => $key['itd_item_id'],
								'itd_date' => $prhdata['ith_date'],
								'itd_srl' => $key['itd_srl'],
								'itd_unit_rate' => $key['itd_unit_rate'],
								'itd_unit_gst_rate' => $key['itd_unit_gst_rate'],
								'itd_unit_qty' => $key['itd_unit_qty'],
								'itd_item_unit_id' => $key['itd_item_unit_id'],
								'itd_unit_sum_amnt' => $key['itd_unit_sum_amnt'],
								'itd_unit_sgst' => ($key['unit_gst']/2),
								'itd_unit_cgst' => ($key['unit_gst']/2),
								'itd_unit_gross_amnt' => $key['itd_unit_gross_amnt']
								
								
							]
						);

			}		
			
		/*------------------------ACCOUNTS--------------------------FOR TRANSFER RETURN, TRANSACTON IS EQUIVALENT TO PURCHASE*/
		$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(3);  /* next journal number FOR TRANSFER */
		
		/* JOURNAL BETWEEN 6.'OTHER STORES A/C' Vs 4.'TRANSFER A/C'  */
		DB::table('accounts_details')		
				->insert
				(
					[
						'acc_store_id' => $store_id,
						'acc_trn_type_id' =>7,	 /*    transfer return  - accounts  */
						'acc_trn_mode_id' => 3,		
						'ith_id' => $ith_id,
						'acc_trn_srl' => $next_acc_trn_srl,
						'acc_trn_date' => $prhdata['ith_date'],
						'dr_acc_head_id' => 8,  /*   transfer return a/c.   */
						'cr_acc_head_id' =>  6, /*   OTHER STORE a/c.   */
						'acc_trn_amnt' => $prhdata['ith_net_amnt'],
						'acc_trn_dscr' => 'transfer return no. ' . $trn_no,
						'acc_trgt_store_id' => $prhdata['target_store_id'],
						'recorded_by' => Auth::guard('admin')->user()->id,
						'wh_credit' => $prhdata['wh_credit']
												
					]
				);
				 
				
		if($prhdata['wh_credit']==0)  //IF NOT CREDIT, MEANS CASH PAID
		{	
			$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(2);  /* next receipt number FOR TRANSFER RETURN*/
		
			/* PAYMENT ENTRY  */
			DB::table('accounts_details') 
					->insert
					(
						[
							
							'acc_store_id' => $store_id,
							'acc_trn_type_id' =>7,	 /*    transfer return  - accounts  */
							'acc_trn_mode_id' => 2,  /* PAYMENT */
							'ith_id' => $ith_id,
							'acc_trn_srl' => $next_acc_trn_srl,
							'acc_trn_date' => $prhdata['ith_date'],
							'dr_acc_head_id' => 6,
							'cr_acc_head_id' => 1,
							'acc_trn_amnt' => $prhdata['ith_net_amnt'],
							'acc_trn_dscr' => 'transfer return no. ' . $trn_no,
							'acc_trgt_store_id' => $prhdata['target_store_id'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
		}	  
			
		return $ith_id;
		
    }
	
	
	public function draft(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		$draft_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(11); /*  draft transfer return srl  */
		
		$store_id = Session::get('sess_store_id');
		

		DB::table('items_transaction_headers')
				->insert
				(
					[
						'stk_trn_type_id' => 11,  /* draft transfer return */
						'ith_trn_no' => $draft_no,
						'store_id' => $store_id,
						'target_store_id' => $prhdata['target_store_id'],
						'ith_date' => $prhdata['ith_date'],
						'ith_inv_no' => $prhdata['ith_inv_no'],
						'ith_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
					]
				);
				
			$ith_id = DB::getPdo()->lastInsertId();
			
			DB::table('items_transaction_payments')
					->insert
					(
						[
							'ith_id' => $ith_id,
							'ith_sum_amnt' => $prhdata['ith_sum_amnt'],
							'ith_sum_sgst' => $prhdata['ith_sum_sgst'],
							'ith_sum_cgst' => $prhdata['ith_sum_cgst'],
							'ith_gross_amnt' => $prhdata['ith_gross_amnt'],
							'ith_ded_amnt' => $prhdata['ith_ded_amnt'],
							'ith_net_amnt' => $prhdata['ith_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
			

		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
		
			foreach ($unitdata as $key)
			{
				
				DB::table('items_transaction_details')
						->insert
						(
							[
								'itd_wh_draft' => 1,
								'store_id' => $store_id,
								'items_transaction_header_id' => $ith_id,
								'itd_item_id' => $key['itd_item_id'],
								'itd_date' => $prhdata['ith_date'],
								'itd_srl' => $key['itd_srl'],
								'itd_unit_rate' => $key['itd_unit_rate'],
								'itd_unit_gst_rate' => $key['itd_unit_gst_rate'],
								'itd_unit_qty' => $key['itd_unit_qty'],
								'itd_item_unit_id' => $key['itd_item_unit_id'],
								'itd_unit_sum_amnt' => $key['itd_unit_sum_amnt'],
								'itd_unit_sgst' => ($key['unit_gst']/2),
								'itd_unit_cgst' => ($key['unit_gst']/2),
								'itd_unit_gross_amnt' => $key['itd_unit_gross_amnt']
								
							]
						);

			}		
			
		return 1;
		
    }
	
	
	public function update_draft(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		$store_id = Session::get('sess_store_id');
				
		DB::table('items_transaction_headers')
				->where('id', $prhdata['id'])
				->update
				(
					[
					
						'target_store_id' => $prhdata['target_store_id'],
						'ith_date' => $prhdata['ith_date'],
						'ith_inv_no' => $prhdata['ith_inv_no'],
						'ith_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id						
						
					]
				);
				
		DB::table('items_transaction_payments')
				->where('ith_id', $prhdata['id'])
				->update
				(
					[
						'ith_sum_amnt' => $prhdata['ith_sum_amnt'],
						'ith_sum_sgst' => $prhdata['ith_sum_sgst'],
						'ith_sum_cgst' => $prhdata['ith_sum_cgst'],
						'ith_gross_amnt' => $prhdata['ith_gross_amnt'],
						'ith_ded_amnt' => $prhdata['ith_ded_amnt'],
						'ith_net_amnt' => $prhdata['ith_net_amnt'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
					]
				);

		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
			$items_transaction_header_id = $prhdata['id'];
		
			DB::table('items_transaction_details')->where('items_transaction_header_id', $items_transaction_header_id)->delete();
			
			foreach ($unitdata as $key)
			{
				
				DB::table('items_transaction_details')
						->insert
						(
							[
								
								'itd_wh_draft' => 1,
								'store_id' => $store_id,
								'items_transaction_header_id' => $items_transaction_header_id,
								'itd_item_id' => $key['itd_item_id'],
								'itd_date' => $prhdata['ith_date'],
								'itd_srl' => $key['itd_srl'],
								'itd_unit_rate' => $key['itd_unit_rate'],
								'itd_unit_gst_rate' => $key['itd_unit_gst_rate'],
								'itd_unit_qty' => $key['itd_unit_qty'],
								'itd_item_unit_id' => $key['itd_item_unit_id'],
								'itd_unit_sum_amnt' => $key['itd_unit_sum_amnt'],
								'itd_unit_sgst' => ($key['unit_gst']/2),
								'itd_unit_cgst' => ($key['unit_gst']/2),
								'itd_unit_gross_amnt' => $key['itd_unit_gross_amnt']
								
								
							]
						);

			}		
			
		return 1;
		
    }
	
	
	public function delete_draft(Request $req)
    {
		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
		
		$items_transaction_header_id = $req->id;
	
		DB::table('items_transaction_details')->where('items_transaction_header_id', $items_transaction_header_id)->delete();
		
		DB::table('items_transaction_headers')->where('id', $items_transaction_header_id)->delete();
			
		return 1;
		
    }
	
	
	public function update_draft_as_transfer_return(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/  
		$req = new \Illuminate\Http\Request();
		$trn_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_ith_trn_no(8); /*  transfer return srl  */
		
		$store_id = Session::get('sess_store_id');
		
		DB::table('items_transaction_headers')
				->where('id', $prhdata['id'])
				->update
				(
					[
						'stk_trn_type_id' => 8,  /* transfer return */
						'ith_trn_no' => $trn_no,
						'target_store_id' => $prhdata['target_store_id'],
						'ith_date' => $prhdata['ith_date'],
						'ith_inv_no' => $prhdata['ith_inv_no'],
						'ith_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
												
					]
				);
				
		DB::table('items_transaction_payments')
				->where('ith_id', $prhdata['id'])
				->update
				(
					[
						'ith_sum_amnt' => $prhdata['ith_sum_amnt'],
						'ith_sum_sgst' => $prhdata['ith_sum_sgst'],
						'ith_sum_cgst' => $prhdata['ith_sum_cgst'],
						'ith_gross_amnt' => $prhdata['ith_gross_amnt'],
						'ith_ded_amnt' => $prhdata['ith_ded_amnt'],
						'ith_net_amnt' => $prhdata['ith_net_amnt'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
						
					]
				);
			

		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
		
			$items_transaction_header_id = $prhdata['id'];
		
			DB::table('items_transaction_details')->where('items_transaction_header_id', $items_transaction_header_id)->delete();
			
			foreach ($unitdata as $key)
			{
				
				DB::table('items_transaction_details')
						->insert
						(
							[
								'itd_wh_draft' => 0,
								'store_id' => $store_id,
								'items_transaction_header_id' => $items_transaction_header_id,
								'itd_item_id' => $key['itd_item_id'],
								'itd_date' => $prhdata['ith_date'],
								'itd_srl' => $key['itd_srl'],
								'itd_unit_rate' => $key['itd_unit_rate'],
								'itd_unit_gst_rate' => $key['itd_unit_gst_rate'],
								'itd_unit_qty' => $key['itd_unit_qty'],
								'itd_item_unit_id' => $key['itd_item_unit_id'],
								'itd_unit_sum_amnt' => $key['itd_unit_sum_amnt'],
								'itd_unit_sgst' => ($key['unit_gst']/2),
								'itd_unit_cgst' => ($key['unit_gst']/2),
								'itd_unit_gross_amnt' => $key['itd_unit_gross_amnt']
								
								
								
							]
						);

			}		
			
		/*------------------------ACCOUNTS--------------------------FOR TRANSFER RETURN, TRANSACTON IS EQUIVALENT TO RECEIVE*/
		$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(3);  /* next journal number FOR TRANSFER */
		
		/* JOURNAL BETWEEN 6.'STORES A/C' Vs 8.'TRANSFER RETURN A/C'  */
		DB::table('accounts_details')		
				->insert
				(
					[  
						
						'acc_store_id' => $store_id,
						'acc_trn_type_id' =>7,  /*    transfer return  - accounts  */
						'acc_trn_mode_id' => 3,		
						'ith_id' => $ith_id,
						'acc_trn_srl' => $next_acc_trn_srl,
						'acc_trn_date' => $prhdata['ith_date'],
						'dr_acc_head_id' => 8,  /*   transfer return a/c.   */
						'cr_acc_head_id' =>  6, /*   OTHER STORE a/c.   */
						'acc_trn_amnt' => $prhdata['ith_net_amnt'],
						'acc_trn_dscr' => 'transfer return no. ' . $trn_no,
						'acc_trgt_store_id' => $prhdata['target_store_id'],
						'recorded_by' => Auth::guard('admin')->user()->id,
						'wh_credit' => $prhdata['wh_credit']
						
						
					]
				);
				
						
		if($prhdata['wh_credit']==0)  //IF NOT CREDIT, MEANS CASH PAID
		{	
			$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(2);  /* next PAYMENT number FOR TRANSFER RETURN*/
		
			/* PAYMENT ENTRY  */
			DB::table('accounts_details') 
					->insert
					(
						[
							
							'acc_store_id' => $store_id,
							'acc_trn_type_id' =>7,	 /*    transfer return  - accounts  */
							'acc_trn_mode_id' => 2,  /* PAYMENT */
							'ith_id' => $ith_id,
							'acc_trn_srl' => $next_acc_trn_srl,
							'acc_trn_date' => $prhdata['ith_date'],
							'dr_acc_head_id' => 6,
							'cr_acc_head_id' => 1,
							'acc_trn_amnt' => $prhdata['ith_net_amnt'],
							'acc_trn_dscr' => 'transfer return no. ' . $trn_no,
							'acc_trgt_store_id' => $prhdata['target_store_id'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
		}	  
		
			
		return 1;
		
    }
		
	
	public function get_all_drafted_and_confirmed_transactions(Request $req)
    {
		$drafted_and_confirmed_purchases = DB::table('items_transaction_headers')
			->leftJoin('items_transaction_payments', 'items_transaction_headers.id', '=', 'items_transaction_payments.ith_id')
			->leftJoin('stores', 'items_transaction_headers.target_store_id', '=', 'stores.id')
			->leftJoin('stock_transaction_types', 'items_transaction_headers.stk_trn_type_id', '=', 'stock_transaction_types.id')
			->select('items_transaction_headers.*', 
			'items_transaction_payments.ith_sum_amnt', 
			'items_transaction_payments.ith_sum_sgst', 
			'items_transaction_payments.ith_sum_cgst', 
			'items_transaction_payments.ith_gross_amnt', 
			'items_transaction_payments.ith_ded_amnt', 
			'items_transaction_payments.ith_net_amnt', 
			'stores.store_name as store_name', 'stock_transaction_types.transaction_type', 'stock_transaction_types.itt_code', 'stock_transaction_types.ptt_code')
			->whereIn('items_transaction_headers.stk_trn_type_id', [8,11]) 
			->where([['items_transaction_headers.store_id', '=', Session::get('sess_store_id')]])
			->orderBy('items_transaction_headers.stk_trn_type_id', 'ASC') 
			->get();
			
		return $drafted_and_confirmed_purchases;
    }
	
	
	public function get_all_draft_transactions(Request $req)
    {
		$draft_transfers = DB::table('items_transaction_headers')
			->leftJoin('items_transaction_payments', 'items_transaction_headers.id', '=', 'items_transaction_payments.ith_id')
			->leftJoin('stores', 'items_transaction_headers.target_store_id', '=', 'stores.id')
			->select('items_transaction_headers.*', 
			'items_transaction_payments.ith_sum_amnt', 
			'items_transaction_payments.ith_sum_sgst', 
			'items_transaction_payments.ith_sum_cgst', 
			'items_transaction_payments.ith_gross_amnt', 
			'items_transaction_payments.ith_ded_amnt', 
			'items_transaction_payments.ith_net_amnt', 
			'stores.store_name as store_name')
			->where([['items_transaction_headers.stk_trn_type_id', '=', 11], ['items_transaction_headers.store_id', '=', Session::get('sess_store_id')]])
			->get();
				
		return $draft_transfers;
    }
	
	
	public function get_transfer_by_id(Request $req)
    {
		$transfer = DB::table('items_transaction_headers')
			->leftJoin('items_transaction_payments', 'items_transaction_headers.id', '=', 'items_transaction_payments.ith_id')
			->leftJoin('stores', 'items_transaction_headers.target_store_id', '=', 'stores.id')
			->leftJoin('accounts_headers', 'items_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->select('items_transaction_headers.*', 
			'items_transaction_payments.ith_sum_amnt', 
			'items_transaction_payments.ith_sum_sgst', 
			'items_transaction_payments.ith_sum_cgst', 
			'items_transaction_payments.ith_gross_amnt', 
			'items_transaction_payments.ith_ded_amnt', 
			'items_transaction_payments.ith_net_amnt', 
			'stores.store_name as store_name')
			->where([['items_transaction_headers.id', '=', $req->id]])
			->first();
				
		return $transfer;
    }
	
	public function get_transaction_details_by_id(Request $req)
    {
		$transaction_details = DB::table('items_transaction_details')
			->leftJoin('raw_items', 'items_transaction_details.itd_item_id', '=', 'raw_items.id')
			->leftJoin('item_units', 'items_transaction_details.itd_item_unit_id', '=', 'item_units.id')
			->select('items_transaction_details.*', 'raw_items.item_name as item_name', 'item_units.item_unit as item_unit')
			->where([['items_transaction_details.items_transaction_header_id', '=', $req->id]])
			->get();
				
		return $transaction_details;
    }
	
	
}
