<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;
use Hash;
use Carbon\Carbon;
use Session;

class ProdPurchaseReturnController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	
	
//============================USER TABLE INSERT=======================================//	

	
	

    public function index()
    {
		$req = new \Illuminate\Http\Request();
		
        //$suppliers = $this->get_all_active_suppliers($req);
        $all_transactions = $this->get_all_drafted_and_confirmed_transactions($req);

        $this->setPageTitle('Product Purchase Returns', 'List of Product Purchase Returns');
        return view('admin.inventory.prod-purchase-returns.index', compact('all_transactions' ));
    }
	
    public function create()
    {
		$req = new \Illuminate\Http\Request();
		
		$stk_trn_type_id = 7;  /* purchase return */
		
		$prh_rtn_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no($stk_trn_type_id); /*  purchase return srl  */
		
		$suppliers = app('App\Http\Controllers\Inv\SupplierController')->get_all_active_suppliers($req);
		
		$products = app('App\Http\Controllers\Inv\CommonController')->get_all_active_products($req);

        $this->setPageTitle('Product Purchase Returns', 'New Purchase Return');
        return view('admin.inventory.prod-purchase-returns.create', compact('suppliers', 'products', 'prh_rtn_no'));
    }
	
	
	
    public function edit($id)
    {
		$req = new \Illuminate\Http\Request();
		$req->initialize(['id' => $id]);
		//$prh_rtn_no = $this->get_next_prh_rtn_no($req);	
		$prh_rtn_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(1); /*  purchase srl  */
		
        $targetRecord = $this->get_purchase_by_id($req);
		
        $transaction_details = $this->get_transaction_details_by_id($req);
		
		$suppliers = app('App\Http\Controllers\Inv\SupplierController')->get_all_active_suppliers($req);
		
		$products = app('App\Http\Controllers\Inv\CommonController')->get_all_active_products($req);

        $this->setPageTitle('Purchase Returns', 'Edit Purchase Returns: '.$targetRecord->pth_trn_no);
		
        return view('admin.inventory.prod-purchase-returns.edit', compact('targetRecord', 'suppliers', 'products', 'prh_rtn_no', 'targetRecord', 'transaction_details'));
    }


	public function store(Request $req)
    {
	
		
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		//$prh_rtn_no = $this->get_next_prh_rtn_no($req);	
		$prh_rtn_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(7); /*  purchase return srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('products_transaction_headers')
				->insert
				(
					[
						
						'stk_trn_type_id' => 7,  /*  purchase return */
						'pth_trn_no' => $prh_rtn_no,
						'store_id' => $store_id,
						'supplier_id' => $prhdata['supplier_id'],
						'pth_date' => $prhdata['pth_date'],
						'pth_inv_no' => $prhdata['pth_inv_no'],
						'pth_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
												
					]
				);
				
			$pth_id = DB::getPdo()->lastInsertId();
			
			DB::table('products_transaction_payments')
					->insert
					(
						[
							'pth_id' => $pth_id,
							'pth_sum_amnt' => $prhdata['pth_sum_amnt'],
							'pth_sum_sgst' => $prhdata['pth_sum_sgst'],
							'pth_sum_cgst' => $prhdata['pth_sum_cgst'],
							'pth_gross_amnt' => $prhdata['pth_gross_amnt'],
							'pth_ded_amnt' => $prhdata['pth_ded_amnt'],
							'pth_net_amnt' => $prhdata['pth_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
							
							
						]
					);

			

		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
		
		
			foreach ($unitdata as $key)
			{
				
				DB::table('products_transaction_details')
						->insert
						(
							[
								'store_id' => $store_id,
								'products_transaction_header_id' => $pth_id,
								'ptd_product_id' => $key['ptd_product_id'],
								'ptd_date' => $prhdata['pth_date'],
								'ptd_srl' => $key['ptd_srl'],
								'ptd_unit_rate' => $key['ptd_unit_rate'],
								'ptd_unit_gst_rate' => $key['ptd_unit_gst_rate'],
								'ptd_unit_qty' => $key['ptd_unit_qty'],
								'ptd_unit_nos' => $key['ptd_unit_nos'],
								'ptd_unit_qty_gross' => $key['ptd_unit_qty_gross'],
								'ptd_unit_qty_net' => $key['ptd_unit_qty_net'],
								'ptd_item_unit_id' => $key['ptd_item_unit_id'],
								'ptd_unit_sum_amnt' => $key['ptd_unit_sum_amnt'],
								'ptd_unit_sgst' => ($key['unit_gst']/2),
								'ptd_unit_cgst' => ($key['unit_gst']/2),
								'ptd_unit_gross_amnt' => $key['ptd_unit_gross_amnt']
								
								
								
							]
						);

			}		
			
		/*------------------------ACCOUNTS-------------------------------------------------------------*/
		$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(3);
		
		DB::table('accounts_details')
				->insert
				(
					[
						'acc_store_id' => $store_id,
						'acc_trn_type_id' => 6,  /*    purchase return  - accounts  */
						'acc_trn_mode_id' => 3,
						'acc_trn_srl' => $next_acc_trn_srl,
						'acc_trn_date' => $prhdata['pth_date'],
						'dr_acc_head_id' => $prhdata['supplier_id'],
						'cr_acc_head_id' => 7,  /*   purchase return a/c.   */
						'acc_trn_amnt' => $prhdata['pth_net_amnt'],
						'acc_trn_dscr' => 'prh.rtn.no. ' . $prh_rtn_no,
						'acc_trn_type_id' =>1,
						'pth_id' => $pth_id,
						'recorded_by' => Auth::guard('admin')->user()->id,
						'wh_credit' => $prhdata['wh_credit']
												
					]
				);
				
				
		if($prhdata['wh_credit']==0)  //IF NOT CREDIT, MEANS IF CASH RECEIVED
		{	
			//$next_acc_trn_srl = $this->get_next_acc_trn_srl($req, 2);	
			$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(1);
		
			DB::table('accounts_details')
					->insert
					(
						[
							'acc_store_id' => $store_id,
							'acc_trn_type_id' =>6,	 /*    purchase return  - accounts  */
							'acc_trn_mode_id' => 1,  /* receipt */
							'pth_id' => $pth_id,
							'acc_trn_srl' => $next_acc_trn_srl,
							'acc_trn_date' => $prhdata['pth_date'],
							'dr_acc_head_id' => 1,
							'cr_acc_head_id' => $prhdata['supplier_id'],
							'acc_trn_amnt' => $prhdata['pth_net_amnt'],
							'acc_trn_dscr' => 'prh.rtn.no. ' . $prh_rtn_no,
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
		}	  
			
		return $pth_id;
		
    }
	
	
	public function draft(Request $req)
    {
		
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		//$prh_draft_no = $this->get_next_prh_draft_no($req);	
		$prh_draft_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(10); /*  purchase return draft srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('products_transaction_headers')
				->insert
				(
					[
						'stk_trn_type_id' => 10,  /* draft purchase return*/
						'pth_trn_no' => $prh_draft_no,
						'store_id' => $store_id,
						'supplier_id' => $prhdata['supplier_id'],
						'pth_date' => $prhdata['pth_date'],
						'pth_inv_no' => $prhdata['pth_inv_no'],
						'pth_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
					]
				);
				
			$pth_id = DB::getPdo()->lastInsertId();
			
			DB::table('products_transaction_payments')
					->insert
					(
						[
							'pth_id' => $pth_id,
							'pth_sum_amnt' => $prhdata['pth_sum_amnt'],
							'pth_sum_sgst' => $prhdata['pth_sum_sgst'],
							'pth_sum_cgst' => $prhdata['pth_sum_cgst'],
							'pth_gross_amnt' => $prhdata['pth_gross_amnt'],
							'pth_ded_amnt' => $prhdata['pth_ded_amnt'],
							'pth_net_amnt' => $prhdata['pth_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
			
					);
		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
			foreach ($unitdata as $key)
			{
				
				DB::table('products_transaction_details')
						->insert
						(
							[
								'ptd_wh_draft' => 1,
								'store_id' => $store_id,
								'products_transaction_header_id' => $pth_id,
								'ptd_product_id' => $key['ptd_product_id'],
								'ptd_date' => $prhdata['pth_date'],
								'ptd_srl' => $key['ptd_srl'],
								'ptd_unit_rate' => $key['ptd_unit_rate'],
								'ptd_unit_gst_rate' => $key['ptd_unit_gst_rate'],
								'ptd_unit_qty' => $key['ptd_unit_qty'],
								'ptd_unit_nos' => $key['ptd_unit_nos'],
								'ptd_unit_qty_gross' => $key['ptd_unit_qty_gross'],
								'ptd_unit_qty_net' => $key['ptd_unit_qty_net'],
								'ptd_item_unit_id' => $key['ptd_item_unit_id'],
								'ptd_unit_sum_amnt' => $key['ptd_unit_sum_amnt'],
								'ptd_unit_sgst' => ($key['unit_gst']/2),
								'ptd_unit_cgst' => ($key['unit_gst']/2),
								'ptd_unit_gross_amnt' => $key['ptd_unit_gross_amnt']
								
							]
						);

			}		
			
		return 1;
		
    }
	
	
	public function update_draft(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		$store_id = Session::get('sess_store_id');
				
		DB::table('products_transaction_headers')
				->where('id', $prhdata['id'])
				->update
				(
					[
						
						'supplier_id' => $prhdata['supplier_id'],
						'pth_date' => $prhdata['pth_date'],
						'pth_inv_no' => $prhdata['pth_inv_no'],
						'pth_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
					]
				);
				
				
			DB::table('products_transaction_payments')
				->where('pth_id', $prhdata['id'])
				->update
					(
						[
							'pth_sum_amnt' => $prhdata['pth_sum_amnt'],
							'pth_sum_sgst' => $prhdata['pth_sum_sgst'],
							'pth_sum_cgst' => $prhdata['pth_sum_cgst'],
							'pth_gross_amnt' => $prhdata['pth_gross_amnt'],
							'pth_ded_amnt' => $prhdata['pth_ded_amnt'],
							'pth_net_amnt' => $prhdata['pth_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
			
					);
				
			

		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
			$products_transaction_header_id = $prhdata['id'];
		
			DB::table('products_transaction_details')->where('products_transaction_header_id', $products_transaction_header_id)->delete();
			
			foreach ($unitdata as $key)
			{
				
				DB::table('products_transaction_details')
						->insert
						(
							[
								
								'ptd_wh_draft' => 1,
								'store_id' => $store_id,
								'products_transaction_header_id' => $products_transaction_header_id,
								'ptd_product_id' => $key['ptd_product_id'],
								'ptd_date' => $prhdata['pth_date'],
								'ptd_srl' => $key['ptd_srl'],
								'ptd_unit_rate' => $key['ptd_unit_rate'],
								'ptd_unit_gst_rate' => $key['ptd_unit_gst_rate'],
								'ptd_unit_qty' => $key['ptd_unit_qty'],
								'ptd_unit_nos' => $key['ptd_unit_nos'],
								'ptd_unit_qty_gross' => $key['ptd_unit_qty_gross'],
								'ptd_unit_qty_net' => $key['ptd_unit_qty_net'],
								'ptd_item_unit_id' => $key['ptd_item_unit_id'],
								'ptd_unit_sum_amnt' => $key['ptd_unit_sum_amnt'],
								'ptd_unit_sgst' => ($key['unit_gst']/2),
								'ptd_unit_cgst' => ($key['unit_gst']/2),
								'ptd_unit_gross_amnt' => $key['ptd_unit_gross_amnt']
								
								
								
								
							]
						);

			}		
			
		return 1;
		
    }
	
	
	public function update_draft_as_purchase_return(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------PURCHASE-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		//$prh_rtn_no = $this->get_next_prh_rtn_no($req);	
		$prh_rtn_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(7); /*  purchase return srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('products_transaction_headers')
				->where('id', $prhdata['id'])
				->update
				(
					[
						
						'stk_trn_type_id' => 7,  /* draft as purchase return */
						'pth_trn_no' => $prh_rtn_no,
						'supplier_id' => $prhdata['supplier_id'],
						'pth_date' => $prhdata['pth_date'],
						'pth_inv_no' => $prhdata['pth_inv_no'],
						'pth_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
																		
					]
				);
				
			DB::table('products_transaction_payments')
				->where('pth_id', $prhdata['id'])
				->update
					(
						[
							'pth_sum_amnt' => $prhdata['pth_sum_amnt'],
							'pth_sum_sgst' => $prhdata['pth_sum_sgst'],
							'pth_sum_cgst' => $prhdata['pth_sum_cgst'],
							'pth_gross_amnt' => $prhdata['pth_gross_amnt'],
							'pth_ded_amnt' => $prhdata['pth_ded_amnt'],
							'pth_net_amnt' => $prhdata['pth_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
			
					);
			

		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
		
			$products_transaction_header_id = $prhdata['id'];
		
			DB::table('products_transaction_details')->where('products_transaction_header_id', $products_transaction_header_id)->delete();
			
			foreach ($unitdata as $key)
			{
				
				DB::table('products_transaction_details')
						->insert
						(
							[
								
								'ptd_wh_draft' => 0,
								'store_id' => $store_id,
								'products_transaction_header_id' => $products_transaction_header_id,
								'ptd_product_id' => $key['ptd_product_id'],
								'ptd_date' => $prhdata['pth_date'],
								'ptd_srl' => $key['ptd_srl'],
								'ptd_unit_rate' => $key['ptd_unit_rate'],
								'ptd_unit_gst_rate' => $key['ptd_unit_gst_rate'],
								'ptd_unit_qty' => $key['ptd_unit_qty'],
								'ptd_unit_nos' => $key['ptd_unit_nos'],
								'ptd_unit_qty_gross' => $key['ptd_unit_qty_gross'],
								'ptd_unit_qty_net' => $key['ptd_unit_qty_net'],
								'ptd_item_unit_id' => $key['ptd_item_unit_id'],
								'ptd_unit_sum_amnt' => $key['ptd_unit_sum_amnt'],
								'ptd_unit_sgst' => ($key['unit_gst']/2),
								'ptd_unit_cgst' => ($key['unit_gst']/2),
								'ptd_unit_gross_amnt' => $key['ptd_unit_gross_amnt']
								
								
							]
						);

			}		
			
		/*------------------------ACCOUNTS-------------------------------------------------------------*/
		//$next_acc_trn_srl = $this->get_next_acc_trn_srl($req, 3);
		$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(3);
		
		DB::table('accounts_details')
				->insert
				(
					[
						
						'acc_store_id' => $store_id,
						'acc_trn_type_id' => 6,  /*    purchase return  - accounts  */
						'acc_trn_mode_id' => 3,
						'pth_id' => $products_transaction_header_id,
						'acc_trn_srl' => $next_acc_trn_srl,
						'acc_trn_date' => $prhdata['pth_date'],
						'dr_acc_head_id' => $prhdata['supplier_id'],
						'cr_acc_head_id' => 7,  /*   purchase return a/c.   */
						'acc_trn_amnt' => $prhdata['pth_net_amnt'],
						'acc_trn_dscr' => 'prh.rtn.no. ' . $prh_rtn_no,
						'recorded_by' => Auth::guard('admin')->user()->id,
						'wh_credit' => $prhdata['wh_credit']
						
												
					]
				);
				
				
		if($prhdata['wh_credit']==0)  //IF NOT CREDIT, MEANS IF CASH RECEIVED
		{	
			//$next_acc_trn_srl = $this->get_next_acc_trn_srl($req, 2);	
			$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(1);
		
			DB::table('accounts_details')
					->insert
					(
						[
							
							'acc_store_id' => $store_id,
							'acc_trn_type_id' =>6,	 /*    purchase return  - accounts  */
							'acc_trn_mode_id' => 1,  /* receipt */
							'pth_id' => $products_transaction_header_id,
							'acc_trn_srl' => $next_acc_trn_srl,
							'acc_trn_date' => $prhdata['pth_date'],
							'dr_acc_head_id' => 1,
							'cr_acc_head_id' => $prhdata['supplier_id'],
							'acc_trn_amnt' => $prhdata['pth_net_amnt'],
							'acc_trn_dscr' => 'prh.rtn.no. ' . $prh_rtn_no,
							'recorded_by' => Auth::guard('admin')->user()->id
							
														
							
						]
					);
		}	  
			
		return 1;
		
    }
	
	
	public function delete_draft(Request $req)
    {
		/*----------------------PURCHASE DETAIL---------------------------------------------------------------*/
		
		$products_transaction_header_id = $req->id;
	
		DB::table('products_transaction_details')->where('products_transaction_header_id', $products_transaction_header_id)->delete();
		
		DB::table('products_transaction_headers')->where('id', $products_transaction_header_id)->delete();
			
		return 1;
		
    }
		
	
	public function get_all_drafted_and_confirmed_transactions(Request $req)
    {
		$drafted_and_confirmed_purchases = DB::table('products_transaction_headers')
			->leftJoin('products_transaction_payments', 'products_transaction_headers.id', '=', 'products_transaction_payments.pth_id')
			->leftJoin('accounts_headers', 'products_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->leftJoin('stock_transaction_types', 'products_transaction_headers.stk_trn_type_id', '=', 'stock_transaction_types.id')
			->select('products_transaction_headers.*', 
			'products_transaction_payments.pth_sum_amnt', 
			'products_transaction_payments.pth_sum_sgst', 
			'products_transaction_payments.pth_sum_cgst', 
			'products_transaction_payments.pth_gross_amnt', 
			'products_transaction_payments.pth_ded_amnt', 
			'products_transaction_payments.pth_net_amnt', 
			'accounts_headers.head_name as head_name', 'stock_transaction_types.transaction_type', 'stock_transaction_types.itt_code', 'stock_transaction_types.ptt_code')
			->whereIn('products_transaction_headers.stk_trn_type_id', [7,10]) 
			->where([['products_transaction_headers.store_id', '=', Session::get('sess_store_id')]])
			->orderBy('products_transaction_headers.stk_trn_type_id', 'ASC') 
			->get();
			
		return $drafted_and_confirmed_purchases;
    }
	
	
	
	public function get_all_draft_purchases(Request $req)
    {
		$draft_purchases = DB::table('products_transaction_headers')
			->leftJoin('products_transaction_payments', 'products_transaction_headers.id', '=', 'products_transaction_payments.pth_id')
			->leftJoin('accounts_headers', 'products_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->select('products_transaction_headers.*', 
			'products_transaction_payments.pth_sum_amnt', 
			'products_transaction_payments.pth_sum_sgst', 
			'products_transaction_payments.pth_sum_cgst', 
			'products_transaction_payments.pth_gross_amnt', 
			'products_transaction_payments.pth_ded_amnt', 
			'products_transaction_payments.pth_net_amnt', 
			'accounts_headers.head_name as head_name')
			->where([['products_transaction_headers.stk_trn_type_id', '=', 7], ['products_transaction_headers.store_id', '=', Session::get('sess_store_id')]])
			->get();
				
		return $draft_purchases;
    }
	
	
	public function get_purchase_by_id(Request $req)
    {
		$purchase = DB::table('products_transaction_headers')
			->leftJoin('products_transaction_payments', 'products_transaction_headers.id', '=', 'products_transaction_payments.pth_id')
			->leftJoin('accounts_headers', 'products_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->select('products_transaction_headers.*', 
			'products_transaction_payments.pth_sum_amnt', 
			'products_transaction_payments.pth_sum_sgst', 
			'products_transaction_payments.pth_sum_cgst', 
			'products_transaction_payments.pth_gross_amnt', 
			'products_transaction_payments.pth_ded_amnt', 
			'products_transaction_payments.pth_net_amnt', 
			'accounts_headers.head_name as head_name')
			->where([['products_transaction_headers.id', '=', $req->id]])
			->first();
				
		return $purchase;
    }
	
	public function get_transaction_details_by_id(Request $req)
    {
		$transaction_details = DB::table('products_transaction_details')
			->leftJoin('products', 'products_transaction_details.ptd_product_id', '=', 'products.id')
			->leftJoin('raw_items', 'products.raw_item_id', '=', 'raw_items.id')
			->leftJoin('item_units', 'products_transaction_details.ptd_item_unit_id', '=', 'item_units.id')
			->select('products_transaction_details.*', 'raw_items.item_name', 'item_units.item_unit', 'products.name as product_name')
			->where([['products_transaction_details.products_transaction_header_id', '=', $req->id]])
			->get();
				
		return $transaction_details;
    }
	
	
}
