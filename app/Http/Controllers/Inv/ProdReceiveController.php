<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;
use Hash;
use Carbon\Carbon;
use Session;

class ProdReceiveController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	
//============================USER TABLE INSERT=======================================//	

	
	

    public function index()
    {
		$req = new \Illuminate\Http\Request();
		
        //$suppliers = $this->get_all_active_suppliers($req);
        $pending_transfers = $this->get_all_pending_transfers($req);
		
		$received_transfers = $this->get_all_received_transfers($req);

        $this->setPageTitle('Product Receives', 'List of Product Receives');
        return view('admin.inventory.prod-receives.index', compact('pending_transfers', 'received_transfers' ));
    }
	
	
	
    public function receive($id)
    {
		$req = new \Illuminate\Http\Request();
		$req->initialize(['id' => $id]);
		//$next_receive_no = $this->get_next_receive_no($req);	
		$next_receive_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(5); /*  receive srl  */
		
        $targetRecord = $this->get_transfer_by_id($req);
		
        $transaction_details = $this->get_transaction_details_by_id($req);
		
		$acive_stores = app('App\Http\Controllers\Inv\CommonController')->get_all_acive_stores($req);
		
		$products = app('App\Http\Controllers\Inv\CommonController')->get_all_active_products($req);

        $this->setPageTitle('Product Receives', 'Receive Transfer No: '.$targetRecord->pth_trn_no);
		
        return view('admin.inventory.prod-receives.receive', compact('targetRecord', 'acive_stores', 'products', 'next_receive_no', 'transaction_details'));
    }
	
	
    public function view($id)
    {
		$req = new \Illuminate\Http\Request();
		$req->initialize(['id' => $id]);
		//$next_receive_no = $this->get_next_receive_no($req);	
		$next_receive_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(5); /*  receive srl  */
		
        $targetRecord = $this->get_transfer_by_id($req);
		
        $transaction_details = $this->get_transaction_details_by_id($req);
		
		$acive_stores = app('App\Http\Controllers\Inv\CommonController')->get_all_acive_stores($req);
		
		$products = app('App\Http\Controllers\Inv\CommonController')->get_all_active_products($req);

        $this->setPageTitle('Product Receives', 'Receive Transfer No: '.$targetRecord->pth_trn_no);
		
        return view('admin.inventory.prod-receives.view', compact('targetRecord', 'acive_stores', 'products', 'next_receive_no', 'transaction_details'));
    }
	
	public function store(Request $req)
    {
	
		
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------Transfer-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		//$next_receive_no = $this->get_next_receive_no($req);	
		$next_receive_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(5); /*  receive srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('products_transaction_headers')
				->insert
				(
					[
						
						'stk_trn_type_id' => 5, /* stock receive */
						'pth_trn_no' => $next_receive_no,
						'store_id' => $store_id,
						'target_store_id' => $prhdata['target_store_id'],
						'pth_date' => $prhdata['pth_date'],
						'pth_inv_no' => $prhdata['pth_inv_no'],
						'pth_wh_credit' => $prhdata['wh_credit'],
						'pth_transfer_id' => $prhdata['pth_transfer_id'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
												
						
					]
				);
				
				
			$pth_id = DB::getPdo()->lastInsertId();
			
			DB::table('products_transaction_headers')
					->where('id', $prhdata['pth_transfer_id'])
					->update
					(
						[
							
							'store_receive_status' => 1
							
						]
					);
					
			
			DB::table('products_transaction_payments')
					->insert
					(
						[
							'pth_id' => $pth_id,
							'pth_sum_amnt' => $prhdata['pth_sum_amnt'],
							'pth_sum_sgst' => $prhdata['pth_sum_sgst'],
							'pth_sum_cgst' => $prhdata['pth_sum_cgst'],
							'pth_gross_amnt' => $prhdata['pth_gross_amnt'],
							'pth_ded_amnt' => $prhdata['pth_ded_amnt'],
							'pth_net_amnt' => $prhdata['pth_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);

			

		/*----------------------Transfer DETAIL---------------------------------------------------------------*/
		
		
			foreach ($unitdata as $key)
			{
				
				DB::table('products_transaction_details')
						->insert
						(
							[
																
								'store_id' => $store_id,
								'products_transaction_header_id' => $pth_id,
								'ptd_product_id' => $key['ptd_product_id'],
								'ptd_date' => $prhdata['pth_date'],
								'ptd_srl' => $key['ptd_srl'],
								'ptd_unit_rate' => $key['ptd_unit_rate'],
								'ptd_unit_gst_rate' => $key['ptd_unit_gst_rate'],
								'ptd_unit_qty' => $key['ptd_unit_qty'],
								'ptd_unit_nos' => $key['ptd_unit_nos'],
								'ptd_unit_qty_gross' => $key['ptd_unit_qty_gross'],
								'ptd_unit_qty_net' => $key['ptd_unit_qty_net'],
								'ptd_item_unit_id' => $key['ptd_item_unit_id'],
								'ptd_unit_sum_amnt' => $key['ptd_unit_sum_amnt'],
								'ptd_unit_sgst' => ($key['unit_gst']/2),
								'ptd_unit_cgst' => ($key['unit_gst']/2),
								'ptd_unit_gross_amnt' => $key['ptd_unit_gross_amnt']
								
								
							]
						);

			}		
			
		/*------------------------ACCOUNTS-------------------------------------------------------------*/
		//$next_acc_trn_srl = $this->get_next_acc_trn_srl($req, 3);
		$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(3);
		
		DB::table('accounts_details')
				->insert
				(
					[
						
						'acc_store_id' => $store_id,
						'acc_trn_mode_id' => 3,		
						'acc_trn_srl' => $next_acc_trn_srl,
						'acc_trn_date' => $prhdata['pth_date'],
						'dr_acc_head_id' => 5,
						'cr_acc_head_id' => 6,
						'acc_trn_amnt' => $prhdata['pth_net_amnt'],
						'acc_trn_dscr' => 'receive.no. ' . $next_acc_trn_srl,
						'acc_trn_type_id' =>5, /*   receive   */
						'pth_id' => $pth_id,
						'acc_trgt_store_id' => $prhdata['target_store_id'],
						'recorded_by' => Auth::guard('admin')->user()->id,
						'wh_credit' => $prhdata['wh_credit']
						
					]
				);
				
				
		if($prhdata['wh_credit']==0)  //IF NOT CREDIT, MEANS IF CASH PAID
		{	
			//$next_acc_trn_srl = $this->get_next_acc_trn_srl($req, 2);	
			$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(2);
		
			DB::table('accounts_details')
					->insert
					(
						[
							
							'acc_store_id' => $store_id,
							'acc_trn_mode_id' => 2,  /*  payment    */
							'acc_trn_srl' => $next_acc_trn_srl,
							'acc_trn_date' => $prhdata['pth_date'],
							'dr_acc_head_id' => 6,
							'cr_acc_head_id' => 1,
							'acc_trn_amnt' => $prhdata['pth_net_amnt'],
							'acc_trn_dscr' => 'receipt.no. ' . $next_acc_trn_srl,
							'acc_trn_type_id' =>5, /*   receive   */
							'pth_id' => $pth_id,
							'acc_trgt_store_id' => $prhdata['target_store_id'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
							
						]
					);
		}	  
			
		return $pth_id;
		
    }
	
	
	public function get_all_pending_transfers(Request $req)
    {
		$pending_transfers = DB::table('products_transaction_headers')
			->leftJoin('products_transaction_payments', 'products_transaction_headers.id', '=', 'products_transaction_payments.pth_id')
			->leftJoin('accounts_headers', 'products_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->leftJoin('stores as source_stores', 'products_transaction_headers.store_id', '=', 'source_stores.id')
			->leftJoin('stores as trgt_stores', 'products_transaction_headers.target_store_id', '=', 'trgt_stores.id')
			->select('products_transaction_headers.*', 
			'products_transaction_payments.pth_sum_amnt', 
			'products_transaction_payments.pth_sum_sgst', 
			'products_transaction_payments.pth_sum_cgst', 
			'products_transaction_payments.pth_gross_amnt', 
			'products_transaction_payments.pth_ded_amnt', 
			'products_transaction_payments.pth_net_amnt', 
			'accounts_headers.head_name as head_name', 'trgt_stores.store_name as trgt_store_name', 'source_stores.store_name as source_store_name' )
			->whereIn('products_transaction_headers.stk_trn_type_id', [2]) 
			->where([['products_transaction_headers.store_receive_status', '=', 0]])
			->where([['products_transaction_headers.target_store_id', '=', Session::get('sess_store_id')]])
			->orderBy('products_transaction_headers.stk_trn_type_id', 'ASC') 
			->get();
			
		return $pending_transfers;
    }
	
	
	public function get_all_received_transfers(Request $req)
    {
		$received_transfers = DB::table('products_transaction_headers')
			->leftJoin('products_transaction_payments', 'products_transaction_headers.id', '=', 'products_transaction_payments.pth_id')
			->leftJoin('accounts_headers', 'products_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->leftJoin('stores as source_stores', 'products_transaction_headers.store_id', '=', 'source_stores.id')
			->leftJoin('stores as trgt_stores', 'products_transaction_headers.target_store_id', '=', 'trgt_stores.id')
			->select('products_transaction_headers.*', 
			'products_transaction_payments.pth_sum_amnt', 
			'products_transaction_payments.pth_sum_sgst', 
			'products_transaction_payments.pth_sum_cgst', 
			'products_transaction_payments.pth_gross_amnt', 
			'products_transaction_payments.pth_ded_amnt', 
			'products_transaction_payments.pth_net_amnt',  
			'accounts_headers.head_name as head_name', 'trgt_stores.store_name as source_store_name', 'source_stores.store_name as trgt_store_name' )
			->whereIn('products_transaction_headers.stk_trn_type_id', [5]) 
			->where([['products_transaction_headers.store_id', '=', Session::get('sess_store_id')]])
			->orderBy('products_transaction_headers.stk_trn_type_id', 'ASC') 
			->get();
			
		return $received_transfers;
    }
	
	
	
	
	public function get_transfer_by_id(Request $req)
    {
		$transfer = DB::table('products_transaction_headers')
			->leftJoin('stores as source_stores', 'products_transaction_headers.store_id', '=', 'source_stores.id')
			->leftJoin('products_transaction_payments', 'products_transaction_headers.id', '=', 'products_transaction_payments.pth_id')
			->leftJoin('accounts_headers', 'products_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->select('products_transaction_headers.*', 
			'products_transaction_payments.pth_sum_amnt', 
			'products_transaction_payments.pth_sum_sgst', 
			'products_transaction_payments.pth_sum_cgst', 
			'products_transaction_payments.pth_gross_amnt', 
			'products_transaction_payments.pth_ded_amnt', 
			'products_transaction_payments.pth_net_amnt', 
			'accounts_headers.head_name as head_name', 'source_stores.store_name as source_store_name' )			
			->where([['products_transaction_headers.id', '=', $req->id]])
			->first();
				
		return $transfer;
    }
	
	
	public function get_transaction_details_by_id(Request $req)
    {
		$transaction_details = DB::table('products_transaction_details')
			->leftJoin('products', 'products_transaction_details.ptd_product_id', '=', 'products.id')
			->leftJoin('raw_items', 'products.raw_item_id', '=', 'raw_items.id')
			->leftJoin('item_units', 'products_transaction_details.ptd_item_unit_id', '=', 'item_units.id')
			->select('products_transaction_details.*', 'raw_items.item_name', 'item_units.item_unit', 'products.name as product_name')
			->where([['products_transaction_details.products_transaction_header_id', '=', $req->id]])
			->get();
				
		return $transaction_details;
    }
	
	
}
