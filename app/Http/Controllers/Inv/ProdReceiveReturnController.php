<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;
use Hash;
use Carbon\Carbon;
use Session;

class ProdReceiveReturnController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	
	
//============================USER TABLE INSERT=======================================//	

    public function index()
    {
		$req = new \Illuminate\Http\Request();
		
        //$suppliers = $this->get_all_active_suppliers($req);
        $all_transactions = $this->get_all_drafted_and_confirmed_transactions($req);

        $this->setPageTitle('Product Receive Returns', 'List of Product Receive Returns');
        return view('admin.inventory.prod-receive-returns.index', compact('all_transactions' ));
    }
	
    public function create()
    {
		$req = new \Illuminate\Http\Request();
		
		//$trans_no = $this->get_transfer_no($req);	
		$trans_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(2); /*  transfer srl  */
				
		$except_self_acive_stores = app('App\Http\Controllers\Inv\CommonController')->get_all_acive_stores_except_self($req);
		
		$products = app('App\Http\Controllers\Inv\CommonController')->get_all_active_products($req);

        $this->setPageTitle('Product Receive Return', 'New Receive Return');
        return view('admin.inventory.prod-receive-returns.create', compact('except_self_acive_stores', 'products', 'trans_no'));
    }
	
	
	
    public function edit($id)
    {
		$req = new \Illuminate\Http\Request();
		$req->initialize(['id' => $id]);
		//$trans_no = $this->get_transfer_no($req);	
		
        $targetRecord = $this->get_transfer_by_id($req);
		
        $transaction_details = $this->get_transaction_details_by_id($req);
		
		$except_self_acive_stores = app('App\Http\Controllers\Inv\CommonController')->get_all_acive_stores_except_self($req);
		
		$products = app('App\Http\Controllers\Inv\CommonController')->get_all_active_products($req);

        $this->setPageTitle('Product Receive Returns', 'Edit Receive Return : '.$targetRecord->pth_trn_no);
		
        return view('admin.inventory.prod-receive-returns.edit', compact('targetRecord', 'except_self_acive_stores', 'products', 'targetRecord', 'transaction_details'));
    }


	public function store(Request $req)
    {
	
		
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------Receive Return-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		//$trans_no = $this->get_transfer_no($req);	
		$trans_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(9); /* receive return srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('products_transaction_headers')
				->insert
				(
					[
						
						'stk_trn_type_id' => 9, /* receive return */
						'pth_trn_no' => $trans_no,
						'store_id' => $store_id,
						'target_store_id' => $prhdata['target_store_id'],
						'pth_date' => $prhdata['pth_date'],
						'pth_inv_no' => $prhdata['pth_inv_no'],
						'pth_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
												
						
					]
				);
				
			$pth_id = DB::getPdo()->lastInsertId();
			
			DB::table('products_transaction_payments')
					->insert
					(
						[
							'pth_id' => $pth_id,
							'pth_sum_amnt' => $prhdata['pth_sum_amnt'],
							'pth_sum_sgst' => $prhdata['pth_sum_sgst'],
							'pth_sum_cgst' => $prhdata['pth_sum_cgst'],
							'pth_gross_amnt' => $prhdata['pth_gross_amnt'],
							'pth_ded_amnt' => $prhdata['pth_ded_amnt'],
							'pth_net_amnt' => $prhdata['pth_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);

			

		/*----------------------Receive Return DETAIL---------------------------------------------------------------*/
		
		
			foreach ($unitdata as $key)
			{
				
				DB::table('products_transaction_details')
						->insert
						(
							[
																
								'store_id' => $store_id,
								'products_transaction_header_id' => $pth_id,
								'ptd_product_id' => $key['ptd_product_id'],
								'ptd_date' => $prhdata['pth_date'],
								'ptd_srl' => $key['ptd_srl'],
								'ptd_unit_rate' => $key['ptd_unit_rate'],
								'ptd_unit_gst_rate' => $key['ptd_unit_gst_rate'],
								'ptd_unit_qty' => $key['ptd_unit_qty'],
								'ptd_unit_nos' => $key['ptd_unit_nos'],
								'ptd_unit_qty_gross' => $key['ptd_unit_qty_gross'],
								'ptd_unit_qty_net' => $key['ptd_unit_qty_net'],
								'ptd_item_unit_id' => $key['ptd_item_unit_id'],
								'ptd_unit_sum_amnt' => $key['ptd_unit_sum_amnt'],
								'ptd_unit_sgst' => ($key['unit_gst']/2),
								'ptd_unit_cgst' => ($key['unit_gst']/2),
								'ptd_unit_gross_amnt' => $key['ptd_unit_gross_amnt']
								
								
							]
						);

			}		
			
		/*------------------------ACCOUNTS-------------------------------------------------------------*/
		//$next_acc_trn_srl = $this->get_next_acc_trn_srl($req, 3);
		$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(3);  /* next journal number FOR Receive Return */
		
		DB::table('accounts_details')
				->insert
				(
					[
						
						'acc_store_id' => $store_id,
						'acc_trn_type_id' =>9,
						'acc_trn_mode_id' => 3,		
						'acc_trn_srl' => $next_acc_trn_srl,
						'acc_trn_date' => $prhdata['pth_date'],
						'dr_acc_head_id' => 6,
						'cr_acc_head_id' => 10,
						'acc_trn_amnt' => $prhdata['pth_net_amnt'],
						'acc_trn_dscr' => 'Receive Return.no. ' . $trans_no,
						'pth_id' => $pth_id,
						'acc_trgt_store_id' => $prhdata['target_store_id'],
						'recorded_by' => Auth::guard('admin')->user()->id,
						'wh_credit' => $prhdata['wh_credit']
						
					]
				);
				
				
		if($prhdata['wh_credit']==0)  //IF NOT CREDIT, MEANS CASH RECEIVED
		{	
			$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(1);  /* next receipt number FOR Receive Return */
			
			/* RECEIPT ENTRY  */
			DB::table('accounts_details')
					->insert
					(
						[
							
							'acc_store_id' => $store_id,
							'acc_trn_type_id' =>9,
							'acc_trn_mode_id' => 1,
							'acc_trn_srl' => $next_acc_trn_srl,
							'acc_trn_date' => $prhdata['pth_date'],
							'dr_acc_head_id' => 1,
							'cr_acc_head_id' => 6,
							'acc_trn_amnt' => $prhdata['pth_net_amnt'],
							'acc_trn_dscr' => 'Receive Return.no. ' . $trans_no,
							'acc_trn_dscr' => 'Receive Return.no. ' . $trans_no,
							'pth_id' => $pth_id,
							'acc_trgt_store_id' => $prhdata['target_store_id'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
							
						]
					);
		}	  
			
		return $pth_id;
		
    }
	
	
	public function draft(Request $req)
    {
		
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------Receive Return-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		//$transfer_draft_no = $this->get_next_transfer_draft_no($req);	
		$transfer_draft_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(12); /*  Receive Return draft srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('products_transaction_headers')
				->insert
				(
					[						
						
						'stk_trn_type_id' => 12,  /* draft Receive Return */
						'pth_trn_no' => $transfer_draft_no,
						'store_id' => $store_id,
						'target_store_id' => $prhdata['target_store_id'],
						'pth_date' => $prhdata['pth_date'],
						'pth_inv_no' => $prhdata['pth_inv_no'],
						'pth_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id

						
					]
				);
				
			$pth_id = DB::getPdo()->lastInsertId();
			
			DB::table('products_transaction_payments')
					->insert
					(
						[
							'pth_id' => $pth_id,
							'pth_sum_amnt' => $prhdata['pth_sum_amnt'],
							'pth_sum_sgst' => $prhdata['pth_sum_sgst'],
							'pth_sum_cgst' => $prhdata['pth_sum_cgst'],
							'pth_gross_amnt' => $prhdata['pth_gross_amnt'],
							'pth_ded_amnt' => $prhdata['pth_ded_amnt'],
							'pth_net_amnt' => $prhdata['pth_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
			

		/*----------------------Receive Return DETAIL---------------------------------------------------------------*/
			foreach ($unitdata as $key)
			{
				
				DB::table('products_transaction_details')
						->insert
						(
							[
								'ptd_wh_draft' => 1,
								'store_id' => $store_id,
								'products_transaction_header_id' => $pth_id,
								'ptd_product_id' => $key['ptd_product_id'],
								'ptd_date' => $prhdata['pth_date'],
								'ptd_srl' => $key['ptd_srl'],
								'ptd_unit_rate' => $key['ptd_unit_rate'],
								'ptd_unit_gst_rate' => $key['ptd_unit_gst_rate'],
								'ptd_unit_qty' => $key['ptd_unit_qty'],
								'ptd_unit_nos' => $key['ptd_unit_nos'],
								'ptd_unit_qty_gross' => $key['ptd_unit_qty_gross'],
								'ptd_unit_qty_net' => $key['ptd_unit_qty_net'],
								'ptd_item_unit_id' => $key['ptd_item_unit_id'],
								'ptd_unit_sum_amnt' => $key['ptd_unit_sum_amnt'],
								'ptd_unit_sgst' => ($key['unit_gst']/2),
								'ptd_unit_cgst' => ($key['unit_gst']/2),
								'ptd_unit_gross_amnt' => $key['ptd_unit_gross_amnt']
								
								
							]
						);

			}		
			
		return 1;
		
    }
	
	
	public function update_draft(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------Receive Return-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		$store_id = Session::get('sess_store_id');
				
		DB::table('products_transaction_headers')
				->where('id', $prhdata['id'])
				->update
				(
					[
						
						'target_store_id' => $prhdata['target_store_id'],
						'pth_date' => $prhdata['pth_date'],
						'pth_inv_no' => $prhdata['pth_inv_no'],
						'pth_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
					]
				);
				
				
			DB::table('products_transaction_payments')
				->where('pth_id', $prhdata['id'])
				->update
					(
						[
							'pth_sum_amnt' => $prhdata['pth_sum_amnt'],
							'pth_sum_sgst' => $prhdata['pth_sum_sgst'],
							'pth_sum_cgst' => $prhdata['pth_sum_cgst'],
							'pth_gross_amnt' => $prhdata['pth_gross_amnt'],
							'pth_ded_amnt' => $prhdata['pth_ded_amnt'],
							'pth_net_amnt' => $prhdata['pth_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
			
					);
				
			

		/*----------------------Receive Return DETAIL---------------------------------------------------------------*/
			$products_transaction_header_id = $prhdata['id'];
		
			DB::table('products_transaction_details')->where('products_transaction_header_id', $products_transaction_header_id)->delete();
			
			foreach ($unitdata as $key)
			{
				
				DB::table('products_transaction_details')
						->insert
						(
							[
								
								
								'ptd_wh_draft' => 1,
								'store_id' => $store_id,
								'products_transaction_header_id' => $products_transaction_header_id,
								'ptd_product_id' => $key['ptd_product_id'],
								'ptd_date' => $prhdata['pth_date'],
								'ptd_srl' => $key['ptd_srl'],
								'ptd_unit_rate' => $key['ptd_unit_rate'],
								'ptd_unit_gst_rate' => $key['ptd_unit_gst_rate'],
								'ptd_unit_qty' => $key['ptd_unit_qty'],
								'ptd_unit_nos' => $key['ptd_unit_nos'],
								'ptd_unit_qty_gross' => $key['ptd_unit_qty_gross'],
								'ptd_unit_qty_net' => $key['ptd_unit_qty_net'],
								'ptd_item_unit_id' => $key['ptd_item_unit_id'],
								'ptd_unit_sum_amnt' => $key['ptd_unit_sum_amnt'],
								'ptd_unit_sgst' => ($key['unit_gst']/2),
								'ptd_unit_cgst' => ($key['unit_gst']/2),
								'ptd_unit_gross_amnt' => $key['ptd_unit_gross_amnt']
								
								
								
								
							]
						);

			}		
			
		return 1;
		
    }
	
	
	public function update_draft_as_receive_return(Request $req)
    {
		$prhdata = $req->prhdata;
		$unitdata = $req->unitdata;
		/*------------------------Receive Return-------------------------------------------------------------*/
		$req = new \Illuminate\Http\Request();
		//$trans_no = $this->get_transfer_no($req);	
		$trans_no = app('App\Http\Controllers\Inv\AccTransController')->get_next_pth_trn_no(9); /*  Receive Return  srl  */
		$store_id = Session::get('sess_store_id');
		
		DB::table('products_transaction_headers')
				->where('id', $prhdata['id'])
				->update
				(
					[
												
						'stk_trn_type_id' => 9, /* receive return */
						'pth_trn_no' => $trans_no,
						'target_store_id' => $prhdata['target_store_id'],
						'pth_date' => $prhdata['pth_date'],
						'pth_inv_no' => $prhdata['pth_inv_no'],
						'pth_wh_credit' => $prhdata['wh_credit'],
						'recorded_by' => Auth::guard('admin')->user()->id
						
						
					]
				);
				
				
			DB::table('products_transaction_payments')
				->where('pth_id', $prhdata['id'])
				->update
					(
						[
							'pth_sum_amnt' => $prhdata['pth_sum_amnt'],
							'pth_sum_sgst' => $prhdata['pth_sum_sgst'],
							'pth_sum_cgst' => $prhdata['pth_sum_cgst'],
							'pth_gross_amnt' => $prhdata['pth_gross_amnt'],
							'pth_ded_amnt' => $prhdata['pth_ded_amnt'],
							'pth_net_amnt' => $prhdata['pth_net_amnt'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
			
					);
			

		/*----------------------Receive Return DETAIL---------------------------------------------------------------*/
		
			$products_transaction_header_id = $prhdata['id'];
		
			DB::table('products_transaction_details')->where('products_transaction_header_id', $products_transaction_header_id)->delete();
			
			foreach ($unitdata as $key)
			{
				
				DB::table('products_transaction_details')
						->insert
						(
							[
								
								
								'ptd_wh_draft' => 0,
								'store_id' => $store_id,
								'products_transaction_header_id' => $products_transaction_header_id,
								'ptd_product_id' => $key['ptd_product_id'],
								'ptd_date' => $prhdata['pth_date'],
								'ptd_srl' => $key['ptd_srl'],
								'ptd_unit_rate' => $key['ptd_unit_rate'],
								'ptd_unit_gst_rate' => $key['ptd_unit_gst_rate'],
								'ptd_unit_qty' => $key['ptd_unit_qty'],
								'ptd_unit_nos' => $key['ptd_unit_nos'],
								'ptd_unit_qty_gross' => $key['ptd_unit_qty_gross'],
								'ptd_unit_qty_net' => $key['ptd_unit_qty_net'],
								'ptd_item_unit_id' => $key['ptd_item_unit_id'],
								'ptd_unit_sum_amnt' => $key['ptd_unit_sum_amnt'],
								'ptd_unit_sgst' => ($key['unit_gst']/2),
								'ptd_unit_cgst' => ($key['unit_gst']/2),
								'ptd_unit_gross_amnt' => $key['ptd_unit_gross_amnt']
								
								
							]
						);

			}		
			
		/*------------------------ACCOUNTS--------------------------FOR Receive Return TRANSACTON IS EQUIVALENT TO TRANSFER*/
		$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(3);  /* next journal number FOR Receive Return */

		
		DB::table('accounts_details')
				->insert
				(
					[
												
						'acc_store_id' => $store_id,
						'acc_trn_type_id' =>9,
						'acc_trn_mode_id' => 3,		
						'acc_trn_srl' => $next_acc_trn_srl,
						'acc_trn_date' => $prhdata['pth_date'],
						'dr_acc_head_id' => 6,
						'cr_acc_head_id' => 10,
						'acc_trn_amnt' => $prhdata['pth_net_amnt'],
						'acc_trn_dscr' => 'Receive Return.no. ' . $trans_no,
						'pth_id' => $products_transaction_header_id,
						'acc_trgt_store_id' => $prhdata['target_store_id'],
						'recorded_by' => Auth::guard('admin')->user()->id,
						'wh_credit' => $prhdata['wh_credit']
						
					]
				);
				
				
		if($prhdata['wh_credit']==0)  //IF NOT CREDIT, MEANS CASH RECEIVED
		{	
			$next_acc_trn_srl = app('App\Http\Controllers\Inv\AccTransController')->get_next_acc_trn_srl(1);  /* next receipt number FOR Receive Return */
		
			/* RECEIPT ENTRY  */
			DB::table('accounts_details')
					->insert
					(
						[
							
							
							'acc_store_id' => $store_id,
							'acc_trn_type_id' =>9,
							'acc_trn_mode_id' => 1,
							'acc_trn_srl' => $next_acc_trn_srl,
							'acc_trn_date' => $prhdata['pth_date'],
							'dr_acc_head_id' => 1,
							'cr_acc_head_id' => 6,
							'acc_trn_amnt' => $prhdata['pth_net_amnt'],
							'acc_trn_dscr' => 'Receive Return.no. ' . $trans_no,
							'pth_id' => $products_transaction_header_id,
							'acc_trgt_store_id' => $prhdata['target_store_id'],
							'recorded_by' => Auth::guard('admin')->user()->id
							
							
						]
					);
		}	  
			
		return 1;
		
    }
	
	
	public function delete_draft(Request $req)
    {
		/*----------------------Receive Return DETAIL---------------------------------------------------------------*/
		
		$products_transaction_header_id = $req->id;
	
		DB::table('products_transaction_details')->where('products_transaction_header_id', $products_transaction_header_id)->delete();
		
		DB::table('products_transaction_headers')->where('id', $products_transaction_header_id)->delete();
			
		return 1;
		
    }
	
	
	
	
	public function get_all_drafted_and_confirmed_transactions(Request $req)
    {
		$store_id = Session::get('sess_store_id');
		
		$drafted_and_confirmed_transfers = DB::table('products_transaction_headers')
			->leftJoin('products_transaction_payments', 'products_transaction_headers.id', '=', 'products_transaction_payments.pth_id')
			->leftJoin('accounts_headers', 'products_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->leftJoin('stores as trgt_stores', 'products_transaction_headers.target_store_id', '=', 'trgt_stores.id')
			->leftJoin('stock_transaction_types', 'products_transaction_headers.stk_trn_type_id', '=', 'stock_transaction_types.id')
			->select('products_transaction_headers.*', 
			'products_transaction_payments.pth_sum_amnt', 
			'products_transaction_payments.pth_sum_sgst', 
			'products_transaction_payments.pth_sum_cgst', 
			'products_transaction_payments.pth_gross_amnt', 
			'products_transaction_payments.pth_ded_amnt', 
			'products_transaction_payments.pth_net_amnt', 
			'accounts_headers.head_name as head_name', 'trgt_stores.store_name as trgt_store_name', 'stock_transaction_types.transaction_type', 'stock_transaction_types.itt_code', 'stock_transaction_types.ptt_code' )
			->whereIn('products_transaction_headers.stk_trn_type_id', [9,12]) 
			->where([['products_transaction_headers.store_id', '=', $store_id]])
			->orderBy('products_transaction_headers.stk_trn_type_id', 'ASC') 
			->get();
			
			
		return $drafted_and_confirmed_transfers;
    }
	
	
	public function get_all_draft_transfers(Request $req)
    {
		$store_id = Session::get('sess_store_id');
		
		$all_transactions = DB::table('products_transaction_headers')
			->leftJoin('products_transaction_payments', 'products_transaction_headers.id', '=', 'products_transaction_payments.pth_id')
			->leftJoin('accounts_headers', 'products_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->select('products_transaction_headers.*', 
			'products_transaction_payments.pth_sum_amnt', 
			'products_transaction_payments.pth_sum_sgst', 
			'products_transaction_payments.pth_sum_cgst', 
			'products_transaction_payments.pth_gross_amnt', 
			'products_transaction_payments.pth_ded_amnt', 
			'products_transaction_payments.pth_net_amnt', 
			'accounts_headers.head_name as head_name' )			
			->where([['products_transaction_headers.stk_trn_type_id', '=', 3]])
			->where([['products_transaction_headers.store_id', '=', $store_id]])
			->get();
				
		return $all_transactions;
    }
	
	
	public function get_transfer_by_id(Request $req)
    {
		$transfer = DB::table('products_transaction_headers')
			->leftJoin('products_transaction_payments', 'products_transaction_headers.id', '=', 'products_transaction_payments.pth_id')
			->leftJoin('accounts_headers', 'products_transaction_headers.supplier_id', '=', 'accounts_headers.id')
			->select('products_transaction_headers.*', 
			'products_transaction_payments.pth_sum_amnt', 
			'products_transaction_payments.pth_sum_sgst', 
			'products_transaction_payments.pth_sum_cgst', 
			'products_transaction_payments.pth_gross_amnt', 
			'products_transaction_payments.pth_ded_amnt', 
			'products_transaction_payments.pth_net_amnt', 
			'accounts_headers.head_name as head_name' )			
			->where([['products_transaction_headers.id', '=', $req->id]])
			->first();
				
		return $transfer;
    }
	
	public function get_transaction_details_by_id(Request $req)
    {
		$transaction_details = DB::table('products_transaction_details')
			->leftJoin('products', 'products_transaction_details.ptd_product_id', '=', 'products.id')
			->leftJoin('raw_items', 'products.raw_item_id', '=', 'raw_items.id')
			->leftJoin('item_units', 'products_transaction_details.ptd_item_unit_id', '=', 'item_units.id')
			->select('products_transaction_details.*', 'raw_items.item_name', 'item_units.item_unit', 'products.name as product_name')
			->where([['products_transaction_details.products_transaction_header_id', '=', $req->id]])
			->get();
				
		return $transaction_details;
    }
	
	
}
