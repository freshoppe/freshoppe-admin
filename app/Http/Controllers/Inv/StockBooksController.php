<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;
use Hash;
use Carbon\Carbon;
use Session;


class StockBooksController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	

	
//============================USER TABLE INSERT=======================================//

    public function index()
    {
		$store_id = Session::get('sess_store_id');
		
		$req = new \Illuminate\Http\Request();
		
		$items = app('App\Http\Controllers\Inv\ItemController')->get_all_active_items_with_balance($req);

        $this->setPageTitle('Items', 'List of all Items');
        return view('admin.inventory.stock-reports.items.index', compact('items'));
    }
	
	
    public function product_index()
    {
		$req = new \Illuminate\Http\Request();
		
		$products = app('App\Http\Controllers\Inv\CommonController')->get_all_active_products($req);

        $this->setPageTitle('Products', 'List of all Products');
        return view('admin.inventory.stock-reports.products.index', compact('products'));
    }

    public function get_common_from_to_dates()
    {
		$from_date = Carbon::now()->firstOfMonth()->format('Y-m-d');
		$to_date = Carbon::now()->format('Y-m-d');

		return [$from_date, $to_date ]; 
		
    }

 
    public function item_ledger(Request $req)
    {
		$store_id = Session::get('sess_store_id');
		
		list($from_date, $to_date ) = $this->get_common_from_to_dates();
				
		(request('from_date') !== null) && $from_date = request('from_date');
		(request('to_date') !== null) && $to_date = request('to_date');
		
 		
		$items_transaction_details = DB::table('items_transaction_details')
			->leftJoin('items_transaction_headers', 'items_transaction_details.items_transaction_header_id', '=', 'items_transaction_headers.id')
			->leftJoin('raw_items', 'items_transaction_details.itd_item_id', '=', 'raw_items.id')
			->leftJoin('stock_transaction_types', 'items_transaction_headers.stk_trn_type_id', '=', 'stock_transaction_types.id')
			->leftJoin('item_units', 'items_transaction_details.itd_item_unit_id', '=', 'item_units.id')
			->select('items_transaction_details.*', 'raw_items.item_name', 'items_transaction_headers.stk_trn_type_id', 'stock_transaction_types.transaction_type', 'stock_transaction_types.in_out', 'item_units.item_unit')
			->where([['items_transaction_headers.ith_date', '>=', $from_date], ['items_transaction_headers.ith_date', '<=', $to_date]])
			->where([['items_transaction_details.itd_item_id', '=', $req->id], ['items_transaction_headers.store_id', '=', $store_id]])
			->whereNotIn('items_transaction_headers.stk_trn_type_id', [3,4]) /*    excempt drafts  */
			->get();
		
		 
		(count($items_transaction_details) > 0 ) ? $itm_unit = $items_transaction_details[0]->item_unit : $itm_unit = '';
		
		(count($items_transaction_details) > 0 ) ? $item_name = $items_transaction_details[0]->item_name : $item_name = '';
		
		$head_id = $req->id;

        $this->setPageTitle('ITEM BALANCE: '. strtoupper($item_name), 'Transactions of ' . $item_name);
        return view('admin.inventory.stock-reports.items.item-ledger', compact('items_transaction_details', 'itm_unit', 'head_id', 'from_date', 'to_date'));
		
		
    }
	
	
    public function product_ledger(Request $req)
    {
		$store_id = Session::get('sess_store_id');
		
		list($from_date, $to_date ) = $this->get_common_from_to_dates();
				
		(request('from_date') !== null) && $from_date = request('from_date');
		(request('to_date') !== null) && $to_date = request('to_date');
		
		$products_transaction_details = DB::table('products_transaction_details')
			->leftJoin('products_transaction_headers', 'products_transaction_details.products_transaction_header_id', '=', 'products_transaction_headers.id')
			->leftJoin('products', 'products_transaction_details.ptd_product_id', '=', 'products.id')
			->leftJoin('stock_transaction_types', 'products_transaction_headers.stk_trn_type_id', '=', 'stock_transaction_types.id')
			->leftJoin('item_units', 'products_transaction_details.ptd_item_unit_id', '=', 'item_units.id')
			->select('products_transaction_details.*', 'products.name as product_name', 'products_transaction_headers.stk_trn_type_id', 'stock_transaction_types.transaction_type', 'stock_transaction_types.in_out', 'item_units.item_unit')
			->where([['products_transaction_headers.pth_date', '>=', $from_date], ['products_transaction_headers.pth_date', '<=', $to_date]])
			->where([['products_transaction_details.ptd_product_id', '=', $req->id], ['products_transaction_headers.store_id', '=', $store_id]])
			->whereNotIn('products_transaction_headers.stk_trn_type_id', [3,4]) /*    excempt drafts  */
			->get();
			
		$head_id = $req->id;
		
		(count($products_transaction_details) > 0 ) ? $itm_unit = $products_transaction_details[0]->item_unit : $itm_unit = '';
		
		(count($products_transaction_details) > 0 ) ? $product_name = $products_transaction_details[0]->product_name : $product_name = '';

        $this->setPageTitle('PRODUCT BALANCE: '. strtoupper($product_name), 'Transactions of ' . $product_name);
        return view('admin.inventory.stock-reports.products.product-ledger', compact('products_transaction_details', 'itm_unit', 'head_id', 'from_date', 'to_date'));
		
		
    }
	
	

	
	
}
