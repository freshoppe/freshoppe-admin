<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use App\Models\ItemTransactionHeader;
use App\Models\ItemTransactionDetail;
use App\Models\AccountsDetail;
use App\Models\StoreItemStock;
use Auth;
use Session;

class StockItemReceiveController extends BaseController
{
	
    public function index(Request $request)
    {
        $query = ItemTransactionHeader::where('stk_trn_type_id',2);
		$now = date('Y-m-d');
		if($request->min_date != NULL){
			$from = $request->min_date;
		}else
			$from = $now;
		if($request->max_date != NULL){
			$to = $request->max_date;
		}else
			$to = $now;
			
		$query->whereBetween('ith_date', [$from, $to]);
		$query->where([['target_store_id', '=', Session::get('sess_store_id')]]);
		
		//if($request->min_date != NULL && $request->max_date != NULL)
		/* else if($request->min_date != NULL)
			$query->whereBetween('ith_date', [$request->min_date, $now]);
		else if($request->max_date != NULL)
			$query->where('ith_date', '<=', $request->max_date); */
		
		$stockItemReceived = $query->get();
		$pending   = $stockItemReceived->filter(function ($pvalue, $pkey) {
			return $pvalue->store_receive_status == 0;
		});

		$confirmed = $stockItemReceived->filter(function ($cvalue, $ckey) {
			return $cvalue->store_receive_status == 1;
		});
        $this->setPageTitle('Item Received', 'List of Received Items');
		session()->flashInput($request->input());
        return view('admin.inventory.store-stock-receives.index', compact('pending','confirmed','from','to' ));
    }
	
		
	public function show($id)
    {
        $targetRecord = ItemTransactionHeader::find($id);
		if($targetRecord->store_receive_status == 1){
			$targetRecord = ItemTransactionHeader::where('ith_trn_no',$targetRecord->ith_trn_no)->where('stk_trn_type_id',5)->first();
		}
        $this->setPageTitle('Stock Received', $targetRecord->ith_trn_no);
        return view('admin.inventory.store-stock-receives.show', compact('targetRecord' ));
    }
	
	
	public function confirm(Request $request)
	{
		$store_id = Session::get('sess_store_id');
		
		$transferId = $request->transfer_id;
		
		$targetRecord = ItemTransactionHeader::find($transferId);
		$targetRecord->store_receive_status = 1;
		$targetRecord->save();
		
		
		$header = ItemTransactionHeader::create([
			'ith_transfer_id' => $transferId,
			'stk_trn_type_id' => 5, /* stock receives */
			'ith_trn_no' => $targetRecord->ith_trn_no,
			'store_id' => $store_id,
			'target_store_id' => $targetRecord->target_store_id,
			'ith_date' => $targetRecord->ith_date,
			'ith_inv_no' => $targetRecord->ith_inv_no,
			'ith_sum_amnt' => $targetRecord->ith_sum_amnt,
			'ith_sum_sgst' => $targetRecord->ith_sum_sgst,
			'ith_sum_cgst' => $targetRecord->ith_sum_cgst,
			'ith_gross_amnt' => $targetRecord->ith_gross_amnt,
			'ith_ded_amnt' => $targetRecord->ith_ded_amnt,
			'ith_net_amnt' => $targetRecord->ith_net_amnt,
			'ith_wh_credit' => $targetRecord->ith_wh_credit,
			'store_receive_status' =>1,
			'recorded_by' => Auth::guard('admin')->user()->id
		]);
		
		foreach ($targetRecord->transactionDetails as $key=>$val){
			$details = ItemTransactionDetail::create([	
				'store_id' => $store_id,
				'items_transaction_header_id' => $header->id,
				'itd_item_id' => $val->itd_item_id,
				'itd_date' => $val->ith_date,
				'itd_srl' => $val->itd_srl,
				'itd_unit_rate' => $val->itd_unit_rate,
				'itd_unit_gst_rate' => $val->itd_unit_gst_rate,
				'itd_unit_qty' => $val->itd_unit_qty,
				'itd_item_unit_id' => $val->itd_item_unit_id,
				'itd_unit_sum_amnt' => $val->itd_unit_sum_amnt,
				'itd_unit_sgst' => $val->unit_gst,
				'itd_unit_cgst' => $val->unit_gst,
				'itd_unit_gross_amnt' => $val->itd_unit_gross_amnt
			]);
		}
		
		/*------------------------ACCOUNTS-------------------------------------------------------------*/
		$nextAccTrnSrl = $this->getNextAccTrnSrl(3);
		
		$accountDetails = AccountsDetail::create([
			'acc_store_id' => $store_id,
			'acc_trn_mode_id' => 3,
			'acc_trn_srl' => $nextAccTrnSrl,
			'acc_trn_date' => $targetRecord->ith_date,
			'dr_acc_head_id' => 5,
			'cr_acc_head_id' => 6,
			'acc_trn_amnt' => $targetRecord->ith_net_amnt,
			'acc_trn_dscr' => 'prh.no. ' . $nextAccTrnSrl,
			'acc_trn_type_id' =>1,
			'ith_id' => $header->id,
			'recorded_by' => Auth::guard('admin')->user()->id,
			'wh_credit' => $targetRecord->ith_wh_credit
		]);
		
		if($targetRecord->ith_wh_credit == 0)  //IF NOT CREDIT, MEANS IF CASH PAID
		{	
			$nextAccTrnSrl = $this->getNextAccTrnSrl(2);
			
			$accountDetails = AccountsDetail::create([
				'acc_store_id' => $store_id,
				'acc_trn_mode_id' => 2,
				'acc_trn_srl' => $nextAccTrnSrl,
				'acc_trn_date' => $targetRecord->ith_date,
				'dr_acc_head_id' => 6,
				'cr_acc_head_id' => 1,
				'acc_trn_amnt' => $targetRecord->ith_net_amnt,
				'acc_trn_dscr' => 'payment.no. ' . $nextAccTrnSrl,
				'acc_trn_type_id' =>1,
				'ith_id' => $header->id,
				'recorded_by' => Auth::guard('admin')->user()->id,
				'wh_credit' => $targetRecord->ith_wh_credit
			]);
		}
		
		return 1;
	}
	
	public function update(Request $request){
	
		$store_id = Session::get('sess_store_id');
		$transferId = $request->targetId;
		$targetRecord = ItemTransactionHeader::find($transferId);
		
		
		if($targetRecord->store_receive_status == 0){
			$targetRecord->store_receive_status = 1;
			$targetRecord->save();
			
			$header = ItemTransactionHeader::create([
				'ith_transfer_id' => $transferId,
				'stk_trn_type_id' => 5, // stock receives
				'ith_trn_no' => $targetRecord->ith_trn_no,
				'store_id' => $store_id,
				'target_store_id' => $targetRecord->target_store_id,
				'ith_date' => $targetRecord->ith_date,
				'ith_inv_no' => $targetRecord->ith_inv_no,
				'ith_sum_amnt' => $request->ith_sum_amnt,
				'ith_sum_sgst' => $request->ith_sum_sgst,
				'ith_sum_cgst' => $request->ith_sum_cgst,
				'ith_gross_amnt' => $request->ith_gross_amnt,
				'ith_ded_amnt' => $request->ith_ded_amnt,
				'ith_net_amnt' => $request->ith_net_amnt,
				'ith_wh_credit' => $targetRecord->ith_wh_credit,
				'store_receive_status' =>1,
				'recorded_by' => Auth::guard('admin')->user()->id
			]);
			
			$newQuantity 		= $request->item_qty;
			$newUnitSum 		= $request->itd_unit_sum_amnt;
			$newUnitSGSTSum 	= $request->itd_unit_sgst;
			$newUnitGrossSum 	= $request->itd_unit_gross_amnt;
			foreach ($targetRecord->transactionDetails as $key=>$val){
				if (array_key_exists($val->id, $newQuantity)) {
					$details = ItemTransactionDetail::create([	
						'store_id' => $store_id,
						'items_transaction_header_id' => $header->id,
						'itd_item_id' => $val->itd_item_id,
						'itd_date' => $val->ith_date,
						'itd_srl' => $val->itd_srl,
						'itd_unit_rate' => $val->itd_unit_rate,
						'itd_unit_gst_rate' => $val->itd_unit_gst_rate,
						'itd_unit_qty' => $newQuantity[$val->id],
						'itd_item_unit_id' => $val->itd_item_unit_id,
						'itd_unit_sum_amnt' => $newUnitSum[$val->id],
						'itd_unit_sgst' => $newUnitSGSTSum[$val->id]/2,
						'itd_unit_cgst' => $newUnitSGSTSum[$val->id]/2,
						'itd_unit_gross_amnt' => $newUnitGrossSum[$val->id]
					]);
				}
			}
			 
			/*------------------------ACCOUNTS-------------------------------------------------------------*/
			$nextAccTrnSrl = $this->getNextAccTrnSrl(3);
			
			$accountDetails = AccountsDetail::create([
				'acc_store_id' => $store_id,
				'acc_trn_mode_id' => 3,
				'acc_trn_srl' => $nextAccTrnSrl,
				'acc_trn_date' => $header->ith_date,
				'dr_acc_head_id' => 5,
				'cr_acc_head_id' => 6,
				'acc_trn_amnt' => $header->ith_net_amnt,
				'acc_trn_dscr' => 'prh.no. ' . $nextAccTrnSrl,
				'acc_trn_type_id' =>1,
				'ith_id' => $header->id,
				'recorded_by' => Auth::guard('admin')->user()->id,
				'wh_credit' => $header->ith_wh_credit
			]);
			
			if($header->ith_wh_credit == 0)  //IF NOT CREDIT, MEANS IF CASH PAID
			{	
				$nextAccTrnSrl = $this->getNextAccTrnSrl(2);
				
				$accountDetails = AccountsDetail::create([
					'acc_store_id' => $store_id,
					'acc_trn_mode_id' => 2,
					'acc_trn_srl' => $nextAccTrnSrl,
					'acc_trn_date' => $header->ith_date,
					'dr_acc_head_id' => 6,
					'cr_acc_head_id' => 1,
					'acc_trn_amnt' => $header->ith_net_amnt,
					'acc_trn_dscr' => 'payment.no. ' . $nextAccTrnSrl,
					'acc_trn_type_id' =>1,
					'ith_id' => $header->id,
					'recorded_by' => Auth::guard('admin')->user()->id,
					'wh_credit' => $header->ith_wh_credit
				]);
			}
			return redirect('/admin/inventory/store/stock-receives');
		}else{
			return $this->responseRedirectBack('This stock is already received.', 'error', true, true);
		}
	
	}
	public function getNextAccTrnSrl($mode)    
	{		
		$nextAccTrnSrl = AccountsDetail::where('acc_trn_mode_id',$mode)->max('acc_trn_srl');
		return $nextAccTrnSrl + 1;
	}
	
}
