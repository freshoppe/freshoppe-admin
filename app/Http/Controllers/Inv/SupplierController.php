<?php

namespace App\Http\Controllers\Inv;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;
use Hash;
use Carbon\Carbon;
use Session;


class SupplierController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //protected $guard = 'admin';

    //protected $redirectTo = '/';
	
//============================USER TABLE INSERT=======================================//	

    public function index()
    {
		$req = new \Illuminate\Http\Request();
		
        $suppliers = $this->get_all_active_suppliers($req);

        $this->setPageTitle('Suppliers', 'List of all Suppliers');
        return view('admin.inventory.suppliers.index', compact('suppliers'));
    }
	
    public function create()
    {


        $this->setPageTitle('Suppliers', 'Create Supplier');
        return view('admin.inventory.suppliers.create');
    }
	
	 
	public function store(Request $req)
    {
		$store_id = Session::get('sess_store_id');
		
		$resp_count = DB::table('accounts_headers')
				->where([['head_name', '=', $req->supplier_name]])
				->count();
						
		if(!$resp_count)
		{
		
			DB::table('accounts_headers')
					->insert
					(
						[
							'head_name' => $req->supplier_name,
							'acc_group_id' => 16,
							'ah_store_id' => $store_id,
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
					
						$acc_head_id = DB::getPdo()->lastInsertId();
						
			DB::table('suppliers')
					->insert
					(
						[
							'acc_head_id' => $acc_head_id,
							'supplier_address' => $req->supplier_address,
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
						
						
			
			return 1;
			
		}
		else
		{
			return 0;
		}
		
		
    }
	
	
	
	public function update(Request $req)
    {
		
		$resp_count = DB::table('accounts_headers')
				->where([['head_name', '=', $req->supplier_name], ['id', '!=', $req->id]])
				->count();
						
		if(!$resp_count)
		{
		
			DB::table('accounts_headers')
					->where([['id', '=', $req->id] ])
					->update
					(
						[
							'head_name' => $req->supplier_name,
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
					
					
			DB::table('suppliers')
					->where([['acc_head_id', '=', $req->id] ])
					->update
					(
						[
							'supplier_address' => $req->supplier_address,
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
					
					
			
			return 1;
			
		}
		else
		{
			return 0;
		}
		
		
    }
	
	
	public function delete(Request $req)
    {
		
			DB::table('accounts_headers')
					->where([['id', '=', $req->id] ])
					->update
					(
						[
							'active' => null,
							'recorded_by' => Auth::guard('admin')->user()->id
							
						]
					);
					
			
			return 1;
			
		
		
    }
	
	
    public function edit($id)
    {
		$req = new \Illuminate\Http\Request();
		$req->initialize(['id' => $id]);
		
        $targetSupplier = $this->get_supplier_by_id($req);
		

        $this->setPageTitle('Suppliers', 'Edit Supplier : '.$targetSupplier->head_name);
        return view('admin.inventory.suppliers.edit', compact('targetSupplier'));
    }

	 
	
	public function get_all_suppliers(Request $req) 
    {
		$suppliers = DB::table('accounts_headers')
			->leftJoin('suppliers', 'accounts_headers.id', '=', 'suppliers.acc_head_id')
			->select('accounts_headers.*', 'suppliers.supplier_address AS supplier_address')
			->where([['accounts_headers.acc_group_id', '=', 16], ['accounts_headers.id', '!=', $req->id]])
			->whereNull('accounts_headers.deleted_at')
			->get();
				
		return $suppliers;
    }
	
	public function get_all_active_suppliers(Request $req)
    {
		$suppliers = DB::table('accounts_headers')
			->leftJoin('suppliers', 'accounts_headers.id', '=', 'suppliers.acc_head_id')
			->select('accounts_headers.*', 'suppliers.supplier_address AS supplier_address')
			->where([['accounts_headers.acc_group_id', '=', 16], ['accounts_headers.id', '!=', $req->id]])
			->whereNotNull('accounts_headers.active')
			->whereNull('accounts_headers.deleted_at')
			->get();
				
		return $suppliers;
    }


	public function get_supplier_by_id(Request $req)
    {
		
		$suppliers = DB::table('accounts_headers')
		->leftJoin('suppliers', 'accounts_headers.id', '=', 'suppliers.acc_head_id')
		->select('accounts_headers.*', 'suppliers.supplier_address AS supplier_address')
		->where([['accounts_headers.id', '=', $req->id] ]) 
		->first();
		
				
		return $suppliers;
			
		
    }
	

	
	
}
