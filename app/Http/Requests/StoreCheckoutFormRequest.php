<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCheckoutFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'		=>  'required',
            'last_name'		 	=>  'required',
			'phone_number'  	=>  'required',
            'email'  			=>  'required|email',
            'address'   		=>  'required',
            'post_code'  		=>  'required|numeric',
            'city' 				=>  'required',
            'state'  			=>  'required',
            'country'  			=>  'required',
			'ship_first_name'	=>  'required',
            'ship_last_name' 	=>  'required',
            'ship_phone_number' =>  'required',
            'ship_email'  		=>  'required|email',
            'ship_address'   	=>  'required',
            'ship_post_code' 	=>  'required|numeric',
            'ship_city' 		=>  'required',
            'ship_state'  		=>  'required',
            'ship_country'  	=>  'required',
            'payment_method'  	=>  'required',
        ];
    }
}
