<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name'      		=>  'required|max:255',
            'price'     		=>  'required|regex:/^\d+(\.\d{1,2})?$/',
            'special_price' 	=>  'regex:/^\d+(\.\d{1,2})?$/',
            //'quantity'  		=>  'required|numeric',
            //'sku'  				=>  'required|unique:products,sku,'.$request->product_id,
            //'number'  			=>  'required|unique:products,number,'.$request->product_id,
            //'processing_id' 	=>  'required',
            'unit_id' 			=>  'required',
            'raw_item_id' 		=>  'required',
            'description' 		=>  'required',
            'short_description' =>  'required',
			'ecom_name' 		=>  'required',
        ];
    }
}
