<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountsDetail extends Model
{
    protected $table = 'accounts_details';

    protected $fillable = [
		'acc_store_id','acc_trn_mode_id','acc_trn_srl','acc_trn_date','dr_acc_head_id','cr_acc_head_id','acc_trn_amnt','acc_trn_dscr','acc_trn_type_id','ith_id','recorded_by','wh_credit'
    ];

	
}
