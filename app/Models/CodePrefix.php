<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodePrefix extends Model
{
    protected $table = 'code_prefix';

    protected $fillable = [
		'name','code'
    ];

	
}
