<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ItemProcessing extends Model
{
    /**
     * @var string
     */
    protected $table = 'item_processing_ways';

    /**
     * @var array
     */
    protected $fillable = [
        'item_id', 'processing_id'
    ];
	
}
