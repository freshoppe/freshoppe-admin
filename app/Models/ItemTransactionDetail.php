<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemTransactionDetail extends Model
{
    protected $table = 'items_transaction_details';

    protected $fillable = [
		'store_id','items_transaction_header_id','itd_item_id','weight','price','itd_date','itd_srl','itd_unit_rate','itd_unit_gst_rate','itd_unit_qty','itd_item_unit_id','itd_unit_sum_amnt','itd_unit_sgst','itd_unit_cgst','itd_unit_gross_amnt','gst','itd_wh_draft'
    ];

    public function item()
    {
        return $this->hasOne(RawItem::class,'id','itd_item_id');
    }
	
	public function itemUnit()
    {
        return $this->hasOne(ItemUnit::class,'id','itd_item_unit_id');
    }
	
}
