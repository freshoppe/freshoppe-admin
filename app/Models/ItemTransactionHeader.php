<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemTransactionHeader extends Model
{
    protected $table = 'items_transaction_headers';

	protected $dates = ['ith_date'];
	
    protected $fillable = [
        'transaction_number', 'stk_trn_type_id', 'item_count', 'notes', 'store_id', 'target_store_id', 'supplier_id', 'ith_date', 'ith_inv_no', 'ith_trn_no', 'ith_sum_amnt', 'ith_sum_sgst', 'ith_sum_cgst', 'ith_gross_amnt', 'ith_ded_amnt', 'ith_net_amnt', 'ith_wh_credit' ,'store_receive_status', 'recorded_by'
    ];

    public function targetStore()
    {
        return $this->belongsTo(Store::class, 'target_store_id');
    }
	
	public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
	
	public function transactionDetails()
    {
        return $this->hasMany(ItemTransactionDetail::class,'items_transaction_header_id', 'id');
    }
}
