<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /**
     * @var string
     */
    protected $table = 'locations';

    /**
     * @var array
     */
    protected $fillable = [
        'location_name', 'location_code', 'city', 'state', 'pincode', 'status'
    ];

}
