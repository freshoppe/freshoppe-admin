<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    protected $table = 'order_transaction_payments';

    protected $fillable = [
        'order_transaction_headers_id','sub_total', 'gst_rate' ,'gst_total', 'discount_rate', 'discount_total', 'grand_total', 'payment_type_id', 'payment_status' 
    ];

	public function paymentType()
    {
        return $this->hasOne(PaymentType::class,'id', 'payment_type_id');
    }
}
