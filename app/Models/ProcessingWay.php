<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessingWay extends Model
{
    /**
     * @var string
     */
    protected $table = 'processing_ways';

    /**
     * @var array
     */
    protected $fillable = [
        'processing_name', 'code', 'status'
    ];

	
}
