<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	/**
     * @var string
     */
    protected $table = 'products';

    /**
     * @var array
     */
    protected $fillable = [
        'raw_item_id', 'unit_id', 'processing_id', 'pieces', 'sku', 'number', 'name', 'ecom_name', 'slug', 'short_description', 'description', 'quantity','gross_weight','net_weight', 'price', 'sale_price', 'gst_rate','size_min','size_max','status', 'featured','whats_in_box','serves','source','source_image','new_arrival','speed_deals','todays_special','ecom_status'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'quantity'  =>  'integer',
        'status'    =>  'boolean',
        'featured'  =>  'boolean'
    ];

    /**
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        //$this->attributes['slug'] = Str::slug($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany(ProductReview::class)->where('verified',1);
    }
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function item()
    {
        return $this->hasOne(RawItem::class,'id','raw_item_id');
    }
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function processing()
    {
        return $this->hasOne(ProcessingWay::class,'id','processing_id');
    }
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function productUnit()
    {
        return $this->hasOne(ProductUnit::class,'id','unit_id');
    }
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recipes()
    {
        return $this->hasMany(Recipe::class);
    }
	
	/**
     * @param $value
     */
    public function setEcomNameAttribute($value)
    {
		$this->attributes['ecom_name'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }
}
