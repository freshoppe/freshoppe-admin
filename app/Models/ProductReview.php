<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    /**
     * @var string
     */
    protected $table = 'product_reviews';

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'name', 'email', 'title', 'comments', 'rating', 'verified'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
