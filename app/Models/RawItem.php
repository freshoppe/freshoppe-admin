<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RawItem extends Model
{
    protected $table = 'raw_items';

    protected $fillable = [
        'item_code', 'item_name', 'item_unit_id', 'item_price', 'item_gst', 'waste_percentage', 'recorded_by', 'item_type_id'
    ];
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unit()
    {
        return $this->hasOne(ItemUnit::class,'id','item_unit_id');
    }
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function processings()
    {
        return $this->belongsToMany(ProcessingWay::class, 'item_processing_ways', 'item_id', 'processing_id');
    }
}
