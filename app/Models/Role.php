<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug',
    ];
	
    public function permissions() {
	   return $this->belongsToMany(Permission::class,'roles_permissions');
	}

	public function admins() {
	   return $this->belongsToMany(Staff::class,'staffs_roles');
	}
	
	/**
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }
}
