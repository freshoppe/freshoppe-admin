<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\HasPermissionsTrait;

class Staff extends Authenticatable
{
    use Notifiable;
	
	use HasPermissionsTrait; //Import The Trait
    
	/**
     * @var string
     */
    protected $table = 'staffs';
	
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','staff_type','department_id','status','super_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
	
	public function warehouse()
	{
		return $this->hasOne(Warehouse::class,'id', 'department_id');
	}
	
	public function store()
	{
		return $this->hasOne(Store::class,'id', 'department_id');
	}
	
}
