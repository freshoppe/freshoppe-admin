<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    /**
     * @var string
     */
    protected $table = 'stores';

    /**
     * @var array
     */
    protected $fillable = [
        'store_name', 'store_code', 'location_id', 'warehouse_id', 'pincode', 'address', 'store_type', 'status','is_wharehouse_store'
    ];
	
	public function location()
    {
        return $this->hasOne(Location::class,'id', 'location_id');
    }
	
	public function warehouse()
    {
        return $this->hasOne(Warehouse::class,'id', 'warehouse_id');
    }

}
