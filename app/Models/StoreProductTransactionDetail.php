<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreProductTransactionDetail extends Model
{
    protected $table = 'store_products_transaction_details';

    protected $fillable = [
		'store_products_transaction_header_id','sptd_product_id','sptd_date','sptd_srl','sptd_unit_rate','sptd_unit_gst_rate','sptd_unit_qty','sptd_unit_nos','sptd_unit_qty_gross','sptd_unit_qty_net','sptd_item_unit_id','sptd_unit_sum_amnt','sptd_unit_sgst','sptd_unit_cgst','sptd_unit_gross_amnt','sptd_wh_draft'
    ];

}
