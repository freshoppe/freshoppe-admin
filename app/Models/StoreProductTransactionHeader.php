<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreProductTransactionHeader extends Model
{
    protected $table = 'store_products_transaction_headers';

	protected $dates = ['spth_date'];
	
    protected $fillable = [
        'transaction_number', 'stk_trn_type_id','store_id', 'order_id', 'user_id', 'address_id', 'spth_date', 'spth_inv_no', 'spth_order_no','recorded_by'
    ];

    public function targetStore()
    {
        return $this->belongsTo(Store::class, 'target_store_id');
    }
	
	public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
	
	public function transactionDetails()
    {
        return $this->hasMany(ItemTransactionDetail::class,'items_transaction_header_id', 'id');
    }
}
