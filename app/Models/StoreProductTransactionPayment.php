<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreProductTransactionPayment extends Model
{
    protected $table = 'store_products_transaction_payments';

    protected $fillable = [
		'store_products_transaction_header_id','order_id','sptp_sum_amnt','sptp_sum_sgst','sptp_sum_cgst','sptp_gross_amnt','sptp_ded_amnt','sptp_net_amnt','sptp_net_bank_amnt','sptp_net_cash_amnt','sptp_transaction_id','payment_type_id','payment_status','recorded_by'
    ];

    
	
}
