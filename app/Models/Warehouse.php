<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    /**
     * @var string
     */
    protected $table = 'warehouses';

    /**
     * @var array
     */
    protected $fillable = [
        'warehouse_name', 'warehouse_code', 'location_id', 'pincode', 'status', 'status'
    ];
	
	public function location()
    {
        return $this->hasOne(Location::class,'id', 'location_id');
    }

}
