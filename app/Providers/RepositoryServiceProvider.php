<?php

namespace App\Providers;

use App\Contracts\CategoryContract;
use Illuminate\Support\ServiceProvider;
use App\Repositories\CategoryRepository;
use App\Contracts\AttributeContract;
use App\Repositories\AttributeRepository;
use App\Contracts\BrandContract;
use App\Contracts\ProductContract;
use App\Repositories\ProductRepository;
use App\Contracts\OrderContract;
use App\Repositories\OrderRepository;
use App\Contracts\UserContract;
use App\Repositories\UserRepository;
use App\Contracts\StoreContract;
use App\Repositories\StoreRepository;
use App\Contracts\LocationContract;
use App\Repositories\LocationRepository;
use App\Contracts\WarehouseContract;
use App\Repositories\WarehouseRepository;
use App\Contracts\ProcessingContract;
use App\Repositories\ProcessingRepository;
use App\Contracts\RecipeContract;
use App\Repositories\RecipeRepository;
use App\Contracts\BannerContract;
use App\Repositories\BannerRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    protected $repositories = [
        CategoryContract::class         =>          CategoryRepository::class,
        AttributeContract::class        =>          AttributeRepository::class,
        ProductContract::class          =>          ProductRepository::class,
        OrderContract::class            =>          OrderRepository::class,
        UserContract::class             =>          UserRepository::class,
        StoreContract::class            =>          StoreRepository::class,
        LocationContract::class         =>          LocationRepository::class,
        WarehouseContract::class        =>          WarehouseRepository::class,
        ProcessingContract::class       =>          ProcessingRepository::class,
        RecipeContract::class       	=>          RecipeRepository::class,
        BannerContract::class       	=>          BannerRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->repositories as $interface => $implementation)
        {
            $this->app->bind($interface, $implementation);
        }
    }
}
