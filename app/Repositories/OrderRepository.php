<?php

namespace App\Repositories;

//use Cart;
use App\Models\Order;
use App\Models\User;
use App\Models\Product;
use App\Models\OrderItem;
use App\Models\OrderPayment;
use App\Models\PaymentType;
use App\Models\RawItem;
use App\Models\CustomerAddress;
use App\Models\AccountsDetail;
use App\Models\StoreProductTransactionHeader;
use App\Models\StoreProductTransactionDetail;
use App\Models\StoreProductTransactionPayment;
use App\Contracts\OrderContract;
use Illuminate\Support\Facades\Hash;
use Auth;

class OrderRepository extends BaseRepository implements OrderContract
{
    public function __construct(Order $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function storeOrderDetails($params)
    {
		$items = cart()->items();
		$totalQunatity = 0;
		foreach ($items as $item)
		{
			$totalQunatity += $item['quantity'];
		}
        $order = Order::create([
            'order_number'      =>  'ORD-'.strtoupper(uniqid()),
            'store_id'          =>  1,
            'user_id'           =>  auth()->user()->id,
            'status'            =>  'pending',
            'grand_total'       =>  cart()->payable(),
            'item_count'        =>  $totalQunatity,//Cart::getTotalQuantity(),
            'payment_status'    =>  0,
            'payment_method'    =>  null,
            'notes'             =>  $params['notes'],
			'order_status'      =>  1,
			'order_type'      	=>  1,
            'shipping_address_id' =>  $params['shipping_address_id'],
            'billing_address_id'  =>  $params['billing_address_id'],
        ]);

        if ($order) {

            $items = cart()->items();

            foreach ($items as $item)
            {
                // A better way will be to bring the product id with the cart items
                // you can explore the package documentation to send product id with the cart
                $product = Product::where('id', $item['modelId'])->first();

                $orderItem = new OrderItem([
                    'product_id'    =>  $product->id,
                    'quantity'      =>  $item['quantity'],
                    'price'         =>  $item['price']
                ]);

                $order->items()->save($orderItem);
            }
        }
        return $order;
    }

    public function listOrders(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    public function findOrderByNumber($orderNumber)
    {
        return Order::where('order_number', $orderNumber)->first();
    }
	
	public function updateOrderStatus($params)
    {
		$orderId		=	$params['orderId'];
		$orderStatus	=	$params['orderStatus'];
		
        $order = Order::find($orderId);
		$order->order_status = $orderStatus;
		$order->save();
		
		
		if($orderStatus == 2){
			$header = StoreProductTransactionHeader::create([
				'stk_trn_type_id' => 6,  /*  order process */
				'store_id' => $order->store_id,
				'order_id' => $orderId,
				'user_id' => $order->user_id,
				'address_id' => $order->address_id,
				'spth_date' => $order->created_at,
				'spth_inv_no' => 'INV-'.strtoupper(uniqid()),
				'spth_order_no' => $order->order_number,
				'pth_wh_credit' => 0,
				'recorded_by' => Auth::guard('admin')->user()->id
			]);
			$orderItems = $order->items;
			if(!empty($orderItems)){
				foreach($orderItems as $key=>$val){
					$details = StoreProductTransactionDetail::create([	
						'store_products_transaction_header_id' => $header->id,
						'sptd_product_id' => $val->product_id,
						'sptd_date' => $order->created_at,
						'sptd_srl' => $key+1,
						'sptd_unit_rate' => $val->price,
						'sptd_unit_gst_rate' => $val->gst_rate,
						'sptd_unit_qty' => $val->quantity,
						'sptd_unit_nos' => $val->quantity,
						'sptd_unit_qty_gross' => $val->gross_weight,
						'sptd_unit_qty_net' => $val->net_weight,
						'sptd_item_unit_id' => $val->product->productUnit->id,
						'sptd_unit_sum_amnt' => $val->sub_total,
						'sptd_unit_sgst' => (($val->price/100)*$val->gst_rate)/2,
						'sptd_unit_cgst' => (($val->price/100)*$val->gst_rate)/2,
						'sptd_unit_gross_amnt' => $val->sub_total,
					]);
				}
			}
		}
		
		if($order->order_status == 3){
			$paymentType	=	$params['paymentType'];
			if($paymentType != 5){
				$cash			=	$params['cashReceived'];
				$transactionId	=	$params['transactionId'];
				$header 		= 	StoreProductTransactionHeader::where('order_id',$order->id)->first();
				
				$netBankAmount	=	$order->payments->grand_total - $cash;
				StoreProductTransactionPayment::create([	
					'store_products_transaction_header_id' => $header->id,
					'order_id' => $order->id,
					'sptp_sum_amnt' => $order->payments->sub_total,
					'sptp_sum_sgst' => $order->payments->gst_total/2,
					'sptp_sum_cgst' => $order->payments->gst_total/2,
					'sptp_gross_amnt' => $order->payments->sub_total+$order->payments->gst_total,
					'sptp_ded_amnt' => $order->payments->discount_total,
					'sptp_net_amnt' => $order->payments->grand_total,
					'sptp_net_bank_amnt' => $netBankAmount,
					'sptp_net_cash_amnt' => $cash,
					'sptp_transaction_id' => $transactionId,
					'payment_type_id' => $paymentType,
					'payment_status' => 1,
					'recorded_by' => Auth::guard('admin')->user()->id
				]);
				
				if($paymentType == 1 || $paymentType == 4)
					$wh_credit = 0;
				else
					$wh_credit = 1;
				/*------------------------ACCOUNTS-------------------------------------------------------------*/
				$nextAccTrnSrl = $this->getNextAccTrnSrl(3);

				$accountDetails = AccountsDetail::create([
					'acc_store_id' => $order->store_id,
					'acc_trn_mode_id' => 3,
					'acc_trn_srl' => $nextAccTrnSrl,
					'acc_trn_date' => $order->created_at,
					'dr_acc_head_id' => $order->user_id,
					'cr_acc_head_id' => 3,
					'acc_trn_amnt' => $order->payments->grand_total,
					'acc_trn_dscr' => 'sales no. ' . $nextAccTrnSrl,
					'acc_trn_type_id' =>3,
					'ith_id' => $header->id,
					'recorded_by' => Auth::guard('admin')->user()->id,
					'wh_credit' => $wh_credit
				]);
				
				if($wh_credit == 0)  //IF NOT CREDIT, MEANS IF CASH PAID
				{	
					if($cash > 0){
						$nextAccTrnSrl = $this->getNextAccTrnSrl(2);
						$accountDetails = AccountsDetail::create([
							'acc_store_id' => $order->store_id,
							'acc_trn_mode_id' => 2,
							'acc_trn_srl' => $nextAccTrnSrl,
							'acc_trn_date' => $order->created_at,
							'dr_acc_head_id' => 1,
							'cr_acc_head_id' => $order->user_id,
							'acc_trn_amnt' => $cash,
							'acc_trn_dscr' => 'payment.no. ' . $nextAccTrnSrl,
							'acc_trn_type_id' =>3,
							'ith_id' => $header->id,
							'recorded_by' => Auth::guard('admin')->user()->id,
							'wh_credit' => 0
						]);
					}
					if($netBankAmount > 0){
						$nextAccTrnSrl = $this->getNextAccTrnSrl(2);
						$accountDetails = AccountsDetail::create([
							'acc_store_id' => $order->store_id,
							'acc_trn_mode_id' => 2,
							'acc_trn_srl' => $nextAccTrnSrl,
							'acc_trn_date' => $order->created_at,
							'dr_acc_head_id' => 1,
							'cr_acc_head_id' => $order->user_id,
							'acc_trn_amnt' => $netBankAmount,
							'acc_trn_dscr' => 'payment.no. ' . $nextAccTrnSrl,
							'acc_trn_type_id' =>3,
							'ith_id' => $header->id,
							'recorded_by' => Auth::guard('admin')->user()->id,
							'wh_credit' => 0
						]);
					}
				}
			
			
			
				$payment = OrderPayment::where('order_transaction_headers_id',$order->id)->first();
				$payment->payment_status =1;
				$payment->save();
			}
		}
		
		return $orderStatus;
    }
	
	public function createOrder($params)
    {
		$user = User::where('mobile',$params['mobile_number'])->first();
		if(empty($user)){
			$input['mobile'] 	=  $params['mobile_number'];
			$input['name'] 		=  ($params['customer_name']) ? $params['customer_name'] : 'New User';
			$input['email'] 	=  $params['mobile_number'].'@guest.in';
			$input['password']  =  Hash::make('password');
			$user = User::create($input);
		}
		
		$customerAddress = CustomerAddress::where('user_id',$user->id)->where('address',$params['address'])->first();
		if(empty($customerAddress)){
			$address['user_id'] 	= $user->id;
			$address['address'] 	= $params['address'];
			//$address['city'] 		= $params['city'];
			$address['mobile'] 		= $params['mobile_number'];
			$customerAddress 		= CustomerAddress::create($address);
		}

		$order = Order::create([
            'order_number'      =>  'ORD-'.strtoupper(uniqid()),
            'user_id'           =>  $user->id,
			'store_id'          =>  1,
            'address_id'        =>  $customerAddress->id,
            'item_count'        =>  0,
            'order_status'      =>  1,
            'order_type'        =>  2,
            'recorded_by'       =>  auth()->user()->id,
		]);	

        if ($order) {
            foreach ($params['productIds'] as $key=>$item){
				if($item > 0){
					$product 	= Product::where('id', $item)->first();
					$orderItem 	= new OrderItem([
						'order_transaction_headers_id'  =>  $order->id,
						'product_id'   					=>  $product->id,
						'quantity'      				=>  $params['productQty'][$key],
						'price'         				=>  $params['productPrice'][$key],
						'gst_rate'         				=>  $params['productGst'][$key],
						'sub_total'         			=>  $params['productSubTotal'][$key],
						'gross_weight'         			=>  $params['productGrossWt'][$key],
						'net_weight'         			=>  $params['productNetWt'][$key],
					]);
					$order->items()->save($orderItem);
				}
            }
			/* if($params['payment_type'] == 1)
				$payment_status	= 1;
			else */
				$payment_status	= 0;
			$orderPayment 	= new OrderPayment([
				'order_transaction_headers_id'  =>  $order->id,
				'sub_total'    			=>  $params['subTotal'],
				'gst_rate'    			=>  $params['taxRate'],
				'gst_total'    			=>  $params['taxTotal'],
				'discount_rate'      	=>  $params['discountRate'],
				'discount_total'      	=>  $params['discountTotal'],
				'grand_total'        	=>  $params['grandTotal'],
				'payment_status'        =>  $payment_status
			]);
			$order->payments()->save($orderPayment);
			
			$order->item_count = count($order->items);
			$order->save();
        }
        return $order;
    }
	
	public function findOrderById($id)
    {
        return Order::where('id', $id)->first();
    }
	
	public function getPaymentTypes()
    {
        return PaymentType::where('status', 1)->get();
    }
	
	public function updateOrder($params)
    {
		$order = $this->findOrderById($params['order_id']);
		
		$user = User::where('mobile',$params['mobile_number'])->first();
		if(empty($user)){
			$input['mobile'] 	=  $params['mobile_number'];
			$input['name'] 		=  ($params['customer_name']) ? $params['customer_name'] : 'New User';
			$input['email'] 	=  $params['mobile_number'].'@guest.in';
			$input['password']  =  Hash::make('password');
			$user = User::create($input);
		}
		
		$customerAddress = CustomerAddress::where('user_id',$user->id)->where('address',$params['address'])->first();
		if(empty($customerAddress)){
			$address['user_id'] 	= $user->id;
			$address['address'] 	= $params['address'];
			//$address['city'] 		= $params['city'];
			$address['mobile'] 		= $params['mobile_number'];
			$customerAddress 		= CustomerAddress::create($address);
		}
		
		
        
        $order->user_id 	= $user->id;
        $order->address_id 	= $customerAddress->id;
        //$order->item_count 	= count($params['productIds']);
        $order->recorded_by = auth()->user()->id;
		$order->save();
		
		
        if ($order) {
			$deleteItems = OrderItem::where('order_transaction_headers_id',$order->id)->delete();
            foreach ($params['productIds'] as $key=>$item){
				if($item > 0){
					$orderItem 	= new OrderItem([
						'order_transaction_headers_id'  =>  $order->id,
						'product_id'   					=>  $item,
						'quantity'      				=>  $params['productQty'][$key],
						'price'         				=>  $params['productPrice'][$key],
						'gst_rate'         				=>  $params['productGst'][$key],
						'sub_total'         			=>  $params['productSubTotal'][$key],
						'gross_weight'         			=>  $params['productGrossWt'][$key],
						'net_weight'         			=>  $params['productNetWt'][$key],
					]);
					$order->items()->save($orderItem);
				}
            }
			/* if($params['payment_type'] == 1)
				$payment_status	= 1;
			else */
				$payment_status	= 0;
			
			$deletePayment 	= OrderPayment::where('order_transaction_headers_id',$order->id)->delete();
			$orderPayment 	= new OrderPayment([
				'order_transaction_headers_id'  =>  $order->id,
				'sub_total'    			=>  $params['subTotal'],
				'gst_rate'    			=>  $params['taxRate'],
				'gst_total'    			=>  $params['taxTotal'],
				'discount_rate'      	=>  $params['discountRate'],
				'discount_total'      	=>  $params['discountTotal'],
				'grand_total'        	=>  $params['grandTotal'],
				//'payment_type_id'       =>  $params['payment_type'],
				'payment_status'        =>  $payment_status
			]);
			$order->payments()->save($orderPayment);
			$order->item_count = count($order->items);
			$order->save();
        }
		
        return $order;
    }
	
	public function getNextAccTrnSrl($mode)    
	{		
		$nextAccTrnSrl = AccountsDetail::where('acc_trn_mode_id',$mode)->max('acc_trn_srl');
		return $nextAccTrnSrl + 1;
	}
	
}
