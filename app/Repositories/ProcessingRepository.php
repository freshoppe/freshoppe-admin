<?php

namespace App\Repositories;

use App\Models\ProcessingWay;
use App\Contracts\ProcessingContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class ProcessingRepository
 *
 * @package \App\Repositories
 */
class ProcessingRepository extends BaseRepository implements ProcessingContract
{
    /**
     * ProcessingRepository constructor.
     * @param ProcessingWay $model
     */
    public function __construct(ProcessingWay $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listProcessing(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findProcessingById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return ProcessingWay|mixed
     */
    public function createProcessing(array $params)
    {
        try {
            $collection = collect($params);


            $status = $collection->has('status') ? 1 : 0;

            $merge = $collection->merge(compact('status'));

            $processing = new ProcessingWay($merge->all());

            $processing->save();

            return $processing;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function updateProcessing(array $params)
    {
        $processing = $this->findProcessingById($params['id']);

        $collection = collect($params)->except('_token');

        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('status'));

        $processing->update($merge->all());

        return $processing;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function deleteProcessing($id)
    {
        $processing = $this->findProcessingById($id);

        $processing->delete();

        return $processing;
    }
	
	public function listActiveProcessing(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
	{	
		$processings = ProcessingWay::where('status',1)->orderBy($order ,$sort)->get();
		return $processings;
	}
    
}
