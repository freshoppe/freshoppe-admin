<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\RawItem;
use App\Models\ProductCategory;
use App\Models\Order;
use App\Models\ProductUnit;
use App\Models\ItemTransactionHeader;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use App\Contracts\ProductContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class ProductRepository
 *
 * @package \App\Repositories
 */
class ProductRepository extends BaseRepository implements ProductContract
{
    use UploadAble;

    /**
     * ProductRepository constructor.
     * @param Product $model
     */
    public function __construct(Product $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listProducts(string $order = 'number', string $sort = 'asc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findProductById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function createProduct(array $params)
    {
        try {
			$latest = $this->model::latest()->first();
			
			if(!empty($latest)){
				$params['number'] = (str_pad((int)$latest->number + 1, 6, '0', STR_PAD_LEFT));
            }else{
				$params['number'] = '000001';
			}
			$item = RawItem::find($params['raw_item_id']);
			
			$itemCode = $item->item_code;
			
			$params['sku'] = $itemCode.$params['number'];
			
			$collection = collect($params);
			$ecom_settings = array();
			if ($collection->has('ecom_settings')) {
				$ecom_settings	= $params['ecom_settings'];
			}
			
            $status 		= $collection->has('status') ? 1 : 0;
            $ecom_status 	= $collection->has('ecom_status') ? 1 : 0;
			$new_arrival 	= in_array(1,$ecom_settings) ? 1 : 0;
			$speed_deals 	= in_array(2,$ecom_settings) ? 1 : 0;
			$todays_special = in_array(3,$ecom_settings) ? 1 : 0;

			$source_image = null;

            if ($collection->has('source_image') && ($params['source_image'] instanceof  UploadedFile)) {
                $source_image = $this->uploadOne($params['source_image'], 'sources');
            }
			
            $merge = $collection->merge(compact('status','ecom_status','source_image','new_arrival','speed_deals','todays_special'));

            $product = new Product($merge->all());

            $product->save();

            if ($collection->has('categories')) {
                $product->categories()->sync($params['categories']);
            }
            return $product;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function updateProduct(array $params)
    {
		
        $product = $this->findProductById($params['product_id']);

        $collection = collect($params)->except('_token');

		if ($collection->has('source_image') && ($params['source_image'] instanceof  UploadedFile)) {

            if ($product->source_image != null) {
                $this->deleteOne($product->source_image);
            }

            $source_image = $this->uploadOne($params['source_image'], 'recipes');
        }else{
			if ($product->source_image != null)
				$source_image = $product->source_image;
			else
				$source_image = null;
		}
		$ecom_settings = array();
		if ($collection->has('ecom_settings')) {
			$ecom_settings	= $params['ecom_settings'];
		}
		$new_arrival 	= in_array(1,$ecom_settings) ? 1 : 0;
		$speed_deals 	= in_array(2,$ecom_settings) ? 1 : 0;
		$todays_special = in_array(3,$ecom_settings) ? 1 : 0;
		
        //$featured 		= $collection->has('featured') ? 1 : 0;
        $status 		= $collection->has('status') ? 1 : 0;
        $ecom_status 	= $collection->has('ecom_status') ? 1 : 0;
		$merge = $collection->merge(compact('status','source_image','new_arrival','speed_deals','todays_special','ecom_status'));
        $product->update($merge->all());

        if ($collection->has('categories')) {
            $product->categories()->sync($params['categories']);
        }
        return $product;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function deleteProduct($id)
    {
        $product = $this->findProductById($id);

        $product->delete();

        return $product;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findProductBySlug($slug)
    {
        $product = Product::with('images')->with('categories')->with('attributes')->with('reviews')->where('slug', $slug)->first();
        return $product;
    }
	
	/**
     * @param 
     * @return mixed
     */
	 public function getProducts()
	 {
		$product = Product::with('images')->with('attributes')->where('status',1)->get();
		return $product;
	 }
	 
	 /**
     * @param 
     * @return mixed
     */
	public function getFeaturedProducts($prductId='')
	{
		if($prductId > 0){
			$product = Product::with('images')->with('reviews')
					->where('featured',1)
					->where('status',1)
					->where('id','<>',$prductId)
					->get();
		}else{
			$product = Product::with('images')->with('reviews')
					->with('attributes')
					->where('featured',1)
					->where('status',1)
					->get();
		}
		return $product;
	}
	
	/**
     * @param 
     * @return mixed
     */
	public function getRelatedProducts($categoryIds,$productId)
	{
		$productCat   = ProductCategory::where('product_id','<>',$productId)
					 ->whereIn('category_id',$categoryIds)
					 ->get();
		$productIds	= $productCat->pluck('product_id')->toArray();			 
		
		$products = Product::with('images')->with('reviews')
				->whereIn('id',$productIds)
				->where('status',1)
				->get();

		return $products;
	}
	
	/**
     * @param 
     * @return mixed
     */
	public function getAllProducts()
	{
		$products = Product::with('images')->with('reviews')->where('status',1)->paginate(8);

		return $products;
	}
	
	/**
     * @param 
     * @return mixed
     */
	public function getShopProducts($sort='')
	{
		if($sort == 'name')
			$products = Product::with('images')->with('reviews')->where('status',1)->orderBy('name', 'asc')->paginate(8);
		else if($sort == 'price_low_high')
			$products = Product::with('images')->with('reviews')->where('status',1)->orderByRaw("CASE WHEN sale_price > 0 THEN sale_price ELSE price END ASC")->paginate(8);
		else if($sort == 'price_high_low')
			$products = Product::with('images')->with('reviews')->where('status',1)->orderByRaw("CASE WHEN sale_price > 0 THEN sale_price ELSE price END DESC")->paginate(8);
		else if($sort == 'id')
			$products = Product::with('images')->with('reviews')->where('status',1)->orderBy('id', 'desc')->paginate(8);
		else
			$products = Product::with('images')->with('reviews')->where('status',1)->paginate(8);

		return $products;
	}
	
	/**
     * @param 
     * @return mixed
     */
	public function getAdminProducts()
	{
		$products = Product::select('id','gst_rate','name','price','number','net_weight','gross_weight')->where('status',1)->get()->toArray();
		return $products;
	}
	
	
	/**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listItems(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
		$items = RawItem::where('active',1)->orderBy($order ,$sort)->get();
        return $items;
    }
	
	public function getItemBalance($itemId,$storeId)
	{
		$itemReceived 	= 	ItemTransactionHeader::
							leftJoin('items_transaction_details', function($join) {
							  $join->on('items_transaction_headers.id', '=', 'items_transaction_details.items_transaction_header_id');
							})
							->where('items_transaction_details.itd_item_id',$itemId)
							->where('items_transaction_details.store_id',$storeId)->where('stk_trn_type_id',5)
							->sum('itd_unit_qty');
		
		$productIds		=	Product::where('raw_item_id' ,'=' ,$itemId)->pluck('id')->toArray();
		
		$itemSold 		= 	Order::
							leftJoin('order_transaction_details', function($join) {
							  $join->on('order_transaction_headers.id', '=', 'order_transaction_details.order_transaction_headers_id');
							})
							->join('products', function($join) {
							  $join->on('order_transaction_details.product_id', '=', 'products.id');
							})
							->whereIn('products.id',$productIds)
							->where('order_transaction_headers.store_id',$storeId)
							->sum('order_transaction_details.gross_weight');
		$balance = ($itemReceived - $itemSold);
		
		return $balance;
	}
	
	/**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function productUnits(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
		$units = ProductUnit::orderBy($order ,$sort)->get();
        return $units;
    }
	
	/**
     * @param 
     * @return mixed
     */
	public function getItemProcessing($id)
	{
		$item 		= RawItem::find($id);
		$processing = $item->processings;
		return $processing;
	}
}
