<?php

namespace App\Repositories;

use App\Models\Recipe;
use App\Contracts\RecipeContract;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
/**
 * Class RecipeRepository
 *
 * @package \App\Repositories
 */
class RecipeRepository extends BaseRepository implements RecipeContract
{
	use UploadAble;
	
    /**
     * RecipeRepository constructor.
     * @param Recipe $model
     */
    public function __construct(Recipe $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listRecipes(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findRecipeById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Recipe|mixed
     */
    public function createRecipe(array $params)
    {
        try {
            $collection = collect($params);
			
			$image = null;

            if ($collection->has('image') && ($params['image'] instanceof  UploadedFile)) {
                $image = $this->uploadOne($params['image'], 'recipes');
            }

			$merge = $collection->merge(compact('image'));
			
			$recipe = new Recipe($merge->all());

            $recipe->save();

            return $recipe;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function updateRecipe(array $params)
    {
        $recipe = $this->findRecipeById($params['id']);

        $collection = collect($params)->except('_token');

		if ($collection->has('image') && ($params['image'] instanceof  UploadedFile)) {

            if ($recipe->image != null) {
                $this->deleteOne($recipe->image);
            }

            $image = $this->uploadOne($params['image'], 'recipes');
        }else{
			if ($recipe->image != null)
				$image = $recipe->image;
			else
				$image = null;
		}

		$merge = $collection->merge(compact('image'));
		
        $recipe->update($merge->all());

        return $recipe;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function deleteRecipe($id)
    {
        $recipe = $this->findRecipeById($id);

        $recipe->delete();

        return $recipe;
    }
	
	
    
}
