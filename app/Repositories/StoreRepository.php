<?php

namespace App\Repositories;

use App\Models\Store;
use App\Contracts\StoreContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class StoreRepository
 *
 * @package \App\Repositories
 */
class StoreRepository extends BaseRepository implements StoreContract
{
    /**
     * StoreRepository constructor.
     * @param Store $model
     */
    public function __construct(Store $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listStores(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
		//$stores = $this->model::where('is_wharehouse_store',0)->orderBy($order ,$sort)->get();
        //return $stores;
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findStoreById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Store|mixed
     */
    public function createStore(array $params)
    {
        try {
            $collection = collect($params);


            $status = $collection->has('status') ? 1 : 0;

            $merge = $collection->merge(compact('status'));

            $store = new Store($merge->all());

            $store->save();

            return $store;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function updateStore(array $params)
    {
        $store = $this->findStoreById($params['id']);

        $collection = collect($params)->except('_token');

        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('status'));

        $store->update($merge->all());

        return $store;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function deleteStore($id)
    {
        $store = $this->findStoreById($id);

        $store->delete();

        return $store;
    }
	
	public function getStoresByWarehouse($id)
	{
		$stores = Store::where('warehouse_id',$id)->where('status',1)->get();
		return $stores;
	}
    
	public function getWarehouseStore($id)
	{
		$whStore = Store::where('warehouse_id',$id)->where('status',1)->where('is_wharehouse_store',1)->first();
		return $whStore;
	
	}
	
}
