<?php

namespace App\Repositories;

use App\Models\Warehouse;
use App\Models\Store;
use App\Contracts\WarehouseContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class WarehouseRepository
 *
 * @package \App\Repositories
 */
class WarehouseRepository extends BaseRepository implements WarehouseContract
{
    /**
     * WarehouseRepository constructor.
     * @param Warehouse $model
     */
    public function __construct(Warehouse $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listWarehouses(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findWarehouseById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Warehouse|mixed
     */
    public function createWarehouse(array $params)
    {
        try {
            $collection = collect($params);


            $status = $collection->has('status') ? 1 : 0;

            $merge = $collection->merge(compact('status'));

            $warehouse = new Warehouse($merge->all());

            $warehouse->save();

			$whStore = Store::create([
				'store_name' => 'warehouse store',
				'store_code' => 'STORE - '.$warehouse->id,
				'location_id' => $warehouse->location_id,
				'warehouse_id' => $warehouse->id,
				'pincode' => $warehouse->pincode,
				'address' => 'address',
				'store_type' => '3',
				'is_wharehouse_store' => '1',
				'status' => '1',
			]);

            return $warehouse;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function updateWarehouse(array $params)
    {
        $warehouse = $this->findWarehouseById($params['id']);

        $collection = collect($params)->except('_token');

        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('status'));

        $warehouse->update($merge->all());

        return $warehouse;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function deleteWarehouse($id)
    {
        $warehouse = $this->findWarehouseById($id);

        $warehouse->delete();

        return $warehouse;
    }
	
	public function getActiveWarehouses()
	{
		$warehouses = Warehouse::where('status',1)->get();
		return $warehouses;
	}
	
	public function getWarehousesByLocation($id)
	{
		$warehouses = Warehouse::where('location_id',$id)->where('status',1)->get();
		return $warehouses;
	}
	
    
}
