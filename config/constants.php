<?php

return [
    'orderStatus' => [
        '1' => 'New',
		'2' => 'Processing',
		'3' => 'Dispatched',
		'4' => 'On Delivery',
		'5' => 'Completed',
		'6' => 'Declined',
		'7' => 'Cancelled'
    ],
	
	
	'staffType' => [
        '1' => 'Warehouse',
		'2' => 'Store'
    ],
	
	'ecomSettings' => [
        '1' => 'New Arrivals',
		'2' => 'Speed Deals',
		'3' => 'Todays Specials'
    ]
];