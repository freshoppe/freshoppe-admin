<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raw_items', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('item_type_id');
            $table->foreign('item_type_id')->references('id')->on('item_types');
            $table->string('item_code');
            $table->string('item_name');
			$table->unsignedBigInteger('item_unit_id');
            $table->foreign('item_unit_id')->references('id')->on('item_units');
            $table->decimal('item_price', $precision = 15, $scale = 2);
            $table->decimal('item_gst', $precision = 15, $scale = 2);
            $table->decimal('waste_percentage', $precision = 15, $scale = 2);
			$table->unsignedBigInteger('recorded_by');
            $table->foreign('recorded_by')->references('id')->on('staffs');
			$table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_items');
    }
}
