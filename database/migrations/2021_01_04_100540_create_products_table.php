<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('raw_item_id');
            $table->foreign('raw_item_id')->references('id')->on('raw_items');
			$table->unsignedBigInteger('processing_id')->nullable();
            $table->foreign('processing_id')->references('id')->on('processing_ways');
			$table->string('sku')->nullable();
            $table->string('number')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('pieces')->nullable();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('quantity')->nullable();
            $table->decimal('gross_weight', 8, 2)->nullable();
            $table->decimal('net_weight', 8, 2)->nullable();
            $table->decimal('price', 8, 2)->nullable();
            $table->decimal('sale_price', 8, 2)->nullable();
            $table->decimal('gst_rate', 8, 2)->nullable();
			$table->decimal('size_min', 8, 2)->nullable();
			$table->decimal('size_max', 8, 2)->nullable();
            $table->boolean('status')->default(1);
            $table->boolean('featured')->default(0);
			$table->text('whats_in_box')->nullable();
			$table->string('serves')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
