<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_transaction_details', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('items_transaction_header_id');
            $table->foreign('items_transaction_header_id')->references('id')->on('items_transaction_headers');
			$table->unsignedBigInteger('item_id');
            $table->foreign('item_id')->references('id')->on('raw_items');
			$table->decimal('weight', 8, 2)->nullable();
            $table->decimal('price', 8, 2)->nullable();
            $table->decimal('gst', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_transaction_details');
    }
}
