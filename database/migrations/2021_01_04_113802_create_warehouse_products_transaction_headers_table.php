<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarehouseProductsTransactionHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_products_transaction_headers', function (Blueprint $table) {
            $table->id();
			$table->string('transaction_number')->unique();
			$table->unsignedBigInteger('transaction_type_id');
            $table->foreign('transaction_type_id','fk_warehouse_products_transaction_transaction_type_id')->references('id')->on('transaction_types');
			$table->unsignedInteger('item_count');
			$table->text('notes')->nullable();
			$table->unsignedBigInteger('recorded_by');
            $table->foreign('recorded_by')->references('id')->on('staffs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_transaction_headers');
    }
}
