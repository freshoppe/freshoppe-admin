<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarehouseProductsTransactionPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_products_transaction_payments', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('warehouse_products_transaction_header_id');
            $table->foreign('warehouse_products_transaction_header_id','fk_warehouse_products_transaction_header_payment_id')->references('id')->on('warehouse_products_transaction_headers');
			$table->decimal('gst_total', 20, 6);
			$table->decimal('discount_total', 20, 6);
			$table->decimal('grand_total', 20, 6);
			$table->unsignedBigInteger('payment_type_id');
            $table->foreign('payment_type_id','fk_warehouse_products_payment_type_id')->references('id')->on('payment_types');
			$table->boolean('payment_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_transaction_payments');
    }
}
