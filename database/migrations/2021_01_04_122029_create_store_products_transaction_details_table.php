<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreProductsTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_products_transaction_details', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('store_products_transaction_header_id');
			$table->foreign('store_products_transaction_header_id','fk_store_products_transaction_header_details_id')->references('id')->on('store_products_transaction_headers');
			$table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');
			$table->decimal('weight', 8, 2)->nullable();
            $table->decimal('price', 8, 2)->nullable();
            $table->decimal('gst', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_products_transaction_details');
    }
}
