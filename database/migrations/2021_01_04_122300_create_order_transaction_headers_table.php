<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTransactionHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_transaction_headers', function (Blueprint $table) {
            $table->id();
			$table->string('order_number')->unique();
            $table->unsignedBigInteger('store_id');
            $table->foreign('store_id')->references('id')->on('stores');
			$table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
			$table->unsignedBigInteger('address_id')->nullable();
            $table->foreign('address_id')->references('id')->on('customer_addresses');
            $table->unsignedInteger('item_count');
            $table->boolean('order_status')->default(1);
			$table->boolean('order_type')->default(1);
            $table->text('notes')->nullable();
			$table->unsignedBigInteger('recorded_by')->nullable();
            $table->foreign('recorded_by')->references('id')->on('staffs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_transaction_headers');
    }
}
