<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_transaction_details', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('order_transaction_headers_id')->index();
            $table->foreign('order_transaction_headers_id')->references('id')->on('order_transaction_headers')->onDelete('cascade');
            $table->unsignedBigInteger('product_id')->index();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->decimal('gross_weight', 8, 3)->nullable();
            $table->decimal('net_weight', 8, 3)->nullable();
            $table->decimal('gst_rate', 8, 2)->nullable();
            $table->decimal('price', 20, 6);
            $table->unsignedInteger('quantity');
            $table->decimal('sub_total', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_transaction_details');
    }
}
