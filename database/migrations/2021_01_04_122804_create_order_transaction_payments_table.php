<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTransactionPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_transaction_payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_transaction_headers_id');
            $table->foreign('order_transaction_headers_id','fk_order_transaction_header_payment_id')->references('id')->on('order_transaction_headers');
			$table->decimal('sub_total', 20, 6);
			$table->decimal('gst_rate', 8, 2);
			$table->decimal('gst_total', 20, 6);
			$table->decimal('discount_rate', 8, 2);
			$table->decimal('discount_total', 20, 6);
			$table->decimal('grand_total', 20, 6);
			$table->unsignedBigInteger('payment_type_id');
            $table->foreign('payment_type_id','fk_order_payment_type_id')->references('id')->on('payment_types');
			$table->boolean('payment_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_transaction_payments');
    }
}
