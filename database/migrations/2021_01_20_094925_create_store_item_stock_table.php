<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreItemStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_item_stock', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('store_id');
            $table->foreign('store_id')->references('id')->on('stores');
			$table->unsignedBigInteger('item_id');
			$table->foreign('item_id')->references('id')->on('raw_items');
			$table->decimal('item_balance_gross_wt',8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_item_stock');
    }
}
