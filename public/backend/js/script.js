// some scripts

// jquery ready start
$(document).ready(function() {
	
	/// login form Validation ///
	$("#loginForm").validate({
		errorElement: 'span', // default input error message container
		errorClass: 'border-danger text-danger', // default input error message class
		rules: {
			email:{
				required: true,
				email: true
			},
			password:{
				required: true,
			}
		},
	});
	/// login form Validation ///
	
	/// location form Validation ///
	$("#locationForm").validate({
		errorElement: 'span', // default input error message container
		errorClass: 'border-danger text-danger', // default input error message class
		rules: {
			location_name:{
				required: true
			},
			city:{
				required: true
			},
			state:{
				required: true
			}
		}
	});
	/// /Create Product Form Validation ///
	
	/// Create Product Form Validation ///
	$("#offline_order_form").validate({
		errorElement: 'span', // default input error message container
		errorClass: 'border-danger text-danger', // default input error message class
		rules: {
			customer_name:{
				//required: true,
			},
			mobile_number:{
				required: true,
				digits: true,
				minlength:10,
				maxlength:10,
			},
			/* pincode:{
				digits: true,
				minlength:6,
				maxlength:6,
			}, */
			payment_method:{
				required: true,
			},
			'productIds[]': {
			  required: true
			},
			'productPrice[]': {
			  required: true
			},
			'productQty[]': {
			  required: true
			}
		},
	});
	/// /Create Product Form Validation ///
	
	/// invoice form Validation ///
	$("#invoice_form").validate({
		errorElement: 'span', // default input error message container
		errorClass: 'border-danger text-danger', // default input error message class
		rules: {
			trans_id:{
				required: true,
			},
			cash:{
				required: true,
			}
		},
	});
	/// invoice form Validation ///
	
	/// banner form Validation ///
	$("#addBannerForm").validate({
		errorElement: 'span', // default input error message container
		errorClass: 'border-danger text-danger', // default input error message class
		ignore: ".ignore",
		rules: {
			name:{
				required: true,
			},
			link:{
				required: true,
				url:true,
			},
			image:{
				required: true,
			}
		},
	});
	/// banner form Validation ///
	
});
// jquery end

