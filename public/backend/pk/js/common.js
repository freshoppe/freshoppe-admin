

	APP_URL ='';
	APP_URL = "{{ url('/') }}";		
	
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	pk_today = now.getFullYear()+"-"+(month)+"-"+(day) ;	
	

	function PKDateDiff(start, end)
	{ 
		var diff = Date.parse(end) - Date.parse(start);
		
		days = (Date.parse(end)- Date.parse(start)) / (1000 * 60 * 60 * 24);
				
		diff_days = Math.round(days);
		
		return diff_days;
	}
	
	
	function PKDateDdMmYyyy(dte)
		{		
			
			var now = new Date(dte);
		 
			var day = ("0" + now.getDate()).slice(-2);
			
			var month = ("0" + (now.getMonth() + 1)).slice(-2);

			var today = (day)+"-"+(month)+"-"+ now.getFullYear();

			return today;
		  
		}		

