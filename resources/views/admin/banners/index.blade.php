@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
        
		<button type="button" class="btn btn-primary pull-right" data-bs-toggle="modal" data-bs-target="#bannerAddModal">
		  Add Banner
		</button>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="bannerTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-center"> Name </th>
                            <th class="text-center"> Link </th>
                            <th class="text-center"> Image </th>
                            <th class="text-center"> Status </th>
							<th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                        </tr>
                        </thead>
                        <tbody>
							@foreach($banners as $key=>$banner)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$banner->name}}</td>
								<td>{{$banner->link}}</td>
								<td class="text-center">
									@if($banner->image)
										<img style="width:80px" src="{{ asset('/uploads/'.$banner->image) }}" alt="image">
									@else
										<img src="https://via.placeholder.com/80" alt="image">
									@endif
								</td>
								<td class="text-center">
									@if ($banner->status == 1)
										<span class="badge badge-success" onclick="changeStatus(0)">Yes</span>
									@else
										<span class="badge badge-danger" onclick="changeStatus(1)">No</span>
									@endif
								</td>
								<td class="text-center">
									<div class="btn-group" role="group" aria-label="Second group">
										<a href="javascript:void(0)" class="btn btn-sm btn-primary" onclick="showEditModal({{$banner->id}})"><i class="fa fa-edit"></i></a>
										<a href="javascript:void(0)" onclick="deleteBanner({{$banner->id}})" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
									</div>
								</td>
							</tr>
							@endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
	<!-- Modal -->
	<div class="modal fade" id="bannerAddModal" tabindex="-1" aria-labelledby="bannerAddModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		<form method="post" id="addBannerForm" enctype="multipart/form-data">
			@csrf
			<input type="hidden" name="id" id="bannerId">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add Banner</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
			
				<div class="form-group">
					<label for="name">Name <span class="m-l-5 text-danger"> *</span></label>
					<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" />
					@error('name') {{ $message }} @enderror
				</div>
				<div class="form-group">
					<label for="link">Link <span class="m-l-5 text-danger"> *</span></label>
					<input type="text" class="form-control @error('link') is-invalid @enderror" name="link" id="link" />
					<div class="invalid-feedback active">
						<i class="fa fa-exclamation-circle fa-fw"></i> @error('link') <span>{{ $message }}</span> @enderror
					</div>
				</div>
				<div class="form-group">
					<label class="control-label" for="image">Image <span class="m-l-5 text-danger"> *</span></label>
					<input type="file" class="form-control @error('image') is-invalid @enderror" name="image" id="image">
					<div class="invalid-feedback active">
						<i class="fa fa-exclamation-circle fa-fw"></i> @error('image') <span>{{ $message }}</span> @enderror
					</div>
					<figure class="mt-2" style="width: 80px; height: auto;">
						<img src="https://via.placeholder.com/80" id="bannerImage" class="img-fluid" alt="img">
					</figure>
				</div>
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Banner</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>

@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#bannerTable').DataTable();</script>
	<script>
	function changeStatus(bannerId){
		$.ajax({
            type: "POST",
            url: '{{route('admin.banners.update')}}',
            data:{
					"_token": "{{ csrf_token() }}",
					"customerid": customerid
				},
            success : function(text){
                window.location.reload();
            }
        });
	}
	
	function deleteBanner(bannerId){
		confirm("Are You sure want to delete !");
		APP_URL = "{{ url('/') }}";
        $.ajax({
            type: "GET",
            url: APP_URL+"/admin/banners/"+ bannerId +"/delete",
            success: function (data) {
                window.location.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
	}
	
	$("#image").change(function() {
	  readURL(this);
	});
	
	function readURL(input) {
	  if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function(e) {
		  $('#bannerImage').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]); // convert to base64 string
	  }
	}
	function showEditModal(bannerId){
		APP_URL = "{{ url('/') }}";
		$.getJSON(APP_URL+"/admin/banners/"+ bannerId +"/edit", function(data){
			$('#exampleModalLabel').html("Edit Banner");
			$('#bannerAddModal').modal('show');
			$('#bannerId').val(data.id);
			$('#name').val(data.name);
			$('#link').val(data.link);
			$('#bannerImage').attr('src', APP_URL+'/uploads/'+data.image);
			$('#image').addClass('ignore');
			
		});
	}
	$(document).ready(function(){
		$('#bannerAddModal').on('show.bs.modal', function (e) {
			$('#bannerId').val('');
			$('#name').val('');
			$('#link').val('');
			$('#image').val('');
			$('#bannerImage').attr('src', '');
		})
		$('#addBannerForm').on('submit', function(event){
			var validator = $("#addBannerForm").validate();
			form_res = validator.form();
			var bannerId = $('#bannerId').val();
			APP_URL = "{{ url('/') }}";
			if(bannerId > 0)
				url = APP_URL+"/admin/banners/update/";
			else
				url = APP_URL+"/admin/banners/store/";
			if(form_res == true){
				event.preventDefault();
				$.ajax({
					type: "POST",
					url: url,
					data: new FormData(this),
					processData: false,
					contentType: false,
					success: (response) => {
						$.notify({
							message: 'Banner has been updated',
							icon: 'flaticon-bar-chart'
						},{
							type: 'success',
							allow_dismiss: true,
							placement: {
								from: "top",
								align: "right"
							},
						});
						
						window.location.reload();
					}
				});
			}
		});
	});
	
	
	</script>
@endpush
