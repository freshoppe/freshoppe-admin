@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-bar-chart"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs order-tabs">
				<li class="nav-item"><a class="nav-link active" href="#billing-order" data-toggle="tab">Billing</a></li>
			</ul>
        </div>
		<div class="col-md-12">
            <div class="tab-content">
				<div class="tab-pane active" id="billing-order">
                    @include('admin.billing.includes.billing')
                </div>
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
	$('#billingOrderTable').DataTable();
	</script>
@endpush
