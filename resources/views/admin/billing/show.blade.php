@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-bar-chart"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <section class="invoice">
					<form id="invoice_form" name="invoice_form" method="POST" role="form" >
						<input type="hidden" id="order_id" value="{{$order->id}}" >
						<div class="row mb-4">
							<div class="col-6">
								<h2 class="page-header"><i class="fa fa-globe"></i> {{ $order->order_number }}</h2>
							</div>
							<div class="col-6">
								<h5 class="text-right">Date: {{ $order->created_at->toDateTimeString() }}</h5>
							</div>
						</div>
						<div class="row invoice-info">
							<div class="col-4">Placed By
								<address><strong>{{ $order->user->name }}</strong><br>Email: {{ $order->user->email }}</address>
							</div>
							<div class="col-4">Ship To
								<address>
									<strong>{{ $order->user->name }}</strong><br>
									@if($order->shipAddress->address != NULL)
										{{ $order->shipAddress->address }}<br>
									@endif
									@if($order->shipAddress->flat != NULL)
										{{ $order->shipAddress->flat }}<br>
									@endif
									@if($order->shipAddress->city != NULL)
										{{ $order->shipAddress->city }}<br>
									@endif
									@if($order->shipAddress->mobile != NULL)
										{{ $order->shipAddress->mobile }}<br>
									@endif
								</address>
							</div>
							<div class="col-4">
								<b>Order ID:</b> {{ $order->order_number }}<br>
								@php $orderStatus = Config::get('constants.orderStatus'); @endphp
								<b>Order Status:</b> {{$orderStatus[$order->order_status]}}  <br>
								<b>Order Type:</b> @if($order->order_type == 1 ) Online @else Offline @endif<br>
							</div>
						</div>
						<div class="row">
							<div class="col-12 table-responsive">
								<table class="table table-striped">
									<thead>
									<tr>
										<th>No</th>
										<th>Product</th>
										<th>Number</th>
										<th class="text-center">GST(%)</th>
										<th>Nt Wt</th>
										<th>Gr Wt</th>
										<th class="text-right">Price</th>
										<th class="text-center">Qty</th>
										<th class="text-right">Amount</th>
									</tr>
									</thead>
									<tbody>
										@foreach($order->items as $key=>$item)
											<tr>
												<td>{{ $key + 1 }}</td>
												<td>{{ $item->product->name }}
												@if($item->product->processing !='')
												- {{ $item->product->processing->processing_name }}
												@endif
												</td>
												<td>{{ $item->product->number }}</td>
												<td class="text-center">{{ $item->gst_rate }}</td>
												<td>{{ $item->net_weight }}</td>
												<td>{{ $item->gross_weight }}</td>
												<td class="text-right">{{ config('settings.currency_symbol') }}{{ $item->price }}</td>
												<td class="text-center">{{ $item->quantity }}</td>
												<td class="text-right">{{ config('settings.currency_symbol') }}{{ $item->sub_total }}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
						<div class="row invoice-info">
							<div class="col-4">
								<b>Sub Total:</b> {{ config('settings.currency_symbol') }}{{ $order->payments->sub_total }}<br>
								<b>Tax (%):</b> {{ $order->payments->gst_rate }}<br>
								<b>Tax Amount:</b> {{ config('settings.currency_symbol') }}{{ $order->payments->gst_total }}<br>
							</div>
							<div class="col-4">
								<b>Discount (%):</b>{{ $order->payments->discount_rate }}<br>
								<b>Discount Amount:</b> {{ config('settings.currency_symbol') }}{{ $order->payments->discount_total }}<br>
								
							</div>
							<div class="col-4">
								<b>Total Amount:</b> {{ config('settings.currency_symbol') }}{{ $order->payments->grand_total }}<br><br>
								<input type="hidden" value="{{$order->payments->grand_total}}" id="grand_total">
								@if($order->payments->payment_type_id != NULL)
								<b>Payment Method:</b> {{ strtoupper($order->payments->paymentType->payment_type) }}<br>
								@endif
								<b>Payment Status:</b> {{ $order->payments->payment_status == 1 ? 'Completed' : 'Not Completed' }}<br>
							</div>
						</div>
						@can('invoicing')
						@if($order->payments->payment_status == 0 && $order->order_status ==2 )
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label class="control-label" for="payment_type">Payment Method</label>
									<select name="payment_type" id="payment_type" class="form-control">
										@foreach($paymentTypes as $payment)
											<option value="{{ $payment->id }}">{{ strtoupper($payment->payment_type) }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" id="cash_div">
									<label class="control-label" for="cash">CASH <span class="m-l-5 text-danger"> *</span></label>
									<input class="form-control @error('cash') is-invalid @enderror" type="text" name="cash" id="cash" value=""/>
									@error('cash') {{ $message }} @enderror
								</div>
								<div class="form-group" id="trans_div" style="display:none">
									<label class="control-label" for="trans_id">Transaction ID <span class="m-l-5 text-danger"> *</span></label>
									<input class="form-control @error('trans_id') is-invalid @enderror" type="text" name="trans_id" id="trans_id" value=""/>
									@error('trans_id') {{ $message }} @enderror
								</div>
							</div>
						</div>
						@endif
						@endcan
						<div class="tile-footer">
							@can('invoicing')
							@if($order->payments->payment_status == 0 && $order->order_status ==2 )
							<a href="javascript:void(0)" onclick="return updateStatus({{$order->id}},3)" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-check-circle"></i>Invoice</a>
							@endif
							@endcan
							<a class="btn btn-primary" href="javascript:history.back()"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Back</a>
						</div>
					</form>
                </section>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
	$('#payment_type').change(function(){
		var paymentType = $(this).val();
		if(paymentType == 1){
			$('#trans_div').hide();
			$('#cash_div').show();
		}else if(paymentType == 2){
			$('#cash_div').hide();
			$('#trans_div').show();
		}else if(paymentType == 3){
			$('#cash_div').hide();
			$('#trans_div').show();
		}else if(paymentType == 5){
			$('#cash_div').hide();
			$('#trans_div').hide();
		}else{
			$('#cash_div').show();
			$('#trans_div').show();
		}
			
	});
	function updateStatus(orderId,orderStatus){
		var validator = $("#invoice_form").validate();
		form_res = validator.form();
		
		var paymentEqual	 = true;
		var paymentType 	= $('#payment_type').val();
		var cashReceived 	= $('#cash').val();
		var grandTotal 		= $('#grand_total').val();
		var transactionId 	= $('#trans_id').val();
		
		if(paymentType == 1){
			if(cashReceived == grandTotal)
				paymentEqual = true;
			else
				paymentEqual = false;
		}
		if(form_res == true && paymentEqual == true){
			
			msg = 'Are you sure to invoice this order?';
			bootbox.confirm({
				message: msg,
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success'
					},
					cancel: {
						label: 'No',
						className: 'btn-danger'
					}
				},
				callback: function (result) {
					if(result == true){
						$.ajax({
							type: "POST",
							url: '{{route('admin.billing.update.status')}}',
							data:{
									"_token": "{{ csrf_token() }}",
									"orderId": orderId,
									"orderStatus": orderStatus,
									"cashReceived": cashReceived,
									"transactionId": transactionId,
									"paymentType": paymentType,
									
								},
							success: (response) => {
								$.notify({
									message: response.message,
									icon: 'flaticon-bar-chart'
								},{
									type: 'success',
									allow_dismiss: true,
									placement: {
										from: "top",
										align: "right"
									},
								});
								
								window.location.reload();
							},
							error: (response) => {
								$.notify("Something went wrong!");
							}
						});
					}
				}
			});
		}else if( paymentEqual == false ){
			alert('payments not matching');
		}
	}
	
	</script>
@endpush