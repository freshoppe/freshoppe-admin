<div class="tile">
    <form action="{{ route('admin.general.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">About Us</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="footer_copyright_text">About Us Text</label>
                <textarea
                    class="form-control summernote"
                    rows="4"
                    placeholder="Enter about us text"
                    id="about_us_text"
                    name="about_us_text"
                >
					@if(isset($general))
					{{$general->aboutus}}
					@endif</textarea>
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>About Us</button>
                </div>
            </div>
        </div>
    </form>
</div>
