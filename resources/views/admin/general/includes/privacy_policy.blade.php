<div class="tile">
    <form action="{{ route('admin.general.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">Privacy Policy</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="footer_copyright_text">Privacy Policy Text</label>
                <textarea
                    class="form-control summernote"
                    rows="4"
                    placeholder="Enter privacy policy text"
                    id="privacy_policy_text"
                    name="privacy_policy_text"
                >
					@if(isset($general))
					{{$general->privacy_policy}}
					@endif</textarea>
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Privacy Policy</button>
                </div>
            </div>
        </div>
    </form>
</div>
