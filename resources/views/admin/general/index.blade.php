@extends('admin.app')

@section('title') {{ $pageTitle }} @endsection

@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-pencil"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    <div class="row user">
        <div class="col-md-3">
            <div class="tile p-0">
                <ul class="nav flex-column nav-tabs user-tabs">
                    <li class="nav-item"><a class="nav-link active" href="#terms" data-toggle="tab">Terms and Conditions</a></li>
                    <li class="nav-item"><a class="nav-link" href="#privacy-policy" data-toggle="tab">Privacy Policy</a></li>
                    <li class="nav-item"><a class="nav-link" href="#about-us" data-toggle="tab">About Us</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="terms">
                    @include('admin.general.includes.terms_and_conditions')
                </div>
                <div class="tab-pane fade" id="privacy-policy">
                    @include('admin.general.includes.privacy_policy')
                </div>
                <div class="tab-pane fade" id="about-us">
                    @include('admin.general.includes.aboutus')
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
        $( document ).ready(function() {
			$('.summernote').summernote({
               height: 300,
			   popover: {
				 image: [],
				 link: [],
				 air: []
			   },
			   toolbar: [
							[ 'style', [ 'style' ] ],
							[ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
							[ 'fontname', [ 'fontname' ] ],
							[ 'fontsize', [ 'fontsize' ] ],
							[ 'color', [ 'color' ] ],
							[ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
							[ 'table', [ 'table' ] ],
							[ 'insert', [ 'link'] ],
							[ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
						]
          });
        });
    </script>
@endpush