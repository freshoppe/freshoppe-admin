@extends('admin.inventory.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
@php use Carbon\Carbon; @endphp
<style>
.sum-title
{
	font-weight:bold;
	color: #3a9629;
	
}
</style>
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
        <a href="{{ route('admin.inventory.acc-books.index') }}" class="btn btn-primary pull-right">SELECT ACCOUNT HEAD</a>
    </div>
    @include('admin.inventory.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered pk-table-last-page-first" id="sampleTable">
                        <thead>
                            <tr>	
								<th style="display:none">id</th>
								<th>DATE.</th>
								<th>HEAD</th>
								<th>TRNS.MODE</th>
								<th>DR.</th>
								<th>CR.</th>
								<th>BALANCE</th>
                            </tr>
                        </thead> 
                        <tbody>
							@php  $bal_amt = 0; $cr_amt = 0; $dr_amt = 0;  @endphp
							@foreach($accounts_details as $key=>$accounts_detail) 
								
                                    <tr>
                                        <td style="display:none">{{ $accounts_detail->id }}</td>
                                        <td>{{ Carbon::parse($accounts_detail->acc_trn_date)->format('d-m-Y') }}</td>
										@if ( $accounts_detail->dr_acc_head_id == $head_id) 
											<td>{{ $accounts_detail->cr_head_name }}</td>
											<td>{{ $accounts_detail->transaction_mode }}</td>
											<td>{{$accounts_detail->acc_trn_amnt}}</td>
											<td></td>
											@php  $bal_amt -= $accounts_detail->acc_trn_amnt  @endphp
											@php  $cr_amt += $accounts_detail->acc_trn_amnt  @endphp
											
										@elseif ( $accounts_detail->cr_acc_head_id == $head_id) 
											<td>{{ $accounts_detail->dr_head_name }}</td>
											<td>{{ $accounts_detail->transaction_mode }}</td>
											<td></td>
											<td>{{$accounts_detail->acc_trn_amnt}}</td>
											@php  $bal_amt += $accounts_detail->acc_trn_amnt  @endphp
											@php  $cr_amt += $accounts_detail->acc_trn_amnt  @endphp
											
										@endif
										
                                        <td>{{$bal_amt  }}</td>
                                    </tr>
									
                            @endforeach
							
							
							
                        </tbody>
                    </table>
					
                </div>
				
					<div class="row pt-3">
						<div class="col-md-3">
							<p class="sum-title"> Total In: {{$cr_amt }} </p>
						</div>
						<div class="col-md-3">
							<p class="sum-title"> Total Out: {{$dr_amt }} </p>
						</div>
						<div class="col-md-3">
							<p class="sum-title"> Balance: {{$bal_amt }}</p>
						</div>
					</div>	
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#sampleTable').DataTable();
		item_id = 0;
		
		$(document).on('click', '.a-delete', function(event)
		{
			item_id = $(this).attr('value');
 

				if(confirm('Are you sure to Delete this Purchase Draft ?'))
				{
					delete_draft();
					location.reload();
				}
			
		});
		

	});
	
	
	
	
	function delete_draft()
	{
		var CSRF_TOKEN = '{{csrf_token()}}';
		
		APP_URL = "{{ url('/') }}";		
		
		var	url_var = APP_URL + '/admin/inventory/purchases/delete_draft';
		
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['id'] = item_id;
						
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: data,
		   async:false,
		   success:function(result_data)
			   {
					alert('Deleted.');
				}
			});
			
	}
	
	
</script>
@endpush
