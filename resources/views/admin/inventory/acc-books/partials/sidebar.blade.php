<div class="app-sidebar__overlay" data-toggle="sidebar">

</div>
<aside class="app-sidebar">
	<ul class="app-menu">
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.dashboard' ? 'active' : '' }}" href="{{ route('admin.inventory.dashboard') }}">                
				<i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span>            
			</a>        
		</li>
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.dashboard' ? 'active' : '' }}" href="{{ route('admin.dashboard') }}">                
				<i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Ecommerce</span>            
			</a>        
		</li>
        <li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.dashboard' ? 'active' : '' }}" href="{{ route('admin.inventory.dashboard') }}">
                <i class="app-menu__icon fa fa-dashboard"></i>
                <span class="app-menu__label">Inventory</span>
            </a>
        </li>
		<!--
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.index' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.index') }}">    		
			<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Account Head Balance</span>   
			</a>        
		</li>
		-->
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.cash-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.cash-book') }}">    		
			<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Cash Book</span>   
			</a>        
		</li>
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.purchase-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.purchase-book') }}">    		
			<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Purchase Book</span>   
			</a>        
		</li>
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.sale-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.sale-book') }}">    		
			<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Sales Book</span>   
			</a>        
		</li>
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.transfer-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.transfer-book') }}">    		
			<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Transfer Book</span>   
			</a>        
		</li>
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.receive-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.receive-book') }}">    		
			<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Receive Book</span>   
			</a>        
		</li>
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.suppliers' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.suppliers') }}">    		
			<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Supplier Book</span>   
			</a>        
		</li>
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.stock-reports.items.index' ? 'active' : '' }}" href="{{ route('admin.inventory.stock-reports.items.index') }}">    		
			<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Balance</span>   
			</a>        
		</li> 
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.stock-reports.products.index' ? 'active' : '' }}" href="{{ route('admin.inventory.stock-reports.products.index') }}">    		
			<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Balance</span>   
			</a>        
		</li> 
   </ul>
</aside>