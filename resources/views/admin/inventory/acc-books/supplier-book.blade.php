@extends('admin.inventory.acc-books.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
@php use Carbon\Carbon; @endphp
<style>
.sum-title
{
	font-weight:bold;
	color: #3a9629;
	
}
</style>
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
        <a href="{{ route('admin.inventory.acc-books.suppliers') }}" class="btn btn-primary pull-right">SELECT SUPPLIER</a>
    </div>
    @include('admin.inventory.acc-books.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
				@include('admin.inventory.partials.dates-row-param')				
                <div class="tile-body">
                    <table class="table table-hover table-bordered pk-table-last-page-first" id="sampleTable">
                        <thead>
                            <tr>	
								<th>DATE.</th>
								<th>HEAD</th>
								<th>ACTION</th>
								<th>PAID</th>
								<th>TO BE PAID</th>
								<th>BALANCE TO BE PAID</th>
                            </tr>
                        </thead> 
                        <tbody>
							@php  $bal_amt = 0; $cr_amt = 0; $dr_amt = 0;  @endphp
							@foreach($accounts_details as $key=>$accounts_detail) 
								
                                    <tr>
                                        <td>{{ Carbon::parse($accounts_detail->acc_trn_date)->format('d-m-Y') }}</td>
										@if ( $accounts_detail->dr_acc_head_id == $head_id) 
											<td>{{ $accounts_detail->cr_head_name }}</td>
											<td>{{ $accounts_detail->acc_trn_mode_id == 3 ? 'Purchase': 'Payment' }}</td>
											<td>{{$accounts_detail->acc_trn_amnt}}</td>
											<td></td>
											@php  $bal_amt -= $accounts_detail->acc_trn_amnt  @endphp
											@php  $dr_amt += $accounts_detail->acc_trn_amnt  @endphp
											
										@elseif ( $accounts_detail->cr_acc_head_id == $head_id) 
											<td>{{ $accounts_detail->dr_head_name }}</td>
											<td>{{ $accounts_detail->acc_trn_mode_id == 3 ? 'Purchase': 'Payment' }}</td>
											<td></td>
											<td>{{$accounts_detail->acc_trn_amnt}}</td>
											@php  $bal_amt += $accounts_detail->acc_trn_amnt  @endphp
											@php  $cr_amt += $accounts_detail->acc_trn_amnt  @endphp
											
										@endif
										
                                        <td>{{$bal_amt  }}</td>
                                    </tr>
									
                            @endforeach
							
							
							
                        </tbody>
                    </table>
					
                </div>
				
					<div class="row pt-3">
						<div class="col-md-3">
							<p class="sum-title"> Total to be Paid: {{$cr_amt }} </p>
						</div>
						<div class="col-md-3">
							<p class="sum-title"> Total Paid: {{$dr_amt }} </p>
						</div>
						<div class="col-md-3">
							<p class="sum-title"> Balance to be Paid:: {{$bal_amt }}</p>
						</div>
					</div>	
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function()
	{
		//$('#sampleTable').DataTable();
		

	});
	
	
	
	
</script>
@endpush
