@extends('admin.inventory.acc-books.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
@php use Carbon\Carbon; @endphp
<style>
.sum-title
{
	font-weight:bold;
	color: #3a9629;
	
}
</style>
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
    </div>
    @include('admin.inventory.acc-books.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
				@include('admin.inventory.partials.dates-row')
                <div class="tile-body">
                    <table class="table table-hover table-bordered pk-table-last-page-first" id="sampleTable">
                        <thead>
                            <tr>	
								<th>DATE.</th>
								<th>SOURCE STORE </th>
								<th>TARGET STORE</th>
								<th>TRANSFER NO.</th>
								<th>AMOUNT</th>
								<th>SUM AMOUNT</th>
                            </tr>
                        </thead>
                        <tbody>
							@php  $bal_amt = 0; $cr_amt = 0; $dr_amt = 0;  @endphp
							@foreach($accounts_details as $key=>$accounts_detail) 
								
                                    <tr>
                                        <td>{{ Carbon::parse($accounts_detail->acc_trn_date)->format('d-m-Y') }}</td>
										@if ( $accounts_detail->cr_acc_head_id == $head_id) 
											<td>{{ $accounts_detail->source_store_name }}</td>
											<td>{{ $accounts_detail->trgt_store_name }}</td>
											<td>Inv.No.{{$accounts_detail->ith_trn_no}}</td>
											<td>{{ $accounts_detail->acc_trn_amnt }}</td>
											@php  $bal_amt += $accounts_detail->acc_trn_amnt  @endphp
											@php  $cr_amt -= $accounts_detail->acc_trn_amnt  @endphp
											
										@endif
										
                                        <td>{{$bal_amt  }}</td>
                                    </tr>
									
                            @endforeach
							
							
							
                        </tbody>
                    </table>
					
                </div>
				
					<div class="row pt-3">
						<div class="col-md-3">
							<p class="sum-title"> </p>
						</div>
						<div class="col-md-3">
							<p class="sum-title"> </p>
						</div>
						<div class="col-md-3">
							<p class="sum-title"> Sum Amount: {{$bal_amt }}</p>
						</div>
					</div>	
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function()
	{
		//$('#sampleTable').DataTable();

	});
	
	
	
</script>
@endpush
