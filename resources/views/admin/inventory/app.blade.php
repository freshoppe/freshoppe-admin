<!DOCTYPE html>
<html lang="en">
<head>

    <title>@yield('title') - {{ config('app.name') }}</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/main.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/font-awesome/4.7.0/css/font-awesome.min.css') }}"/>
	<!-- include summernote css-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/metismenu.css') }}" />
	<link rel="icon" type="image/png" href="{{ asset('backend/pk/img/favicon.png') }}"/>	
	
	
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/pk/css/common.css') }}" /> <!--added by praveen--->
	<script src="{{ asset('backend/pk/js/common.js') }}"></script> <!--added by praveen--->

    @yield('styles')
	
</head>
<body class="app sidebar-mini rtl">
    @include('admin.partials.header')
    @include('admin.partials.sidebar')
    <main class="app-content" id="app">
        @yield('content')
    </main>
    <script src="{{ asset('backend/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('backend/js/popper.min.js') }}"></script>
    <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/js/main.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/pace.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
	<!-- include summernote js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('backend/js/metismenu.js') }}"></script>
	
	
	<script type="text/javascript">
	$("#metismenu").metisMenu({
	   //toggle: false
	 });
	 
	 $(document).ready(function()
	{
		$('.pk-table-last-page-first').dataTable({
   
   dom: 'lBfrtip',
  buttons: [
            {
                extend: 'excelHtml5',
                text: 'Export to Excel',
				/*title: 'js-tutorials.com : Export to datatable data to Excel',
				 download: 'open',
				 orientation:'landscape',
				  exportOptions: {
				  columns: ':visible'
				}*/
            }
        ],
   "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
  }).fnPageChange( 'last' );
  
	/*	
		$('.pk-table-last-page-first').DataTable({
   
   dom: 'lBfrtip',
   buttons: [
    'excel', 'csv', 'pdf', 'copy'
   ],
   "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
  }).fnPageChange( 'last' );
		*/
		
/*		
		$('.pk-table-last-page-first').dataTable({
   
   dom: 'lBfrtip',
  buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
   "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
  }).fnPageChange( 'last' );
  
  */
		
	});
	 

	 
	</script>
    
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> <!--added by praveen--->
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" /> <!--added by praveen--->

	<!--added by praveen to export dataTable buttons--- begin--->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
  <!--added by praveen to export dataTable buttons---end --->
  @stack('scripts')
</body>
</html>
