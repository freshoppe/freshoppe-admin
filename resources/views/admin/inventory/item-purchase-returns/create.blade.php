@extends('admin.inventory.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
@php use Carbon\Carbon; @endphp
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.inventory.partials.flash')
    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
				
                <form action="" enctype="multipart/form-data">
                    <div class="tile-header">
					
						<div class="row">
						
							<div class="col-md-3">
							
								<div class="form-group">
									<label for="ith_date" class="placeholder">DATE</label>
									<input tabindex="0" id="ith_date" name="ith_date" type="date" class="form-control " value="{{Carbon::now()->format('Y-m-d')}}" required>
									
									
								</div>
								
							</div>
						
							<div class="col-md-3">
								<div class="form-group">
									<label for="supplier_id">SELECT SUPPLIER</label>
										<select  tabindex="1" class="form-control"  id="supplier_id">
											@foreach($suppliers as $key=>$supplier) 
												<option value="{{$supplier->id}}">{{$supplier->head_name}}</option>
											@endforeach
										</select>
								</div>
							</div>
							
							<div class="col-md-2">
							
								<div class="form-group">
									<label for="ith_inv_no" class="placeholder">BILL REF.NO.(if any)</label>
									<input  tabindex="2" id="ith_inv_no" name="ith_inv_no" type="text" class="form-control " value="0" required>
								</div>
								
							</div>
							<div class="col-md-2">
							
								<div class="form-group">
									<label for="prh_rtn_no" class="placeholder">PURCHASE SRL NO.</label>
									<input  tabindex="-1" id="prh_rtn_no" name="prh_rtn_no" type="number" class="form-control " value="{{$prh_rtn_no}}" readonly>
								</div>
								
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="wh_credit">CASH/CREDIT</label>
										<select  tabindex="3" class="form-control"  id="wh_credit">
												<option value="0">CASH</option>
												<option value="1">CREDIT</option>
										</select>
								</div>
							</div>
							
						</div>
						
						
										
						<div class="row">
						
							<div class="col-md-2">
								<div class="form-group">
									<label for="item_id">Select Item</label>
										<select  tabindex="4" class="form-control"  id="item_id">
											@foreach($items as $key=>$item) 
												<option value="{{$item->id}}">{{$item->item_name}}</option>
											@endforeach
										</select>
								</div>
							</div>
																					
							<div class="col-md-2">
								<div class="form-group">
									<label for="item_unit" class="placeholder">Unit</label>
									<input  id="item_unit_id" name="item_unit_id" type="hidden" class="form-control " value="" >
									<input tabindex="-1" id="item_unit" name="item_unit" type="text" class="form-control" readonly value="" required>
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group ">
									<label for="item_qty" class="placeholder">Qty</label>
									<input  tabindex="5" id="item_qty" name="item_qty" type="number" class="form-control"  value="0" required>
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="item_price" class="placeholder">Rate</label>
									<input  tabindex="6" id="item_price" name="item_price" type="number" class="form-control"  value="0" required>
								</div>
							
							</div>														
							
							<div class="col-md-2">
								<div class="form-group ">
									<label for="item_gst" class="placeholder">GST %</label>
									<input  tabindex="7"  id="item_gst" name="item_gst" type="number" class="form-control" value="0" required>
								</div>
							
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="grs_amnt" class="placeholder">Sum</label>
									<input  tabindex="-1" readonly id="grs_amnt" name="item_code" type="number" class="form-control" value="0" required>
								</div>
							
							</div>
							
							
							
						</div>
						<div class="row">
							<div class="col-md-12">
								<a  tabindex="8" id="btn-add-to-list" class="btn btn-success " style="float:right;">Add to List</a>
							</div>
						</div>
													
													
					</div><!--ends tile-header-->
				</div><!--ends tile1 -->
				
				
				
			</div><!--ends col-md-12 -->
		</div><!--ends row -->
						
						
		<div class="row"><!--begins list row -->
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body">
					
						
									<div class="table-responsive table-hover table-sales">
										<form id="listForm">
											<table id="table-item-list" class="table table-hover table-bordered" id="sampleTable" style="width:100%">
												<thead>
													<tr>
														<th style="border-bottom: 1px solid #35cd3a!important">SRL.NO.</th>
														<th style="display:none;">ITEM ID</th>
														<th style="border-bottom: 1px solid #35cd3a!important">ITEM NAME</th>
														<th style="border-bottom: 1px solid #35cd3a!important">UNIT</th>
														<th style="border-bottom: 1px solid #35cd3a!important">QTY</th>
														<th style="border-bottom: 1px solid #35cd3a!important">RATE</th>
														<th style="border-bottom: 1px solid #35cd3a!important">SUM</th>
														<th style="border-bottom: 1px solid #35cd3a!important">GST%</th>
														<th style="border-bottom: 1px solid #35cd3a!important">GST</th>
														<th style="border-bottom: 1px solid #35cd3a!important">GROSS</th>
														<th style="border-bottom: 1px solid #35cd3a!important" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
													</tr>
												</thead>
												<tbody >
												
												
												</tbody>
												<!--
												<tfoot>
													<tr>
														<th>SRL.NO.</th>
														<th style="display:none;">ITEM ID</th>
														<th>ITEM NAME</th>
														<th>UNIT</th>
														<th>QTY</th>
														<th>RATE</th>
														<th>SUM</th>
														<th>GST%</th>
														<th>GST</th>
														<th>GROSS</th>
													</tr>
												</tfoot>
												-->
											</table>												
										</form>
									</div>
						
						
						
						
					</div>
				</div>
			</div>
		</div><!--ends list row -->

		<div class="row tile" style="padding: 20px!important; border-radius:3px;">
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="prh_rtn_no" class="placeholder">SUM TOTAL</label>
							<input  id="ith_sum_amnt" name="prh_rtn_no" type="number" class="form-control " value="" readonly>
						</div>
						
					</div>
				
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="prh_rtn_no" class="placeholder">SGST TOTAL</label>
							<input  id="ith_sum_sgst" name="prh_rtn_no" type="number" class="form-control " value="" readonly>
						</div>
						
					</div>
						
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="prh_rtn_no" class="placeholder">CGST TOTAL</label>
							<input  id="ith_sum_cgst" name="prh_rtn_no" type="number" class="form-control " value="" readonly>
						</div>
						
					</div>
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="prh_rtn_no" class="placeholder">GROSS SUM</label>
							<input  id="ith_gross_amnt" name="prh_rtn_no" type="number" class="form-control " value="" readonly>
						</div>
						
					</div>
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="ith_ded_amnt" class="placeholder">DEDUCTION</label>
							<input  id="ith_ded_amnt" name="ith_ded_amnt" type="number" class="form-control " value="" >
						</div>
						
					</div>
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="ith_net_amnt" class="placeholder">NET AMOUNT</label>
							<input  id="ith_net_amnt" name="ith_net_amnt" type="number" class="form-control " value="" readonly>
						</div>
						
					</div>
					
		</div>



						
    </div>
				
					
                    <div class="tile-footer">
						<!--
                        <a class="btn btn-primary a-save" data-draft="0" href="{{ route('admin.inventory.item-purchase-returns.index') }}" style="color:white;"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Purchase</a>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-primary a-save" data-draft="1" href="{{ route('admin.inventory.item-purchase-returns.index') }}" style="color:white;"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save as Draft</a>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.inventory.item-purchase-returns.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
						-->
						
                        <a class="btn btn-primary a-save" data-draft="0" href="#" style="color:white;"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Purchase Return</a>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-primary a-save" data-draft="1" href="#" style="color:white;"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save as Draft</a>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.inventory.item-purchase-returns.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
						
                    </div>
                </form>
				
				
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function(){
	sel_item_type_id = 0;
	tdata = [];
	APP_URL = "{{ url('/') }}";		
	
	
	items = '{!! $items !!}';		
	my_items = $.parseJSON(items);

		
    $('#table-item-list').DataTable();
	$('#sampleTable').DataTable();
	$('.dataTables_empty').remove();
	$('#supplier_id').focus();
	
		$("input").focusin(function()
		{
			$(this).select();
		});

		$(document).on('keypress',function(e) {
			if(e.which == 13) 
			{
				e.preventDefault();
				var tabindex = $(e.target).attr('tabindex');
				
				nextindex = (tabindex == 8)? 4: (+tabindex + 1);
				
				$('[tabindex=' + nextindex + ']').focus();

			}
		});
	
	
		$(document).on('click', '.a-save', function(event)
		{
			wh_draft = $(this).data('draft');
			
			collect_table_data();
			if($('#table-item-list tr').length > 2)
			{
				if(wh_draft == 1)
				{
					if(confirm('Are you Sure to Draft this Purchase ?'))
					{
						store(wh_draft);
					}
				}
				else
				{
					if(confirm('Are you Sure to Save this Purchase ?'))
					{
						store(wh_draft);
					}
				}
			}
			else
			{
				alert('Please Add Items.');
			}
				
		});
		
		
		$(document).on('change', '#item_type_id', function()
		{
			sel_item_type_id = $(this).val();
			get_items_by_type_id();
			
			$('#item_id').trigger('change');
			
		});
		
		
		$(document).on('change', '#item_id', function()
		{
			
			sel_item_id = $(this).val();
			
			get_item_by_id();
			
			$('#item_qty').trigger('keyup');
		});
		
		
		$(document).on('keyup', '#ith_ded_amnt', function()
		{
			 ith_gross_amnt = $("#ith_gross_amnt").val();
			 ith_ded_amnt = $("#ith_ded_amnt").val();
			 $("#ith_net_amnt").val(+ith_gross_amnt - +ith_ded_amnt).toFixed(2);
			
		});
		
		
		$(document).on('keyup', '#item_qty, #item_price, #item_gst', function()
		{
			var item_qty = $('#item_qty').val();
			var item_price = $('#item_price').val();
			var item_gst = $('#item_gst').val();
			
			sum_amnt = (item_qty * item_price).toFixed(2);
			
			gst_amnt = ((sum_amnt/100) * item_gst).toFixed(2);
			
			sgst_total = (gst_amnt/2).toFixed(2);
			cgst_total = (gst_amnt/2).toFixed(2);
			 
			grs_amnt = (+sum_amnt + +gst_amnt).toFixed(2);
			 
			 $('#grs_amnt').val(grs_amnt);
			 
			 
			
		});
		
		
		$(document).on('click', '.remove_item', function()
		{
			var item_index = $(this).attr('value');
			
			$('#table-item-list tr:eq(' + item_index + ')').remove();
			
			calculate_table_data();
		});

	
		$(document).on('click keypress', '#btn-add-to-list', function(event)
		{
			if( $('#item_qty').val() > 0)
			{
				var rowCount = $('#table-item-list tr').length -1;
												
				var item_row = '<tr>'+
						'<td id="itd_srl'+ ( rowCount) + '" data-fldName="itd_srl">'+ ( rowCount) +'</td>'+
						'<td data-fldName="itd_item_id" style="display:none;">'+ $('#item_id option:selected').val() +'</td>'+
						'<td data-fldName="strn_item">'+ $('#item_id option:selected').text() +'</td>'+
						'<td data-fldName="itd_item_unit_id" style="display:none;">'+ $('#item_unit_id').val() +'</td>'+
						'<td data-fldName="item_unit">'+ $('#item_unit').val() +'</td>'+
						'<td data-fldName="itd_unit_qty">'+ $('#item_qty').val() +'</td>'+
						'<td data-fldName="itd_unit_rate">'+ $('#item_price').val() +'</td>'+
						'<td data-fldName="itd_unit_sum_amnt">'+ sum_amnt +'</td>'+
						'<td data-fldName="itd_unit_gst_rate">'+ $('#item_gst').val() +'</td>'+
						'<td data-fldName="unit_gst">'+ gst_amnt +'</td>'+
						'<td data-fldName="itd_unit_gross_amnt">'+ grs_amnt +'</td>'+
						'<td value="' + ( rowCount + 1 ) + '" data-fldName="remove_item" class="text-center text-danger remove_item"><i class="fa fa-trash"> </i></td>'+
					'</tr>';
		
														
			   
				$("#table-item-list tbody").append(item_row);
						   
						   /*
				 $("#ith_sum_amnt").val(function() {return +this.value + +(+sum_amnt).toFixed(2);});
				 $("#ith_sum_sgst").val(function() {return +this.value + +(+sgst_total).toFixed(2);});
				 $("#ith_sum_cgst").val(function() {return +this.value + +(+cgst_total).toFixed(2);});
				 $("#ith_gross_amnt").val(function() {return +this.value + +(+grs_amnt).toFixed(2);});
				 */
				 /*
				 
				 ith_gross_amnt = $("#ith_gross_amnt").val();
				 ith_ded_amnt = $("#ith_ded_amnt").val();
				 $("#ith_net_amnt").val(+ith_gross_amnt - +ith_ded_amnt);
				 */
				 calculate_table_data();
			}
			else
			{
				alert('Please enter Quantity.')
			}
			 
		});
		
		
		
		$('#item_id').trigger('change');


	});
	
		
	
	function get_items_by_type_id()
	{
		var CSRF_TOKEN = '{{csrf_token()}}';

		var	url_var = APP_URL + '/get_items_by_type_id';
		
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['item_type_id'] = sel_item_type_id;
		
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: data,
		   async:false,
		   success:function(result_data)
			   {
				   
					$("#item_id").empty();
					var $select_item_id = $("#item_id");
						
					$.each(result_data, function() 
					{
						$select_item_id.append($("<option />").val(this.id).text(this.item_name));
					});					   
					   
					
				}
			});
			
	}

	
	function store(wh_draft)
	{
		var CSRF_TOKEN = '{{csrf_token()}}';
		var action = (wh_draft == 1)? 'draft' : 'store';
		
		var	url_var = APP_URL + '/admin/inventory/item-purchase-returns/' + action;
				
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['supplier_id'] = $('#supplier_id').val();
		data['ith_date'] = $('#ith_date').val();
		data['ith_inv_no'] = $('#ith_inv_no').val();
		data['ith_sum_amnt'] = $('#ith_sum_amnt').val();
		data['ith_sum_sgst'] = $('#ith_sum_sgst').val();
		data['ith_sum_cgst'] = $('#ith_sum_cgst').val();
		data['ith_ded_amnt'] = $('#ith_ded_amnt').val();
		data['ith_net_amnt'] = $('#ith_net_amnt').val();
		data['ith_gross_amnt'] = $('#ith_gross_amnt').val();
		data['wh_credit'] = $('#wh_credit').val();
				
		
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: {prhdata: data, unitdata: tdata},
		   headers: {'X-CSRF-TOKEN':  CSRF_TOKEN},
		   async:false,
		   success:function(result_data)
			   {
				   
					if(wh_draft == 1)
					{
						alert('Purchase Drafted.');
					}
					else
					{
						alert('Purchase Saved.');
					}
					
					location.reload();
					
				}
			});
			
	}
	
	
	function collect_table_data()
	{
		
		tdata = {};
		var rows = $('#table-item-list tr').length;
		
		$('#table-item-list tr').each(function (rowIndex, r) 
		{
			if (!this.rowIndex) return; // skip first row
			if (this.rowIndex==1) return; // skip second row
			//if (this.rowIndex==(rows-1)) return; // skip footer row
			
			var row_number = rowIndex-2;
			tdata[row_number] = {};
			
				$(this).find('td').each(function (colIndex, c) 
				{
					var fldname = $(this).data('fldname');
					tdata[row_number][fldname] = c.textContent;
				});
			
			
			
			
		});	
		
	}	
	
	
	function calculate_table_data()
	{
		
		tdata = {};
		var rows = $('#table-item-list tr').length;
		var itd_unit_sum_amnt = 0;
		var unit_gst = 0;
		var itd_unit_gross_amnt = 0;
		
		$('#table-item-list tr').each(function (rowIndex, r) 
		{
			if (!this.rowIndex) return; // skip first row
			if (this.rowIndex==1) return; // skip second row
			//if (this.rowIndex==(rows-1)) return; // skip footer row
			
			var row_number = rowIndex-2;
			var row_srl = rowIndex-1;
			
				$(this).find('td').each(function (colIndex, c) 
				{
					var fldname = $(this).data('fldname');
					
					fldname == 'itd_srl' && $(this).text(row_srl);
					
					fldname == 'itd_unit_sum_amnt' && (itd_unit_sum_amnt += +c.textContent);	
	
					fldname == 'unit_gst' && (unit_gst += +c.textContent);
					
					fldname == 'itd_unit_gross_amnt' && (itd_unit_gross_amnt += +c.textContent);
					
					fldname == 'remove_item' && $(this).attr('value', +row_srl+1);
					
				});
			
		});	
		
				 $("#ith_sum_amnt").val(itd_unit_sum_amnt.toFixed(2));				 
				 $("#ith_sum_sgst").val(unit_gst.toFixed(2)/2);
				 $("#ith_sum_cgst").val(unit_gst.toFixed(2)/2);
				 $("#ith_gross_amnt").val(itd_unit_gross_amnt.toFixed(2));
				 
				 ith_gross_amnt = $("#ith_gross_amnt").val();
				 ith_ded_amnt = $("#ith_ded_amnt").val();
				 $("#ith_net_amnt").val((+ith_gross_amnt - +ith_ded_amnt).toFixed(2));
		
		
	}	
	
	
	function get_item_by_id_jquery()
	{
		var CSRF_TOKEN = '{{csrf_token()}}';

		var	url_var = APP_URL + '/admin/inventory/items/get_by_id_json';
		
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['id'] = sel_item_id;
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: data,
		   async:true,
		   success:function(result_data)
			   {
					$('#item_unit_id').val(result_data[0].item_unit_id);
					$('#item_unit').val(result_data[0].item_unit);
					$('#item_price').val(result_data[0].item_price);
					$('#item_gst').val(result_data[0].item_gst);
					
				}
			});
			
	}
	
	
	
		function get_item_by_id_XMLHttpRequest()
		{	
			var CSRF_TOKEN = '{{csrf_token()}}';
			var	url_var = APP_URL + '/admin/inventory/items/get_by_id_json';

			var data ='id='+ sel_item_id;
			
			var xhttp=new XMLHttpRequest();			
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var msg = this.responseText;
					received_messages = msg;
					received_messages = JSON.parse(received_messages);
					cnt = received_messages.length;
					
					$('#item_unit_id').val(received_messages[0].item_unit_id);
					$('#item_unit').val(received_messages[0].item_unit);
					$('#item_price').val(received_messages[0].item_price);
					$('#item_gst').val(received_messages[0].item_gst);
					
					
					
				}
			  };
			
			xhttp.open("POST", url_var, false);
			xhttp.setRequestHeader("x-csrf-token", CSRF_TOKEN); 
			xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhttp.send(data);
		}					
	
	 
	function get_item_by_id()
	{

		Object.keys(my_items).forEach(function(index)
		{
	
			var item = my_items[index];
			if(item.id == sel_item_id)
			{
			
				$('#item_unit_id').val(item.item_unit_id);
				$('#item_unit').val(item.item_unit);
				$('#item_price').val(item.item_price);
				$('#item_gst').val(item.item_gst);
				return false;	
			}
			
		});

	}
	
</script>
@endpush
