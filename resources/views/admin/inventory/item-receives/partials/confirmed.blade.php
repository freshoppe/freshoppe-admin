@php use Carbon\Carbon; @endphp
<div class="tile">
	<div class="tile-body">
		<table class="table table-hover table-bordered" id="pendingTable">
			<thead>
				<tr>	
					<th style="display:none">id</th>
					<th>Srl.No.</th>
					<th>Mode</th>
					<th>Inv.No</th>
					<th>Date</th>
					<th>Source Store</th>
					<th>Amount</th>
					<th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
				</tr>
			</thead>
			<tbody>
				@foreach($received_transfers as $key=>$received_transfer) 
						<tr>
							<td style="display:none">{{ $received_transfer->id }}</td>
							<td>{{ $key + 1 }}</td>
							<td>Received</td>
							<td>{{ $received_transfer->ith_trn_no }}</td>
							<td>{{ Carbon::parse($received_transfer->ith_date)->format('d-m-Y') }}</td>
							<td>{{ $received_transfer->source_store_name }}</td>
							<td>{{ $received_transfer->ith_net_amnt }}</td>
							<td class="text-center">
								<div class="btn-group" role="group" aria-label="Second group">
									<a  href="{{ route('admin.inventory.item-receives.view', $received_transfer->id) }}" class="btn btn-sm btn-primary">view</a>
								</div>
							</td>
						</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

