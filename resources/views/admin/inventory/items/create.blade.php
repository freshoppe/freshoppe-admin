@extends('admin.inventory.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.inventory.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
				
				
				
                <form action="" enctype="multipart/form-data">
                    <div class="tile-body">
					
                        <div class="form-group">
                            <label for="parent">ITEM TYPE <span class="m-l-5 text-danger"> *</span></label>
                            <select id="item_type_id" class="form-control custom-select mt-15 " name="item_type_id">
                                @foreach($item_types as $key => $item_type)
                                    <option value="{{ $item_type->id }}"> {{ $item_type->item_type }} </option>
                                @endforeach
                            </select>
                        </div>																									
					<!--
                        <div class="form-group">
                            <label class="control-label" for="item_code">ITEM CODE <span class="m-l-5 text-danger"> *</span></label>
							<input class="form-control req-field" type="text" id="item_code" name="item_code"  oninput="this.value = this.value.toUpperCase()">
							<p id="err_tag_item_code"  style="display:none;" class="text-danger" >required field</p>
                        </div>
					-->	
                        <div class="form-group">
                            <label class="control-label" for="item_name">ITEM NAME <span class="m-l-5 text-danger"> *</span></label>
							<input class="form-control req-field" type="text" id="item_name" name="item_name"  oninput="this.value = this.value.toUpperCase()">
                           <p id="err_tag_item_name"  style="display:none;" class="text-danger" >required field</p>
                        </div>
						
                        <div class="form-group">
                            <label class="control-label" for="item_gst">ITEM RATE </label>
							<input  id="item_price" name="item_price" type="number" class="form-control " >
                        </div>
						
                        <div class="form-group">
                            <label class="control-label" for="item_gst">GST RATE </label>
							<input  id="item_gst" name="item_gst" type="number" class="form-control " >
                        </div>
						
															
                        <div class="form-group">
                            <label for="parent">ITEM UNIT <span class="m-l-5 text-danger"> *</span></label>
                            <select id="item_unit_id" class="form-control custom-select mt-15 " name="item_unit_id">
                                @foreach($item_units as $key => $item_unit)
                                    <option value="{{ $item_unit->id }}"> {{ $item_unit->item_unit }} </option>
                                @endforeach
                            </select>
                        </div>
						
						<div class="form-group">
							<label class="control-label" for="processing">Processing <span class="m-l-5 text-danger"> *</span></label>
							<select name="processing[]" id="processing" class="form-control @error('processing') is-invalid @enderror" multiple>
								@foreach($processings as $processing)
									<option value="{{ $processing->id }}">
									{{ $processing->processing_name }}
									</option>
								@endforeach
							</select>
							@error('roles')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
															
												
                    </div>
					
                    <div class="tile-footer">
                        <a class="btn btn-primary" href="#" id="a-save" style="color:white;"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Item</a>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.inventory.items.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
				
				
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('backend/js/plugins/select2.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#processing').select2();
		$('#item_code').focus();
		
		$(document).on('click', '#a-save', function(event)
		{
			
			$('.req-field').each(function (i) {
				if ($(this).val() == '') 
				{
					$(this).addClass('is-invalid')
					var id = $(this).attr('id');
					$('#err_tag_' + id).show()
				}
				else
				{
					$(this).removeClass('is-invalid')
					var id = $(this).attr('id');
					$('#err_tag_' + id).hide()
				}
			});		
			
			var valid = $('.req-field').filter(function () 
			{
					return this.value != '';
			});			
			
			
			if(valid.length > 0)
			{
				if(confirm('Are you sure to Save this item ?'))
				{
					store();
					location.reload();
				}
			}
			
		});

	});
	
	
	function validate_fields()
	{
			var count = $('.req-field').length;
			$('.req-field').each(function (i) {
				if ($(this).val() == '') 
				{
					$(this).addClass('is-invalid')
					var id = $(this).attr('id');
					$('#err_tag_' + id).show()
					var valid = false;
					if((i+1)===count)
					{
						
					}
				}
			});		
		
	}
	
	
	function store()
	{
		var CSRF_TOKEN = '{{csrf_token()}}';
		APP_URL = "{{ url('/') }}";		
		
		var	url_var = 'store';
		
		var data = {};
		data['_token'] = CSRF_TOKEN;
		//data['item_code'] = $('#item_code').val();
		data['item_name'] = $('#item_name').val();
		data['item_price'] = $('#item_price').val();
		data['item_gst'] = $('#item_gst').val();
		data['item_unit_id'] = $('#item_unit_id').val();
		data['item_type_id'] = $('#item_type_id').val();
		data['processing'] = $('#processing').val();
						
						
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: data,
		   async:false,
		   success:function(result_data)
			   {
					if(result_data == 0)
					{
						alert('Code or Name already Exists.');
					}
					else
					{
						alert('Saved.');
						window.location.href = "{{ url('/admin/inventory/items') }}";
					}
				}
			});
			
	}
	
	
</script>
@endpush
