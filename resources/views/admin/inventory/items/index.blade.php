@extends('admin.inventory.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
        <a href="{{ route('admin.inventory.items.create') }}" class="btn btn-primary pull-right">Add Item</a>
    </div>
    @include('admin.inventory.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>	
								<th style="display:none">id</th>
								<th>SRL.NO.</th>
								<th>ITEM CODE</th>
								<th>ITEM NAME</th>
								<th class="text-center" >EDIT</th>
								<th class="text-center" >DELETE</th>
                            </tr>
                        </thead>
                        <tbody>
							@foreach($items as $key=>$item) 
                                    <tr>
                                        <td style="display:none">{{ $item->id }}</td>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->item_code }}</td>
                                        <td>{{ $item->item_name }}</td>
                                        <td class="text-center">
                                                <a href="{{ route('admin.inventory.items.edit', $item->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                                        </td>
                                        <td class="text-center">
                                                <a href="#" class="btn btn-sm btn-danger a-delete" value="{{$item->id}}"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                            @endforeach
							
							
							
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#sampleTable').DataTable();
		item_id = 0;
		
		$(document).on('click', '.a-delete', function(event)
		{
			item_id = $(this).attr('value');
 

				if(confirm('Are you sure to Remove this Item ?'))
				{
					delete_item();
					location.reload();
				}
			
		});
		

	});
	
	
	
	
	function delete_item()
	{
		var CSRF_TOKEN = '{{csrf_token()}}';
		
		APP_URL = "{{ url('/') }}";		
		
		var	url_var = APP_URL + '/admin/inventory/items/delete';
		
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['id'] = item_id;
						
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: data,
		   async:false,
		   success:function(result_data)
			   {
					alert('Removed.');
				}
			});
			
	}
	
	
</script>
@endpush
