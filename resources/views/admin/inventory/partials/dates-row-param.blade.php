@php use Carbon\Carbon; @endphp
<form action="{{route(Route::currentRouteName(), $head_id)}}" method="post">
@csrf <!-- {{ csrf_field() }} -->
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<label class="control-label" for="supplier_name">FROM DATE <span class="m-l-5 text-danger"> </span></label>
			<input class="form-control req-field" value="{{ ($from_date) ? $from_date : Carbon::now()->firstOfMonth()->format('Y-m-d')}}" type="date" id="from_date" name="from_date" >
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label class="control-label" for="supplier_name">TO DATE  <span class="m-l-5 text-danger"> </span></label>
			<input class="form-control req-field" value="{{($to_date) ? $to_date :Carbon::now()->format('Y-m-d')}}" type="date" id="to_date" name="to_date">
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group ">
			<button type="submit" class="btn btn-primary form-control " style="margin-top: 28px!important;">Apply Date Change</button>
		</div>
	</div>
</div>
</form>
