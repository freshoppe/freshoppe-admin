<div class="app-sidebar__overlay" data-toggle="sidebar">

</div>
<aside class="app-sidebar">
	<ul class="app-menu">
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.dashboard' ? 'active' : '' }}" href="{{ route('admin.inventory.dashboard') }}">                
				<i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span>            
			</a>        
		</li>
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.dashboard' ? 'active' : '' }}" href="{{ route('admin.dashboard') }}">
				<i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Ecommerce</span>            
			</a>        
		</li>
		@can('supplier-master')
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.suppliers.index' ? 'active' : '' }}" href="{{ route('admin.inventory.suppliers.index') }}">
				<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Suppliers</span>
			</a>        
		</li>
		@endcan
		@can('item-master')
		<li>             
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.items.index' ? 'active' : '' }}" href="{{ route('admin.inventory.items.index') }}">
				<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Items</span> 
			</a>        
		</li>
		@endcan
		@can('item-purchase')
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.item-purchases.index' ? 'active' : '' }}" href="{{ route('admin.inventory.item-purchases.index') }}">
				<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Purchase</span>   
			</a>        
		</li>
		@endcan
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.item-purchase-returns.index' ? 'active' : '' }}" href="{{ route('admin.inventory.item-purchase-returns.index') }}">
				<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Purchase Return</span>   
			</a>        
		</li>
		@can('item-transfer-to-stores')
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.item-transfers.index' ? 'active' : '' }}" href="{{ route('admin.inventory.item-transfers.index') }}">    		
				<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Transfer</span>   
			</a>        
		</li>
		@endcan
		@can('item-receiving')
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'aadmin.inventory.store-stock-receives.index' ? 'active' : '' }}" href="{{ route('admin.inventory.store-stock-receives.index') }}">
				<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Receive</span>   
			</a>        
		</li>
		@endcan
		@can('product-purchase')
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.prod-purchases.index' ? 'active' : '' }}" href="{{ route('admin.inventory.prod-purchases.index') }}">
				<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Purchase</span>   
			</a>        
		</li>
		@endcan
		@can('product-transfer-to-stores')
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.prod-transfers.index' ? 'active' : '' }}" href="{{ route('admin.inventory.prod-transfers.index') }}">
				<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Transfer</span>   
			</a>        
		</li>
		@endcan
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.prod-receives.index' ? 'active' : '' }}" href="{{ route('admin.inventory.prod-receives.index') }}">
				<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Receive</span>   
			</a>        
		</li>
		<li>            
			<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.index' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.index') }}">    		
			<i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Account Books</span>   
			</a>        
		</li>

   </ul>
</aside>