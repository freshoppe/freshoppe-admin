@extends('admin.inventory.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
@php use Carbon\Carbon; @endphp

    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
        <a href="{{ route('admin.inventory.prod-purchases.create') }}" class="btn btn-primary pull-right">New Purchase</a>
    </div>
    @include('admin.inventory.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered pk-table-last-page-first" id="sampleTable">
                        <thead>
                            <tr>	
								<th style="display:none">id</th>
								<th>Srl.No.</th>
								<th>Mode</th>
								<th>Prh/DraftSrl.</th>
								<th>Date</th>
								<th>Supplier</th>
								<th>Amount</th>
								<th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        <tbody>
							@foreach($draft_purchases as $key=>$draft_purchase) 
                                    <tr>
                                        <td style="display:none">{{ $draft_purchase->id }}</td>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $draft_purchase->stk_trn_type_id ==1 ? 'Purchase': 'Draft'}}</td>
                                        <td>{{ $draft_purchase->pth_trn_no }}</td>
                                        <td>{{ Carbon::parse($draft_purchase->pth_date)->format('d-m-Y') }}</td>
                                        <td>{{ $draft_purchase->head_name }}</td>
                                        <td>{{ $draft_purchase->pth_net_amnt }}</td>
										<td class="text-center">
											<div class="btn-group" role="group" aria-label="Second group">
												<a  href="{{ route('admin.inventory.prod-purchases.edit', $draft_purchase->id) }}" class="btn btn-sm btn-primary">view</a>
												<!--<a href="#" onclick="" class="btn btn-sm btn-info">confirm</a>-->
												
												@if($draft_purchase->stk_trn_type_id !=1)
													<a href="#" value="{{$draft_purchase->id}}" onclick="" class="btn btn-sm btn-danger a-delete">decline</a>
												@endif
											</div>
										</td>
                                    </tr>
                            @endforeach
							
							
							
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function()
	{
		//$('#sampleTable').DataTable();
		item_id = 0;
		
		$(document).on('click', '.a-delete', function(event)
		{
			item_id = $(this).attr('value');
 

				if(confirm('Are you sure to Delete this Purchase Draft ?'))
				{
					delete_draft();
					location.reload();
				}
			
		});
		

	});
	
	
	
	
	function delete_draft()
	{
		var CSRF_TOKEN = '{{csrf_token()}}';
		
		APP_URL = "{{ url('/') }}";		
		
		var	url_var = APP_URL + '/admin/inventory/prod-purchases/delete_draft';
		
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['id'] = item_id;
						
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: data,
		   async:false,
		   success:function(result_data)
			   {
					alert('Deleted.');
				}
			});
			
	}
	
	
</script>
@endpush
