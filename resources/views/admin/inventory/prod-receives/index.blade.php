@extends('admin.inventory.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
@php use Carbon\Carbon; @endphp

    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
    </div>
    @include('admin.inventory.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
			
				<div class="col-md-12">
					<ul class="nav nav-tabs order-tabs">
						<li class="nav-item"><a class="nav-link active" href="#pending-transfer" data-toggle="tab">Pending</a></li>
						<li class="nav-item"><a class="nav-link" href="#confirmed-transfer" data-toggle="tab">Confirmed</a></li>
					</ul>
				</div>
				<div class="col-md-12">
					<div class="tab-content">
						<div class="tab-pane active" id="pending-transfer">
							@include('admin.inventory.prod-receives.partials.pending')
						</div>
						<div class="tab-pane fade" id="confirmed-transfer">
							@include('admin.inventory.prod-receives.partials.confirmed')
						</div>
						
					</div>
				</div>
				
				
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function()
	{
		//$('#sampleTable').DataTable();
		item_id = 0;
		
		$(document).on('click', '.a-delete', function(event)
		{
			item_id = $(this).attr('value');
 

				if(confirm('Are you sure to Delete this transfer Draft ?'))
				{
					delete_draft();
					location.reload();
				}
			
		});
		

	});
	
	
	
	
	function delete_draft()
	{
		var CSRF_TOKEN = '{{csrf_token()}}';
		
		APP_URL = "{{ url('/') }}";		
		
		var	url_var = APP_URL + '/admin/inventory/prod-receives/delete_draft';
		
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['id'] = item_id;
						
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: data,
		   async:false,
		   success:function(result_data)
			   {
					alert('Deleted.');
				}
			});
			
	}
	
	
</script>
@endpush
