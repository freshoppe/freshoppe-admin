@php use Carbon\Carbon; @endphp
<div class="tile">
	<div class="tile-body">
		<table class="table table-hover table-bordered" id="pendingTable">
			<thead>
				<tr>	
					<th style="display:none">id</th>
					<th>Srl.No.</th>
					<th>Transfer Srl.</th>
					<th>Date</th>
					<th>Source Store</th>
					<th>Amount</th>
					<th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
				</tr>
			</thead>
			<tbody>
				@foreach($pending_transfers as $key=>$pending_transfer) 
						<tr>
							<td style="display:none">{{ $pending_transfer->id }}</td>
							<td>{{ $key + 1 }}</td>
							<td>{{ $pending_transfer->pth_trn_no }}</td>
							<td>{{ Carbon::parse($pending_transfer->pth_date)->format('d-m-Y') }}</td>
							<td>{{ $pending_transfer->source_store_name }}</td>
							<td>{{ $pending_transfer->pth_net_amnt }}</td>
							<td class="text-center">
								<div class="btn-group" role="group" aria-label="Second group">
									<a  href="{{ route('admin.inventory.prod-receives.receive', $pending_transfer->id) }}" class="btn btn-sm btn-primary">receive</a>
									<!--<a href="#" onclick="" class="btn btn-sm btn-info">confirm</a>-->
									@if($pending_transfer->stk_trn_type_id !=2)
									<a href="#" value="{{$pending_transfer->id}}" onclick="" class="btn btn-sm btn-danger a-delete">decline</a>
									@endif
								</div>
							</td>
						</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

