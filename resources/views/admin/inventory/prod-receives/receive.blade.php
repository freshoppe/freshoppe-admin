@extends('admin.inventory.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
@php use Carbon\Carbon; @endphp

    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.inventory.partials.flash')
    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
				
                <form action="" enctype="multipart/form-data">
                    <div class="tile-header">
					
						<div class="row">
						
							<div class="col-md-3">
							
								<div class="form-group">
									<label for="pth_date" class="placeholder">DATE</label>
									<input tabindex="0" id="pth_date" name="pth_date" type="date" class="form-control " value="{{Carbon::now()->format('Y-m-d')}}" required>
									
									
								</div>
								
							</div>
						
							<div class="col-md-3">
								
								<div class="form-group">
									<label for="target_store_id">SOURCE STORE</label>
										<select  tabindex="1" class="form-control"  id="target_store_id" disabled>
											@foreach($acive_stores as $key=>$acive_store) 
												<option value="{{$acive_store->id}}" {{$targetRecord->store_id == $acive_store->id ? 'selected':''}}>{{$acive_store->store_name}}</option>
											@endforeach
										</select>
								</div>
								
							</div>
							
							<div class="col-md-2">
							
								<div class="form-group">
									<label for="pth_inv_no" class="placeholder">BILL REF.NO.(if any)</label>
									<input  tabindex="2" id="pth_inv_no" name="pth_inv_no" type="text" class="form-control " value="0" required>
								</div>
								
							</div>
							<div class="col-md-2">
							
								<div class="form-group">
									<label for="next_receive_no" class="placeholder">RECEIVE SRL NO.</label>
									<input  tabindex="-1" id="next_receive_no" name="next_receive_no" type="number" class="form-control " value="{{$next_receive_no}}" readonly>
									<input  tabindex="-1" id="pth_transfer_id" name="pth_transfer_id" type="hidden" class="form-control " value="{{$targetRecord->id}}" >
								</div>
								
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="wh_credit">CASH/CREDIT</label>
										<select  tabindex="3" class="form-control"  id="wh_credit">
												<option value="0">CASH</option>
												<option value="1">CREDIT</option>
										</select>
								</div>
							</div>
							
						</div>
						
						
										
						<div class="row">
						
							<div class="col-md-2">
								<div class="form-group">
									<label for="product_id">Select product</label>
										<select  tabindex="4" class="form-control"  id="product_id">
											@foreach($products as $key=>$product) 
												<option value="{{$product->id}}">{{$product->name}}</option>
											@endforeach
										</select>
								</div>
							</div>
																
							<div class="col-md-2">
								<div class="form-group ">
									<label for="ptd_unit_qty" class="placeholder">Qty <span id="item_unit" style="color: blueviolet; font-weight: bold;"> </span></label>
									<input  tabindex="5" id="ptd_unit_qty" name="ptd_unit_qty" type="number" class="form-control " value="0" required>
									<input  tabindex="-1" id="item_unit_name" name="item_unit_name" type="hidden" class="form-control " value="0" required>
									<input  tabindex="-1" id="item_unit_id" name="item_unit_id" type="hidden" class="form-control " value="0" required>
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group ">
									<label for="ptd_unit_qty_net" class="placeholder">Net Qty </label>
									<input  tabindex="6" id="ptd_unit_qty_net" name="ptd_unit_qty_net" type="number" class="form-control " value="0" required>
								</div>
							</div>
							
							<div class="col-md-1">
								<div class="form-group ">
									<label for="ptd_unit_nos" class="placeholder">Nos</label>
									<input  tabindex="7" id="ptd_unit_nos" name="ptd_unit_nos" type="number" class="form-control " value="0" required>
								</div>
							</div>
							
							<div class="col-md-1">
								<div class="form-group ">
									<label for="ptd_unit_qty_gross" class="placeholder">Grs.Qty</label>
									<input  tabindex="-1" id="ptd_unit_qty_gross" name="ptd_unit_qty_gross" type="number" class="form-control " value="0" required>
								</div>
							</div>
							
							<div class="col-md-1">
								<div class="form-group">
									<label for="item_price" class="placeholder">Rate</label>
									<input  tabindex="8" id="item_price" name="item_price" type="number" class="form-control " value="0" required>
								</div>
							
							</div>														
							
							<div class="col-md-1">
								<div class="form-group ">
									<label for="item_gst" class="placeholder">GST %</label>
									<input  tabindex="9"  id="item_gst" name="item_gst" type="number" class="form-control " value="0" required>
								</div>
							
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="grs_amnt" class="placeholder">Sum</label>
									<input  tabindex="-1" readonly id="grs_amnt" name="item_code" type="number" class="form-control " value="0" required>
								</div>
							
							</div>
							
							
							
						</div>
						<div class="row">
							<div class="col-md-12">
								<!--<a  tabindex="10" id="btn-add-to-list" class="btn btn-success " style="float:right;">Add to List</a>-->
							</div>
						</div>
													
													
					</div><!--ends tile-header-->
				</div><!--ends tile1 -->
				
				
				
			</div><!--ends col-md-12 -->
		</div><!--ends row -->
						
						
		<div class="row"><!--begins list row -->
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body">
					
						
									<div class="table-responsive table-hover table-sales">
										<form id="listForm">
											<table id="table-product-list" class="table table-hover table-bordered"  style="width:100%">
												<thead>
													<tr>
														<th style="border-bottom: 1px solid #35cd3a!important">SRL.NO.</th>
														<th style="display:none;">ITEM ID</th>
														<th style="border-bottom: 1px solid #35cd3a!important">ITEM NAME</th>
														<th style="display:none;">ITEM unit ID</th>
														<th style="border-bottom: 1px solid #35cd3a!important">UNIT</th>
														<th ptd_unit_qty style="border-bottom: 1px solid #35cd3a!important">UNIT QTY</th>
														<th ptd_unit_nos style="border-bottom: 1px solid #35cd3a!important">NOS</th>
														<th ptd_unit_qty_gross style="border-bottom: 1px solid #35cd3a!important">GROSS</th>
														<th ptd_unit_qty_net style="border-bottom: 1px solid #35cd3a!important">NET</th>
														<th style="border-bottom: 1px solid #35cd3a!important">RATE</th>
														<th style="border-bottom: 1px solid #35cd3a!important">SUM</th>
														<th style="border-bottom: 1px solid #35cd3a!important">GST%</th>
														<th style="border-bottom: 1px solid #35cd3a!important">GST</th>
														<th style="border-bottom: 1px solid #35cd3a!important">GROSS</th>
														<th style="border-bottom: 1px solid #35cd3a!important" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
													</tr>
												</thead>
												<tbody >
													@foreach($transaction_details as $key=>$transaction_detail) 
													<tr>
														<td  data-fldName="ptd_srl">{{$transaction_detail->ptd_srl}}</td>
														<td data-fldName="ptd_product_id" style="display:none;">{{$transaction_detail->ptd_product_id}}</td>
														<td data-fldName="strn_item">{{$transaction_detail->product_name}}</td>
														<td data-fldName="ptd_item_unit_id" style="display:none;">{{$transaction_detail->ptd_item_unit_id}}</td>
														<td data-fldName="item_unit">{{$transaction_detail->item_unit}}</td>
														<td data-fldName="ptd_unit_qty">{{$transaction_detail->ptd_unit_qty}}</td>
														<td data-fldName="ptd_unit_nos">{{$transaction_detail->ptd_unit_nos}}</td>
														<td data-fldName="ptd_unit_qty_gross">{{$transaction_detail->ptd_unit_qty_gross}}</td>
														<td data-fldName="ptd_unit_qty_net">{{$transaction_detail->ptd_unit_qty_net}}</td>
														<td data-fldName="ptd_unit_rate">{{$transaction_detail->ptd_unit_rate}}</td>
														<td data-fldName="ptd_unit_sum_amnt">{{$transaction_detail->ptd_unit_sum_amnt}}</td>
														<td data-fldName="ptd_unit_gst_rate">{{$transaction_detail->ptd_unit_gst_rate}}</td>
														<td data-fldName="unit_gst">{{$transaction_detail->ptd_unit_sgst * 2}}</td>
														<td data-fldName="ptd_unit_gross_amnt">{{$transaction_detail->ptd_unit_gross_amnt}}</td>
														<td value="{{$key +2}}" data-fldName="remove_item" class="text-center text-danger remove_item"><i class="fa fa-trash"> </i></td>
													</tr>
													@endforeach
												</tbody>
												
												
												<!--
												<tfoot >
													<tr>
														<th style="border-bottom: 1px solid #35cd3a!important">SRL.NO.</th>
														<th style="display:none;">ITEM ID</th>
														<th style="border-bottom: 1px solid #35cd3a!important">ITEM NAME</th>
														<th style="border-bottom: 1px solid #35cd3a!important">UNIT</th>
														<th ptd_unit_qty style="border-bottom: 1px solid #35cd3a!important">UNIT QTY</th>
														<th ptd_unit_nos style="border-bottom: 1px solid #35cd3a!important">NOS</th>
														<th ptd_unit_qty_gross style="border-bottom: 1px solid #35cd3a!important">GROSS</th>
														<th ptd_unit_qty_net style="border-bottom: 1px solid #35cd3a!important">NET</th>
														<th style="border-bottom: 1px solid #35cd3a!important">RATE</th>
														<th style="border-bottom: 1px solid #35cd3a!important">SUM</th>
														<th style="border-bottom: 1px solid #35cd3a!important">GST%</th>
														<th style="border-bottom: 1px solid #35cd3a!important">GST</th>
														<th style="border-bottom: 1px solid #35cd3a!important">GROSS</th>
														<th style="border-bottom: 1px solid #35cd3a!important" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
													</tr>
												</tfoot>
												-->
											</table>												
										</form>
									</div>
						
						
						
						
					</div>
				</div>
			</div>
		</div><!--ends list row -->

		<div class="row tile" style="padding: 20px!important; border-radius:3px;">
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="prh_no" class="placeholder">SUM TOTAL</label>
							<input  id="pth_sum_amnt" name="prh_no" type="number" class="form-control " value="{{$targetRecord->pth_sum_amnt}}" readonly>
						</div>
						
					</div>
				
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="prh_no" class="placeholder">SGST TOTAL</label>
							<input  id="pth_sum_sgst" name="prh_no" type="number" class="form-control " value="{{$targetRecord->pth_sum_sgst}}" readonly>
						</div>
						
					</div>
						
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="prh_no" class="placeholder">CGST TOTAL</label>
							<input  id="pth_sum_cgst" name="pth_sum_cgst" type="number" class="form-control " value="{{$targetRecord->pth_sum_cgst}}" readonly>
						</div>
						
					</div>
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="prh_no" class="placeholder">GROSS SUM</label>
							<input  id="pth_gross_amnt" name="pth_gross_amnt" type="number" class="form-control " value="{{$targetRecord->pth_gross_amnt}}" readonly>
						</div>
						
					</div>
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="pth_ded_amnt" class="placeholder">DEDUCTION</label>
							<input  id="pth_ded_amnt" name="pth_ded_amnt" type="number" class="form-control " value="{{$targetRecord->pth_ded_amnt}}" >
						</div>
						
					</div>
					<div class="col-md-2">
					
						<div class="form-group">
							<label for="pth_net_amnt" class="placeholder">NET AMOUNT</label>
							<input  id="pth_net_amnt" name="pth_net_amnt" type="number" class="form-control " value="{{$targetRecord->pth_net_amnt}}" readonly>
						</div>
						
					</div>
					
		</div>



						
    </div>
				
					
                    <div class="tile-footer">
                        <a class="btn btn-primary a-save" data-draft="1"  style="color:white;" href="{{ route('admin.inventory.prod-receives.index') }}"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Receive</a>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.inventory.prod-receives.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
				
				
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function(){
	sel_item_type_id = 0;
	tdata = [];
	APP_URL = "{{ url('/') }}";		
	
	draft_id = "{{$targetRecord->id}}";
	
	products = '{!! $products !!}';		
	my_products = $.parseJSON(products);
	
	
    $('#table-product-list').DataTable();
	$('.dataTables_empty').remove();
	$('#target_store_id').focus();
	
		$("input").focusin(function()
		{
			$(this).select();
		});


		$(document).on('keypress',function(e) {
			if(e.which == 13) 
			{
				e.preventDefault();
				var tabindex = $(e.target).attr('tabindex');
				
				nextindex = (tabindex == 8)? 4: (+tabindex + 1);
				
				$('[tabindex=' + nextindex + ']').focus();

			}
		});
	
	
		$(document).on('click', '.a-save', function(event)
		{
			wh_draft = $(this).data('draft');
						
			collect_table_data();
			if($('#table-product-list tr').length >= 2)
			{
				if(wh_draft == 1)
				{
					if(confirm('Are you Sure to Receive ?'))
					{
						store(wh_draft);
					}
				}
			}
			else
			{
				alert('Please Add Items.');
			}
				
		});
		
		
		$(document).on('change', '#item_type_id', function()
		{
			sel_item_type_id = $(this).val();
			get_items_by_type_id();
			
			$('#product_id').trigger('change');
			
		});
		
		
		$(document).on('change', '#product_id', function()
		{
			 
			sel_product_id = $(this).val();
			
			get_item_by_id();
			
			$('#item_qty').trigger('keyup');
		});
		
		
		$(document).on('keyup', '#pth_ded_amnt', function()
		{
			 pth_gross_amnt = $("#pth_gross_amnt").val();
			 pth_ded_amnt = $("#pth_ded_amnt").val();
			 $("#pth_net_amnt").val(+pth_gross_amnt - +pth_ded_amnt).toFixed(2);
			
		});

		
		
		$(document).on('keyup', '#ptd_unit_qty, #ptd_unit_nos, #item_price, #item_gst', function()
		{
			var ptd_unit_qty = $('#ptd_unit_qty').val();
			var ptd_unit_nos = $('#ptd_unit_nos').val();
			
			ptd_unit_qty_gross = ( ptd_unit_qty * ptd_unit_nos).toFixed(2);
			
			$('#ptd_unit_qty_gross').val(ptd_unit_qty_gross);
			
			var item_price = $('#item_price').val();
			var item_gst = $('#item_gst').val();
			
			sum_amnt = ( ptd_unit_qty_gross * item_price).toFixed(2);
			
			gst_amnt = ((sum_amnt/100) * item_gst).toFixed(2);
			
			sgst_total = (gst_amnt/2).toFixed(2);
			cgst_total = (gst_amnt/2).toFixed(2);
			 
			grs_amnt = (+sum_amnt + +gst_amnt).toFixed(2);
			 
			 $('#grs_amnt').val(grs_amnt);
			 
			 
			
		});
		
		
		$(document).on('click', '.remove_item', function()
		{
			var item_index = $(this).attr('value');
			
			$('#table-product-list tr:eq(' + item_index + ')').remove();
			
			calculate_table_data();
		});
		
		
		$(document).on('click keypress', '#btn-add-to-list', function(event)
		{
			if( $('#ptd_unit_nos').val() > 0 && $('#ptd_unit_qty').val() > 0 )
			{
				
				var rowCount = $('#table-product-list tr').length;
															
					
				var item_row = '<tr>'+
						'<td id="ptd_srl'+ ( rowCount) + '" data-fldName="ptd_srl">'+ ( rowCount) +'</td>'+
						'<td data-fldName="ptd_product_id" style="display:none;">'+ $('#product_id option:selected').val() +'</td>'+
						'<td data-fldName="strn_item">'+ $('#product_id option:selected').text() +'</td>'+
						'<td data-fldName="ptd_item_unit_id" style="display:none;">'+ $('#item_unit_id').val() +'</td>'+
						'<td data-fldName="item_unit">'+ $('#item_unit_name').val() +'</td>'+
						'<td data-fldName="ptd_unit_qty">'+ $('#ptd_unit_nos').val() +'</td>'+
						'<td data-fldName="ptd_unit_nos">'+ $('#ptd_unit_nos').val() +'</td>'+
						'<td data-fldName="ptd_unit_qty_gross">'+ $('#ptd_unit_qty_gross').val() +'</td>'+
						'<td data-fldName="ptd_unit_qty_net">'+ $('#ptd_unit_qty_net').val() +'</td>'+
						'<td data-fldName="ptd_unit_rate">'+ $('#item_price').val() +'</td>'+
						'<td data-fldName="ptd_unit_sum_amnt">'+ sum_amnt +'</td>'+
						'<td data-fldName="ptd_unit_gst_rate">'+ $('#item_gst').val() +'</td>'+
						'<td data-fldName="unit_gst">'+ gst_amnt +'</td>'+
						'<td data-fldName="ptd_unit_gross_amnt">'+ grs_amnt +'</td>'+
						'<td value="' + ( rowCount + 1 ) + '" data-fldName="remove_item" class="text-center text-danger remove_item"><i class="fa fa-trash"> </i></td>'+
					'</tr>';
	
													
		   
				$("#table-product-list tbody").append(item_row);
	
		   
					   
				 calculate_table_data();
			}
			else
			{
				alert('Please enter Quantity and Nos.')
			}
			 
		});
		
		
		
		$('#product_id').trigger('change');


	});
	
	
	
	function get_item_by_id()
	{

		Object.keys(my_products).forEach(function(index)
		{
	
			var item = my_products[index];
			if(item.id == sel_product_id)
			{
			
				$('#item_unit_id').val(item.item_unit_id);
				$('#item_unit').html( '( Unit: ' + item.item_unit + ' )');
				$('#item_unit_name').val(item.item_unit);
				$('#item_price').val(item.item_price);
				$('#item_gst').val(item.item_gst);
				
				return false;	
			}
			
		});

	}
	
	
	function get_items_by_type_id()
	{
		var CSRF_TOKEN = '{{csrf_token()}}';

		var	url_var = APP_URL + '/get_items_by_type_id';
		
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['item_type_id'] = sel_item_type_id;
		
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: data,
		   async:false,
		   success:function(result_data)
			   {
				   
					$("#product_id").empty();
					var $select_product_id = $("#product_id");
						
					$.each(result_data, function() 
					{
						$select_product_id.append($("<option />").val(this.id).text(this.item_name));
					});					   
					   
					
				}
			});
			
	}

	
	function store(wh_draft)
	{
		var CSRF_TOKEN = '{{csrf_token()}}';
		
		var	url_var = APP_URL + '/admin/inventory/prod-receives/store' ;
				
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['target_store_id'] = $('#target_store_id').val();
		data['pth_date'] = $('#pth_date').val();
		data['pth_inv_no'] = $('#pth_inv_no').val();
		data['pth_sum_amnt'] = $('#pth_sum_amnt').val();
		data['pth_sum_sgst'] = $('#pth_sum_sgst').val();
		data['pth_sum_cgst'] = $('#pth_sum_cgst').val();
		data['pth_ded_amnt'] = $('#pth_ded_amnt').val();
		data['pth_net_amnt'] = $('#pth_net_amnt').val();
		data['pth_gross_amnt'] = $('#pth_gross_amnt').val();
		data['wh_credit'] = $('#wh_credit').val();
		data['pth_transfer_id'] = $('#pth_transfer_id').val();
				
		
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: {prhdata: data, unitdata: tdata},
		   headers: {'X-CSRF-TOKEN':  CSRF_TOKEN},
		   async:false,
		   success:function(result_data)
			   {
					alert('Received.');
					//location.reload();
				}
			});
			
	}
	
	
	function collect_table_data()
	{
		
		tdata = {};
		var rows = $('#table-product-list tr').length;
		
		$('#table-product-list tr').each(function (rowIndex, r) 
		{
			if (!this.rowIndex) return; // skip first row
			//if (this.rowIndex==1) return; // skip second row
			//if (this.rowIndex==(rows-1)) return; // skip footer row
			
			var row_number = rowIndex-1;
			tdata[row_number] = {};
			
				$(this).find('td').each(function (colIndex, c) 
				{
					var fldname = $(this).data('fldname');
					tdata[row_number][fldname] = c.textContent;
				});
			
			
			
			
		});	
		
	}	
	
	function calculate_table_data()
	{
		
		tdata = {};
		var rows = $('#table-product-list tr').length;
		var ptd_unit_sum_amnt = 0;
		var unit_gst = 0;
		var ptd_unit_gross_amnt = 0;
		
		$('#table-product-list tr').each(function (rowIndex, r) 
		{
					

			if (!this.rowIndex) return; // skip first row
			//if (this.rowIndex==1) return; // skip second row
			//if (this.rowIndex==(rows-1)) return; // skip footer row
			
			var row_number = rowIndex-2;
			var row_srl = rowIndex;
			
				$(this).find('td').each(function (colIndex, c) 
				{
					var fldname = $(this).data('fldname');
					
					fldname == 'ptd_srl' && $(this).text(row_srl);
					
					fldname == 'ptd_unit_sum_amnt' && (ptd_unit_sum_amnt += +c.textContent);	
	
					fldname == 'unit_gst' && (unit_gst += +c.textContent);
					
					fldname == 'ptd_unit_gross_amnt' && (ptd_unit_gross_amnt += +c.textContent);
					
					fldname == 'remove_product' && $(this).attr('value', +row_srl+1);
					
				});
			
		});	
		
				 $("#pth_sum_amnt").val(ptd_unit_sum_amnt.toFixed(2));				 
				 $("#pth_sum_sgst").val(unit_gst.toFixed(2)/2);
				 $("#pth_sum_cgst").val(unit_gst.toFixed(2)/2);
				 $("#pth_gross_amnt").val(ptd_unit_gross_amnt.toFixed(2));
				 
				 pth_gross_amnt = $("#pth_gross_amnt").val();
				 pth_ded_amnt = $("#pth_ded_amnt").val();
				 $("#pth_net_amnt").val((+pth_gross_amnt - +pth_ded_amnt).toFixed(2));
		
		
	}	
	
	
</script>
@endpush
