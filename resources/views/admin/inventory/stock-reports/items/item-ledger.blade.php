@extends('admin.inventory.acc-books.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
@php use Carbon\Carbon; @endphp
<style>
.sum-title
{
	font-weight:bold;
	color: #3a9629;
	
}
</style>
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
        <a href="{{ route('admin.inventory.stock-reports.items.index') }}" class="btn btn-primary pull-right">SELECT ITEM</a>
    </div>
    @include('admin.inventory.acc-books.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
			@include('admin.inventory.partials.dates-row-param')
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>	
								<th>DATE.</th>
								<th>TRNS.TYPE</th>
								<th>IN</th>
								<th>OUT</th>
								<th>BALANCE</th>
                            </tr>
                        </thead>
                        <tbody>
							@php  $bal_qty = 0; $cr_qty = 0; $dr_qty = 0;  @endphp
							@foreach($items_transaction_details as $key=>$items_transaction_detail) 
								
                                    <tr>
                                        <td>{{ Carbon::parse($items_transaction_detail->itd_date)->format('m-d-Y') }}</td>
                                        <td>{{ $items_transaction_detail->transaction_type }}</td>
										@if ( $items_transaction_detail->in_out == 1) 
											<td>{{ $items_transaction_detail->itd_unit_qty . ' ' . $items_transaction_detail->item_unit }}</td>
											@php  $bal_qty += $items_transaction_detail->itd_unit_qty  @endphp
											@php  $cr_qty += $items_transaction_detail->itd_unit_qty  @endphp
										@else
											<td></td>
										@endif
										
										@if ( $items_transaction_detail->in_out == 2 ) 
											<td>{{ $items_transaction_detail->itd_unit_qty . ' ' . $items_transaction_detail->item_unit }}</td>
											@php  $bal_qty -= $items_transaction_detail->itd_unit_qty  @endphp
											@php  $dr_qty += $items_transaction_detail->itd_unit_qty  @endphp
										@else
											<td></td>
										@endif
										
										
										
										
                                        <td>{{$bal_qty . ' ' . $itm_unit }}</td>
                                    </tr>
									
                            @endforeach
							
							
							
                        </tbody>
                    </table>
					
                </div>
				
					<div class="row pt-3">
						<div class="col-md-3">
							<p class="sum-title"> Total In: {{$cr_qty . ' ' . $itm_unit}} </p>
						</div>
						<div class="col-md-3">
							<p class="sum-title"> Total Out: {{$dr_qty . ' ' . $itm_unit}} </p>
						</div>
						<div class="col-md-3">
							<p class="sum-title"> Balance: {{$bal_qty . ' ' . $itm_unit}}</p>
						</div>
					</div>	
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#sampleTable').DataTable();

	});
	
</script>
@endpush
