@extends('admin.inventory.acc-books.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
@php use Carbon\Carbon; @endphp
<style>
.sum-title
{
	font-weight:bold;
	color: #3a9629;
	
}
</style>
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
        <a href="{{ route('admin.inventory.stock-reports.products.index') }}" class="btn btn-primary pull-right">SELECT PRODUCT</a>
    </div>
    @include('admin.inventory.acc-books.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
			@include('admin.inventory.partials.dates-row-param')
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>	
								<th style="display:none">id</th>
								<th>DATE.</th>
								<th>TRNS.TYPE</th>
								<th>IN</th>
								<th>OUT</th>
								<th>BALANCE</th>
                            </tr>
                        </thead>
                        <tbody>
							@php  $bal_qty = 0; $cr_qty = 0; $dr_qty = 0;  @endphp
							@foreach($products_transaction_details as $key=>$products_transaction_detail) 
								
                                    <tr>
                                        <td style="display:none">{{ $products_transaction_detail->id }}</td>
                                        <td>{{ Carbon::parse($products_transaction_detail->ptd_date)->format('m-d-Y') }}</td>
                                        <td>{{ $products_transaction_detail->transaction_type }}</td>
										@if ( $products_transaction_detail->in_out == 1) 
											<td>{{ $products_transaction_detail->ptd_unit_qty . ' ' . $products_transaction_detail->item_unit }}</td>
											@php  $bal_qty += $products_transaction_detail->ptd_unit_qty  @endphp
											@php  $cr_qty += $products_transaction_detail->ptd_unit_qty  @endphp
										@else
											<td></td>
										@endif
										
										@if ( $products_transaction_detail->in_out == 2) 
											<td>{{ $products_transaction_detail->ptd_unit_qty . ' ' . $products_transaction_detail->item_unit }}</td>
											@php  $bal_qty -= $products_transaction_detail->ptd_unit_qty  @endphp
											@php  $dr_qty += $products_transaction_detail->ptd_unit_qty  @endphp
										@else
											<td></td>
										@endif
										
                                        <td>{{$bal_qty . ' ' . $itm_unit }}</td>
                                    </tr>
									
                            @endforeach
							
							
							
                        </tbody>
                    </table>
					
                </div>
				
					<div class="row pt-3">
						<div class="col-md-3">
							<p class="sum-title"> Total In: {{$cr_qty . ' ' . $itm_unit}} </p>
						</div>
						<div class="col-md-3">
							<p class="sum-title"> Total Out: {{$dr_qty . ' ' . $itm_unit}} </p>
						</div>
						<div class="col-md-3">
							<p class="sum-title"> Balance: {{$bal_qty . ' ' . $itm_unit}}</p>
						</div>
					</div>	
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#sampleTable').DataTable();
		item_id = 0;
		
		$(document).on('click', '.a-delete', function(event)
		{
			item_id = $(this).attr('value');
 

				if(confirm('Are you sure to Delete this Purchase Draft ?'))
				{
					delete_draft();
					location.reload();
				}
			
		});
		

	});
	
	
	
	
	function delete_draft()
	{
		var CSRF_TOKEN = '{{csrf_token()}}';
		
		APP_URL = "{{ url('/') }}";		
		
		var	url_var = APP_URL + '/admin/inventory/purchases/delete_draft';
		
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['id'] = item_id;
						
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: data,
		   async:false,
		   success:function(result_data)
			   {
					alert('Deleted.');
				}
			});
			
	}
	
	
</script>
@endpush
