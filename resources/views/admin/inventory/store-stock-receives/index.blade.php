@extends('admin.inventory.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
    </div>
    @include('admin.inventory.partials.flash')
	<div class="row">
		 <div class="col-md-12 mx-auto">
			<div class="tile">
				<form action="{{ route('admin.inventory.store-stock-receives.search') }}" method="POST" role="form" id="stock_search_form">
					@csrf
					<div class="tile-body">
						<div class="form-row">
							<div class="col form-group">
								<label class="control-label" for="from">From</label>
								<input type="date" name="min_date" id="min_date" class="col-md-6 form-control" value ="{{ $from }}" placeholder="From:">
					
							</div>
							<div class="col form-group">
								<label class="control-label" for="to">To</label>
								<input type="date" name="max_date" id="max_date" class="col-md-6 form-control" value ="{{ $to }}"  placeholder="To:">

							</div>
							<div class="col form-group">
								<button type="submit" class="btn btn-primary">Search</button>
							</div>
						</div>
					</div>
				</form>
				<div class="col-md-12">
					<ul class="nav nav-tabs order-tabs">
						<li class="nav-item"><a class="nav-link active" href="#pending-transfer" data-toggle="tab">Pending</a></li>
						<li class="nav-item"><a class="nav-link" href="#confirmed-transfer" data-toggle="tab">Confirmed</a></li>
					</ul>
				</div>
				<div class="col-md-12">
					<div class="tab-content">
						<div class="tab-pane active" id="pending-transfer">
							@include('admin.inventory.store-stock-receives.partials.pending')
						</div>
						<div class="tab-pane fade" id="confirmed-transfer">
							@include('admin.inventory.store-stock-receives.partials.confirmed')
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
	$('#pendingTable').DataTable();
	$('#confirmTable').DataTable();
	
</script>
@endpush
