<div class="tile">
	<div class="tile-body">
		<table class="table table-hover table-bordered"  id="confirmTable">
			<thead>
				<tr>	
					<th class="text-center">#</th>
					<th class="text-center">TRANSFER SRL NO.</th>
					<th>DATE</th>
					<th>FROM</th>
					<th>TO</th>
					<th>AMOUNT</th>
					<th>PAYMENT</th>
					<th>STATUS</th>
					<th class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
				</tr>
			</thead>
			<tbody>
				@php 
					$i = 1;
				@endphp
				@foreach($confirmed as $ckey=>$cstock)
					<tr>
						<td class="text-center">{{ $i }}</td>
						<td class="text-center">{{ $cstock->ith_trn_no }}</td>
						<td>{{ $cstock->ith_date->format('m/d/Y H:i')}}</td>
						<td>{{ $cstock->store->store_name }}</td>
						<td>{{ $cstock->targetStore->store_name }}</td>
						<td>{{ $cstock->ith_net_amnt }}</td>
						<td>{{ $cstock->ith_wh_credit == 0 ? 'CASH':'CREDIT' }}</td>
						<td>{{ $cstock->store_receive_status == 1 ? 'RECEIVED':'NOT RECEIVED' }}</td>
						<td class="text-center">
							<div class="btn-group" role="group" aria-label="Second group">
								<a  href="{{ route('admin.inventory.store-stock-receives.show', $cstock->id) }}" class="btn btn-sm btn-primary">view</a>
							</div>
						</td>
					</tr>
					@php 
						$i++;
					@endphp
				@endforeach
			</tbody>
		</table>
	</div>
</div>
