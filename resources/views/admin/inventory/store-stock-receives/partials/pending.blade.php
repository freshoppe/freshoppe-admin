<div class="tile">
	<div class="tile-body">
		<table class="table table-hover table-bordered" id="pendingTable">
			<thead>
				<tr>	
					<th class="text-center">#</th>
					<th class="text-center">TRANSFER SRL NO.</th>
					<th>DATE</th>
					<th>FROM</th>
					<th>TO</th>
					<th>AMOUNT</th>
					<th>PAYMENT</th>
					<th>STATUS</th>
					<th class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
				</tr>
			</thead>
			<tbody>
				@php 
					$i = 1;
				@endphp
				@foreach($pending as $key=>$stock) 
					<tr>
						<td class="text-center">{{ $key + 1 }}</td>
						<td class="text-center">{{ $stock->ith_trn_no }}</td>
						<td>{{ $stock->ith_date->format('m/d/Y H:i')}}</td>
						<td>{{ $stock->store->store_name }}</td>
						<td>{{ $stock->targetStore->store_name }}</td>
						<td>{{ $stock->ith_net_amnt }}</td>
						<td>{{ $stock->ith_wh_credit == 0 ? 'CASH':'CREDIT' }}</td>
						<td>{{ $stock->store_receive_status == 1 ? 'RECEIVED':'NOT RECEIVED' }}</td>
						<td class="text-center">
							<div class="btn-group" role="group" aria-label="Second group">
								<a  href="{{ route('admin.inventory.store-stock-receives.show', $stock->id) }}" class="btn btn-sm btn-primary">view</a>
							</div>
						</td>
					</tr>
					@php 
						$i++;
					@endphp
				@endforeach
			</tbody>
		</table>
	</div>
</div>

