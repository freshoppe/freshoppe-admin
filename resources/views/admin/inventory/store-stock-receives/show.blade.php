@extends('admin.inventory.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.inventory.partials.flash')
    <form action="{{ route('admin.inventory.store-stock-receives.update') }}" method="POST" role="form" id="stock_update_form">
		@csrf
		<input type="hidden" value="{{$targetRecord->id}}" name="targetId" id="targetId">
		<div class="row">
			<div class="col-md-12 mx-auto">
				<div class="tile">
					<h3 class="tile-title">{{ $subTitle }}</h3>
					<div class="tile-header">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="from">FROM</label>
									<input id="from" name="from" type="text" class="form-control " value="{{$targetRecord->store->store_name}}" readonly>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="to">TO</label>
									<input id="to" name="to" type="text" class="form-control " value="{{$targetRecord->targetStore->store_name}}" readonly>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="ith_date" class="placeholder">TRANSFER DATE</label>
									<input readonly id="ith_date" name="ith_date" type="date" class="form-control " value="{{ $targetRecord->ith_date->format('Y-m-d') }}">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="ith_inv_no" class="placeholder">INVOICE NO.</label>
									<input readonly id="ith_inv_no" name="ith_inv_no" type="text" class="form-control " value="{{$targetRecord->ith_inv_no}}">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="prh_no" class="placeholder">TRANSFER SRL NO.</label>
									<input  id="prh_no" name="prh_no" type="text" class="form-control " value="{{$targetRecord->ith_trn_no}}" readonly>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="ith_wh_credit">CASH/CREDIT</label>
									<input id="ith_wh_credit" name="ith_wh_credit" type="text" class="form-control " value="{{$targetRecord->ith_wh_credit == 0 ? 'CASH':'CREDIT'}}" readonly>
								</div>
							</div>
							
							
							
						</div>
					</div><!--ends tile-header-->
				</div><!--ends tile1 -->
			</div><!--ends col-md-12 -->
		</div><!--ends row -->
							
							
		<div class="row"><!--begins list row -->
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body">
						<div class="table-responsive table-hover table-sales">
							<table id="table-item-list" class="table table-hover table-bordered" id="sampleTable" style="width:100%">
								<thead>
									<tr>
										<th style="border-bottom: 1px solid #35cd3a!important">SRL.NO.</th>
										<th style="border-bottom: 1px solid #35cd3a!important">ITEM NAME</th>
										<th style="border-bottom: 1px solid #35cd3a!important">UNIT</th>
										<th style="border-bottom: 1px solid #35cd3a!important">QTY</th>
										<th style="border-bottom: 1px solid #35cd3a!important">RATE</th>
										<th style="border-bottom: 1px solid #35cd3a!important">SUM</th>
										<th style="border-bottom: 1px solid #35cd3a!important">GST%</th>
										<th style="border-bottom: 1px solid #35cd3a!important">GST</th>
										<th style="border-bottom: 1px solid #35cd3a!important">GROSS</th>
										@if($targetRecord->store_receive_status == 0)
										<th class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
										@endif
									</tr>
								</thead>
								<tbody >
									@foreach($targetRecord->transactionDetails as $key=>$transaction_detail) 
									<tr>
										<td data-fldName="itd_srl" style="border-bottom: 1px solid #35cd3a!important">{{$transaction_detail->itd_srl}}</td>
										<td data-fldName="strn_item" style="border-bottom: 1px solid #35cd3a!important">{{$transaction_detail->item->item_name}}</td>
										<td data-fldName="item_unit" style="border-bottom: 1px solid #35cd3a!important">{{$transaction_detail->itemUnit->item_unit}}</td>
										<td data-fldName="itd_unit_qty" class="col-md-2" style="border-bottom: 1px solid #35cd3a!important">
											@if($targetRecord->store_receive_status == 0)
											<input onkeyup="return updatePriceValues({{$transaction_detail->id}},this.value)" id="item_qty_{{$transaction_detail->id}}" name="item_qty[{{$transaction_detail->id}}]" type="text" class="form-control item_qty" value="{{$transaction_detail->itd_unit_qty}}" required>
											@else
												{{$transaction_detail->itd_unit_qty}}
											@endif
											
										</td>
										<td data-fldName="itd_unit_rate" style="border-bottom: 1px solid #35cd3a!important">
											@if($targetRecord->store_receive_status == 0)
											<input id="itd_unit_rate_{{$transaction_detail->id}}" type="text" class="form-control" value="{{$transaction_detail->itd_unit_rate}}" readonly>
											@else
												{{$transaction_detail->itd_unit_rate}}
											@endif
										
										</td>
										<td data-fldName="itd_unit_sum_amnt" style="border-bottom: 1px solid #35cd3a!important">
											@if($targetRecord->store_receive_status == 0)
											<input id="itd_unit_sum_amnt_{{$transaction_detail->id}}" name="itd_unit_sum_amnt[{{$transaction_detail->id}}]" type="text" class="form-control itd_unit_sum_amnt" value="{{$transaction_detail->itd_unit_sum_amnt}}" readonly>
											@else
												{{$transaction_detail->itd_unit_sum_amnt}}
											@endif
										
										</td>
										<td data-fldName="itd_unit_gst_rate" style="border-bottom: 1px solid #35cd3a!important">
											@if($targetRecord->store_receive_status == 0)
											<input id="itd_unit_gst_rate_{{$transaction_detail->id}}" type="text" class="form-control" value="{{$transaction_detail->itd_unit_gst_rate}}" readonly>
											@else
												{{$transaction_detail->itd_unit_gst_rate}}
											@endif
										</td>
										<td data-fldName="itd_unit_sgst" style="border-bottom: 1px solid #35cd3a!important">
											@if($targetRecord->store_receive_status == 0)
											<input id="itd_unit_sgst_{{$transaction_detail->id}}" name="itd_unit_sgst[{{$transaction_detail->id}}]" type="text" class="form-control itd_unit_sgst" value="{{$transaction_detail->itd_unit_sgst * 2}}" readonly>
											@else
												{{($transaction_detail->itd_unit_sgst * 2)}}
											@endif
											
											
										</td>
										<td data-fldName="itd_unit_gross_amnt" style="border-bottom: 1px solid #35cd3a!important">
											@if($targetRecord->store_receive_status == 0)
											<input id="itd_unit_gross_amnt_{{$transaction_detail->id}}" name="itd_unit_gross_amnt[{{$transaction_detail->id}}]" type="text" class="form-control itd_unit_gross_amnt" value="{{$transaction_detail->itd_unit_gross_amnt}}" readonly>
											@else
												{{$transaction_detail->itd_unit_gross_amnt}}
											@endif
										</td>
										@if($targetRecord->store_receive_status == 0)
										<td data-fldName="remove_item" class="text-center text-danger remove_item"><i class="fa fa-trash"> </i></td>
										@endif
									</tr>
									@endforeach
								</tbody>
							</table>												
						</div>
					</div>
				</div>
			</div>
		</div><!--ends list row -->

		<div class="row tile" style="padding: 20px!important; border-radius:3px;">
			<div class="col-md-2">
				<div class="form-group">
					<label for="ith_sum_amnt" class="placeholder">SUM TOTAL</label>
					<input  id="ith_sum_amnt" name="ith_sum_amnt" type="text" class="form-control " value="{{$targetRecord->ith_sum_amnt}}" readonly>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label for="ith_sum_sgst" class="placeholder">SGST TOTAL</label>
					<input  id="ith_sum_sgst" name="ith_sum_sgst" type="text" class="form-control " value="{{$targetRecord->ith_sum_sgst}}" readonly>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label for="ith_sum_cgst" class="placeholder">CGST TOTAL</label>
					<input  id="ith_sum_cgst" name="ith_sum_cgst" type="text" class="form-control " value="{{$targetRecord->ith_sum_cgst}}" readonly>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label for="ith_gross_amnt" class="placeholder">GROSS SUM</label>
					<input  id="ith_gross_amnt" name="ith_gross_amnt" type="text" class="form-control " value="{{$targetRecord->ith_gross_amnt}}" readonly>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label for="ith_ded_amnt" class="placeholder">DEDUCTION</label>
					<input  id="ith_ded_amnt" name="ith_ded_amnt" type="text" class="form-control " value="{{$targetRecord->ith_ded_amnt}}" readonly>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label for="ith_net_amnt" class="placeholder">NET AMOUNT</label>
					<input  id="ith_net_amnt" name="ith_net_amnt" type="text" class="form-control " value="{{$targetRecord->ith_net_amnt}}" readonly>
				</div>
			</div>
		</div>
		<div class="tile-footer">
			@if($targetRecord->store_receive_status == 0)
			<a class="btn btn-primary a-save" receive-status="0" style="color:white;" ><i class="fa fa-fw fa-lg fa-check-circle"></i>Confirm Receive</a>
			@endif
			&nbsp;&nbsp;&nbsp;
			<a class="btn btn-secondary" href="{{ route('admin.inventory.store-stock-receives.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Back</a>
		</div>
	</form>
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function(){
	$('.remove_item').click(function(){
		$(this).closest('tr').remove();
		calculate_total_values();
	});
	APP_URL = "{{ url('/') }}";		
	transfer_id = "{{$targetRecord->id}}";
	
	 $('.a-save').click(function(){
		
		bootbox.confirm({
			message: 'Are you sure to confirm this transfer?',
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if(result == true){
					$('#stock_update_form').submit();
					/* var CSRF_TOKEN = '{{csrf_token()}}';
					var	url_var = APP_URL + '/admin/inventory/store/stock-receives/confirm';
					$.ajax({
						type:'post',
						url: url_var,
						data: {'transfer_id': transfer_id},
						headers: {'X-CSRF-TOKEN':  CSRF_TOKEN},
						async:false,
						success:function(response){
							bootbox.alert('Stock received');
							location.reload();
						}
					}); */
				}
			}
		});
	});
});	
function updatePriceValues(id,value){
	var item_qty 	= value;
	var item_price 	= $('#itd_unit_rate_'+id).val();
	var item_gst 	= $('#itd_unit_gst_rate_'+id).val();

	sum_amnt = item_qty * item_price;
	$('#itd_unit_sum_amnt_'+id).val(sum_amnt.toFixed(2));
	gst_amnt = ((sum_amnt/100) * item_gst).toFixed(2);
	$('#itd_unit_sgst_'+id).val(gst_amnt);

	grs_amnt = (+sum_amnt + +gst_amnt).toFixed(2);

	$('#itd_unit_gross_amnt_'+id).val(grs_amnt);
	
	calculate_total_values();
	
}


function calculate_total_values(){
	
	var sum_amnt = 0;
	$( ".itd_unit_sum_amnt" ).each(function() {
	  sum_amnt += parseFloat(this.value);
	console.log(sum_amnt)
	});
	$('#ith_sum_amnt').val(sum_amnt.toFixed(2));
	
	var gst_sum_amnt = 0;
	$( ".itd_unit_sgst" ).each(function() {
	  gst_sum_amnt += parseFloat(this.value);
	});
	sgst_total = (gst_sum_amnt/2);
	cgst_total = (gst_sum_amnt/2);
	
	$('#ith_sum_sgst').val(sgst_total.toFixed(2));
	$('#ith_sum_cgst').val(cgst_total.toFixed(2));
	
	
	var gross_amnt = 0;
	$( ".itd_unit_gross_amnt" ).each(function() {
	  gross_amnt += parseFloat(this.value);
	});
	$('#ith_gross_amnt').val(gross_amnt.toFixed(2));
	
	var ith_ded_amnt = $("#ith_ded_amnt").val();
	$("#ith_net_amnt").val((+gross_amnt - +ith_ded_amnt).toFixed(2));
	
	
}

</script>
@endpush
