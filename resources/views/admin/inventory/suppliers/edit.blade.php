@extends('admin.inventory.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.inventory.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
				
				
				
                <form action="" enctype="multipart/form-data">
                    <div class="tile-body">
					
						
                        <div class="form-group">
                            <label class="control-label" for="supplier_name">SUPPLIER NAME <span class="m-l-5 text-danger"> *</span></label>
							<input  value="{{$targetSupplier->head_name}}" class="form-control req-field" type="text" id="supplier_name" name="supplier_name"  oninput="this.value = this.value.toUpperCase()">
                           <p id="err_tag_supplier_name"  style="display:none;" class="text-danger" >required field</p>
                        </div>
						
                        <div class="form-group">
                            <label class="control-label" for="supplier_address">SUPPLIER ADDRESS </label>
							<input  value="{{$targetSupplier->supplier_address}}" class="form-control " type="text" id="supplier_address" name="supplier_address"  oninput="this.value = this.value.toUpperCase()">
                           <p id="err_tag_supplier_name"  style="display:none;" class="text-danger" >required field</p>
                        </div>
						
												
                    </div>
					
                    <div class="tile-footer">
                        <a class="btn btn-primary" id="a-save" style="color:white;"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Supplier</a>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.inventory.suppliers.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
				
				
				
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function()
	{
		edit_id = '{{$targetSupplier->id}}';
	
		$('#item_code').focus();
		
		$(document).on('click', '#a-save', function(event)
		{
			
			$('.req-field').each(function (i) {
				if ($(this).val() == '') 
				{
					$(this).addClass('is-invalid')
					var id = $(this).attr('id');
					$('#err_tag_' + id).show()
				}
				else
				{
					$(this).removeClass('is-invalid')
					var id = $(this).attr('id');
					$('#err_tag_' + id).hide()
				}
			});		
			
			var valid = $('.req-field').filter(function () 
			{
					return this.value != '';
			});		


			if(valid.length > 0)
			{
				if(confirm('Are you sure to Save this item ?'))
				{
					update();
					location.reload();
				}
			}
			
			
				
			
		});
		

	});
	
	
	function validate_fields()
	{
			var count = $('.req-field').length;
			$('.req-field').each(function (i) {
				if ($(this).val() == '') 
				{
					$(this).addClass('is-invalid')
					var id = $(this).attr('id');
					$('#err_tag_' + id).show()
					var valid = false;
					if((i+1)===count)
					{
						
					}
				}
			});		
		
	}
	
	
	function update()
	{
		var CSRF_TOKEN = '{{csrf_token()}}';
		
		APP_URL = "{{ url('/') }}";		
		
		var	url_var = APP_URL + '/admin/inventory/suppliers/update';
		
		var data = {};
		data['_token'] = CSRF_TOKEN;
		data['id'] = edit_id;
		data['supplier_name'] = $('#supplier_name').val();
		data['supplier_address'] = $('#supplier_address').val();
						
		$.ajax({
		   type:'post',
		   url: url_var,
		   data: data,
		   async:false,
		   success:function(result_data)
			   {
					if(result_data == 0)
					{
						alert('Name already Exists.');
					}
					else
					{
						alert('Saved.');
						window.location.href = "{{ url('/admin/inventory/suppliers') }}";
					}
				}
			});
			
	}
	
	
</script>
@endpush
