@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.locations.update') }}" method="POST" role="form" id="locationForms">
                    @csrf
                    <div class="tile-body">
                        <div class="form-group">
                            <label class="control-label" for="location_name">Name <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('location_name') is-invalid @enderror" type="text" name="location_name" id="location_name" value="{{ old('location_name', $targetLocation->location_name) }}"/>
                            <input type="hidden" name="id" value="{{ $targetLocation->id }}">
                            @error('location_name') {{ $message }} @enderror
                        </div>
						<div class="form-group">
                            <label class="control-label" for="location_code">Code <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('location_code') is-invalid @enderror" type="text" name="location_code" id="location_code" value="{{ old('location_code', $targetLocation->location_code) }}"/>
                            @error('location_code') {{ $message }} @enderror
                        </div>
						<div class="form-group">
                            <label class="control-label" for="city">City <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('city') is-invalid @enderror" type="text" name="city" id="city" value="{{ old('city', $targetLocation->city) }}"/>
                            @error('city') {{ $message }} @enderror
                        </div>
						<div class="form-group">
                            <label class="control-label" for="state">State <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('state') is-invalid @enderror" type="text" name="state" id="state" value="{{ old('state', $targetLocation->state) }}"/>
                            @error('state') {{ $message }} @enderror
                        </div>
						<div class="form-group">
                            <label class="control-label" for="pincode">Pincode <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('pincode') is-invalid @enderror" type="text" name="pincode" id="pincode" value="{{ old('pincode', $targetLocation->pincode) }}"/>
                            @error('pincode') {{ $message }} @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="status" name="status"
                                    {{ $targetLocation->status == 1 ? 'checked' : '' }}
                                    />Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Location</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.locations.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

@endpush
