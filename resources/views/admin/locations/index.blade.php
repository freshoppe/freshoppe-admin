@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
        <a href="{{ route('admin.locations.create') }}" class="btn btn-primary pull-right">Add Location</a>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Name </th>
                                <th> Code </th>
                                <th> City </th>
                                <th> State </th>
                                <th> Pincode </th>
                                <th class="text-center"> Active </th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($locations as $key=>$location)
								<tr>
									<td>{{ $key+1 }}</td>
									<td>{{ $location->location_name }}</td>
									<td>{{ $location->location_code }}</td>
									<td>{{ $location->city }}</td>
									<td>{{ $location->state }}</td>
									<td>{{ $location->pincode }}</td>
									<td class="text-center">
										@if ($location->status == 1)
											<span class="badge badge-success">Yes</span>
										@else
											<span class="badge badge-danger">No</span>
										@endif
									</td>
									<td class="text-center">
										<div class="btn-group" role="group" aria-label="Second group">
											<a href="{{ route('admin.locations.edit', $location->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
										</div>
									</td>
								</tr>
						@endforeach
					</tbody>
				</table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endpush
