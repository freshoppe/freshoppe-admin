@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-bar-chart"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.orders.update') }}" method="POST" role="form" id="offline_order_form">
                    @csrf
                    <div class="tile-body">
						<div class="form-row">
							<input type="hidden" name="order_id" value="{{ $order->id }}">
							<div class="col form-group">
								<label class="control-label" for="customer_name">Customer Name </label>
								<input class="form-control @error('customer_name') is-invalid @enderror" type="text" name="customer_name" id="customer_name" value="{{ old('customer_name', $order->user->name) }}"/>
								@error('customer_name') {{ $message }} @enderror
							</div>
							<div class="col form-group">
								<label class="control-label" for="mobile_number">Mobile Number <span class="m-l-5 text-danger"> *</span></label>
								<input class="form-control @error('mobile_number') is-invalid @enderror" type="text" name="mobile_number" id="mobile_number" value="{{ old('mobile_number', $order->user->mobile) }}"/>
								@error('mobile_number') {{ $message }} @enderror
							</div>
						</div>
						<div class="form-row">
							<div class="col form-group">
								<label class="control-label" for="location">Location </label>
								<input class="form-control @error('location') is-invalid @enderror" type="text" name="location" id="location" value="{{ old('location',$order->shipAddress->flat) }}"/>
								@error('location') {{ $message }} @enderror
							</div>
							<div class="col form-group">
								<label class="control-label" for="city">City </label>
								<input class="form-control @error('city') is-invalid @enderror" type="text" name="city" id="city" value="{{ old('city',$order->shipAddress->city ) }}"/>
								@error('city') {{ $message }} @enderror
							</div>
						</div>
						<div class="form-row">
							<div class="col form-group">
								<label class="control-label" for="address">Address </label>
								<textarea name="address" id="address" rows="4" class="form-control @error('address') is-invalid @enderror">{{ old('address',$order->shipAddress->address) }}</textarea>
								@error('address') {{ $message }} @enderror
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<h3>Products</h3>
									<product-search-component :orderid="{{$order->id}}" ></product-search-component>
								</div>
							</div>
						</div>
						<!--<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label class="control-label" for="payment_type">Payment Method</label>
									<select name="payment_type" id="payment_type" class="form-control">
										@foreach($paymentTypes as $payment)
											<option value="{{ $payment->id }}">{{ strtoupper($payment->payment_type) }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>-->
						
                    </div>
					
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Order</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.orders.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset(mix('backend/js/app.js')) }}"></script>
<script>
    $('.select2').select2();
</script>
@endpush
