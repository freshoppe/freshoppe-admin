<div class="tile">
	<div>
		<h2>Cancelled</h2>
	</div>
	<div class="tile-body">
		<table class="table table-hover table-bordered" id="cancelledOrderTable">
			<thead>
				<tr>
					<th> Order Number </th>
					<th> Type </th>
					<th> Customer </th>
					<th> Mobile </th>
					<th> Date </th>
					<th class="text-center"> Amount </th>
					<th class="text-center"> Payment </th>
					<th class="text-center"> Status </th>
					<th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
				</tr>
			</thead>
			<tbody>
			@foreach($cancelled as $order)
				<tr>
					<td>{{ $order->order_number }}</td>
					<td>{{ ($order->order_type == 1) ?  'Online':'Offline' }}</td>
					<td>{{ $order->user->name }}</td>
					<td>{{ $order->user->mobile }}</td>
					<td>{{ date('d/m/Y',strtotime($order->created_at))}}</td>
					<td class="text-center">{{ config('settings.currency_symbol') }}{{ round($order->payments->grand_total, 2) }}</td>
					<td class="text-center">
						@if ($order->payments->payment_status == 1)
							<span class="badge badge-success">Completed</span>
						@else
							<span class="badge badge-danger">Not Completed</span>
						@endif
					</td>
					<td class="text-center">
						<span class="badge badge-success">Cancelled</span>
					</td>
					<td class="text-center">
						<div class="btn-group" role="group" aria-label="Second group">
							<a href="{{ route('admin.orders.show', $order->order_number) }}" class="btn btn-sm btn-primary">view</a>
						</div>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>