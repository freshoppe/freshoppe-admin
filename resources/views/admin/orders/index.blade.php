@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-bar-chart"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
		<a href="{{ route('admin.orders.create') }}" class="btn btn-primary pull-right">New Order</a>
    </div>
    <div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs order-tabs" id="orderTab" role="tablist">
				@can('order-processing')
				<li class="nav-item"><a class="nav-link active" data-bs-toggle="tab" href="#new-order" data-toggle="tab">New</a></li>
				<li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#processing-order" data-toggle="tab">On Process</a></li>
				<li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#despatched-order" data-toggle="tab">Dispatched</a></li>
				<li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#delivered-order" data-toggle="tab">On Delivery</a></li>
				<li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#completed-order" data-toggle="tab">Completed</a></li>
				<li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#cancelled-order" data-toggle="tab">Cancelled</a></li>
				<li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#declined-order" data-toggle="tab">Declined</a></li>
				@endcan
			</ul>
        </div>
		<div class="col-md-12">
            <div class="tab-content">
				@can('order-processing')
                <div class="tab-pane active" id="new-order">
                    @include('admin.orders.includes.new')
                </div>
				<div class="tab-pane fade" id="processing-order">
                    @include('admin.orders.includes.processing')
                </div>
				<div class="tab-pane fade" id="despatched-order">
                    @include('admin.orders.includes.despatched')
                </div>
				<div class="tab-pane fade" id="delivered-order">
                    @include('admin.orders.includes.delivery')
                </div>
				<div class="tab-pane fade" id="completed-order">
                    @include('admin.orders.includes.completed')
                </div>
				<div class="tab-pane fade" id="cancelled-order">
                    @include('admin.orders.includes.cancelled')
                </div>
				<div class="tab-pane fade" id="declined-order">
                    @include('admin.orders.includes.declined')
                </div>
				@endcan
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
	$('#newOrderTable').DataTable();
	$('#processingOrderTable').DataTable();
	$('#despatchedOrderTable').DataTable();
	$('#deliveryOrderTable').DataTable();
	$('#completedOrderTable').DataTable();
	$('#cancelledOrderTable').DataTable();
	$('#declinedOrderTable').DataTable();
	$('#billingOrderTable').DataTable();
	
	function updateStatus(orderId,orderStatus){
		var msg = '';
		if(orderStatus == 2)
			msg = 'Are you sure to move this order to processing?';
		else if(orderStatus == 3)
			msg = 'Are you sure to dispatch this order?';
		else if(orderStatus == 4)
			msg = 'Are you sure to move this order to delivery?';
		else if(orderStatus == 5)
			msg = 'Are you sure to move this order to completed?';
		else if(orderStatus == 6)
			msg = 'Are you sure to decline this order?';
		else if(orderStatus == 7)
			msg = 'Are you sure to cancel this order?';
		bootbox.confirm({
			message: msg,
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if(result == true){
					$.ajax({
						type: "POST",
						url: '{{route('admin.orders.update.status')}}',
						data:{
								"_token": "{{ csrf_token() }}",
								"orderId": orderId,
								"orderStatus": orderStatus
							},
						success: (response) => {
							$.notify({
								message: response.message,
								icon: 'flaticon-bar-chart'
							},{
								type: 'success',
								allow_dismiss: true,
								placement: {
									from: "top",
									align: "right"
								},
							});
							
							window.location.reload();
						},
						error: (response) => {
							$.notify("Something went wrong!");
						}
					});
				}
			}
		});
		/* $.ajax({
            type: "POST",
            url: '{{route('admin.orders.update.status')}}',
            data:{
					"_token": "{{ csrf_token() }}",
					"orderId": orderId,
					"orderStatus": orderStatus
				},
            success: (response) => {
				$.notify({
					message: response.message,
					icon: 'flaticon-bar-chart'
				},{
					type: 'success',
					allow_dismiss: true,
					placement: {
						from: "top",
						align: "right"
					},
				});
				
				window.location.reload();
			},
			error: (response) => {
				$.notify("Something went wrong!");
			}
        }); */
	}
	
	</script>
@endpush
