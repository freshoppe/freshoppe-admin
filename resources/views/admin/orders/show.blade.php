@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-bar-chart"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <section class="invoice">
					<div class="row mb-4">
						<div class="col-6">
							<h2 class="page-header"><i class="fa fa-globe"></i> {{ $order->order_number }}</h2>
						</div>
						<div class="col-6">
							<h5 class="text-right">Date: {{ $order->created_at->toDateTimeString() }}</h5>
						</div>
					</div>
					<div class="row invoice-info">
						<div class="col-4">Placed By
							<address><strong>{{ $order->user->name }}</strong><br>Email: {{ $order->user->email }}</address>
						</div>
						<div class="col-4">Ship To
							<address>
								<strong>{{ $order->user->name }}</strong><br>
								@if($order->shipAddress->address != NULL)
									{{ $order->shipAddress->address }}<br>
								@endif
								@if($order->shipAddress->flat != NULL)
									{{ $order->shipAddress->flat }}<br>
								@endif
								@if($order->shipAddress->city != NULL)
									{{ $order->shipAddress->city }}<br>
								@endif
								@if($order->shipAddress->mobile != NULL)
									{{ $order->shipAddress->mobile }}<br>
								@endif
							</address>
						</div>
						<div class="col-4">
							<b>Order ID:</b> {{ $order->order_number }}<br>
							@php $orderStatus = Config::get('constants.orderStatus'); @endphp
							<b>Order Status:</b> {{$orderStatus[$order->order_status]}}  <br>
							<b>Order Type:</b> @if($order->order_type == 1 ) Online @else Offline @endif<br>
						</div>
					</div>
					<div class="row">
						<div class="col-12 table-responsive">
							<table class="table table-striped">
								<thead>
								<tr>
									<th>No</th>
									<th>Product</th>
									<th>Number</th>
									<th class="text-center">GST(%)</th>
									<th>Nt Wt</th>
									<th>Gr Wt</th>
									<th class="text-right">Price</th>
									<th class="text-center">Qty</th>
									<th class="text-right">Amount</th>
								</tr>
								</thead>
								<tbody>
									@foreach($order->items as $key=>$item)
										<tr>
											<td>{{ $key + 1 }}</td>
											<td>{{ $item->product->name }} 
											@if($item->product->processing !='')
											- {{ $item->product->processing->processing_name }}
											@endif
											</td>
											<td>{{ $item->product->number }}</td>
											<td class="text-center">{{ $item->gst_rate }}</td>
											<td>{{ $item->net_weight }}</td>
											<td>{{ $item->gross_weight }}</td>
											<td class="text-right">{{ config('settings.currency_symbol') }}{{ $item->price }}</td>
											<td class="text-center">{{ $item->quantity }}</td>
											<td class="text-right">{{ config('settings.currency_symbol') }}{{ $item->sub_total }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="row invoice-info">
						<div class="col-4">
							<b>Sub Total:</b> {{ config('settings.currency_symbol') }}{{ $order->payments->sub_total }}<br>
							<b>Tax (%):</b> {{ $order->payments->gst_rate }}<br>
							<b>Tax Amount:</b> {{ config('settings.currency_symbol') }}{{ $order->payments->gst_total }}<br>
						</div>
						<div class="col-4">
							<b>Discount (%):</b>{{ $order->payments->discount_rate }}<br>
							<b>Discount Amount:</b> {{ config('settings.currency_symbol') }}{{ $order->payments->discount_total }}<br>
							
						</div>
						<div class="col-4">
							<b>Total Amount:</b> {{ config('settings.currency_symbol') }}{{ $order->payments->grand_total }}<br><br>
							<input type="hidden" value="{{$order->payments->grand_total}}" id="grand_total">
							@if($order->payments->payment_type_id != NULL)
							<b>Payment Method:</b> {{ strtoupper($order->payments->paymentType->payment_type) }}<br>
							@endif
							<b>Payment Status:</b> {{ $order->payments->payment_status == 1 ? 'Completed' : 'Not Completed' }}<br>
						</div>
					</div>
					<div class="tile-footer">
						<a class="btn btn-primary" href="javascript:history.back()"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Back</a>
					</div>
                </section>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@endpush