<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu metismenu" id="metismenu">
		<li class="{{ Route::currentRouteName() == 'admin.dashboard' ? 'mm-active' : '' }}">
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.dashboard' ? 'active' : '' }}" href="{{ route('admin.dashboard') }}" aria-expanded="false" >
				<i class="app-menu__icon fa fa-dashboard"></i> Dashboard <span class="icon-fa arrow icon-fa-fw"></span>
			</a>
        </li>
		@if(Auth::user('admin')->hasRole('super-admin','wh-administrator','wh-manager','store-keeper','e-com-manager','it-manager'))
		<li>
            <a class="has-arrow app-menu__item"  href="#" aria-expanded="false" >
				<i class="app-menu__icon fa fa-dashboard"></i> Warehouse <span class="icon-fa arrow icon-fa-fw"></span>
			</a>
			<ul>
				@if(Auth::user('admin')->hasRole('super-admin','it-manager'))
					<li class="{{set_active('admin/locations*','mm-active')}}">
						<a class="app-menu__item {{set_active('admin/locations*','active')}}"  href="{{ route('admin.locations.index') }}">
							<i class="app-menu__icon fa fa-map-marker"></i><span class="app-menu__label">Locations</span>
						</a>
					</li>
					<li class="{{set_active('admin/warehouses*','mm-active')}}">
						<a class="app-menu__item {{set_active('admin/warehouses*','active')}}"  href="{{ route('admin.warehouses.index') }}">
							<i class="app-menu__icon fa fa-industry"></i><span class="app-menu__label">Warehouses</span>
						</a>
					</li>
					<li class="{{set_active('admin/stores*','mm-active')}}">
						<a class="app-menu__item {{set_active('admin/stores*','active')}}" href="{{ route('admin.stores.index') }}">
							<i class="app-menu__icon fa fa-building"></i>
							<span class="app-menu__label">Stores</span>
						</a>
					</li>
				@endif
				@if(Auth::user('admin')->hasRole('super-admin','wh-manager'))
				@can('item-master')
				<li class="{{set_active('admin/inventory/items*','mm-active')}}">
					<a class="app-menu__item {{set_active('admin/inventory/items*','active')}}" href="{{ route('admin.inventory.items.index') }}">
						<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Items</span> 
					</a>        
				</li>
				@endcan
				@endif
				@if(Auth::user('admin')->hasRole('super-admin','wh-manager','store-keeper','e-com-manager'))
				<li class="{{set_active('admin/products*','mm-active')}}">
					<a class="app-menu__item {{set_active('admin/products*','active')}}" href="{{ route('admin.products.index') }}">
						<i class="app-menu__icon fa fa-shopping-bag"></i>
						<span class="app-menu__label">Products</span>
					</a>
				</li>
				@endif
				@if(Auth::user('admin')->hasRole('super-admin','wh-manager'))
				<li class="{{set_active('admin/categories*','mm-active')}}">
					<a class="app-menu__item {{set_active('admin/categories*','active')}}" href="{{ route('admin.categories.index') }}">
						<i class="app-menu__icon fa fa-tags"></i>
						<span class="app-menu__label">Categories</span>
					</a>
				</li>
				@endif
				@if(Auth::user('admin')->hasRole('super-admin','wh-manager'))
				<li class="{{set_active('admin/inventory/suppliers*','mm-active')}}">
					<a class="app-menu__item {{set_active('admin/inventory/suppliers*','active')}}" href="{{ route('admin.inventory.suppliers.index') }}">
						<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Suppliers</span>
					</a>        
				</li>
				<li class="{{set_active('admin/processing-ways*','mm-active')}}">
					<a class="app-menu__item {{set_active('admin/processing-ways*','active')}}" href="{{ route('admin.processing.index') }}">
						<i class="app-menu__icon fa fa-cutlery"></i>
						<span class="app-menu__label">Processing Ways</span>
					</a>
				</li>
				<li class="{{set_active('admin/attributes*','mm-active')}}">
					<a class="app-menu__item {{set_active('admin/attributes*','active')}}" href="{{ route('admin.attributes.index') }}">
						<i class="app-menu__icon fa fa-th"></i>
						<span class="app-menu__label">Attributes</span>
					</a>
				</li>
				@endif
				@if(Auth::user('admin')->hasRole('super-admin','wh-administrator'))
					@can('item-purchase')
					<li class="{{set_active('admin/inventory/item-purchases*','mm-active')}}"> 
						<a class="app-menu__item {{set_active('admin/inventory/item-purchases*','active')}}" href="{{ route('admin.inventory.item-purchases.index') }}">
							<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Purchase</span>   
						</a>        
					</li>
					@endcan
					<li>            
						<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.item-purchase-returns.index' ? 'active' : '' }}" href="{{ route('admin.inventory.item-purchase-returns.index') }}">
							<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Purchase Return</span>   
						</a>        
					</li>
					@can('item-transfer-to-stores')
					<li class="{{set_active('admin/inventory/item-transfers*','mm-active')}}"> 
						<a class="app-menu__item {{set_active('admin/inventory/item-transfers*','active')}}" href="{{ route('admin.inventory.item-transfers.index') }}">    		
							<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Transfer</span>   
						</a>        
					</li>
					@endcan
					<li>            
						<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.item-transfer-returns.index' ? 'active' : '' }}" href="{{ route('admin.inventory.item-transfer-returns.index') }}">
							<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Transfer Return</span>   
						</a>        
					</li>
					@can('product-purchase')
					<li class="{{set_active('admin/inventory/prod-purchases*','mm-active')}}">
						<a class="app-menu__item {{set_active('admin/inventory/prod-purchases*','active')}}" href="{{ route('admin.inventory.prod-purchases.index') }}">
							<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Purchase</span>   
						</a>        
					</li>
					<li class="{{set_active('admin/inventory/prod-purchase-returns*','mm-active')}}">
						<a class="app-menu__item {{set_active('admin/inventory/prod-purchase-returns*','active')}}" href="{{ route('admin.inventory.prod-purchase-returns.index') }}">
							<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Purchase Return</span>   
						</a>        
					</li>
					@endcan
					@can('product-transfer-to-stores')
					<li class="{{set_active('admin/inventory/prod-transfers*','mm-active')}}">
						<a class="app-menu__item {{set_active('admin/inventory/prod-transfers*','active')}}" href="{{ route('admin.inventory.prod-transfers.index') }}">
							<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Transfer</span>   
						</a>        
					</li>
					<li class="{{set_active('admin/inventory/prod-transfers*','mm-active')}}">
						<a class="app-menu__item {{set_active('admin/inventory/prod-transfer-returns*','active')}}" href="{{ route('admin.inventory.prod-transfer-returns.index') }}">
							<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Transfer Return</span>   
						</a>        
					</li>
					@endcan
				@endif
			</ul>
        </li>
		@endif
		@if(Auth::user('admin')->hasRole('super-admin','order-taker','billing-cash','store-keeper',))
		<li>
            <a class="has-arrow app-menu__item"  href="#" aria-expanded="false" >
				<i class="app-menu__icon fa fa-dashboard"></i> Stores <span class="icon-fa arrow icon-fa-fw"></span>
			</a>
			<ul>
				@if(Auth::user('admin')->hasRole('super-admin','order-taker'))
				<li class="{{set_active('admin/orders*','mm-active')}}">
					<a class="app-menu__item {{set_active('admin/orders*','active')}}" href="{{ route('admin.orders.index') }}">
						<i class="app-menu__icon fa fa-bar-chart"></i>
						<span class="app-menu__label">Orders</span>
					</a>
				</li>
				@endif
				@can('item-receiving')
				<li class="{{set_active('admin/inventory/item-receives*','mm-active')}}">      
					<a class="app-menu__item {{set_active('admin/inventory/item-receives*','active')}}" href="{{ route('admin.inventory.item-receives.index') }}">
						<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Receive</span>   
					</a>        
				</li>
				@endcan
				<li>            
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.item-receive-returns.index' ? 'active' : '' }}" href="{{ route('admin.inventory.item-receive-returns.index') }}">
						<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Receive Return</span>   
					</a>        
				</li>
				@can('product-receiving')
				<li class="{{set_active('admin/inventory/prod-receives*','mm-active')}}">      
					<a class="app-menu__item {{set_active('admin/inventory/prod-receives*','active')}}" href="{{ route('admin.inventory.prod-receives.index') }}">
						<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Receive</span>   
					</a>        
				</li>
				<li>            
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.prod-receive-returns.index' ? 'active' : '' }}" href="{{ route('admin.inventory.prod-receive-returns.index') }}">
						<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Receive Return</span>   
					</a>        
				</li>
				@endcan
				@can('invoicing')
				<li class="{{set_active('admin/billing*','mm-active')}}">        
					<a class="app-menu__item {{set_active('admin/billing*','active')}}" href="{{ route('admin.billing.index') }}">
						<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Billing</span>   
					</a>        
				</li>
				@endcan
			</ul>
        </li>
		@endif
		@if(Auth::user('admin')->hasRole('super-admin','e-com-manager'))
		<li>
            <a class="has-arrow app-menu__item"  href="#" aria-expanded="false" >
				<i class="app-menu__icon fa fa-dashboard"></i> Ecommerce <span class="icon-fa arrow icon-fa-fw"></span>
			</a>
			<ul>
				<li class="{{set_active('admin/customers*','mm-active')}}">
					<a class="app-menu__item {{set_active('admin/customers*','active')}}" href="{{ route('admin.customers.index') }}">
						<i class="app-menu__icon fa fal fa-user"></i>
						<span class="app-menu__label">Customers</span>
					</a>
				</li>
			</ul>
        </li>
		@endif
		@if(Auth::user('admin')->hasRole('super-admin','it-manager'))
		<li>
            <a class="has-arrow app-menu__item"  href="#" aria-expanded="false" >
				<i class="app-menu__icon fa fa-dashboard"></i> Users <span class="icon-fa arrow icon-fa-fw"></span>
			</a>
			<ul>
				<li class="{{set_active('admin/designations*','mm-active')}}">
					<a class="app-menu__item {{set_active('admin/designations*','active')}}" href="{{ route('admin.roles.index') }}">
						<i class="app-menu__icon fa fa-address-book"></i>
						<span class="app-menu__label">Designations</span>
					</a>
				</li>
				<li class="{{set_active('admin/staffs*','mm-active')}}">
					<a class="app-menu__item {{set_active('admin/staffs*','active')}}" href="{{ route('admin.staffs.index') }}">
						<i class="app-menu__icon fa fa-users"></i>
						<span class="app-menu__label">Staffs</span>
					</a>
				</li>
			</ul>
        </li>
		@endif
		<li>
            <a class="has-arrow app-menu__item"  href="#" aria-expanded="false" >
				<i class="app-menu__icon fa fa-dashboard"></i> Reports <span class="icon-fa arrow icon-fa-fw"></span>
			</a>
			<ul>
				<li class="{{ Route::currentRouteName() == 'admin.inventory.acc-books.cash-book' ? 'mm-active' : '' }}">
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.cash-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.cash-book') }}">    		
					<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Cash Book</span>   
					</a>        
				</li>
				<li class="{{ Route::currentRouteName() == 'admin.inventory.acc-books.purchase-book' ? 'mm-active' : '' }}">            
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.purchase-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.purchase-book') }}">    		
					<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Purchase Book</span>   
					</a>        
				</li>
				<li class="{{ Route::currentRouteName() == 'admin.inventory.acc-books.purchase-return-book' ? 'mm-active' : '' }}">            
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.purchase-return-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.purchase-return-book') }}">    		
					<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Purchase Return Book</span>   
					</a>        
				</li>
				<li class="{{ Route::currentRouteName() == 'admin.inventory.acc-books.sale-book' ? 'mm-active' : '' }}">
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.sale-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.sale-book') }}">    		
					<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Sales Book</span>   
					</a>        
				</li>
				<li class="{{ Route::currentRouteName() == 'admin.inventory.acc-books.transfer-book' ? 'mm-active' : '' }}">            
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.transfer-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.transfer-book') }}">    		
					<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Transfer Book</span>   
					</a>        
				</li>
				<li class="{{ Route::currentRouteName() == 'admin.inventory.acc-books.transfer-return-book' ? 'mm-active' : '' }}">            
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.transfer-return-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.transfer-return-book') }}">    		
					<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Transfer Return Book</span>   
					</a>        
				</li>
				<li class="{{ Route::currentRouteName() == 'admin.inventory.acc-books.receive-book' ? 'mm-active' : '' }}">            
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.receive-book' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.receive-book') }}">    		
					<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Receive Book</span>   
					</a>        
				</li>
				<li class="{{ Route::currentRouteName() == 'admin.inventory.acc-books.suppliers' ? 'mm-active' : '' }}">            
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.acc-books.suppliers' ? 'active' : '' }}" href="{{ route('admin.inventory.acc-books.suppliers') }}">    		
					<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Supplier Book</span>   
					</a>        
				</li>
				<li class="{{ Route::currentRouteName() == 'admin.inventory.stock-reports.items.index' ? 'mm-active' : '' }}">            
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.stock-reports.items.index' ? 'active' : '' }}" href="{{ route('admin.inventory.stock-reports.items.index') }}">    		
					<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Item Balance</span>   
					</a>        
				</li> 
				<li class="{{ Route::currentRouteName() == 'admin.inventory.stock-reports.products.index' ? 'mm-active' : '' }}">            
					<a class="app-menu__item {{ Route::currentRouteName() == 'admin.inventory.stock-reports.products.index' ? 'active' : '' }}" href="{{ route('admin.inventory.stock-reports.products.index') }}">    		
					<i class="app-menu__icon fa fal fa-user"></i><span class="app-menu__label">Product Balance</span>   
					</a>        
				</li>
			</ul>
        </li>
		@if(Auth::user('admin')->hasRole('super-admin'))
		<li class="{{ Route::currentRouteName() == 'admin.settings' ? 'mm-active' : '' }}">
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.settings' ? 'active' : '' }}" href="{{ route('admin.settings') }}">
                <i class="app-menu__icon fa fa-cogs"></i>
                <span class="app-menu__label">Settings</span>
            </a>
        </li>
		<li class="{{ Route::currentRouteName() == 'admin.general' ? 'mm-active' : '' }}">
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.general' ? 'active' : '' }}" href="{{ route('admin.general') }}">
                <i class="app-menu__icon fa fa-pencil"></i>
                <span class="app-menu__label">General Contents</span>
            </a>
        </li>
        <li class="{{set_active('admin/recipes*','mm-active')}}">
			<a class="app-menu__item {{set_active('admin/recipes*','active')}}" href="{{ route('admin.recipes.index') }}">
				<i class="app-menu__icon fa fa-cutlery"></i>
				<span class="app-menu__label">Recipes</span>
			</a>
		</li>
		<li class="{{set_active('admin/banners*','mm-active')}}">
			<a class="app-menu__item {{set_active('admin/banners*','active')}}" href="{{ route('admin.banners.index') }}">
				<i class="app-menu__icon fa fa-cutlery"></i>
				<span class="app-menu__label">Banners</span>
			</a>
		</li>
		@endcan
    </ul>
</aside>
