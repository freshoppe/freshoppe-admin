@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.processing.update') }}" method="POST" role="form">
                    @csrf
                    <div class="tile-body">
                        <div class="form-group">
                            <label class="control-label" for="processing_name">Processing Name <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('processing_name') is-invalid @enderror" type="text" name="processing_name" id="processing_name" value="{{ old('processing_name', $processing->processing_name) }}"/>
                            <input type="hidden" name="id" value="{{ $processing->id }}">
                            @error('processing_name') {{ $message }} @enderror
                        </div>
						<div class="form-group">
                            <label class="control-label" for="code">Processing Code <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('code') is-invalid @enderror" type="text" name="code" id="code" value="{{ old('code', $processing->code) }}"/>
                            <input type="hidden" name="id" value="{{ $processing->id }}">
                            @error('code') {{ $message }} @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="status" name="status"
                                    {{ $processing->status == 1 ? 'checked' : '' }}
                                    />Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Processing Way</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.processing.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@endpush
