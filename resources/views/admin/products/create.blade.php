@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-shopping-bag"></i> {{ $pageTitle }} - {{ $subTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row user">
        <div class="col-md-3">
            <div class="tile p-0">
                <ul class="nav flex-column nav-tabs user-tabs">
                    <li class="nav-item"><a class="nav-link active" href="#general" data-toggle="tab">General</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="general">
                    <div class="tile">
                        <form action="{{ route('admin.products.store') }}" method="POST" role="form" enctype="multipart/form-data">
                            @csrf
                            <h3 class="tile-title">Product Information</h3>
                            <hr>
                            <div class="tile-body">
								<div class="row">
                                    <div class="col form-group">
										<label class="control-label" for="raw_item_id">Item</label>
										<select name="raw_item_id" id="raw_item_id" class="form-control @error('raw_item_id') is-invalid @enderror">
											<option value="">Select Item</option>
											@foreach($rawItems as $raw_item)
												<option value="{{ $raw_item->id }}" @if(old('raw_item_id') == $raw_item->id) selected @endif>{{ $raw_item->item_code }} - {{ $raw_item->item_name }}</option>
											@endforeach
										</select>
										<div class="invalid-feedback active">
											<i class="fa fa-exclamation-circle fa-fw"></i> @error('raw_item_id') <span>{{ $message }}</span> @enderror
										</div>
										
									</div>
                                </div>
								<div class="row">
									<div class="col form-group">
										<label class="control-label" for="name">Name</label>
										<input
											class="form-control @error('name') is-invalid @enderror"
											type="text"
											placeholder="Enter product name"
											id="name"
											name="name"
											value="{{ old('name') }}"
										/>
										<div class="invalid-feedback active">
											<i class="fa fa-exclamation-circle fa-fw"></i> @error('name') <span>{{ $message }}</span> @enderror
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col form-group">
										<label class="control-label" for="unit">Unit</label>
										<select name="unit_id" id="unit_id" class="form-control @error('unit_id') is-invalid @enderror">
											<option value="">Select Unit</option>
											@foreach($productUnits as $unit)
												<option value="{{ $unit->id }}" @if(old('unit_id') == $unit->id) selected @endif>
												{{ $unit->product_unit }}
												</option>
											@endforeach
										</select>
										<div class="invalid-feedback active">
											<i class="fa fa-exclamation-circle fa-fw"></i> @error('unit_id') <span>Unit is required</span> @enderror
										</div>
									</div>
									<div class="col form-group">
										<label class="control-label" for="processing_id">Processing Way</label>
										<select name="processing_id" id="processing_id" class="form-control @error('processing_id') is-invalid @enderror">
											<option value="">Select processing way</option>
											@foreach($processings as $process)
												<option value="{{ $process->id }}" @if(old('processing_id') == $process->id) selected @endif>
												{{ $process->processing_name }}
												</option>
											@endforeach
										</select>
										<div class="invalid-feedback active">
											<i class="fa fa-exclamation-circle fa-fw"></i> @error('processing_id') <span>{{ $message }}</span> @enderror
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label" for="ecom_name">Ecommerce Product Name</label>
									<input
										class="form-control @error('ecom_name') is-invalid @enderror"
										type="text"
										placeholder="Enter ecommerce product name"
										id="ecom_name"
										name="ecom_name"
										value="{{ old('ecom_name') }}"
									/>
									<div class="invalid-feedback active">
										<i class="fa fa-exclamation-circle fa-fw"></i> @error('ecom_name') <span>{{ $message }}</span> @enderror
									</div>
								</div>
                                <div class="row">
									<div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="categories">Categories</label>
                                            <select name="categories[]" id="categories" class="form-control" multiple>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" @if(!empty(old('categories')) && in_array($category->id,old('categories'))) selected @endif>{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="ecom_settings">Ecom Settings</label>
											@php $ecom_settings = Config::get('constants.ecomSettings'); @endphp
                                            <select name="ecom_settings[]" id="ecom_settings" class="form-control" multiple>
												@foreach($ecom_settings as $key=>$value)
                                                    <option value="{{ $key }}" @if(!empty(old('ecom_settings')) && in_array($key,old('ecom_settings'))) selected @endif>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="price">Price</label>
                                            <input
                                                class="form-control @error('price') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter product price"
                                                id="price"
                                                name="price"
                                                value="{{ old('price') }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('price') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="sale_price">Special Price</label>
                                            <input
                                                class="form-control"
                                                type="text"
                                                placeholder="Enter product special price"
                                                id="sale_price"
                                                name="sale_price"
                                                value="{{ old('sale_price') }}"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="pieces">Number of Pieces</label>
                                            <input
                                                class="form-control @error('pieces') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter number of pieces"
                                                id="pieces"
                                                name="pieces"
                                                value="{{ old('pieces') }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('pieces') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="gst_rate">GST %</label>
											<input class="form-control col-md-3" type="number" min="0" max="100" name="gst_rate" id="gst_rate" value="{{ old('gst_rate') }}" />
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="net_weight">Net Weight</label>
                                            <input
                                                class="form-control"
                                                type="text"
                                                placeholder="Enter net weight"
                                                id="net_weight"
                                                name="net_weight"
                                                value="{{ old('net_weight') }}"
                                            />
                                        </div>
                                    </div>
									<div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="gross_weight">Gross Weight</label>
                                            <input
                                                class="form-control @error('gross_weight') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter gross weight"
                                                id="gross_weight"
                                                name="gross_weight"
                                                value="{{ old('gross_weight') }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('gross_weight') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="size_min">Size Range</label>
											<div class="row d-flex">
												<div class="col-5">
													<input
														class="form-control"
														type="number"
														step="0.1"
														placeholder="Enter min size"
														id="size_min"
														name="size_min"
														value="{{ old('size_min') }}"
													/>
												</div>
												<div><span style="vertical-align: -moz-middle-with-baseline;">to</span></div>
												<div class="col-5">
													<input
														class="form-control"
														type="number"
														step="0.1"
														placeholder="Enter max size"
														id="size_max"
														name="size_max"
														value="{{ old('size_max') }}"
													/>
												</div>
											</div>
                                        </div>
                                    </div>
									<div class="col-md-6">
										<div class="form-group">
                                            <label class="control-label" for="serves">Serves</label>
                                            <input
                                                class="form-control @error('serves') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter no of serves"
                                                id="serves"
                                                name="serves"
                                                value="{{ old('serves') }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('serves') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
									</div>
                                </div>
								<div class="form-group">
                                    <label class="control-label" for="short_description">Short Description</label>
                                    <textarea name="short_description" id="short_description" rows="4" class="form-control @error('short_description') is-invalid @enderror">{{ old('short_description') }}</textarea>
									<div class="invalid-feedback active">
										<i class="fa fa-exclamation-circle fa-fw"></i> @error('short_description') <span>{{ $message }}</span> @enderror
									</div>
                                </div>
								
								<div class="form-group">
									<label class="control-label" for="description">Description</label>
									<textarea name="description" id="description" rows="8" class="form-control summernote  @error('description') is-invalid @enderror">{{ old('description') }}</textarea>
									<div class="invalid-feedback active">
										<i class="fa fa-exclamation-circle fa-fw"></i> @error('description') <span>{{ $message }}</span> @enderror
									</div>
								</div>
								
								<div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="whats_in_box">What is in the box</label>
                                            <textarea name="whats_in_box" id="whats_in_box" rows="8" class="form-control summernote  @error('whats_in_box') is-invalid @enderror">{{ old('whats_in_box') }}</textarea>
											<div class="invalid-feedback active">
												<i class="fa fa-exclamation-circle fa-fw"></i> @error('whats_in_box') <span>{{ $message }}</span> @enderror
											</div>
                                        </div>
                                    </div>
                                </div>
								
								<div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="source">Sourcing</label>
                                            <textarea name="source" id="source" rows="8" class="form-control summernote  @error('source') is-invalid @enderror">{{ old('source') }}</textarea>
											<div class="invalid-feedback active">
												<i class="fa fa-exclamation-circle fa-fw"></i> @error('source') <span>{{ $message }}</span> @enderror
											</div>
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                
									<label class="control-label" for="source_image">Sourcing Image <span class="m-l-5 text-danger"> *</span></label>
									<input type="file" class="form-control @error('source_image') is-invalid @enderror" name="source_image" id="source_image">
									<div class="invalid-feedback active">
										<i class="fa fa-exclamation-circle fa-fw"></i> @error('source_image') <span>{{ $message }}</span> @enderror
									</div>
									<figure class="mt-2" style="width: 80px; height: auto;">
										<img src="https://via.placeholder.com/80" id="sourceImage" class="img-fluid" alt="img">
									</figure>
								</div>
                                
                                <div class="form-group row col-md-12">
                                    <div class="form-check col-md-2">
                                        <label class="form-check-label">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   id="status"
                                                   name="status"
												   @if(old('status') == "on") checked @endif
                                                />Active
                                        </label>
                                    </div>
                                    <div class="form-check col-md-2">
                                        <label class="form-check-label">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   id="ecom_status"
                                                   name="ecom_status"
												   @if(old('ecom_status') == "on") checked @endif
                                                />Ecom Active
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="tile-footer">
                                <div class="row d-print-none mt-2">
                                    <div class="col-12 text-right">
                                        <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Product</button>
                                        <a class="btn btn-danger" href="{{ route('admin.products.index') }}"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Go Back</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/select2.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#categories').select2();
            $('#ecom_settings').select2();
			
			$('.summernote').summernote({
               height: 300,
			   popover: {
				 image: [],
				 link: [],
				 air: []
			   },
			   toolbar: [
							[ 'style', [ 'style' ] ],
							[ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
							[ 'fontname', [ 'fontname' ] ],
							[ 'fontsize', [ 'fontsize' ] ],
							[ 'color', [ 'color' ] ],
							[ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
							[ 'table', [ 'table' ] ],
							[ 'insert', [ 'link'] ],
							[ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
						]
          });
        });
		$("#source_image").change(function() {
		  readURL(this);
		});
	
		function readURL(input) {
		  if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function(e) {
			  $('#sourceImage').attr('src', e.target.result);
			}
			
			reader.readAsDataURL(input.files[0]); // convert to base64 string
		  }
		}
		
		$("#name").blur(function() {
			var name 		= $('#name').val();
			var processing = '';
			if($("#processing_id").val() > 0)
				processing = $("#processing_id option:selected").text();
			$('#ecom_name').val($.trim(name)+' '+$.trim(processing));
		});
		
		$("#processing_id").change(function() {
			var name 		= $('#name').val();
			var processing = '';
			if($("#processing_id").val() > 0)
				processing = $("#processing_id option:selected").text();
			$('#ecom_name').val($.trim(name)+' '+$.trim(processing));
		});
		

		APP_URL = "{{ url('/') }}";	
		$( "#raw_item_id" ).change(function() {
			$.getJSON(APP_URL+"/admin/products/item/processing/"+ $(this).val(), function(jsonData){
				select = '<select name="processing_id" class="form-control" id="processing_id" >';
				select +='<option value="">Select processing way</option>';
				$.each(jsonData, function(i,data){
				   select +='<option value="'+data.id+'">'+data.processing_name+'</option>';
				});
				select += '</select>';
				$("#processing_id").html(select);
			});
		});
    </script>
@endpush
