@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/js/plugins/dropzone/dist/min/dropzone.min.css') }}"/>
@endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-shopping-bag"></i> {{ $pageTitle }} - {{ $subTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs product-tabs" id="productTab" role="tablist">
				<li class="nav-item"><a class="nav-link active" data-bs-toggle="tab" href="#general" data-toggle="tab">General</a></li>
				<li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#images" data-toggle="tab">Images</a></li>
				<!--<li class="nav-item"><a class="nav-link" href="#attributes" data-toggle="tab">Attributes</a></li>-->
			</ul>
        </div>
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane active" id="general">
                    <div class="tile">
                        <form action="{{ route('admin.products.update') }}" method="POST" role="form" enctype="multipart/form-data">
                            @csrf
                            <h3 class="tile-title">Product Information</h3>
                            <hr>
                            <div class="tile-body">
								<div class="row">
                                    <div class="col form-group">
										<label class="control-label" for="raw_item_id">Item</label>
										<select name="raw_item_id" id="raw_item_id" class="form-control @error('raw_item_id') is-invalid @enderror">
											<option value="">Select Item</option>
											@foreach($rawItems as $raw_item)
												<option value="{{ $raw_item->id }}" @if($product->raw_item_id == $raw_item->id) selected @endif>{{ $raw_item->item_code }} - {{ $raw_item->item_name }}</option>
											@endforeach
										</select>
										<div class="invalid-feedback active">
											<i class="fa fa-exclamation-circle fa-fw"></i> @error('raw_item_id') <span>{{ $message }}</span> @enderror
										</div>
									</div>
                                </div>
								<div class="row">
									<div class="col form-group">
										<label class="control-label" for="name">Name</label>
										<input
											class="form-control @error('name') is-invalid @enderror"
											type="text"
											placeholder="Enter attribute name"
											id="name"
											name="name"
											value="{{ old('name', $product->name) }}"
										/>
										<input type="hidden" name="product_id" value="{{ $product->id }}">
										<div class="invalid-feedback active">
											<i class="fa fa-exclamation-circle fa-fw"></i> @error('name') <span>{{ $message }}</span> @enderror
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col form-group">
										<label class="control-label" for="unit">Unit</label>
										<select name="unit_id" id="unit_id" class="form-control @error('unit_id') is-invalid @enderror">
											<option value="">Select Unit</option>
											@foreach($productUnits as $unit)
												<option value="{{ $unit->id }}" @if($product->unit_id == $unit->id) selected @endif>
												{{ $unit->product_unit }}
												</option>
											@endforeach
										</select>
										<div class="invalid-feedback active">
											<i class="fa fa-exclamation-circle fa-fw"></i> @error('unit_id') <span>Unit is required</span> @enderror
										</div>
									</div>
									<div class="col form-group">
										<label class="control-label" for="processing_id">Processing Way</label>
										<select name="processing_id" id="processing_id" class="form-control @error('processing_id') is-invalid @enderror">
											<option value="">Select processing way</option>
											@foreach($processings as $process)
												<option value="{{ $process->id }}" @if($product->processing_id == $process->id) selected @endif>
												{{ $process->processing_name }}
												</option>
											@endforeach
										</select>
										<div class="invalid-feedback active">
											<i class="fa fa-exclamation-circle fa-fw"></i> @error('processing_id') <span>{{ $message }}</span> @enderror
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label" for="ecom_name">Ecommerce Product Name</label>
									<input
										class="form-control @error('ecom_name') is-invalid @enderror"
										type="text"
										placeholder="Enter ecommerce product name"
										id="ecom_name"
										name="ecom_name"
										value="{{ old('ecom_name', $product->ecom_name) }}"
									/>
									<div class="invalid-feedback active">
										<i class="fa fa-exclamation-circle fa-fw"></i> @error('ecom_name') <span>{{ $message }}</span> @enderror
									</div>
								</div>
                                <div class="form-row">
                                    <div class="col form-group">
										<label class="control-label" for="sku">SKU</label>
										<input
											class="form-control @error('sku') is-invalid @enderror"
											type="text"
											readonly
											placeholder="Enter product sku"
											id="sku"
											name="sku"
											value="{{ old('sku', $product->sku) }}"
										/>
										<div class="invalid-feedback active">
											<i class="fa fa-exclamation-circle fa-fw"></i> @error('sku') <span>{{ $message }}</span> @enderror
										</div>
									</div>
									<div class="col form-group">
										<label class="control-label" for="number">PLU Number</label>
										<input
											class="form-control @error('number') is-invalid @enderror"
											type="text"
											placeholder="Enter product number"
											readonly
											id="number"
											name="number"
											value="{{ old('number', $product->number) }}"
										/>
										<div class="invalid-feedback active">
											<i class="fa fa-exclamation-circle fa-fw"></i> @error('number') <span>{{ $message }}</span> @enderror
										</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="categories">Categories</label>
                                            <select name="categories[]" id="categories" class="form-control" multiple>
                                                @foreach($categories as $category)
                                                    @php $check = in_array($category->id, $product->categories->pluck('id')->toArray()) ? 'selected' : ''@endphp
                                                    <option value="{{ $category->id }}" {{ $check }}>{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
									<div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="ecom_settings">Ecom Settings</label>
											@php $ecom_settings = Config::get('constants.ecomSettings'); @endphp
                                            <select name="ecom_settings[]" id="ecom_settings" class="form-control" multiple>
												@foreach($ecom_settings as $key=>$value)
												@php $check = array_key_exists($key, $ecomSettings) ? 'selected' : ''@endphp
                                                    <option value="{{ $key }}" {{ $check }}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="price">Price</label>
                                            <input
                                                class="form-control @error('price') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter product price"
                                                id="price"
                                                name="price"
                                                value="{{ old('price', $product->price) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('price') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="sale_price">Special Price</label>
                                            <input
                                                class="form-control"
                                                type="text"
                                                placeholder="Enter product special price"
                                                id="sale_price"
                                                name="sale_price"
                                                value="{{ old('sale_price', $product->sale_price) }}"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="pieces">No of Pieces</label>
                                            <input
                                                class="form-control @error('pieces') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter product pieces"
                                                id="pieces"
                                                name="pieces"
                                                value="{{ old('pieces', $product->pieces) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('pieces') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="gst_rate">GST %</label>
											<input class="form-control col-md-3" type="number" min="0" max="100" name="gst_rate" id="gst_rate" value="{{ old('gst_rate', $product->gst_rate) }}" />
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="net_weight">Net Weight</label>
                                            <input
                                                class="form-control"
                                                type="text"
                                                placeholder="Enter net weight"
                                                id="net_weight"
                                                name="net_weight"
                                                value="{{ old('net_weight', $product->net_weight) }}"
                                            />
                                        </div>
                                    </div>
									<div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="gross_weight">Gross Weight</label>
                                            <input
                                                class="form-control @error('gross_weight') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter gross weight"
                                                id="gross_weight"
                                                name="gross_weight"
                                                value="{{ old('gross_weight', $product->gross_weight) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('gross_weight') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="size_min">Size Range</label>
											<div class="row d-flex">
												<div class="col-5">
													<input
														class="form-control"
														type="number"
														step="0.1"
														placeholder="Enter min size"
														id="size_min"
														name="size_min"
														value="{{ old('size_min', $product->size_min) }}"
													/>
												</div>
												<div><span style="vertical-align: -moz-middle-with-baseline;">to</span></div>
												<div class="col-5">
													<input
														class="form-control"
														type="number"
														step="0.1"
														placeholder="Enter max size"
														id="size_max"
														name="size_max"
														value="{{ old('size_max', $product->size_max) }}"
													/>
												</div>
											</div>
                                        </div>
                                    </div>
									<div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="serves">Serves</label>
                                            <input
                                                class="form-control @error('serves') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter no of serves"
                                                id="serves"
                                                name="serves"
                                                value="{{ old('serves', $product->serves) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('serves') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label" for="short_description">Short Description</label>
                                    <textarea name="short_description" id="short_description" rows="4" class="form-control">{{$product->short_description}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="description">Description</label>
                                    <textarea name="description" id="description" rows="8" class="form-control summernote">{{ old('description', $product->description) }}</textarea>
                                </div>
								<div class="form-group">
                                    <label class="control-label" for="whats_in_box">What is in the box</label>
                                    <textarea name="whats_in_box" id="whats_in_box" rows="8" class="form-control summernote">{{ old('whats_in_box', $product->whats_in_box) }}</textarea>
                                </div>
								<div class="form-group">
                                    <label class="control-label" for="source">Sourcing</label>
                                    <textarea name="source" id="source" rows="8" class="form-control summernote">{{ old('source', $product->source) }}</textarea>
                                </div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-2">
											<figure class="mt-2" style="width: 80px; height: auto;">
												@if ($product->source_image != null)
													<img src="{{ asset('uploads/'.$product->source_image) }}" id="sourceImage" class="img-fluid" alt="img">
												@else
													<img src="https://via.placeholder.com/80x80?text=Placeholder+Image" id="sourceImage" style="width: 80px; height: auto;">
												@endif
												
											</figure>
										</div>
										<div class="col-md-10">
											<label class="control-label">Source Image</label>
											<input class="form-control @error('source_image') is-invalid @enderror" type="file" id="source_image" name="source_image"/>
											@error('source_image') {{ $message }} @enderror
										</div>
									</div>
								</div>
                                <div class="form-group row col-md-12">
                                    <div class="form-check col-md-2">
                                        <label class="form-check-label">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   id="status"
                                                   name="status"
                                                   {{ $product->status == 1 ? 'checked' : '' }}
                                                />Active
                                        </label>
                                    </div>
                                    <div class="form-check col-md-2">
                                        <label class="form-check-label">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   id="ecom_status"
                                                   name="ecom_status"
                                                   {{ $product->ecom_status == 1 ? 'checked' : '' }}
                                                />Ecom Active
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="tile-footer">
                                <div class="row d-print-none mt-2">
                                    <div class="col-12 text-right">
                                        <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Product</button>
                                        <a class="btn btn-danger" href="{{ route('admin.products.index') }}"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Go Back</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="images">
                    <div class="tile">
                        <h3 class="tile-title">Upload Image</h3>
                        <hr>
                        <div class="tile-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="" class="dropzone" id="dropzone" style="border: 2px dashed rgba(0,0,0,0.3)">
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                            <div class="row d-print-none mt-2">
                                <div class="col-12 text-right">
                                    <button class="btn btn-success" type="button" id="uploadButton">
                                        <i class="fa fa-fw fa-lg fa-upload"></i>Upload Images
                                    </button>
                                </div>
                            </div>
                            @if ($product->images)
                                <hr>
                                <div class="row">
                                    @foreach($product->images as $image)
                                        <div class="col-md-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <img src="{{ asset('uploads/'.$image->full) }}" id="brandLogo" class="img-fluid" alt="img">
                                                    <a class="card-link float-right text-danger" href="{{ route('admin.products.images.delete', $image->id) }}">
                                                        <i class="fa fa-fw fa-lg fa-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!--<div class="tab-pane" id="attributes">
                    <product-attributes :productid="{{ $product->id }}"></product-attributes>
                </div>-->
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dropzone/dist/min/dropzone.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/bootstrap-notify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/app.js') }}"></script>
    <script>
		$('#categories').select2();
		$('#ecom_settings').select2();
        Dropzone.autoDiscover = false;
		
        $( document ).ready(function() {
			$('.summernote').summernote({
               height: 300,
			   popover: {
				 image: [],
				 link: [],
				 air: []
			   },
			   toolbar: [
							[ 'style', [ 'style' ] ],
							[ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
							[ 'fontname', [ 'fontname' ] ],
							[ 'fontsize', [ 'fontsize' ] ],
							[ 'color', [ 'color' ] ],
							[ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
							[ 'table', [ 'table' ] ],
							[ 'insert', [ 'link'] ],
							[ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
						]
			});
            

            let myDropzone = new Dropzone("#dropzone", {
                paramName: "image",
                addRemoveLinks: false,
                maxFilesize: 4,
                parallelUploads: 2,
                uploadMultiple: false,
                url: "{{ route('admin.products.images.upload') }}",
                autoProcessQueue: false,
            });
            myDropzone.on("queuecomplete", function (file) {
                window.location.reload();
                showNotification('Completed', 'All product images uploaded', 'success', 'fa-check');
            });
            $('#uploadButton').click(function(){
                if (myDropzone.files.length === 0) {
                    showNotification('Error', 'Please select files to upload.', 'danger', 'fa-close');
                } else {
                    myDropzone.processQueue();
                }
            });
            function showNotification(title, message, type, icon)
            {
                $.notify({
                    title: title + ' : ',
                    message: message,
                    icon: 'fa ' + icon
                },{
                    type: type,
                    allow_dismiss: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            }
        });
		$("#source_image").change(function() {
		  readURL(this);
		});

		function readURL(input) {
		  if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function(e) {
			  $('#sourceImage').attr('src', e.target.result);
			}
			
			reader.readAsDataURL(input.files[0]); // convert to base64 string
		  }
		}
		$("#name").blur(function() {
			var name 		= $('#name').val();
			var processing = '';
			if($("#processing_id").val() > 0)
				processing = $("#processing_id option:selected").text();
			$('#ecom_name').val($.trim(name)+' '+$.trim(processing));
		});
		
		$("#processing_id").change(function() {
			var name 		= $('#name').val();
			var processing = '';
			if($("#processing_id").val() > 0)
				processing = $("#processing_id option:selected").text();
			$('#ecom_name').val($.trim(name)+' '+$.trim(processing));
		});
		
		APP_URL = "{{ url('/') }}";	
		$( "#raw_item_id" ).change(function() {
			$.getJSON(APP_URL+"/admin/products/item/processing/"+ $(this).val(), function(jsonData){
				select = '<select name="processing_id" class="form-control" id="processing_id" >';
				select +='<option value="">Select processing way</option>';
				$.each(jsonData, function(i,data){
				   select +='<option value="'+data.id+'">'+data.processing_name+'</option>';
				});
				select += '</select>';
				$("#processing_id").html(select);
			});
		});
    </script>
@endpush
