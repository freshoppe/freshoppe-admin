@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.recipes.store') }}" method="POST" role="form" enctype="multipart/form-data">
                    @csrf
                    <div class="tile-body">
						<div class="form-group">
                            <label class="control-label" for="product_id">Product <span class="m-l-5 text-danger"> *</span></label>
							<select name="product_id" id="product_id" class="form-control @error('product_id') is-invalid @enderror">
								<option value="">Select Product</option>
								@foreach($products as $product)
									<option value="{{ $product->id }}" @if(old('product_id') == $product->id) selected @endif>{{ $product->name }}</option>
								@endforeach
							</select>
							<div class="invalid-feedback active">
								<i class="fa fa-exclamation-circle fa-fw"></i> @error('product_id') <span>{{ $message }}</span> @enderror
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label" for="name">Name <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" id="name" value="{{ old('name') }}"/>
                            <div class="invalid-feedback active">
								<i class="fa fa-exclamation-circle fa-fw"></i> @error('name') <span>{{ $message }}</span> @enderror
							</div>
                        </div>
                        <div class="form-group">
							<label class="control-label" for="ingredients">Ingredients</label>
							<textarea name="ingredients" id="ingredients" rows="8" class="form-control summernote  @error('ingredients') is-invalid @enderror">{{ old('ingredients') }}</textarea>
							<div class="invalid-feedback active">
								<i class="fa fa-exclamation-circle fa-fw"></i> @error('ingredients') <span>{{ $message }}</span> @enderror
							</div>
						</div>
						<div class="form-group">
							<label class="control-label" for="method">Cooking Method</label>
							<textarea name="method" id="method" rows="8" class="form-control summernote  @error('method') is-invalid @enderror">{{ old('method') }}</textarea>
							<div class="invalid-feedback active">
								<i class="fa fa-exclamation-circle fa-fw"></i> @error('method') <span>{{ $message }}</span> @enderror
							</div>
						</div>
						<div class="form-group">
                            <label class="control-label" for="time">Cooking Time <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('time') is-invalid @enderror" type="text" name="time" id="time" value="{{ old('time') }}"/>
                            <div class="invalid-feedback active">
								<i class="fa fa-exclamation-circle fa-fw"></i> @error('time') <span>{{ $message }}</span> @enderror
							</div>
                        </div>
						<div class="form-group">
                                
							<label class="control-label" for="image">Image <span class="m-l-5 text-danger"> *</span></label>
							<input type="file" class="form-control @error('image') is-invalid @enderror" name="image" id="image">
							<div class="invalid-feedback active">
								<i class="fa fa-exclamation-circle fa-fw"></i> @error('image') <span>{{ $message }}</span> @enderror
							</div>
							<figure class="mt-2" style="width: 80px; height: auto;">
								<img src="https://via.placeholder.com/80" id="recipeImage" class="img-fluid" alt="img">
							</figure>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Recipe</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.recipes.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
	$( document ).ready(function() {
		$('.summernote').summernote({
		   height: 300,
		   popover: {
			 image: [],
			 link: [],
			 air: []
		   },
		   toolbar: [
				[ 'style', [ 'style' ] ],
				[ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
				[ 'fontname', [ 'fontname' ] ],
				[ 'fontsize', [ 'fontsize' ] ],
				[ 'color', [ 'color' ] ],
				[ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
				[ 'table', [ 'table' ] ],
				[ 'insert', [ 'link'] ],
				[ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
			]
		});
	});
	$("#image").change(function() {
	  readURL(this);
	});
	
	function readURL(input) {
	  if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function(e) {
		  $('#recipeImage').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]); // convert to base64 string
	  }
	}
</script>
@endpush
