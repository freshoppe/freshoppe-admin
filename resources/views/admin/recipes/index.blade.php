@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
        <a href="{{ route('admin.recipes.create') }}" class="btn btn-primary pull-right">Add Recipe</a>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Product </th>
                                <th> Recipe Name </th>
                                <th> Cooking Time </th>
                                <th> Image </th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($recipes as $key=>$recipe)
								<tr>
									<td>{{ $key+1 }}</td>
									<td>{{ $recipe->product->name }}</td>
									<td>{{ $recipe->name }}</td>
									<td>{{ $recipe->time }}</td>
									<td class="text-center">
										@if($recipe->image)
											<img style="width:80px" src="{{ asset('/uploads/'.$recipe->image) }}" alt="image">
										@else
											<img src="https://via.placeholder.com/80" alt="image">
										@endif
									</td>
									<td class="text-center">
										<div class="btn-group" role="group" aria-label="Second group">
											<a href="{{ route('admin.recipes.edit', $recipe->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
											<a href="{{ route('admin.recipes.delete', $recipe->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
										</div>
									</td>
								</tr>
						@endforeach
					</tbody>
				</table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endpush
