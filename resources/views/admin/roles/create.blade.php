@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.roles.store') }}" method="POST" role="form">
                    @csrf
                    <div class="tile-body">
                        <div class="form-group">
                            <label class="control-label" for="name">Name <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" id="name" value="{{ old('name') }}"/>
							@error('name')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror	
                        </div>
						<div class="form-group">
                            <strong>Permissions:</strong>
							<br/>
							<br/>
							<div class="row">
							@foreach($permission->chunk(10) as $chunk)
								<div class="col-sm-4">
								@foreach($chunk as $value)
									<div class="form-check">
										<label class="control-label">
											<input class="form-check-input"
											   type="checkbox"
											   id="{{$value->id}}"
											   value="{{$value->id}}"
											   name="permission[]"
											/>{{ $value->name }}
										</label>
									</div>
								@endforeach 
								<br/>
								</div>
							@endforeach
							</div>
							@error('permission') 
							<span class="border-danger text-danger" role="alert">{{ $message }} </span>
							@enderror
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Role</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.roles.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
