@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
		@can('designation-creation')
        <a href="{{ route('admin.roles.create') }}" class="btn btn-primary pull-right">Add Designation</a>
		@endcan
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th class="text-center"> # </th>
                                <th> Name </th>
                                <th> Permissions </th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $key=>$role)
								<tr>
									<td class="text-center">{{ $key+1 }}</td>
									<td>{{ $role->name }}</td>
									<td>
										@if(!empty($role->permissions))
											<ul>
											@foreach($role->permissions as $v)
												<li>{{ $v->name }}</li>
											@endforeach
											</ul>
										@endif
									</td>
									<td class="text-center">
										<div class="btn-group" role="group" aria-label="Second group">
											@can('designation-edit')
												<a class="btn btn-primary" href="{{ route('admin.roles.edit',$role->id) }}"><i class="fa fa-edit"></i></a>
											@endcan
											@can('designation-delete')
												<a href="{{ route('admin.roles.destroy', $role->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
											@endcan
										</div>
									</td>
								</tr>
						@endforeach
					</tbody>
				</table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endpush
