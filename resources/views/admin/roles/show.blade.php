@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.stores.update') }}" method="POST" role="form">
                    @csrf
                    <div class="tile-body">
                        <div class="form-group">
							<strong>Name:</strong>
							{{ $role->name }}
                        </div>
						<div class="form-group">
                            <strong>Permissions:</strong>
							@if(!empty($role->permissions))
								<ul>
								@foreach($role->permissions as $v)
									<li>{{ $v->name }}</li>
								@endforeach
								</ul>
							@endif
                        </div>
                    </div>
                    <div class="tile-footer">
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-primary" href="{{ route('admin.roles.index') }}"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@endpush
