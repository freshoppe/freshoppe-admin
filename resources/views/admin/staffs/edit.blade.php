@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fal fa-user"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.staffs.update') }}" method="POST" role="form" autocomplete="nope">
                    <input autocomplete="nope" name="hidden" type="text" style="display:none;">
					@csrf
                    <div class="tile-body">
						<div class="form-group">
							<input type="hidden" name="id" value="{{ $staff->id }}">
							<label class="control-label" for="name">Name <span class="m-l-5 text-danger"> *</span></label>
							<input class="form-control @error('name') is-invalid @enderror" type="text" name="name" id="name" value="{{ old('name', $staff->name) }}"/>
							@error('name')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror	
						</div>
						<div class="form-group">
                            <label class="control-label" for="email">Email <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('email') is-invalid @enderror" type="text" name="email" id="email" value="{{ old('email', $staff->email) }}"/>
							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="form-group">
							<label class="control-label" for="password">Password</label>
							<input class="form-control @error('password') is-invalid @enderror" type="password" name="password" id="password" value=""/>
							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror	
						</div>
						<div class="form-group">
							<label class="control-label" for="name">Confirm Password</label>
							<input type="password" class="form-control form-control-danger" name="password_confirmation" id="password_confirmation">
							@error('password_confirmation')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror	
						</div>
						<div class="form-group">
							<label class="control-label" for="staff_type">Department <span class="m-l-5 text-danger"> *</span></label>
							<div class="row">
								<div class="form-group col-md-3">
									<div class="form-check">
										<label class="form-check-label">
											<input class="form-check-input"
												   type="radio"
												   id="warehouse_staff"
												   name="staff_type"
												   value="1"
												   {{ $staff->staff_type == 1 ? 'checked' : '' }}
												/>Warehouse
												
										</label>
									</div>
								</div>
								<div class="form-group col-md-3">
									<div class="form-check">
										<label class="form-check-label">
											<input class="form-check-input"
												   type="radio"
												   id="store_staff"
												   name="staff_type"
												   value="2"
												   {{ $staff->staff_type == 2 ? 'checked' : '' }}
												/>Store
										</label>
									</div>
								</div>
							</div>
							@error('staff_type')
								{{ $message }}
							@enderror
						</div>
						<div class="form-group">
							<label class="control-label" for="warehouse_id" >Warehouse <span class="m-l-5 text-danger"> *</span></label>
							<select name="warehouse_id" id="warehouse_id" class="form-control @error('warehouse_id') is-invalid @enderror">
								<option value="">Select Warehouse</option>
								@foreach($warehouses as $warehouse)
									<option value="{{ $warehouse->id }}" @if($warehouse_id == $warehouse->id) selected @endif>{{ $warehouse->warehouse_name }} - {{ $warehouse->warehouse_code }}</option>
								@endforeach
							</select>
							@error('warehouse_id')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="form-group" id="store_div" style="display:none">
							<label class="control-label" for="store_id" >Store <span class="m-l-5 text-danger"> *</span></label>
							<select name="store_id" id="store_id" class="form-control @error('store_id') is-invalid @enderror">
								@if(!empty($stores))
									@foreach($stores as $store)
										<option value="{{ $store->id }}" @if($staff->department_id == $store->id) selected @endif>{{ $store->store_name }} - {{ $store->store_code }}</option>
									@endforeach
								@endif
							</select>
							@error('store_id')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="form-group">
							<label class="control-label" for="role">Role <span class="m-l-5 text-danger"> *</span></label>
							<select name="roles[]" id="roles" class="form-control @error('roles') is-invalid @enderror" multiple>
								@foreach($roles as $role)
									@php $check = in_array($role->id, $staff->roles->pluck('id')->toArray()) ? 'selected' : ''@endphp
									<option value="{{ $role->id }}" {{ $check }}>{{ $role->name }}</option>
								@endforeach
							</select>
							@error('roles')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="form-group">
							<div class="form-check">
								<label class="form-check-label">
									<input class="form-check-input"
										   type="checkbox"
										   id="status"
										   name="status"
										   {{ $staff->status == 1 ? 'checked' : '' }}
										/>Status
								</label>
							</div>
						</div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Staff</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.staffs.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
$( document ).ready(function() {
	$('#roles').select2();
	var staff_type = $('input[name="staff_type"]:checked').val();
	if(staff_type == 2)
		$("#store_div").show();
});
APP_URL = "{{ url('/') }}";
$("#warehouse_id" ).change(function() {
	var staff_type = $('input[name="staff_type"]:checked').val();
	if(staff_type == 2){
		$.getJSON(APP_URL+"/admin/warehouses/"+ $(this).val() +"/stores", function(jsonData){
			select = '<select name="store_id" class="form-control" required id="store_id" >';
				select +='<option value="">Select</option>';
			  $.each(jsonData, function(i,data)
			  {
				   select +='<option value="'+data.id+'">'+data.store_name+'-'+data.store_code+'</option>';
			   });
			select += '</select>';
			$("#store_id").html(select);
			$("#store_div").show();
		});
	}else{
		$("#store_id").val('');
		$("#store_div").hide();
	}
});

$('input[name="staff_type"]').change(function() {
	var warehouse_id = $('#warehouse_id').val();
	if(warehouse_id > 0){
		$( "#warehouse_id" ).trigger( "change" );
	}
});
</script>
@endpush
