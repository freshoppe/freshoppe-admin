@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fal fa-user"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
		<a href="{{ route('admin.staffs.create') }}" class="btn btn-primary pull-right">Add Staff</a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-center"> Name </th>
                            <th class="text-center"> Email </th>
                            <th class="text-center"> Roles </th>
                            <th class="text-center"> Location / Department </th>
                            <th class="text-center"> Created On </th>
							<th class="text-center"> Active </th>
							<th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                        </tr>
                        </thead>
                        <tbody>
							@foreach($staffs as $key=>$staff)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$staff->name}}</td>
								<td>{{$staff->email}}</td>
								<td>
								  @if(!empty($staff->roles))
									@foreach($staff->roles as $v)
									   <label class="badge badge-success">{{ $v->name }}</label>
									@endforeach
								  @endif
								</td>
								<td>
									@php $staffType = Config::get('constants.staffType'); @endphp
									@if($staff->staff_type != NULL)
										{{$staffType[$staff->staff_type]}}
									@endif
									</br>
									@if($staff->staff_type == 1)
										<b>{{$staff->warehouse->warehouse_name}}</b>
									@endif
									
									@if($staff->staff_type == 2)
										<b>{{$staff->store->store_name}}</b>
									@endif
								</td>
								<td class="text-center">{{ date('d/m/Y',strtotime($staff->created_at))}}</td>
								<td class="text-center">
									@if ($staff->status == 1)
										<span class="badge badge-success">Yes</span>
									@else
										<span class="badge badge-danger">No</span>
									@endif
								</td>
								<td class="text-center">
									<div class="btn-group" role="group" aria-label="Second group">
                                        <a href="{{ route('admin.staffs.edit', $staff->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
									</div>
                                </td>
							</tr>
							@endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
	<script>
	function activate(customerid)
	{
		$.ajax({
            type: "POST",
            url: '{{route('admin.customers.activate')}}',
            data:{
					"_token": "{{ csrf_token() }}",
					"customerid": customerid
				},
            success : function(text){
                window.location.reload();
            }
        });
	}
	</script>
@endpush
