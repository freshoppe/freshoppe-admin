@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.stores.store') }}" method="POST" role="form" enctype="multipart/form-data">
                    @csrf
                    <div class="tile-body">
                        <div class="form-group">
                            <label class="control-label" for="store_name">Store Name <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('store_name') is-invalid @enderror" type="text" name="store_name" id="store_name" value="{{ old('store_name') }}"/>
                            @error('store_name') {{ $message }} @enderror
                        </div>
						<div class="form-group">
                            <label class="control-label" for="store_code">Store Code <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('store_code') is-invalid @enderror" type="text" name="store_code" id="store_code" value="{{ old('store_code') }}"/>
                            @error('store_code') {{ $message }} @enderror
                        </div>
						<div class="form-group">
							<label class="control-label" for="location_id">Location <span class="m-l-5 text-danger"> *</span></label>
							<select name="location_id" id="location_id" class="form-control @error('location_id') is-invalid @enderror">
								<option value="">Select Location</option>
								@foreach($locations as $location)
									<option value="{{ $location->id }}" @if(old('location_id') == $location->id) selected @endif>{{ $location->location_name }} - {{ $location->location_code }}</option>
								@endforeach
							</select>
							@error('location_id') {{ $message }} @enderror
						</div>
						<div class="form-group">
							<label class="control-label" for="warehouse_id">Warehouse <span class="m-l-5 text-danger"> *</span></label>
							<select name="warehouse_id" id="warehouse_id" class="form-control @error('warehouse_id') is-invalid @enderror">
								<option value="">Select Warehouse</option>
								@foreach($warehouses as $warehouse)
									<option value="{{ $warehouse->id }}" @if(old('warehouse_id') == $warehouse->id) selected @endif>{{ $warehouse->warehouse_name }} - {{ $warehouse->warehouse_code }}</option>
								@endforeach
							</select>
							@error('warehouse_id') {{ $message }} @enderror
						</div>
						<div class="form-group">
							<label class="control-label" for="address">Address <span class="m-l-5 text-danger"> *</span></label>
							<textarea name="address" id="address" rows="4" class="form-control @error('address') is-invalid @enderror">{{ old('address') }}</textarea>
							@error('address') {{ $message }} @enderror
						</div>
						<div class="form-group">
                            <label class="control-label" for="pincode">Pincode <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('pincode') is-invalid @enderror" type="text" name="pincode" id="pincode" value="{{ old('pincode') }}"/>
                            @error('pincode') {{ $message }} @enderror
                        </div>
                        <div class="form-group">
                            <label for="store_type">Store Type <span class="m-l-5 text-danger"> *</span></label>
                            <select id="store_type" class="form-control custom-select mt-15 @error('store_type') is-invalid @enderror" name="store_type">
								<option value="1" @if(old('store_type') == 1)selected @endif> Online </option>
								<option value="2" @if(old('store_type') == 2)selected @endif> Takeaway </option>
								<option value="3" @if(old('store_type') == 3)selected @endif> Both </option>
                            </select>
                            @error('store_type') {{ $message }} @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="status" name="status"/>Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Store</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.stores.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">

APP_URL = "{{ url('/') }}";
$( "#location_id" ).change(function() {
    $.getJSON(APP_URL+"/admin/stores/"+ $(this).val() +"/warehouses", function(jsonData){
        select = '<select name="warehouse_id" class="form-control" required id="warehouse_id" >';
			select +='<option value="">Select</option>';
          $.each(jsonData, function(i,data)
          {
               select +='<option value="'+data.id+'">'+data.warehouse_name+'-'+data.warehouse_code+'</option>';
           });
        select += '</select>';
        $("#warehouse_id").html(select);
    });
});

</script>
@endpush
