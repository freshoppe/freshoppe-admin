@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.warehouses.update') }}" method="POST" role="form">
                    @csrf
                    <div class="tile-body">
						<div class="form-group">
                            <label class="control-label" for="warehouse_name">Warehouse Name <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('warehouse_name') is-invalid @enderror" type="text" name="warehouse_name" id="warehouse_name" value="{{ old('warehouse_name', $warehouse->warehouse_name) }}"/>
                            @error('warehouse_name') {{ $message }} @enderror
							<input type="hidden" name="id" value="{{ $warehouse->id }}">
                        </div>
						<div class="form-group">
                            <label class="control-label" for="warehouse_code">Warehouse Code <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('warehouse_code') is-invalid @enderror" type="text" name="warehouse_code" id="warehouse_code" value="{{ old('warehouse_code', $warehouse->warehouse_code) }}"/>
                            @error('warehouse_code') {{ $message }} @enderror
							<input type="hidden" name="id" value="{{ $warehouse->id }}">
                        </div>
                        <div class="form-group">
							<label class="control-label" for="location_id">Location <span class="m-l-5 text-danger"> *</span></label>
							<select name="location_id" id="location_id" class="form-control @error('location_id') is-invalid @enderror">
								<option value="">Select Location</option>
								@foreach($locations as $location)
									<option value="{{ $location->id }}" @if($warehouse->location_id == $location->id) selected @endif>
									{{ $location->location_name }} - {{ $location->location_code }}
									</option>
								@endforeach
							</select>
							@error('location_id') {{ $message }} @enderror
						</div>
						<div class="form-group">
                            <label class="control-label" for="pincode">Pincode <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('pincode') is-invalid @enderror" type="text" name="pincode" id="pincode" value="{{ old('pincode', $warehouse->pincode) }}"/>
                            @error('pincode') {{ $message }} @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="status" name="status"
                                    {{ $warehouse->status == 1 ? 'checked' : '' }}
                                    />Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Warehouse</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.warehouses.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

@endpush
