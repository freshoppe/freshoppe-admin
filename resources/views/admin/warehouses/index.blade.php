@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
        <a href="{{ route('admin.warehouses.create') }}" class="btn btn-primary pull-right">Add Warehouse</a>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Warehouse Name </th>
                                <th> Code </th>
                                <th> Location </th>
                                <th> Pincode </th>
                                <th class="text-center"> Active </th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($warehouses as $key=>$warehouse)
								<tr>
									<td>{{ $key+1 }}</td>
									<td>{{ $warehouse->warehouse_name }}</td>
									<td>{{ $warehouse->warehouse_code }}</td>
									<td>{{ $warehouse->location->location_name }} - {{ $warehouse->location->location_code }}</td>
									<td>{{ $warehouse->pincode }}</td>
									<td class="text-center">
										@if ($warehouse->status == 1)
											<span class="badge badge-success">Yes</span>
										@else
											<span class="badge badge-danger">No</span>
										@endif
									</td>
									<td class="text-center">
										<div class="btn-group" role="group" aria-label="Second group">
											<a href="{{ route('admin.warehouses.edit', $warehouse->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
											<!--<a href="{{ route('admin.warehouses.delete', $warehouse->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>-->
										</div>
									</td>
								</tr>
						@endforeach
					</tbody>
				</table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endpush
