@extends('site.app')
@section('title', 'Login')
@section('content')
	<!-- Start Login Area -->
	<section class="login-area ptb-100">
		<div class="container">
			<div class="login-form">
				<h2>Login</h2>

				<form action="{{ route('login') }}" method="POST" role="form" name="login" id="login">
					@csrf
					<div class="form-group">
						<label for="email">Email Address</label>
						<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}">
						@error('email')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>

					<div class="form-group">
						 <label for="password">Password</label>
						<input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password">
						@error('password')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>

					<div class="row align-items-center">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
								<label class="form-check-label" for="remember">Remember me</label>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 lost-your-password">
							<a href="{{route('password.request')}}" class="lost-your-password">Lost your password?</a>
						</div>
					</div>

					<button type="submit" class="default-btn">Login</button>
					<div class="card-body text-center">Don't have an account? <a href="{{ route('register') }}">Sign Up</a></div>
				</form>
			</div>
		</div>
	</section>
	<!-- End Login Area -->
@stop
