@extends('site.app')
@section('title', 'Login Reset')
@section('content')
	<section class="login-area ptb-100">
		<div class="container">
			<div class="login-form">
				<h2>Reset Password</h2>
				<form action="{{route('password.email')}}" method="post" name="login" id="login">
					{{csrf_field()}}
					<div class="form-group">
						<input type="email" class="form-control form-control-danger" placeholder="Enter email" name="email">
					</div>
					<button type="submit" class="default-btn">Send Reset Link</button>
				</form>
			</div>
		</div>
	</section>
@stop
