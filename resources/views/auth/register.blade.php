@extends('site.app')


@section('title', 'Register')

@push('scripts')
	
	<script>
      // This sample uses the Autocomplete widget to help the user select a
      // place, then it retrieves the address components associated with that
      // place, and then it populates the form fields with those details.
      // This sample requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script
      // src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      /* let placeSearch;
      let autocomplete;

      const componentForm = {
        locality: "long_name",
        administrative_area_level_1: "long_name",
        country: "long_name",
        postal_code: "short_name",
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(document.getElementById("address"));
        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields(["address_component"]);
        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener("place_changed", fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        const place = autocomplete.getPlace();

        for (const component in componentForm) {
          document.getElementById(component).value = "";
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        for (const component of place.address_components) {
          const addressType = component.types[0];

          if (componentForm[addressType]) {
            const val = component[componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      } */
      
    </script>
	@endpush
@section('content')
    <section class="register-area ptb-100">
		<div class="container">
			<div class="register-form">
				<h2>Register</h2>
				<form action="{{ route('register') }}" method="POST" role="form" name="register" id="register">
					@csrf
					<div class="form-row">
						<div class="col form-group">
							<label for="first_name">First name</label>
							<input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" id="first_name" value="{{ old('first_name') }}">
							@error('first_name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
						<div class="col form-group">
							<label for="last_name">Last name</label>
							<input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" id="last_name" value="{{ old('last_name') }}">
							@error('last_name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}">
						@error('email')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password">
						@error('password')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<div class="form-group">
						<label for="password_confirmation">Confirm Password</label>
						<input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="password_confirmation">
						@error('password_confirmation')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<div id="locationField" class="form-group">
						<label for="address">Address</label>
						<input name="address" id="address" class="form-control" placeholder="Enter your address" type="text" />
					</div>

					<!--<div class="form-group">
						<label for="address">Address</label>
						<input class="form-control" type="text" name="address" id="address" value="{{ old('address') }}">
					</div>-->
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="city">City</label>
							<input type="text" class="form-control" name="city" id="city" value="{{ old('city') }}">
						</div>
						<div class="form-group col-md-6">
							<label for="state">State</label>
							<input type="text" class="form-control" name="state" id="state" value="{{ old('state') }}">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="pincode">Pin Code</label>
							<input type="text" class="form-control" name="pincode" id="pincode" value="{{ old('pincode') }}">
						</div>
						<div class="form-group col-md-6">
							<label for="country">Country</label>
							<!--<input type="text" class="form-control" name="country" id="country" value="{{ old('country') }}">-->
							<select id="country" class="form-control" name="country">
								<option value=""> Choose...</option>
								<option value="India">India</option>
								<option value="United Kingdom">United Kingdom</option>
								<option value="France">France</option>
								<option value="United States">United States</option>
							</select>
						</div>
					</div>
					<button type="submit" class="default-btn">Register</button>
					<div class="text-center">
						<small class="text-muted text-center">By clicking the 'Sign Up' button, you confirm that you accept our Terms of use and Privacy Policy.</small>
					</div>
                    <div class="card-body text-center">Have an account? <a href="{{ route('login') }}">Log In</a></div>
				</form>
            </div>
        </div>
    </section>
	
@stop
