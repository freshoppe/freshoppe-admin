<?php

Route::get('/', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::group(['prefix'  =>  'admin'], function () {
	Route::get('/', 'Admin\LoginController@showLoginForm')->name('login');
    Route::get('login', 'Admin\LoginController@showLoginForm')->name('login');
    Route::get('login', 'Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Admin\LoginController@login')->name('admin.login.post');
    Route::get('logout', 'Admin\LoginController@logout')->name('admin.logout');
	
    Route::group(['middleware' => ['auth:admin']], function () {


        Route::get('/', function () {
            return view('admin.dashboard.index');
        })->name('admin.dashboard');

		Route::group(['middleware' => 'role:super-admin'], function() {
			Route::get('/settings', 'Admin\SettingController@index')->name('admin.settings');
			Route::post('/settings', 'Admin\SettingController@update')->name('admin.settings.update');
			Route::get('/general', 'Admin\GeneralContentController@index')->name('admin.general');
			Route::post('/general', 'Admin\GeneralContentController@update')->name('admin.general.update');
		});

		Route::group(['middleware' => 'permission:product-category-master'], function() {
			Route::group(['prefix'  =>   'categories'], function() {
				Route::get('/', 'Admin\CategoryController@index')->name('admin.categories.index');
				Route::get('/create', 'Admin\CategoryController@create')->name('admin.categories.create');
				Route::post('/store', 'Admin\CategoryController@store')->name('admin.categories.store');
				Route::get('/{id}/edit', 'Admin\CategoryController@edit')->name('admin.categories.edit');
				Route::post('/update', 'Admin\CategoryController@update')->name('admin.categories.update');
				Route::get('/{id}/delete', 'Admin\CategoryController@delete')->name('admin.categories.delete');
			});
		});
		
		Route::group(['middleware' => 'permission:product-attribute-list'], function() {
			Route::group(['prefix'  =>   'attributes'], function() {
				Route::get('/', 'Admin\AttributeController@index')->name('admin.attributes.index');
				Route::get('/create', 'Admin\AttributeController@create')->name('admin.attributes.create');
				Route::post('/store', 'Admin\AttributeController@store')->name('admin.attributes.store');
				Route::get('/{id}/edit', 'Admin\AttributeController@edit')->name('admin.attributes.edit');
				Route::post('/update', 'Admin\AttributeController@update')->name('admin.attributes.update');
				Route::get('/{id}/delete', 'Admin\AttributeController@delete')->name('admin.attributes.delete');
				
				Route::post('/get-values', 'Admin\AttributeValueController@getValues');
				Route::post('/add-values', 'Admin\AttributeValueController@addValues');
				Route::post('/update-values', 'Admin\AttributeValueController@updateValues');
				Route::post('/delete-values', 'Admin\AttributeValueController@deleteValues');
			});
		});
		
		
        Route::group(['prefix' => 'products'], function () {

			Route::get('/', 'Admin\ProductController@index')->name('admin.products.index')->middleware('permission:product-list');
			Route::get('/create', 'Admin\ProductController@create')->name('admin.products.create')->middleware('permission:product-create');
			Route::post('/store', 'Admin\ProductController@store')->name('admin.products.store')->middleware('permission:product-create');
			Route::get('/edit/{id}', 'Admin\ProductController@edit')->name('admin.products.edit')->middleware('permission:product-edit');
			Route::post('/update', 'Admin\ProductController@update')->name('admin.products.update')->middleware('permission:product-edit');
			Route::get('/list', 'Admin\ProductController@getActiveProducts');
			Route::get('/details/{id}', 'Admin\ProductController@details');
			
			Route::post('images/upload', 'Admin\ProductImageController@upload')->name('admin.products.images.upload');
			Route::get('images/{id}/delete', 'Admin\ProductImageController@delete')->name('admin.products.images.delete');

			Route::get('item/processing/{id}', 'Admin\ProductController@itemProcessing');
			
			Route::get('attributes/load', 'Admin\ProductAttributeController@loadAttributes');
			Route::post('attributes', 'Admin\ProductAttributeController@productAttributes');
			Route::post('attributes/values', 'Admin\ProductAttributeController@loadValues');
			Route::post('attributes/add', 'Admin\ProductAttributeController@addAttribute');
			Route::post('attributes/delete', 'Admin\ProductAttributeController@deleteAttribute');
        });
		Route::group(['prefix' => 'orders'], function () {
		   Route::get('/', 'Admin\OrderController@index')->name('admin.orders.index')->middleware('permission:order-detail');
		   Route::get('/create', 'Admin\OrderController@create')->name('admin.orders.create')->middleware('permission:offline-order-taking');
		   Route::get('/{order}/edit', 'Admin\OrderController@edit')->name('admin.orders.edit')->middleware('permission:offline-order-taking');
		   Route::post('/update', 'Admin\OrderController@update')->name('admin.orders.update')->middleware('permission:order-processing');
		   Route::post('/store', 'Admin\OrderController@store')->name('admin.orders.store');
		   Route::post('/update-status', 'Admin\OrderController@updateStatus')->name('admin.orders.update.status');
		   Route::get('/{order}/show', 'Admin\OrderController@show')->name('admin.orders.show')->middleware('permission:order-detail');
		   Route::get('/details/{id}', 'Admin\OrderController@getOrderDetails');
		});
		
		Route::group(['prefix' => 'billing'], function () {
		   Route::get('/', 'Admin\OrderController@billing')->name('admin.billing.index')->middleware('permission:invoicing');
		   Route::post('/update-status', 'Admin\OrderController@updateStatus')->name('admin.billing.update.status');
		   Route::get('/{order}/show', 'Admin\OrderController@billingShow')->name('admin.billing.show')->middleware('permission:invoicing');
		});
		
		
		Route::group(['prefix' => 'customers'], function () {
		   Route::get('/', 'Admin\UserController@index')->name('admin.customers.index')->middleware('permission:customer-management');
		   Route::get('/{id}/edit', 'Admin\UserController@edit')->name('admin.customers.edit')->middleware('permission:customer-management');
		   Route::post('/update', 'Admin\UserController@update')->name('admin.customers.update')->middleware('permission:customer-management');
		   Route::get('/{id}/destroy', 'Admin\UserController@destroy')->name('admin.customers.destroy')->middleware('permission:customer-management');
		   Route::post('/activate', 'Admin\UserController@activate_user')->name('admin.customers.activate')->middleware('permission:customer-management');
		});
		
		
		Route::group(['prefix'  =>   'designations'], function() {
			Route::get('/', 'Admin\RoleController@index')->name('admin.roles.index')->middleware('permission:designation-list');
			Route::get('/create', 'Admin\RoleController@create')->name('admin.roles.create')->middleware('permission:designation-creation');
			Route::post('/store', 'Admin\RoleController@store')->name('admin.roles.store')->middleware('permission:designation-creation');
			Route::get('/{id}/edit', 'Admin\RoleController@edit')->name('admin.roles.edit')->middleware('permission:designation-edit');
			Route::post('/update', 'Admin\RoleController@update')->name('admin.roles.update')->middleware('permission:designation-edit');
		});
	
		Route::group(['prefix'  => 'staffs'], function() {
			Route::get('/', 'Admin\StaffController@index')->name('admin.staffs.index')->middleware('permission:staff-list');
			Route::get('/create', 'Admin\StaffController@create')->name('admin.staffs.create')->middleware('permission:staff-creation');
			Route::post('/store', 'Admin\StaffController@store')->name('admin.staffs.store')->middleware('permission:staff-creation');
			Route::get('/{id}/edit', 'Admin\StaffController@edit')->name('admin.staffs.edit')->middleware('permission:staff-edit');
			Route::post('/update', 'Admin\StaffController@update')->name('admin.staffs.update')->middleware('permission:staff-edit');
		});
		
		Route::group(['prefix'  => 'stores'], function() {
			Route::get('/', 'Admin\StoreController@index')->name('admin.stores.index')->middleware('permission:store-list');
			Route::get('/create', 'Admin\StoreController@create')->name('admin.stores.create')->middleware('permission:store-creation');
			Route::post('/store', 'Admin\StoreController@store')->name('admin.stores.store')->middleware('permission:store-creation');
			Route::get('/{id}/edit', 'Admin\StoreController@edit')->name('admin.stores.edit')->middleware('permission:store-edit');
			Route::post('/update', 'Admin\StoreController@update')->name('admin.stores.update')->middleware('permission:store-edit');
			Route::get('/{id}/warehouses', 'Admin\StoreController@getWarehousesByLocation');
		});
	
		Route::group(['prefix'  => 'locations'], function() {

			Route::get('/', 'Admin\LocationController@index')->name('admin.locations.index')->middleware('permission:location-list');
			Route::get('/create', 'Admin\LocationController@create')->name('admin.locations.create')->middleware('permission:location-creation');
			Route::post('/store', 'Admin\LocationController@store')->name('admin.locations.store')->middleware('permission:location-creation');
			Route::get('/{id}/edit', 'Admin\LocationController@edit')->name('admin.locations.edit')->middleware('permission:location-edit');
			Route::post('/update', 'Admin\LocationController@update')->name('admin.locations.update')->middleware('permission:location-edit');

		});
	
		Route::group(['prefix'  => 'warehouses'], function() {

			Route::get('/', 'Admin\WarehouseController@index')->name('admin.warehouses.index')->middleware('permission:warehouse-list');
			Route::get('/create', 'Admin\WarehouseController@create')->name('admin.warehouses.create')->middleware('permission:warehouse-creation');
			Route::post('/store', 'Admin\WarehouseController@store')->name('admin.warehouses.store')->middleware('permission:warehouse-creation');
			Route::get('/{id}/edit', 'Admin\WarehouseController@edit')->name('admin.warehouses.edit')->middleware('permission:warehouse-edit');
			Route::post('/update', 'Admin\WarehouseController@update')->name('admin.warehouses.update')->middleware('permission:warehouse-edit');
			Route::get('/{id}/delete', 'Admin\WarehouseController@delete')->name('admin.warehouses.delete')->middleware('permission:warehouse-delete');
			Route::get('/{id}/stores', 'Admin\WarehouseController@getStores');

		});
		Route::group(['middleware' => 'permission:processing-way-master'], function() {
			Route::group(['prefix'  => 'processing-ways'], function() {
				Route::get('/', 'Admin\ProcessingController@index')->name('admin.processing.index');
				Route::get('/create', 'Admin\ProcessingController@create')->name('admin.processing.create');
				Route::post('/store', 'Admin\ProcessingController@store')->name('admin.processing.store');
				Route::get('/{id}/edit', 'Admin\ProcessingController@edit')->name('admin.processing.edit');
				Route::post('/update', 'Admin\ProcessingController@update')->name('admin.processing.update');
				Route::get('/{id}/delete', 'Admin\ProcessingController@delete')->name('admin.processing.delete');

			});
		});
		
		//Route::group(['middleware' => 'permission:recipes-master'], function() {
			Route::group(['prefix'  => 'recipes'], function() {
				Route::get('/', 'Admin\RecipeController@index')->name('admin.recipes.index');
				Route::get('/create', 'Admin\RecipeController@create')->name('admin.recipes.create');
				Route::post('/store', 'Admin\RecipeController@store')->name('admin.recipes.store');
				Route::get('/{id}/edit', 'Admin\RecipeController@edit')->name('admin.recipes.edit');
				Route::post('/update', 'Admin\RecipeController@update')->name('admin.recipes.update');
				Route::get('/{id}/delete', 'Admin\RecipeController@delete')->name('admin.recipes.delete');

			});
		//});
		
		//Route::group(['middleware' => 'permission:banners-master'], function() {
			Route::group(['prefix'  => 'banners'], function() {
				Route::get('/', 'Admin\BannerController@index')->name('admin.banners.index');
				Route::get('/create', 'Admin\BannerController@create')->name('admin.banners.create');
				Route::post('/store', 'Admin\BannerController@store')->name('admin.banners.store');
				Route::get('/{id}/edit', 'Admin\BannerController@edit')->name('admin.banners.edit');
				Route::post('/update', 'Admin\BannerController@update')->name('admin.banners.update');
				Route::get('/{id}/delete', 'Admin\BannerController@delete')->name('admin.banners.delete');

			});
		//});
		
    });
	Route::get('/roles-list', 'Admin\PermissionController@permission');
});
