<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 


Route::group(['middleware' => ['auth:admin']], function () {
	
	Route::get('/admin/inventory/dashboard', function () {
		return view('admin.inventory.dashboard.index');
	})->name('admin.inventory.dashboard');
	
	/* ============ITEM MASTER======================= */
	Route::group(['middleware' => 'permission:item-master'], function() {
		Route::group(['prefix'  =>   '/admin/inventory/items'], function() {

			Route::get('/', 'Inv\ItemController@index')->name('admin.inventory.items.index');
			Route::get('/create', 'Inv\ItemController@create')->name('admin.inventory.items.create');
			Route::post('/store', 'Inv\ItemController@store')->name('admin.inventory.items.store');
			Route::get('/{id}/edit', 'Inv\ItemController@edit')->name('admin.inventory.items.edit');
			Route::post('/update', 'Inv\ItemController@update')->name('admin.inventory.items.update');
			Route::post('/delete', 'Inv\ItemController@delete')->name('admin.inventory.items.delete');
			
			Route::post('/get_by_id_json', 'Inv\ItemController@get_by_id_json'); 
		});
	});
	
	/* ============SUPPLIER MASTER======================= */
	Route::group(['middleware' => 'permission:supplier-master'], function() {
		Route::group(['prefix'  =>   '/admin/inventory/suppliers'], function() {

			Route::get('/', 'Inv\SupplierController@index')->name('admin.inventory.suppliers.index');
			Route::get('/create', 'Inv\SupplierController@create')->name('admin.inventory.suppliers.create');
			Route::post('/store', 'Inv\SupplierController@store')->name('admin.inventory.suppliers.store');
			Route::get('/{id}/edit', 'Inv\SupplierController@edit')->name('admin.inventory.suppliers.edit');
			Route::post('/update', 'Inv\SupplierController@update')->name('admin.inventory.suppliers.update');
			Route::post('/delete', 'Inv\SupplierController@delete')->name('admin.inventory.suppliers.delete');

		});
	});
	
	
	/* =============================ITEM TRANSACTIONS========================================================================== */
	
	/* ============ITEM PURCHASE======================= */
	Route::group(['middleware' => 'permission:item-purchase'], function() {
		Route::group(['prefix'  =>   '/admin/inventory/item-purchases'], function() {
			Route::get('/', 'Inv\ItemPurchaseController@index')->name('admin.inventory.item-purchases.index');
			Route::get('/create', 'Inv\ItemPurchaseController@create')->name('admin.inventory.item-purchases.create');
			Route::post('/store', 'Inv\ItemPurchaseController@store')->name('admin.inventory.item-purchases.store');
			Route::post('/draft', 'Inv\ItemPurchaseController@draft')->name('admin.inventory.item-purchases.draft');
			Route::get('/{id}/edit', 'Inv\ItemPurchaseController@edit')->name('admin.inventory.item-purchases.edit');
			Route::post('/update', 'Inv\ItemPurchaseController@update')->name('admin.inventory.item-purchases.update');
			Route::get('/{id}/delete', 'Inv\ItemPurchaseController@delete')->name('admin.inventory.item-purchases.delete');
			Route::post('/update_draft', 'Inv\ItemPurchaseController@update_draft')->name('admin.inventory.item-purchases.update_draft');
			Route::post('/update_draft_as_purchase', 'Inv\ItemPurchaseController@update_draft_as_purchase')->name('admin.inventory.item-purchases.update_draft_as_purchase');
			Route::post('/delete_draft', 'Inv\ItemPurchaseController@delete_draft')->name('admin.inventory.item-purchases.delete_draft');
		});
	});
	
	
	/* ============ITEM TRANSFER======================= */
	Route::group(['middleware' => 'permission:item-transfer-to-stores'], function() {
		Route::group(['prefix'  =>   '/admin/inventory/item-transfers'], function() {

			Route::get('/', 'Inv\ItemTransferController@index')->name('admin.inventory.item-transfers.index');
			Route::get('/create', 'Inv\ItemTransferController@create')->name('admin.inventory.item-transfers.create');
			Route::post('/store', 'Inv\ItemTransferController@store')->name('admin.inventory.item-transfers.store');
			Route::post('/draft', 'Inv\ItemTransferController@draft')->name('admin.inventory.item-transfers.draft');
			Route::get('/{id}/edit', 'Inv\ItemTransferController@edit')->name('admin.inventory.item-transfers.edit');
			Route::post('/update', 'Inv\ItemTransferController@update')->name('admin.inventory.item-transfers.update');
			Route::get('/{id}/delete', 'Inv\ItemTransferController@delete')->name('admin.inventory.item-transfers.delete');
			Route::post('/update_draft', 'Inv\ItemTransferController@update_draft')->name('admin.inventory.item-transfers.update_draft');
			Route::post('/update_draft_as_transfer', 'Inv\ItemTransferController@update_draft_as_transfer')->name('admin.inventory.item-transfers.update_draft_as_transfer');
			Route::post('/delete_draft', 'Inv\ItemTransferController@delete_draft')->name('admin.inventory.item-transfers.delete_draft');
			

		}); 
	}); 
	
	/*
	Route::group(['middleware' => 'permission:item-receiving'], function() {
		Route::group(['prefix'  =>   '/admin/inventory/store/stock-receives'], function() {
			Route::get('/', 'Inv\StockItemReceiveController@index')->name('admin.inventory.store-stock-receives.index');
			Route::post('/search', 'Inv\StockItemReceiveController@index')->name('admin.inventory.store-stock-receives.search');
			Route::get('/{id}/show', 'Inv\StockItemReceiveController@show')->name('admin.inventory.store-stock-receives.show');
			Route::post('/confirm', 'Inv\StockItemReceiveController@confirm')->name('admin.inventory.store-stock-receives.confirm');
			Route::post('/update', 'Inv\StockItemReceiveController@update')->name('admin.inventory.store-stock-receives.update');

		});
	});
	*/
	
	/* ============ITEM RECEIVE======================= */
	Route::group(['middleware' => 'permission:item-receiving'], function() 
	{
		Route::group(['prefix'  =>   '/admin/inventory/item-receives'], function(){

			Route::get('/', 'Inv\ItemReceiveController@index')->name('admin.inventory.item-receives.index');
			Route::get('/{id}/receive', 'Inv\ItemReceiveController@receive')->name('admin.inventory.item-receives.receive');
			Route::get('/{id}/view', 'Inv\ItemReceiveController@view')->name('admin.inventory.item-receives.view');
			Route::post('/store', 'Inv\ItemReceiveController@store')->name('admin.inventory.item-receives.store');

		});
		
	});
	
	/* ============ITEM PURCHASE RETURNS======================= */		
		Route::group(['prefix'  =>   '/admin/inventory/item-purchase-returns'], function() {
			Route::get('/', 'Inv\ItemPurchaseReturnController@index')->name('admin.inventory.item-purchase-returns.index');
			Route::get('/create', 'Inv\ItemPurchaseReturnController@create')->name('admin.inventory.item-purchase-returns.create');
			Route::post('/store', 'Inv\ItemPurchaseReturnController@store')->name('admin.inventory.item-purchase-returns.store');
			Route::post('/draft', 'Inv\ItemPurchaseReturnController@draft')->name('admin.inventory.item-purchase-returns.draft');
			Route::get('/{id}/edit', 'Inv\ItemPurchaseReturnController@edit')->name('admin.inventory.item-purchase-returns.edit');
			Route::post('/update', 'Inv\ItemPurchaseReturnController@update')->name('admin.inventory.item-purchase-returns.update');
			Route::get('/{id}/delete', 'Inv\ItemPurchaseReturnController@delete')->name('admin.inventory.item-purchase-returns.delete');
			Route::post('/update_draft', 'Inv\ItemPurchaseReturnController@update_draft')->name('admin.inventory.item-purchase-returns.update_draft');
			Route::post('/update_draft_as_purchase_return', 'Inv\ItemPurchaseReturnController@update_draft_as_purchase_return')->name('admin.inventory.item-purchase-returns.update_draft_as_purchase_return');
			Route::post('/delete_draft', 'Inv\ItemPurchaseReturnController@delete_draft')->name('admin.inventory.item-purchase-returns.delete_draft');
		});
	
	
	
	/* ============ITEM TRANSFER RETURNS======================= */
	
	Route::group(['prefix'  =>   '/admin/inventory/item-transfer-returns'], function() {

		Route::get('/', 'Inv\ItemTransferReturnController@index')->name('admin.inventory.item-transfer-returns.index');
		Route::get('/create', 'Inv\ItemTransferReturnController@create')->name('admin.inventory.item-transfer-returns.create');
		Route::post('/store', 'Inv\ItemTransferReturnController@store')->name('admin.inventory.item-transfer-returns.store');
		Route::post('/draft', 'Inv\ItemTransferReturnController@draft')->name('admin.inventory.item-transfer-returns.draft');
		Route::get('/{id}/edit', 'Inv\ItemTransferReturnController@edit')->name('admin.inventory.item-transfer-returns.edit');
		Route::post('/update', 'Inv\ItemTransferReturnController@update')->name('admin.inventory.item-transfer-returns.update');
		Route::get('/{id}/delete', 'Inv\ItemTransferReturnController@delete')->name('admin.inventory.item-transfer-returns.delete');
		Route::post('/update_draft', 'Inv\ItemTransferReturnController@update_draft')->name('admin.inventory.item-transfer-returns.update_draft');
		Route::post('/update_draft_as_transfer_return', 'Inv\ItemTransferReturnController@update_draft_as_transfer_return')->name('admin.inventory.item-transfer-returns.update_draft_as_transfer_return');
		Route::post('/delete_draft', 'Inv\ItemTransferReturnController@delete_draft')->name('admin.inventory.item-transfer-returns.delete_draft');
		

	}); 
	
	/* ============ITEM RECEIVE RETURNS======================= */
	/*
	Route::group(['prefix'  =>   '/admin/inventory/item-receive-returns'], function() 
	{
		
		Route::get('/', 'Inv\ItemReceiveReturnController@index')->name('admin.inventory.item-receive-returns.index');
		Route::get('/{id}/return-receive', 'Inv\ItemReceiveReturnController@return_receive')->name('admin.inventory.item-receive-returns.return-receive');
		Route::get('/{id}/view-receive-return', 'Inv\ItemReceiveReturnController@view_receive_return')->name('admin.inventory.item-receive-returns.view-receive-return');
		Route::post('/store', 'Inv\ItemReceiveReturnController@store')->name('admin.inventory.item-receive-returns.store');
		
		
	});
	*/
		Route::group(['prefix'  =>   '/admin/inventory/item-receive-returns'], function() {

			Route::get('/', 'Inv\ItemReceiveReturnController@index')->name('admin.inventory.item-receive-returns.index');
			Route::get('/create', 'Inv\ItemReceiveReturnController@create')->name('admin.inventory.item-receive-returns.create');
			Route::post('/store', 'Inv\ItemReceiveReturnController@store')->name('admin.inventory.item-receive-returns.store');
			Route::post('/draft', 'Inv\ItemReceiveReturnController@draft')->name('admin.inventory.item-receive-returns.draft');
			Route::get('/{id}/edit', 'Inv\ItemReceiveReturnController@edit')->name('admin.inventory.item-receive-returns.edit');
			Route::post('/update', 'Inv\ItemReceiveReturnController@update')->name('admin.inventory.item-receive-returns.update');
			Route::get('/{id}/delete', 'Inv\ItemReceiveReturnController@delete')->name('admin.inventory.item-receive-returns.delete');
			Route::post('/update_draft', 'Inv\ItemReceiveReturnController@update_draft')->name('admin.inventory.item-receive-returns.update_draft');
			Route::post('/update_draft_as_receive_return', 'Inv\ItemReceiveReturnController@update_draft_as_receive_return')->name('admin.inventory.item-receive-returns.update_draft_as_receive_return');
			Route::post('/delete_draft', 'Inv\ItemReceiveReturnController@delete_draft')->name('admin.inventory.item-receive-returns.delete_draft');
			

		}); 
	
	
	/* =============================PRODUCT TRANSACTIONS========================================================================== */
	
	/* ============PRODUCT PURCHASE======================= */
	Route::group(['middleware' => 'permission:product-purchase'], function() {
		Route::group(['prefix'  =>   '/admin/inventory/prod-purchases'], function(){

			Route::get('/', 'Inv\ProdPurchaseController@index')->name('admin.inventory.prod-purchases.index');
			Route::get('/create', 'Inv\ProdPurchaseController@create')->name('admin.inventory.prod-purchases.create');
			Route::post('/store', 'Inv\ProdPurchaseController@store')->name('admin.inventory.prod-purchases.store');
			Route::post('/draft', 'Inv\ProdPurchaseController@draft')->name('admin.inventory.prod-purchases.draft');
			Route::get('/{id}/edit', 'Inv\ProdPurchaseController@edit')->name('admin.inventory.prod-purchases.edit');
			Route::post('/update', 'Inv\ProdPurchaseController@update')->name('admin.inventory.prod-purchases.update');
			Route::get('/{id}/delete', 'Inv\ProdPurchaseController@delete')->name('admin.inventory.prod-purchases.delete');
			Route::post('/update_draft', 'Inv\ProdPurchaseController@update_draft')->name('admin.inventory.prod-purchases.update_draft');
			Route::post('/update_draft_as_purchase', 'Inv\ProdPurchaseController@update_draft_as_purchase')->name('admin.inventory.prod-purchases.update_draft_as_purchase');
			Route::post('/delete_draft', 'Inv\ProdPurchaseController@delete_draft')->name('admin.inventory.prod-purchases.delete_draft');

		});
	});
	
	
	/* ============PRODUCT TRANSFER======================= */
	Route::group(['middleware' => 'permission:product-transfer-to-stores'], function() {
		Route::group(['prefix'  =>   '/admin/inventory/prod-transfers'], function(){

			Route::get('/', 'Inv\ProdTransferController@index')->name('admin.inventory.prod-transfers.index');
			Route::get('/create', 'Inv\ProdTransferController@create')->name('admin.inventory.prod-transfers.create');
			Route::post('/store', 'Inv\ProdTransferController@store')->name('admin.inventory.prod-transfers.store');
			Route::post('/draft', 'Inv\ProdTransferController@draft')->name('admin.inventory.prod-transfers.draft');
			Route::get('/{id}/edit', 'Inv\ProdTransferController@edit')->name('admin.inventory.prod-transfers.edit');
			Route::post('/update', 'Inv\ProdTransferController@update')->name('admin.inventory.prod-transfers.update');
			Route::get('/{id}/delete', 'Inv\ProdTransferController@delete')->name('admin.inventory.prod-transfers.delete');
			Route::post('/update_draft', 'Inv\ProdTransferController@update_draft')->name('admin.inventory.prod-transfers.update_draft');
			Route::post('/update_draft_as_transfer', 'Inv\ProdTransferController@update_draft_as_transfer')->name('admin.inventory.prod-transfers.update_draft_as_transfer');
			Route::post('/delete_draft', 'Inv\ProdTransferController@delete_draft')->name('admin.inventory.prod-transfers.delete_draft');

		});
	});
	
	
	/* ============PRODUCT RECEIVE======================= */
	Route::group(['prefix'  =>   '/admin/inventory/prod-receives'], function(){

		Route::get('/', 'Inv\ProdReceiveController@index')->name('admin.inventory.prod-receives.index');
		Route::get('/{id}/receive', 'Inv\ProdReceiveController@receive')->name('admin.inventory.prod-receives.receive');
		Route::get('/{id}/view', 'Inv\ProdReceiveController@view')->name('admin.inventory.prod-receives.view');
		Route::post('/store', 'Inv\ProdReceiveController@store')->name('admin.inventory.prod-receives.store');

	});
	
	
	/* ============PRODUCT PURCHASE RETURNS======================= */	
	/*
	Route::group(['prefix'  =>   '/admin/inventory/prod-purchase-returns'], function() 
	{
		
		Route::get('/', 'Inv\ProdPurchaseReturnController@index')->name('admin.inventory.prod-purchase-returns.index');
		Route::get('/{id}/return-purchase', 'Inv\ProdPurchaseReturnController@return_purchase')->name('admin.inventory.prod-purchase-returns.return-purchase');
		Route::get('/{id}/view-purchase-return', 'Inv\ProdPurchaseReturnController@view_purchase_return')->name('admin.inventory.prod-purchase-returns.view-purchase-return');
		Route::post('/store', 'Inv\ProdPurchaseReturnController@store')->name('admin.inventory.prod-purchase-returns.store');
		
		
	});
	*/
	
	Route::group(['prefix'  =>   '/admin/inventory/prod-purchase-returns'], function(){

		Route::get('/', 'Inv\ProdPurchaseReturnController@index')->name('admin.inventory.prod-purchase-returns.index');
		Route::get('/create', 'Inv\ProdPurchaseReturnController@create')->name('admin.inventory.prod-purchase-returns.create');
		Route::post('/store', 'Inv\ProdPurchaseReturnController@store')->name('admin.inventory.prod-purchase-returns.store');
		Route::post('/draft', 'Inv\ProdPurchaseReturnController@draft')->name('admin.inventory.prod-purchase-returns.draft');
		Route::get('/{id}/edit', 'Inv\ProdPurchaseReturnController@edit')->name('admin.inventory.prod-purchase-returns.edit');
		Route::post('/update', 'Inv\ProdPurchaseReturnController@update')->name('admin.inventory.prod-purchase-returns.update');
		Route::get('/{id}/delete', 'Inv\ProdPurchaseReturnController@delete')->name('admin.inventory.prod-purchase-returns.delete');
		Route::post('/update_draft', 'Inv\ProdPurchaseReturnController@update_draft')->name('admin.inventory.prod-purchase-returns.update_draft');
		Route::post('/update_draft_as_purchase_return', 'Inv\ProdPurchaseReturnController@update_draft_as_purchase_return')->name('admin.inventory.prod-purchase-returns.update_draft_as_purchase_return');
		Route::post('/delete_draft', 'Inv\ProdPurchaseReturnController@delete_draft')->name('admin.inventory.prod-purchase-returns.delete_draft');

	});
	
	
	
	/* ============PRODUCT TRANSFER RETURNS======================= */
	
	Route::group(['prefix'  =>   '/admin/inventory/prod-transfer-returns'], function(){

		Route::get('/', 'Inv\ProdTransferReturnController@index')->name('admin.inventory.prod-transfer-returns.index');
		Route::get('/create', 'Inv\ProdTransferReturnController@create')->name('admin.inventory.prod-transfer-returns.create');
		Route::post('/store', 'Inv\ProdTransferReturnController@store')->name('admin.inventory.prod-transfer-returns.store');
		Route::post('/draft', 'Inv\ProdTransferReturnController@draft')->name('admin.inventory.prod-transfer-returns.draft');
		Route::get('/{id}/edit', 'Inv\ProdTransferReturnController@edit')->name('admin.inventory.prod-transfer-returns.edit');
		Route::post('/update', 'Inv\ProdTransferReturnController@update')->name('admin.inventory.prod-transfer-returns.update');
		Route::get('/{id}/delete', 'Inv\ProdTransferReturnController@delete')->name('admin.inventory.prod-transfer-returns.delete');
		Route::post('/update_draft', 'Inv\ProdTransferReturnController@update_draft')->name('admin.inventory.prod-transfer-returns.update_draft');
		Route::post('/update_draft_as_transfer_return', 'Inv\ProdTransferReturnController@update_draft_as_transfer_return')->name('admin.inventory.prod-transfer-returns.update_draft_as_transfer_return');
		Route::post('/delete_draft', 'Inv\ProdTransferReturnController@delete_draft')->name('admin.inventory.prod-transfer-returns.delete_draft');

	});
		
		
	/* ============PRODUCT RECEIVE RETURNS======================= */
	/*
	Route::group(['prefix'  =>   '/admin/inventory/prod-receive-returns'], function() 
	{
		
		Route::get('/', 'Inv\ProdReceiveReturnController@index')->name('admin.inventory.prod-receive-returns.index');
		Route::get('/{id}/return-receive', 'Inv\ProdReceiveReturnController@return_receive')->name('admin.inventory.prod-receive-returns.return-receive');
		Route::get('/{id}/view-receive-return', 'Inv\ProdReceiveReturnController@view_receive_return')->name('admin.inventory.prod-receive-returns.view-receive-return');
		Route::post('/store', 'Inv\ProdReceiveReturnController@store')->name('admin.inventory.prod-receive-returns.store');
		
	});
	*/
	
		Route::group(['prefix'  =>   '/admin/inventory/prod-receive-returns'], function(){

		Route::get('/', 'Inv\ProdReceiveReturnController@index')->name('admin.inventory.prod-receive-returns.index');
		Route::get('/create', 'Inv\ProdReceiveReturnController@create')->name('admin.inventory.prod-receive-returns.create');
		Route::post('/store', 'Inv\ProdReceiveReturnController@store')->name('admin.inventory.prod-receive-returns.store');
		Route::post('/draft', 'Inv\ProdReceiveReturnController@draft')->name('admin.inventory.prod-receive-returns.draft');
		Route::get('/{id}/edit', 'Inv\ProdReceiveReturnController@edit')->name('admin.inventory.prod-receive-returns.edit');
		Route::post('/update', 'Inv\ProdReceiveReturnController@update')->name('admin.inventory.prod-receive-returns.update');
		Route::get('/{id}/delete', 'Inv\ProdReceiveReturnController@delete')->name('admin.inventory.prod-receive-returns.delete');
		Route::post('/update_draft', 'Inv\ProdReceiveReturnController@update_draft')->name('admin.inventory.prod-receive-returns.update_draft');
		Route::post('/update_draft_as_receive_return', 'Inv\ProdReceiveReturnController@update_draft_as_receive_return')->name('admin.inventory.prod-receive-returns.update_draft_as_receive_return');
		Route::post('/delete_draft', 'Inv\ProdReceiveReturnController@delete_draft')->name('admin.inventory.prod-receive-returns.delete_draft');

	});

		
		
	/* ============STOCK REPORTS=============================================================================== */
	
	/* ============INVENTORY BOOKS======================= */
	Route::group(['middleware' => 'permission:stock-reports'], function() {
		Route::group(['prefix'  =>   '/admin/inventory/stock-reports/products'], function() {
			
			Route::get('/', 'Inv\StockBooksController@product_index')->name('admin.inventory.stock-reports.products.index');
			Route::get('/{id}/product-ledger', 'Inv\StockBooksController@product_ledger')->name('admin.inventory.stock-reports.products.product-ledger');
			Route::post('/{id}/product-ledger', 'Inv\StockBooksController@product_ledger')->name('admin.inventory.stock-reports.products.product-ledger');
		});

		Route::group(['prefix'  =>   '/admin/inventory/stock-reports/items'], function() {
			Route::get('/', 'Inv\StockBooksController@index')->name('admin.inventory.stock-reports.items.index');
			Route::get('/{id}/item-ledger', 'Inv\StockBooksController@item_ledger')->name('admin.inventory.stock-reports.items.item-ledger');
			Route::post('/{id}/item-ledger', 'Inv\StockBooksController@item_ledger')->name('admin.inventory.stock-reports.items.item-ledger');
			
			Route::get('/test', 'Inv\ItemController@get_all_active_items_with_balance')->name('admin.inventory.stock-reports.items.test');
		});
	});
	
	/* ============ACCOUNT BOOKS======================= */
	Route::group(['prefix'  =>   '/admin/inventory/acc-books'], function()  {
		Route::get('/', 'Inv\AccBooksController@index')->name('admin.inventory.acc-books.index');
		Route::get('/{id}/acc-ledger', 'Inv\AccBooksController@acc_ledger')->name('admin.inventory.acc-books.acc-ledger');
		
		
		Route::get('/suppliers', 'Inv\AccBooksController@suppliers')->name('admin.inventory.acc-books.suppliers');
		Route::get('/{id}/supplier-book', 'Inv\AccBooksController@supplier_book')->name('admin.inventory.acc-books.supplier-book');
		Route::post('/{id}/supplier-book', 'Inv\AccBooksController@supplier_book')->name('admin.inventory.acc-books.supplier-book');
		
		Route::get('/cash-book', 'Inv\AccBooksController@cash_book')->name('admin.inventory.acc-books.cash-book');
		Route::post('/cash-book', 'Inv\AccBooksController@cash_book')->name('admin.inventory.acc-books.cash-book');
		
		Route::get('/purchase-book', 'Inv\AccBooksController@purchase_book')->name('admin.inventory.acc-books.purchase-book');
		Route::post('/purchase-book', 'Inv\AccBooksController@purchase_book')->name('admin.inventory.acc-books.purchase-book');
		
		Route::get('/purchase-return-book', 'Inv\AccBooksController@purchase_return_book')->name('admin.inventory.acc-books.purchase-return-book');
		Route::post('/purchase-return-book', 'Inv\AccBooksController@purchase_return_book')->name('admin.inventory.acc-books.purchase-return-book');
		
		Route::get('/sale-book', 'Inv\AccBooksController@sale_book')->name('admin.inventory.acc-books.sale-book');
		Route::post('/sale-book', 'Inv\AccBooksController@sale_book')->name('admin.inventory.acc-books.sale-book');
		
		Route::get('/transfer-book', 'Inv\AccBooksController@transfer_book')->name('admin.inventory.acc-books.transfer-book');
		Route::post('/transfer-book', 'Inv\AccBooksController@transfer_book')->name('admin.inventory.acc-books.transfer-book');
		
		Route::get('/transfer-return-book', 'Inv\AccBooksController@transfer_return_book')->name('admin.inventory.acc-books.transfer-return-book');
		Route::post('/transfer-return-book', 'Inv\AccBooksController@transfer_return_book')->name('admin.inventory.acc-books.transfer-return-book');
		
		Route::get('/receive-book', 'Inv\AccBooksController@receive_book')->name('admin.inventory.acc-books.receive-book');
		Route::post('/receive-book', 'Inv\AccBooksController@receive_book')->name('admin.inventory.acc-books.receive-book');
		
	});
		
		
		
		
		
	
});


